//var reporter = require('cucumberjs-allure-reporter');
//var rmdir = require('rmdir');
exports.config = {
    seleniumAddress: 'http://127.0.0.1:4444/wd/hub',
    getPageTimeout: 60000,
    allScriptsTimeout: 600000,
    framework: 'custom',
    // path relative to the current config file
    frameworkPath: require.resolve('protractor-cucumber-framework'),
    capabilities: {
        'browserName': 'chrome',
        chromeOptions: {
            args: ["--window-size=1024,768"]
        }
    },

    // Spec patterns are relative to this directory.
    specs: [
        'features/ibaOptOutHappyPath.feature'
    ],

    cucumberOpts: {
        require: [
            'features/stepDefinition/stepDefinition.js', 
            'features/support/hook.js',
            'features/support/world.js'
        ],
        tags: false,
        format: 'pretty',
        strict: true,
        profile: false,
        'no-source': true,
        format: 'json:report/cucumber_report.html.json'
    },

    baseUrl: 'https://www.t-mobile.com'
};
