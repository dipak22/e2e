
'use strict';
(function () {

	var BrowseDevices = function () {

		this.getBrowsePageSubNavList = function () {
			return element.all(by.repeater("nav in navData track by $index"));
		};

		this.getBrowsePageSubNavTabText = function (RepeaterElement) {
			return RepeaterElement.element(by.cssValueBasedSelector("sub-nav-text", "span", "class"));
		};

		this.getBrowsePageSubNavTab = function (RepeaterElement) {
			return RepeaterElement.element(by.tagName("a"));
		};

		this.getBrowsePageTabletSubNavTab = function () {
			return element.all(by.repeater("nav in navData track by $index")).get(0).element(by.tagName("a"));
		};

		this.getBrowsePagePhonesSubNavTab = function () {
			return element.all(by.repeater("nav in navData track by $index")).get(1).element(by.tagName("a"));
		};

		this.getSortingDropDown = function () {
			return element(by.cssCustomSelector("vm.toggleSortMenu()", "button", "ng-click"));
		};

		this.getSortOptions = function () {
			return element.all(by.repeater("sort in vm.searchOptions.sortConfig.options"));
		};

		this.getSelectedSortElement = function (RepeaterElement) {
			return RepeaterElement;
		};

		this.getSelectedSortOption = function () {
			return element(by.id("sort")).element(by.tagName("span"));
		};

		this.getSortOptionValue = function (RepeaterElement) {
			return RepeaterElement.element(by.tagName("a"));
		};

		this.getFilterDropDown = function () {
			return element(by.cssCustomSelector("vm.toggleFilterMenu()", "button", "ng-click"));
		};

		this.getFilterOptions = function () {
			return element.all(by.repeater('field in filterGroup.options'));
		};

		this.getFilterOptionCheckBox = function (ParentElement) {
			return ParentElement.element(by.tagName("label"));
		};

		this.getFirstFilterOption = function () {
			return element.all(by.repeater("filterGroup in vm.searchOptions.filterGroups")).get(0).all(by.repeater("field in filterGroup.options")).get(0).element(by.tagName("label"));
		};

		this.closeBrowseFilter = function () {
			return element(by.cssCustomSelector("Close", "i", "title"));
		};

		this.getDefaultFilterName = function () {
			return element(by.id("dropdownMenu1")).element(by.tagName("span"));
		};

		this.getAllFilterSelection = function () {
			return element(by.cssCustomSelector("vm.searchOptions.selectedFilterOptions.length === 0", "span", "ng-if"));
		};

		this.getBrowseDevicesLegalText = function () {
			return element(by.cssCustomSelector("viewLessOrMoreDesc='true'", "div", "ng-init")).element(by.cssValueBasedSelector("legal-text-top", "div", "class"));
		};

		this.getBackToTopButton = function () {
			return element(by.cssCustomSelector("scrollToTop()", "div", "ng-click"));
		};

		this.getSkipCreditCheckLink = function () {
			return element(by.cssCustomSelector("vm.model.creditClassData.skipLinkTarget != 'newwindow'", "div", "ng-if")).element(by.tagName("a"));
		};

		this.getBrowseDeviceList = function () {
			//return element.all(by.repeater("product in vm.model.products track by $index"));
			return element.all(by.cssCustomSelector("product in vm.model.products track by $index", "div", "ng-repeat-start"));
		};

		this.getBrowseDeviceName = function (RepeaterElement) {
			return RepeaterElement.element(by.binding("vm.product.productName"));
		};

		this.getBrowseDeviceImage = function (RepeaterElement) {
			return RepeaterElement.element(by.tagName("img"));
		};

		this.getBrowsePageFirstDevicePriceLockup = function () {
			return element.all(by.cssCustomSelector("product in vm.model.products track by $index", "div", "ng-repeat-start")).get(0).element(by.cssCustomSelector("price-lockup-small", "div", "size-class"));
		};

		this.getBrowsePageSecondDevicePriceLockup = function () {
			return element.all(by.cssCustomSelector("product in vm.model.products track by $index", "div", "ng-repeat-start")).get(1).element(by.cssCustomSelector("price-lockup-small", "div", "size-class"));
		};

		this.getBrowsePageFirstDeviceColorSwatchList = function () {
			return element.all(by.repeater("product in vm.model.products track by $index")).get(0).all(by.repeater("color in vm.product.colorSwatch|splitpipe track by $index"));
		};

		this.getResultsCount = function () {
			return element(by.cssValueBasedSelector("results-count totCount hidden-xs", "span", "class"));
		};

		this.getFirstCriteriaUnderCategory1 = function () {
			//return element.all(by.repeater('group in vm.filterGroups'));
			return element.all(by.repeater('filterGroup in vm.searchOptions.filterGroups')).get(0).all(by.repeater("field in filterGroup.options")).get(0).element(by.cssCustomSelector("label-copy", "div", "class")).element(by.tagName("p"));
		};

		this.getSecondCriteriaUnderCategory1 = function () {
			//return element.all(by.repeater('group in vm.filterGroups'));
			return element.all(by.repeater('filterGroup in vm.searchOptions.filterGroups')).get(0).all(by.repeater("field in filterGroup.options")).get(1).element(by.cssCustomSelector("label-copy", "div", "class")).element(by.tagName("p"));
		};

		this.getFirtsCriteriaUnderCategory2 = function () {
			//return element.all(by.repeater('group in vm.filterGroups'));
			return element.all(by.repeater('filterGroup in vm.searchOptions.filterGroups')).get(1).all(by.repeater("field in filterGroup.options")).get(0).element(by.cssCustomSelector("label-copy", "div", "class")).element(by.tagName("p"));
		};

		this.getFirtsCriteriaUnderCategory3 = function () {
			//return element.all(by.repeater('group in vm.filterGroups'));
			return element.all(by.repeater('filterGroup in vm.searchOptions.filterGroups')).get(2).all(by.repeater("field in filterGroup.options")).get(0).element(by.cssCustomSelector("label-copy", "div", "class")).element(by.tagName("p"));
		};

		this.getSelectedFilterOptionsList = function () {
			return element.all(by.repeater("field in vm.searchOptions.selectedFilterOptions"));
		};

		this.getSelectedFilterOption = function (RepeaterElement) {
			return RepeaterElement.element(by.tagName("span"));
		};

		this.getFourthSelectedFilterOptionRemoveIcon = function () {
			return element.all(by.repeater("field in vm.searchOptions.selectedFilterOptions")).get(3).element(by.tagName("i"));
		};

		this.getThirdSelectedFilterOptionRemoveIcon = function () {
			return element.all(by.repeater("field in vm.searchOptions.selectedFilterOptions")).get(2).element(by.tagName("i"));
		};

		this.getClearFilterMinus = function (RepeaterElement) {
			return RepeaterElement.element(by.tagName("i"));
		};

		this.getClearFilters = function () {
			return element(by.cssCustomSelector("vm.searchOptions.selectedFilterOptions.length > 0 || (vm.model.compatibleSelected || vm.compatibleSelection)", "span", "ng-if"));
		};

		this.getZeroResultFilterMessageHeader = function () {
			return element(by.cssCustomSelector("vm.model.products.length == 0", "div", "ng-if")).element(by.tagName("h3"));
		};

		this.getZeroResultFilterMessageSubText = function () {
			return element(by.cssCustomSelector("vm.model.products.length == 0", "div", "ng-if")).element(by.tagName("p"));
		};

		this.getCreditClassRepeater = function () {
			//return element.all(by.repeater("creditClass in creditClassData"));
			return element.all(by.repeater("creditClass in vm.model.creditClassData"));
		};

		this.getBrowsePageFirstReviewCount = function () {
			return element.all(by.cssCustomSelector("product in vm.model.products track by $index", "div", "ng-repeat-start")).get(0).element(by.cssCustomSelector("Read all reviews", "span", "title"));
		};

		this.getBrowsePageFirstReviewStars = function () {
			return element.all(by.cssCustomSelector("product in vm.model.products track by $index", "div", "ng-repeat-start")).get(0).element(by.cssCustomSelector("BVImgOrSprite", "img", "class"));
		};

		/** Added on Feb 25th **/

		this.getBrowsePageFirstDeviceName = function () {
			return element.all(by.cssCustomSelector("product in vm.model.products track by $index", "div", "ng-repeat-start")).get(0).element(by.binding("vm.product.productName"));
		};

		this.getBrowsePageFirstDeviceImage = function () {
			return element.all(by.cssCustomSelector("product in vm.model.products track by $index", "div", "ng-repeat-start")).get(0).element(by.tagName("img"));
		};

		this.getBrowsePageFirstColorSwatchList = function () {
			return element.all(by.cssCustomSelector("(vm.product.colorSwatch.split('|').length > 1)", "ul", "ng-if")).get(0);
		};

		this.getAnchorNavigationBar = function () {
			return element(by.id("navBar"));
		};

		this.getFirstOfferLink = function () {
			//return element.all(by.repeater("creditClass in creditClassData"));
			return element.all(by.binding("vm.product.offerDisplayName")).get(0);
		};

		this.getOfferModalHeader = function () {
			//return element.all(by.repeater("creditClass in creditClassData"));
			return element(by.cssValueBasedSelector("offer-text-modal", "div", "class")).element(by.tagName("h3"));
		};

		this.getOfferModalContent = function () {
			//return element.all(by.repeater("creditClass in creditClassData"));
			//return element(by.cssValueBasedSelector("offer-text-modal","div","class")).element(by.cssCustomSelector("modal-body","div","class")).element(by.tagName("article")).element(by.tagName("span"));
			return element(by.cssValueBasedSelector("offer-text-modal", "div", "class")).element(by.cssCustomSelector("vm.htmlContent | parseHtml", "span", "ng-bind-html")).element(by.xpath("./div[1]"));
		};

		this.getOfferModalSubtext = function () {
			//return element.all(by.repeater("creditClass in creditClassData"));
			return element(by.cssValueBasedSelector("offer-text-modal", "div", "class")).element(by.cssCustomSelector("subhead", "div", "class")).element(by.tagName("b"));
		};

		this.getOfferModalImage = function () {
			//return element.all(by.repeater("creditClass in creditClassData"));
			return element(by.cssValueBasedSelector("offer-text-modal", "div", "class")).element(by.tagName("article")).element(by.tagName("img"));
		};

		this.getOfferCloseButton = function () {
			//return element.all(by.repeater("creditClass in creditClassData"));
			return element(by.cssValueBasedSelector("vm.dismiss()", "button", "ng-click"));
		};
		this.getDevieNameinRepeater = function (RepeaterElement) {
			//return element.all(by.repeater("creditClass in creditClassData"));
			return RepeaterElement.element(by.cssValueBasedSelector("vm.product.productName", "a", "ng-bind"));
		};

		this.getFirstFilterCategoryRepeater = function () {
			//return element.all(by.repeater('group in vm.filterGroups'));
			return element.all(by.repeater('filterGroup in vm.searchOptions.filterGroups')).get(0).all(by.repeater("field in filterGroup.options"));
		};

		this.getFilterCriteriaName = function (RepeaterElement) {
			//return element.all(by.repeater("creditClass in creditClassData"));
			return RepeaterElement.element(by.tagName("p"));
		};

		this.getSelectedCreditClass = function () {
			return element(by.cssCustomSelector("outline-none creditSelectorActive", "div", "class")).element(by.cssValueBasedSelector("credit-main-text", "span", "class"));
		};

		this.getBrowsePageFirstDeviceDefaultColor = function () {
			return element.all(by.cssCustomSelector("product in vm.model.products track by $index", "div", "ng-repeat-start")).get(0).element(by.cssCustomSelector("inline-block ng-scope active", "li", "class")).element(by.tagName("img"));
		};

		this.getFirstNonDefaultColorSwatch = function () {
			return element.all(by.repeater("color in vm.product.colorSwatch|splitpipe track by $index")).get(0).all(by.cssValueBasedSelector("inline-block ng-scope", "li", "class")).get(0).element(by.tagName("img"));
		}

		/**********Accessory PageObjects *********************/

		this.getAccessoryList = function () {
			return element.all(by.cssCustomSelector("product in vm.model.products track by $index", "div", "ng-repeat-start"));
		};

		this.getResultsPerPage = function () {
			return element.all(by.repeater("limit in vm.model.itemsPerPageLimits"));
		}

		this.getAccessoryPaginationLastCount = function () {
			return element(by.cssCustomSelector("vm.init()", "ul", "ng-init")).element(by.cssCustomSelector("last-page ng-binding", "a", "class"))
		}

		this.getAccessoryPaginationLastCountUsingRepeat = function () {
			return element.all(by.repeater('num in vm.slicedPageNumbersArray'));
		}

		this.getAccessoryPaginationActivePage = function () {
			return element(by.cssCustomSelector("vm.init()", "ul", "ng-init")).element(by.cssValueBasedSelector("active", "a", "class"));
		};

		this.getAccessoryPaginationPreviousPage = function () {
			return element(by.cssCustomSelector("vm.init()", "ul", "ng-init")).element(by.cssCustomSelector("vm.previousPage()", "a", "ng-click"))
		};

		this.getAccessoryPaginationPreviousPageDisabled = function () {
			return element(by.cssCustomSelector("vm.init()", "ul", "ng-init")).element(by.cssCustomSelector("prev disabled", "a", "class"))
		};

		this.getAccessoryPaginationPreviousPageEnabled = function () {
			return element(by.cssCustomSelector("vm.init()", "ul", "ng-init")).element(by.cssCustomSelector("prev", "a", "class"))
		};

		this.getAccessoryPaginationNextPage = function () {
			return element(by.cssCustomSelector("vm.init()", "ul", "ng-init")).element(by.cssCustomSelector("vm.nextPage()", "a", "ng-click"))
		};

		this.getAccessoryPaginationNextPageDisabled = function () {
			return element(by.cssCustomSelector("vm.init()", "ul", "ng-init")).element(by.cssCustomSelector("next disabled", "a", "class"))
		};

		this.getAccessoryPaginationNextPageEnabled = function () {
			return element(by.cssCustomSelector("vm.init()", "ul", "ng-init")).element(by.cssCustomSelector("next", "a", "class"));
		};

		this.getFilterCloseCTA = function () {
			return element(by.cssCustomSelector("Close", "i", "title"));
		};

		this.getDeviceName = function (RepeaterElement) {
			return RepeaterElement.element(by.binding("vm.product.productName"));
		};

		this.getBrowsePageSecondDeviceName = function () {
			return element.all(by.cssCustomSelector("product in vm.model.products track by $index", "div", "ng-repeat-start")).get(1).element(by.binding("vm.product.productName"));
		};

		/*** Browse Pageobjects for UDF **/

		this.getPriceLockupWrapper = function (ParentElement) {
			return ParentElement.element(by.cssValueBasedAndSelector('div', 'price-lockup-wrapper ng-isolate-scope', 'class', 'true', 'show-total-pricing'));
		}

		this.getBrowseDeviceColorSwatchContainer = function (ParentElement) {
			return ParentElement.element(by.cssCustomSelector("(vm.product.colorSwatch.split('|').length > 1)", "ul", "ng-if"));
		}

		this.getDefaultSelectedColorSwatch = function (ParentElement) {
			return ParentElement.element(by.cssCustomSelector("inline-block ng-scope active", "li", "class"));
		}

		this.getColorSwatchList = function (ParentElement) {
			return ParentElement.all(by.cssValueBasedSelector("inline-block ng-scope", "li", "class"));
		}

		this.getBrowsePageDeviceImage = function (ParentElement) {
			return ParentElement.element(by.cssCustomSelector("m-b-15 product-img text-center", "div", "class")).element(by.tagName("img"));
		};

		this.getBrowsePageResultsNumber = function () {
			return element(by.cssValueBasedAndSelector("span", "vm.model.products.length > 0", "ng-if", "results-count totCount hidden-xs ng-binding ng-scope", "class"));
		};

		this.getBrowsePageResultsNumberAfterFilter = function () {
			return element(by.cssValueBasedAndSelector("span", "vm.model.products.length > 0", "ng-if"));
		};

		this.getBrowsePageFourthDeviceName = function () {
			return element.all(by.cssCustomSelector("product in vm.model.products track by $index", "div", "ng-repeat-start")).get(3).element(by.binding("vm.product.productName"));
		};

		this.getDeviceSearchBox = function () {
			return element(by.id("devicesSearchInput"));
		};

		/*** Added on 27th Feb **/

		this.getClearFilterCTA = function () {
			return element(by.cssCustomSelector("clear-filter ng-scope", "span", "class"));
		};

		this.getManufacturerFilters = function () {
			return element(by.id('filter-group-pm')).all(by.repeater('field in filterGroup.options'));
		};

		this.getPriceRangeFilters = function () {
			return element(by.id('filter-group-prg')).all(by.repeater('field in filterGroup.options'));
		};

		this.getConditionFilters = function () {
			return element(by.id('filter-group-cd')).all(by.repeater('field in filterGroup.options'));
		};

		this.getOSFilters = function () {
			return element(by.id('filter-group-osy')).all(by.repeater('field in filterGroup.options'));
		};

		this.getCarousalNavigationPreviousDisabled = function () {
			return element(by.tagName('carousel-navigation')).element(by.cssCustomSelector("nav-prev disabled", "a", "class"))
		};

		this.getCarousalNavigationPreviousEnabled = function () {
			return element(by.tagName('carousel-navigation')).element(by.cssCustomSelector("nav-prev", "a", "class"))
		};

		this.getCarousalNavigationNextDisabled = function () {
			return element(by.tagName('carousel-navigation')).element(by.cssCustomSelector("nav-next disabled", "a", "class"))
		};

		this.getCarousalNavigationNextEnabled = function () {
			return element(by.tagName('carousel-navigation')).element(by.cssCustomSelector("nav-next", "a", "class"))
		};

		this.getCarousalNavigation = function () {
			return element(by.tagName('carousel-navigation'));
		};

		this.getCarousalNavigationNextArrow = function () {
			return element(by.tagName('carousel-navigation')).element(by.cssValueBasedSelector("nav-next", "a", "class"))
		};

		this.getCarousalNavigationPrevArrow = function () {
			return element(by.tagName('carousel-navigation')).element(by.cssValueBasedSelector("nav-prev", "a", "class"))
		};

		this.getBrowsePageSubNavTablet = function () {
			return element.all(by.repeater("nav in navData track by $index")).get(0);
		};

		this.getBrowsePageSubNavCellPhone = function () {
			return element.all(by.repeater("nav in navData track by $index")).get(1);
		};

		this.getBrowsePageSubNavAccessories = function () {
			return element.all(by.repeater("nav in navData track by $index")).get(2);
		};

		/*** Browse Pageobjects for UDF End **/


		/********************Sprint 31*********************************/

		this.getMainBrowseNoCreditCreditClass = function () {
			//return element.all(by.repeater("creditClass in vm.model.creditClassData")).get(1).element(by.cssCustomSelector("vm.selectCreditClass(creditClass)","div","ng-click"));
			//return element.all(by.repeater("creditClass in vm.model.creditClassData")).get(2).element(by.cssValueBasedSelector("creditClassText","span","class"));
			return element(by.id("crd_id_2"));
		};


		this.getMainBrowseAwesomeCreditClass = function () {
			//return element.all(by.repeater("creditClass in vm.model.creditClassData")).get(0).element(by.cssCustomSelector("vm.selectCreditClass(creditClass)","div","ng-click"));
			//return element.all(by.repeater("creditClass in vm.model.creditClassData")).get(0).element(by.cssValueBasedSelector("creditClassText","span","class"));
			return element(by.id("crd_id_0"));
		};

		this.getMainBrowseAwesomeCreditClassStatus = function () {
			//return element.all(by.repeater("creditClass in creditClassData"));
			return element(by.id("crd_id_0"));
		};

		this.getMainBrowseAverageCreditClass = function () {
			//return element.all(by.repeater("creditClass in vm.model.creditClassData")).get(1).element(by.cssCustomSelector("vm.selectCreditClass(creditClass)","div","ng-click"));
			//return element.all(by.repeater("creditClass in vm.model.creditClassData")).get(1).element(by.cssValueBasedSelector("creditClassText","span","class"));
			return element(by.id("crd_id_1"));
		};

		this.getMainBrowseAverageCreditClassStatus = function () {
			//return element.all(by.repeater("creditClass in creditClassData"));
			return element(by.id("crd_id_1"));
		};

		this.getFirstRowEIPLeglText = function () {

			return element.all(by.cssCustomSelector("vm.model.creditClassSelected.class!='creditClass3' && vm.showMessage($index) || $last && vm.model.creditClassSelected.class!='creditClass3'", "div", "ng-if")).get(0).element(by.tagName("p"));
		};

		/*** MSankar 04/04/2017 Added getFirstDevice Page Object for SelectInStockDevice UDF ***/

		this.getFirstDevice = function () {
			return element.all(by.cssCustomSelector("product in vm.model.products track by $index", "div", "ng-repeat-start")).get(0);
		};

		/*****************Created by Gowthami on Apr 17, 2017**********************/

		this.getMainBrowseNoCreditCreditClassText = function () {
			//return element.all(by.repeater("creditClass in vm.model.creditClassData")).get(1).element(by.cssCustomSelector("vm.selectCreditClass(creditClass)","div","ng-click"));
			return element.all(by.repeater("creditClass in vm.model.creditClassData")).get(2).element(by.cssValueBasedSelector("credit-main-text", "span", "class"));
		};

		this.getMainBrowseNoCreditCreditClassSubText = function () {
			//return element.all(by.repeater("creditClass in vm.model.creditClassData")).get(1).element(by.cssCustomSelector("vm.selectCreditClass(creditClass)","div","ng-click"));
			return element.all(by.repeater("creditClass in vm.model.creditClassData")).get(2).element(by.cssValueBasedSelector("credit-subtext", "span", "class"));
		};

		/****************Created by Paramesh on Apr 22, 2017 ************************/

		this.getCartCounter = function () {
			return element(by.cssCustomSelector("mail_test", "li", "menu-id")).element(by.cssCustomSelector("count", "a", "class"));
		};

		/**********************Gowthami Created on APr 25 for Insprint Automation*********************************/

		this.getBrowsePageFirstEIPDevciePriceLockup = function () {

			return element.all(by.cssCustomSelector("product in vm.model.products track by $index", "div", "ng-repeat-start")).filter(function (Element, ElementIndex) {
				return Element.element(by.cssCustomSelector("price-lockup-small", "div", "size-class")).getAttribute("show-total-pricing").then(function (attributeValue) {
					return attributeValue === "true";
				})
			}).get(0).element(by.cssCustomSelector("price-lockup-small", "div", "size-class"))

		};
		this.getBrowsePageFirstEIPDeviceName = function () {

			return element.all(by.cssCustomSelector("product in vm.model.products track by $index", "div", "ng-repeat-start")).filter(function (Element, ElementIndex) {
				return Element.element(by.cssCustomSelector("price-lockup-small", "div", "size-class")).getAttribute("show-total-pricing").then(function (attributeValue) {
					return attributeValue === "true";
				})
			}).get(0).element(by.binding("vm.product.productName"))

		};

		this.getBrowsePageFirstFRPDevicePriceLockup = function () {

			return element.all(by.cssCustomSelector("product in vm.model.products track by $index", "div", "ng-repeat-start")).filter(function (Element, ElementIndex) {
				return Element.element(by.cssCustomSelector("price-lockup-small", "div", "size-class")).getAttribute("show-total-pricing").then(function (attributeValue) {
					return attributeValue === null;
				})
			}).get(0).element(by.cssCustomSelector("price-lockup-small", "div", "size-class"))

		};

		this.getBrowsePageFirstFRPDeviceName = function () {

			return element.all(by.cssCustomSelector("product in vm.model.products track by $index", "div", "ng-repeat-start")).filter(function (Element, ElementIndex) {
				return Element.element(by.cssCustomSelector("price-lockup-small", "div", "size-class")).getAttribute("show-total-pricing").then(function (attributeValue) {
					return attributeValue === null;
				})
			}).get(0).element(by.binding("vm.product.productName"))

		};

		/******************Added by Gowthami on May 4 for InSPrint********************/

		this.getCreditClassText = function (RepeaterElement) {
			return RepeaterElement.element(by.cssValueBasedSelector("credit-main-text", "span", "class"));

		};

		/*****************Created by Paramesh - 05/02 ***********************/

		this.getSixthCriteriaUnderCategory1 = function () {
			//return element.all(by.repeater('group in vm.filterGroups'));
			return element.all(by.repeater('filterGroup in vm.searchOptions.filterGroups')).get(0).all(by.repeater("field in filterGroup.options")).get(5).element(by.cssCustomSelector("label-copy", "div", "class")).element(by.tagName("p"));
		};

		/*****************Created by Paramesh - 05/02 ***********************/
		this.getBrowsePageThirdDeviceName = function () {
			return element.all(by.cssCustomSelector("product in vm.model.products track by $index", "div", "ng-repeat-start")).get(2).element(by.binding("vm.product.productName"));
		};

		this.getBrowsePageThirdDeviceImage = function () {
			return element.all(by.cssCustomSelector("product in vm.model.products track by $index", "div", "ng-repeat-start")).get(2).element(by.tagName("img"));
		};

		/***Cd Bug automation related 17 MAY*********/

		this.getSelectedSortingCategory = function () {
			return element.all(by.cssCustomSelector("sort in vm.searchOptions.sortConfig.options", "li", "ng-repeat")).filter(function (elem, index) {
				return elem.getAttribute("class").then(function (Value) {
					return Value.indexOf("selected") != -1;
				});
			}).get(0).element(by.tagName("a"));
		};

		this.getFirstDeviceWithZeroRatingAndComments = function () {
			return element.all(by.cssCustomSelector("product in vm.model.products track by $index", "div", "ng-repeat-start")).filter(function (elem, index) {
				return elem.element(by.cssCustomSelector("vm.product.rating != ''? vm.product.reviewCount : 0", "span", "ng-bind")).getText().then(function (Value) {
					console.log("value iskfd " + Value);
					return Value === "0";
				});
			}).get(0).element(by.binding("vm.product.productName"));
		};

		this.getBrowsePageSubNavTabletText = function () {
			return element.all(by.repeater("nav in navData track by $index")).get(0).element(by.css(".sub-nav-text"));
		};

		this.getBrowsePageSubNavCellPhoneText = function () {
			return element.all(by.repeater("nav in navData track by $index")).get(1).element(by.css(".sub-nav-text"));
		};

		this.getBrowsePageSubNavAccessoriesText = function () {
			return element.all(by.repeater("nav in navData track by $index")).get(2).element(by.css(".sub-nav-text"));
		};

		this.getBrowsePageLegalBottomText = function () {
			return element(by.cssValueBasedSelector("ListStyle legal", "div", "class"));
		};

		this.getMainBrowseAwesomeCreditClassText = function () {
			return element.all(by.repeater("creditClass in vm.model.creditClassData")).get(0).element(by.cssValueBasedSelector("credit-main-text", "span", "class"));
		};

		this.getMainBrowseAverageCreditClassText = function () {
			return element.all(by.repeater("creditClass in vm.model.creditClassData")).get(1).element(by.cssValueBasedSelector("credit-main-text", "span", "class"));
		};

		this.getRedColorSwatch = function () {
			return element.all(by.cssCustomSelector("(PRODUCT)RED", "img", "title")).get(0);
		};
		/******************Created by Gowthami - 05/16 *********************/
		this.getBrowsePageEIPDeviceList = function () {

			return element.all(by.cssCustomSelector("product in vm.model.products track by $index", "div", "ng-repeat-start")).filter(function (Element, ElementIndex) {
				return Element.element(by.cssCustomSelector("price-lockup-small", "div", "size-class")).getAttribute("show-total-pricing").then(function (attributeValue) {
					return attributeValue === "true";
				})
			})
		};

		this.getBrowsePageFRPDeviceList = function () {
			return element.all(by.cssCustomSelector("product in vm.model.products track by $index", "div", "ng-repeat-start")).filter(function (Element, ElementIndex) {
				return Element.element(by.cssCustomSelector("price-lockup-small", "div", "size-class")).getAttribute("show-total-pricing").then(function (attributeValue) {
					return attributeValue === "false";
				})
			})
		};
	};
	module.exports = function () {
		return new BrowseDevices();
	};
}());