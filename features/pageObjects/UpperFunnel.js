'use strict';
(function () {

	var upperFunnelDetails = function () {

		/**************************************************************************************************************************
		 *                                           	   Upper Funnel                                                           *
		 * ************************************************************************************************************************/

		this.getAwesomeCredit = function () {
			return element.all(by.repeater('creditClass in vm.model.creditClassData')).get(0).element(by.css('.creditClassText'));
		};

		this.getAverageCredit = function () {
			return element.all(by.repeater('creditClass in vm.model.creditClassData')).get(1).element(by.css('.creditClassText'));
		};

		this.getNoCredit = function () {
			return element.all(by.repeater('creditClass in vm.model.creditClassData')).get(2).element(by.css('.creditClassText'));
		};

		this.getDeviceFromMainBrowse = function () {
			return element(by.css('ng-bind="vm.product.productName"'));
		};

		this.getAccessoryFromMainBrowse = function () {
			//return element(by.cssContainingText('.product-name', accessoryName));
			return element(by.css('ng-bind="vm.product.productName"'));
		};

		this.getMainPDPDetails = function () {
			//return element(by.id('productDescription')).element(by.css('h3[role="heading"]'));
			//return element(by.css('h3[ng-hide="vm.showTitle"]'));
			return element(by.css('h1[ng-hide="vm.showTitle"]'));
		};

		this.getAccessoryMainPDPDetails = function () {
			return element(by.css('h1[ng-hide="vm.showTitle"]'));
		};

		this.getAddToCartButton = function () {
			return element(by.css('[ng-click="vm.addDeviceToCart(vm.productDetailsService.model.selectedProduct,vm.productDetailsService.model.productCategoryType)"]'));
		};

		this.getColorSwatch = function () {
			return element(by.css('ng-class="{outLineGrey: $index ==  vm.model.selectedColorIndex}"'));
		};

		this.getSwatchUP = function () {
			return element.all(by.css('[ng-click="vm.setColor(product.associatedMemorySKUS,vm.selectedMemory)"]')).get(2);
		};

		this.getPaymentFRPUP = function () {
			return element(by.css('[ng-if="showFRPPricing()"]'));
		};

		this.getPaymentEIPUP = function () {
			return element(by.css('[ng-if="showMonthyPricing()"]'));
		};

		this.getMemoryUP = function () {
			return element.all(by.css('[ng-click="vm.filterProductDataByMemory(product.associatedColorSKUS, vm.model.selectedProduct.Color)"]')).get(1);
		};

			    this.checkRStars = function () {
    			return element(by.css('[class="review-star"]'));
    		};

    		this.checkRating = function () {
    			return element(by.css('[title="Read all reviews"]'));
    		};

    		this.checkEIPLegal = function () {
    			return element(by.id('legaltxt'));
    		};

    		this.checkSIMKit = function () {
    			return element(by.css('[class="p-xs ng-binding"]'));
    		};

		this.loginMyTMo = function () {
			return element(by.css('a[ng-click="vm.myTMOLogin()"]'));
		};

		this.myTMOUpgradeCTA = function () {
			return element(by.css('div[ng-click="vm.goToShop(vm.upgradeCTAText)"]'));
		};

		this.myTMOAddaLineCTA = function () {
			return element(by.css('button[ng-click="vm.goToShop(vm.addALineCTAText)"]'));
		};

		this.selectHTL = function () {
			return element.all(by.css('[ng-click="vm.selectSortOption(sort)"]')).get(0);
		};
	};

	module.exports = function () {
		return new upperFunnelDetails();
	};
}());
