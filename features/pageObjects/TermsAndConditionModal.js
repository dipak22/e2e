'use strict';
(function () {

    var TermsAndConditionModal = function () {

        this.getTermsAgreeButton = function () {
            //browser.executeScript('window.scrollTo(0,1000);');
            return element(by.css('button[ng-click="vm.close()"]'));
        }

    };

    module.exports = function () {
        return new TermsAndConditionModal();
    };

}());