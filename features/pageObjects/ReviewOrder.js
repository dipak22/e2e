(function () {
    var ReviewOrderPage = function () {

        this.getReviewOrderDetails = function () {
            return element(by.class("review-order-modal-content bg-white pull-left"));
        };

        this.getReviewOrderTitle = function () {
            return element(by.id("reviewOrderHeader"));
        };

        this.getReviewicon = function () {
            return element(by.id("page-name-4"));
        };

        this.getReviewOrderGroups = function () {
            return element.all(by.repeater("(key,groupItem) in vm.cartObj.lineItems | groupBy : 'orderGroup'"));
        };

        this.getReviewOrderGroupTypeHeader = function (RepeaterElement) {
            return RepeaterElement.element(by.tagName('h5'));
        };

        this.deviceFamilyName = function () {
            return element(by.id("deviceFamilyName"));
        };

		this.getsimKitName = function () {
            return element.all(by.id("simKitName"));
        };

        this.accessoryName = function () {
            return element(by.id("AccessoryName"));
        };

        //Checkout Trade-In start
        this.getTradeInDeviceModelName = function () {
            return element(by.css('p[class="p-t-10 p-b-10 small text-gray-dark trade-details-width ng-binding"]'));
        };

        this.getDeviceTradeInValue = function () {
            return element(by.css('li[ng-if="item.isTradeInStandardFlow"]')).element(by.tagName('p'));
        };

        this.getReviewTradeinValueLink = function(){
			return element(by.id('tradeInValueModalLink')).element(by.tagName('a'));
		};

        this.getTradeInTCText = function () {
            return element(by.id("tradeInTermsAndConditionsInfoTextCta"));
        };

        this.getLinkIntradeInTCText = function () {
            //return element(by.id("tradeInTermsAndConditionsInfoTextCta")).element(by.tagName('p')).element(by.tagName('a'));
            return element(by.css('[ng-if="vm.tradeInTnC"]')).element(by.tagName('a'));
        };

        this.getTradeInTCModalContent = function () {
            return element(by.id("modalcontent"));
        };

        this.getTradeInTCModalClose = function () {
            return element(by.css('button[ng-click="$dismiss()"]'));
        };
        //Checkout Trade-In end

        this.getReviewOrderShippingTimeline = function (RepeaterElement) {
            return RepeaterElement.element(by.tagName('h5')).element(by.tagName('a'));
        };

        this.getReviewOrderLineItems = function (RepeaterElement) {
            if (RepeaterElement == null || RepeaterElement == "" || RepeaterElement == undefined) {
                return element.all(by.repeater("item in groupItem | orderBy:'1*lineNo'"));
            } else {
                return RepeaterElement.all(by.repeater("item in groupItem | orderBy:'1*lineNo'"));
            }
        };

        this.getReviewOrderGroupLineItemNumber = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssValueBasedSelector("line-number", "span", "class"));
        };

        this.getReviewOrderGroupLineItemName = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssCustomSelector("!item.simKitDetails.isBYODAdded", "div", "ng-if"));
        };

        this.getReviewOrderGroupLineItemAutoPayText = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssCustomSelector("autoPayOn === 'true'", "span", "ng-if"));
        };

        this.getReviewOrderGroupLineChevronIcon = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssCustomSelector("Expand-collapse-icon", "div", "aria-label")).element(by.tagName("i"));
        };

        this.getReviewOrderGroupLineItemPrice = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssCustomSelector("price-lockup-medium", "div", "size-class"));
        };

        this.getReviewOrderGroupLineItemNameInCollapse = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("item.lineItemDetails.deviceDetails.isProductAdded", "device-simkit-spliter", "ng-if")).element(by.xpath("./div[1]/p"));
        };

        this.getReviewOrderGroupLineItemImageInCollapse = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("item.mvcAttrs.isCollapsed", "div", "collapse")).element(by.tagName("img"));
        };

        this.getReviewOrderGroupLineItemTodayPriceInCollapse = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("item.lineItemDetails.deviceDetails.isProductAdded", "device-simkit-spliter", "ng-if")).element(by.xpath("./p[1]/div"));
        };

        this.getReviewOrderGroupLineItemMonthlyPriceInCollapse = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("item.lineItemDetails.deviceDetails.isProductAdded", "device-simkit-spliter", "ng-if")).element(by.xpath("./div[2]"));
        };

        this.getReviewOrderGroupLineItemColorAndMemoryInCollapse = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("item.lineItemDetails.deviceDetails.isProductAdded", "device-simkit-spliter", "ng-if")).element(by.xpath("./p[3]"));
        };

        this.getReviewOrderGroupLineItemPlanNameInCollapse = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("item.lineItemDetails.planDetails.familyName.length>0", "li", "ng-if")).element(by.xpath("./p[1]/span[1]"));
        };

        this.getReviewOrderGroupLineItemPlanAutoPayInCollapse = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("item.lineItemDetails.planDetails.familyName.length>0", "li", "ng-if")).element(by.xpath("./p[1]/span[2]"));
        };

        this.getReviewOrderGroupLineItemPlanTodayPriceInCollapse = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("item.lineItemDetails.planDetails.familyName.length>0", "li", "ng-if")).element(by.xpath("./p[3]/div"));
        };

        this.getReviewOrderGroupLineItemPlanMonthlyPriceInCollapse = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("item.lineItemDetails.planDetails.familyName.length>0", "li", "ng-if")).element(by.xpath("./p[5]/div"));
        };

        this.getReviewOrderGroupLineItemSimKitNameInCollapse = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("item.lineItemDetails.deviceDetails.isProductAdded && item.simKitDetails.displayName", "li", "ng-if")).element(by.xpath("./p[1]/span[2]"));
        };

        this.getReviewOrderGroupLineItemSimKitTodayPriceInCollapse = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("item.lineItemDetails.deviceDetails.isProductAdded && item.simKitDetails.displayName", "li", "ng-if")).element(by.xpath("./p[3]/div"));
        };

        /***************** 8 lines chevron down icon*******************/

        this.getReviewOrderGroupFirstLineChevronDownIcon = function (RepeaterElement) {
            return element.all(by.repeater("item in groupItem | orderBy:'1*lineNo'")).filter(function (elem, index) {
                return elem.element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssValueBasedSelector("line-number", "span", "class")).getText().then(function (Value) {
                    return Value === "1";
                });
            }).get(0).element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssValueBasedSelector("fa-chevron-down", "i", "class"));
        };

        this.getReviewOrderGroupSecondLineChevronDownIcon = function (RepeaterElement) {
            return element.all(by.repeater("item in groupItem | orderBy:'1*lineNo'")).filter(function (elem, index) {
                return elem.element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssValueBasedSelector("line-number", "span", "class")).getText().then(function (Value) {
                    return Value === "2";
                });
            }).get(0).element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssValueBasedSelector("fa-chevron-down", "i", "class"));
        };

        this.getReviewOrderGroupThirdLineChevronDownIcon = function (RepeaterElement) {
            return element.all(by.repeater("item in groupItem | orderBy:'1*lineNo'")).filter(function (elem, index) {
                return elem.element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssValueBasedSelector("line-number", "span", "class")).getText().then(function (Value) {
                    return Value === "3";
                });
            }).get(0).element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssValueBasedSelector("fa-chevron-down", "i", "class"));
        };

        this.getReviewOrderGroupFourthLineChevronDownIcon = function (RepeaterElement) {
            return element.all(by.repeater("item in groupItem | orderBy:'1*lineNo'")).filter(function (elem, index) {
                return elem.element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssValueBasedSelector("line-number", "span", "class")).getText().then(function (Value) {
                    return Value === "4";
                });
            }).get(0).element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssValueBasedSelector("fa-chevron-down", "i", "class"));
        };

        this.getReviewOrderGroupFifthLineChevronDownIcon = function (RepeaterElement) {
            return element.all(by.repeater("item in groupItem | orderBy:'1*lineNo'")).filter(function (elem, index) {
                return elem.element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssValueBasedSelector("line-number", "span", "class")).getText().then(function (Value) {
                    return Value === "5";
                });
            }).get(0).element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssValueBasedSelector("fa-chevron-down", "i", "class"));
        };

        this.getReviewOrderGroupSixthLineChevronDownIcon = function (RepeaterElement) {
            return element.all(by.repeater("item in groupItem | orderBy:'1*lineNo'")).filter(function (elem, index) {
                return elem.element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssValueBasedSelector("line-number", "span", "class")).getText().then(function (Value) {
                    return Value === "6";
                });
            }).get(0).element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssValueBasedSelector("fa-chevron-down", "i", "class"));
        };

        this.getReviewOrderGroupSeventhLineChevronDownIcon = function (RepeaterElement) {
            return element.all(by.repeater("item in groupItem | orderBy:'1*lineNo'")).filter(function (elem, index) {
                return elem.element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssValueBasedSelector("line-number", "span", "class")).getText().then(function (Value) {
                    return Value === "7";
                });
            }).get(0).element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssValueBasedSelector("fa-chevron-down", "i", "class"));
        };

        this.getReviewOrderGroupEighthLineChevronDownIcon = function (RepeaterElement) {
            return element.all(by.repeater("item in groupItem | orderBy:'1*lineNo'")).filter(function (elem, index) {
                return elem.element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssValueBasedSelector("line-number", "span", "class")).getText().then(function (Value) {
                    return Value === "8";
                });
            }).get(0).element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssValueBasedSelector("fa-chevron-down", "i", "class"));
        };

        /***************** 8 lines price wrapper*******************/

        this.getReviewOrderGroupFirstLineItemPrice = function (RepeaterElement) {
            return element.all(by.repeater("item in groupItem | orderBy:'1*lineNo'")).filter(function (elem, index) {
                return elem.element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssValueBasedSelector("line-number", "span", "class")).getText().then(function (Value) {
                    return Value === "1";
                });
            }).get(0).element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssCustomSelector("price-lockup-medium", "div", "size-class"));
        };

        this.getReviewOrderGroupSecondLineItemPrice = function (RepeaterElement) {
            return element.all(by.repeater("item in groupItem | orderBy:'1*lineNo'")).filter(function (elem, index) {
                return elem.element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssValueBasedSelector("line-number", "span", "class")).getText().then(function (Value) {
                    return Value === "2";
                });
            }).get(0).element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssCustomSelector("price-lockup-medium", "div", "size-class"));
        };

        this.getReviewOrderGroupThirdLineItemPrice = function (RepeaterElement) {
            return element.all(by.repeater("item in groupItem | orderBy:'1*lineNo'")).filter(function (elem, index) {
                return elem.element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssValueBasedSelector("line-number", "span", "class")).getText().then(function (Value) {
                    return Value === "3";
                });
            }).get(0).element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssCustomSelector("price-lockup-medium", "div", "size-class"));
        };

        this.getReviewOrderGroupFourthLineItemPrice = function (RepeaterElement) {
            return element.all(by.repeater("item in groupItem | orderBy:'1*lineNo'")).filter(function (elem, index) {
                return elem.element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssValueBasedSelector("line-number", "span", "class")).getText().then(function (Value) {
                    return Value === "4";
                });
            }).get(0).element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssCustomSelector("price-lockup-medium", "div", "size-class"));
        };

        this.getReviewOrderGroupFifthLineItemPrice = function (RepeaterElement) {
            return element.all(by.repeater("item in groupItem | orderBy:'1*lineNo'")).filter(function (elem, index) {
                return elem.element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssValueBasedSelector("line-number", "span", "class")).getText().then(function (Value) {
                    return Value === "5";
                });
            }).get(0).element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssCustomSelector("price-lockup-medium", "div", "size-class"));
        };

        this.getReviewOrderGroupSixthLineItemPrice = function (RepeaterElement) {
            return element.all(by.repeater("item in groupItem | orderBy:'1*lineNo'")).filter(function (elem, index) {
                return elem.element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssValueBasedSelector("line-number", "span", "class")).getText().then(function (Value) {
                    return Value === "6";
                });
            }).get(0).element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssCustomSelector("price-lockup-medium", "div", "size-class"));
        };

        this.getReviewOrderGroupSeventhLineItemPrice = function (RepeaterElement) {
            return element.all(by.repeater("item in groupItem | orderBy:'1*lineNo'")).filter(function (elem, index) {
                return elem.element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssValueBasedSelector("line-number", "span", "class")).getText().then(function (Value) {
                    return Value === "7";
                });
            }).get(0).element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssCustomSelector("price-lockup-medium", "div", "size-class"));
        };

        this.getReviewOrderGroupEighthLineItemPrice = function (RepeaterElement) {
            return element.all(by.repeater("item in groupItem | orderBy:'1*lineNo'")).filter(function (elem, index) {
                return elem.element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssValueBasedSelector("line-number", "span", "class")).getText().then(function (Value) {
                    return Value === "8";
                });
            }).get(0).element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssCustomSelector("price-lockup-medium", "div", "size-class"));
        };

        /*************************************BYOD related*******************************/

        this.getReviewOrderGroupLineBYODName = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssValueBasedSelector("item.simKitDetails.isBYODAdded", "div", "ng-if"));
        };

        this.getReviewOrderGroupLineBYODNameInCollapse = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("item.simKitDetails.isBYODAdded", "device-simkit-spliter", "ng-if")).element(by.xpath("./div[1]/p"));
        };

        this.getReviewOrderGroupLineBYODTodayPriceInCollapse = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("item.simKitDetails.isBYODAdded", "device-simkit-spliter", "ng-if")).element(by.xpath("./p[1]/div"));
        };

        /*****************************Accessory Related**********************/

        this.ReviewOrderAccessoryHeader = function () {
            return element(by.tagName("accessories-breakdown-details")).element(by.tagName("h5"));
        };

        this.getReviewOrderAccessoriesList = function () {
            return element.all(by.repeater("item in vm.cartData.accessories.accessoryDetails"));
        };

        this.getReviewOrderAccessoryName = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.tagName('p'));
        };

        this.getReviewOrderAccessoryImage = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.tagName('img'));
        };

        this.getReviewOrderAccessoryPriceWrapper = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssCustomSelector("price-lockup-small", "div", "size-class"));
        };

        this.getAccessoryCollapseIcon = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.tagName("i"));
        };

        this.getReviewOrderAccessoryNameInCollapse = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("item.mvcAttrs.isCollapsed", "div", "collapse")).element(by.xpath("./div/div[2]/p[1]"));
        };

        this.getReviewOrderAccessoryQuantityInCollapse = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("item.mvcAttrs.isCollapsed", "div", "collapse")).element(by.xpath("./div/div[2]/p[2]"));
        };

        this.getReviewOrderAccessoryPriceDisplayInCollapse = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("item.mvcAttrs.isCollapsed", "div", "collapse")).element(by.xpath("./div/div[2]/p[3]"));
        };

        this.getReviewOrderFirstAccessoryPriceWrapper = function (RepeaterElement) {
            return element.all(by.repeater("item in vm.cartData.accessories.accessoryDetails")).get(0).element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssCustomSelector("price-lockup-small", "div", "size-class"));
        };

        this.getReviewOrderSecondAccessoryPriceWrapper = function (RepeaterElement) {
            return element.all(by.repeater("item in vm.cartData.accessories.accessoryDetails")).get(1).element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssCustomSelector("price-lockup-small", "div", "size-class"));
        };

        this.getReviewOrderThirdAccessoryPriceWrapper = function (RepeaterElement) {
            return element.all(by.repeater("item in vm.cartData.accessories.accessoryDetails")).get(2).element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssCustomSelector("price-lockup-small", "div", "size-class"));
        };

        /******Total Price related***********************/

        this.getReviewOrderTotalPrice = function () {
            return element(by.cssCustomSelector("price-lockup-large", "div", "size-class"));
        };

        this.getReviewOrderTotalPriceAutopayText = function () {
            return element(by.cssCustomSelector("price-lockup-large", "div", "size-class")).element(by.cssValueBasedSelector("cost-sub-header", "span", "class"));
        };

        /**********************Shipping section related*********************/

        this.getShippingDeliveryOptions = function () {
            return element(by.css(".shipOpts")).element(by.cssCustomSelector("selectOpt.selected=='true'", "div", "ng-if")).element(by.tagName("p"));
        };

        this.getShippingSectionFirstPTagInReviewOrder = function () {
            return element(by.css(".shipOpts")).all(by.tagName("p")).get(0);
        };

        this.getShippingSectionSecondPTagInReviewOrder = function () {
            return element(by.css(".shipOpts")).all(by.tagName("p")).get(1);
        };

        this.getShippingSectionThirdPTagInReviewOrder = function () {
            return element(by.css(".shipOpts")).all(by.tagName("p")).get(2);
        };

        this.getShippingSectionFourthPTagInReviewOrder = function () {
            return element(by.css(".shipOpts")).all(by.tagName("p")).get(3);
        };

        this.getShippingEditButton = function () {
            return element(by.cssCustomSelector("vm.setSectionToBeDisplayed('shippingInfoSection')", "a", "ng-click"));
        };
        this.getShippingEditButtondisplay = function () {
            //return element(by.cssCustomSelector("vm.setSectionToBeDisplayed('shippingInfoSection')","a","ng-click"));
            return element(by.class('col-lg-6 col-md-6 col-sm-6 col-xs-12 payment-section border p-lg-l-20 p-xs-l-20 right shipOpts'));
        };

        /**********************Payment section related*********************/

        this.getPaymentSectionFirstPTagInReviewOrder = function () {
            return element(by.css(".shipOpts")).element(by.xpath('following-sibling::div')).all(by.tagName("p")).get(0);
        };

        this.getPaymentSectionSecondPTagInReviewOrder = function () {
            return element(by.css(".shipOpts")).element(by.xpath('following-sibling::div')).all(by.tagName("p")).get(1);
        };

        this.getPaymentSectionThirdPTagInReviewOrder = function () {
            return element(by.css(".shipOpts")).element(by.xpath('following-sibling::div')).all(by.tagName("p")).get(2);
        };

        this.getPaymentSectionFourthPTagInReviewOrder = function () {
            return element(by.css(".shipOpts")).element(by.xpath('following-sibling::div')).all(by.tagName("p")).get(3);
        };

        this.getPaymentSectionFifthPTagInReviewOrder = function () {
            return element(by.css(".shipOpts")).element(by.xpath('following-sibling::div')).all(by.tagName("p")).get(4);
        };

        this.getPaymentEditButton = function () {
            //return element(by.cssCustomSelector("vm.setSectionToBeDisplayed('shippingInfoSectionBilling')","a","ng-click"));
            return element(by.cssCustomSelector("vm.setSectionToBeDisplayed('shippingInfoSection')", "a", "ng-click"));
        };

        /***************************Sub total related***************************/

        this.getOneTimeFees = function () {
            return element(by.cssCustomSelector("vm.sectionToBeDisplayed == 'reviewOrderInfoSection'", "div", "ng-if")).element(by.xpath("./div/div[3]/div[2]/div[3]"));
        };

        this.getOneTimeFeesText = function () {
            return element(by.xpath(".//*[@id='tmobileApp']/div[2]/div/div[3]/div[2]/div[1]"));
        };

        this.getSalesTax = function () {
            return element(by.cssCustomSelector("vm.sectionToBeDisplayed == 'reviewOrderInfoSection'", "div", "ng-if")).element(by.xpath("./div/div[3]/div[3]/div[3]/span"));
        };

        this.getSalesTaxText = function () {
            return element(by.xpath(".//*[@id='tmobileApp']/div[2]/div/div[3]/div[3]/div[1]"));
        };

        this.getShippingFees = function () {
            return element(by.cssCustomSelector("vm.sectionToBeDisplayed == 'reviewOrderInfoSection'", "div", "ng-if")).element(by.xpath("./div/div[3]/div[4]/div[3]/span"));
        };

        this.getShippingFeesText = function () {
            return element(by.xpath(".//*[@id='tmobileApp']/div[2]/div/div[3]/div[4]/div[1]"));
        };

        /********************others **********************************/

        this.getAddedServicesRepeater = function (RepeaterElement) {
            return RepeaterElement.all(by.cssCustomSelector("service in item.lineItemDetails.serviceDetails", "li", "ng-repeat"));
        };

        this.getAddedServiceName = function (RepeaterElement) {
            return RepeaterElement.all(by.tagName("p")).first();
        };

        this.getAddedServiceMonthlyPrice = function (RepeaterElement) {
            return RepeaterElement.all(by.tagName("p")).get(3).element(by.tagName("div"));
        };

        this.getReviewOrderChevronDownIcon = function (RepeaterElement) {
            return RepeaterElement.element(by.cssValueBasedSelector("fa-chevron-down", "i", "class"));
        };

        this.getSubmitOrderButton = function () {
            return element(by.cssCustomSelector("vm.openTermsAndCondtionModal('app/checkOut/termsConditionModal/termsAndConditions.html','app/checkOut/authFail/authFailShipping.html')", "button", "ng-click"));
        };

        this.getEditInCartLink = function () {
            //return element(by.cssCustomSelector("vm.sectionToBeDisplayed == 'reviewOrderInfoSection'", "div", "ng-if")).element(by.cssCustomSelector("myCart", "a", "ui-sref"));
            return element(by.id('editInCart'));
        };

        /*************************Created on Mar 15***********************/

        this.getReviewOrderFirstAccessoryPriceWrapperFRP = function (RepeaterElement) {
            return element.all(by.repeater("item in vm.cartData.accessories.accessoryDetails")).get(0).element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssCustomSelector("price-lockup-medium", "div", "size-class"));
        };

        this.getReviewOrderSecondAccessoryPriceWrapperFRP = function (RepeaterElement) {
            return element.all(by.repeater("item in vm.cartData.accessories.accessoryDetails")).get(1).element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssCustomSelector("price-lockup-medium", "div", "size-class"));
        };

        this.getReviewOrderThirdAccessoryPriceWrapperFRP = function (RepeaterElement) {
            return element.all(by.repeater("item in vm.cartData.accessories.accessoryDetails")).get(2).element(by.cssCustomSelector("vm.toggleCollapse(item)", "div", "ng-click")).element(by.cssCustomSelector("price-lockup-medium", "div", "size-class"));
        };

        /***************************Mar 20******************************/
        this.getReviewOrderGroupLineBYODMonthlyPriceInCollapse = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector("item.simKitDetails.isBYODAdded", "device-simkit-spliter", "ng-if")).element(by.xpath("./p[3]/div"));
        };

        this.getTCDisclaimer = function () {
            return element(by.id('disclaimerLabel'));
        };

        this.getReviewAgreeSubmitButton = function () {
            return element(by.id('submitOrder'));
        };

    };
    module.exports = function () {
        return new ReviewOrderPage();
    }
}());