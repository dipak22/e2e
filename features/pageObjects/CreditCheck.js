
'use strict';
(function () {

	var CreditCheck = function () {

		this.getNextSubmitButton = function () {
			return element(by.cssCustomSelector("creditForm.isSubmitted && !creditForm.$valid", "button", "ng-disabled"));
		};

		this.getNextButton = function () {
			return element(by.css('button[ng-disabled="creditForm.isSubmitted && !creditForm.$valid"]'));
		};
		
		//Credit check field error message
		this.getIdTypeErrorMessage = function () {
			return element(by.id("idTypeErrorMessage")).element(by.tagName("p"));
		};

		this.getIdNumberErrorMessage = function () {
			return element(by.id("idNumberErrorMessage")).element(by.tagName("p"));
		};

		this.getExpirationDateErrorMessage = function () {
			return element(by.id("expiryDateerror")).element(by.tagName("p"));
		};

		this.getSocialSecurityErrorMessage = function () {
			return element(by.id("ssnerror")).element(by.tagName("p"));
		};

		this.getDateOfBirthErrorMessage = function () {
			return element(by.id("doberror")).element(by.tagName("p"));
		};

		this.getSelectYourStateErrorMessage = function () {
			return element(by.id("stateerror")).element(by.tagName("p"));
		};

		//Credit check field Highlight
		this.getIdTypeFieldHighlight = function () {
			return element(by.model("vm.checkOutInfo.creditcheck.idType"));
		};

		this.getIdNumberFieldHighlight = function () {
			return element(by.model("vm.checkOutInfo.creditcheck.idNumber"));
		};

		this.getIdNumberInput = function () {
			return element(by.model("vm.checkOutInfo.creditcheck.idNumber"));
		};

		this.getExpirationDateFieldInput = function () {
			return element(by.name("expiryDate"));
		};

		this.getExpirationDateFieldHighlight = function () {
			return element(by.model("vm.checkOutInfo.creditcheck.expiryDate"));
		};

		this.getSelectYourStateFieldHighlight = function () {
			return element(by.model("vm.checkOutInfo.creditcheck.state"));
		};

		this.getSocialSecurityFieldHighlight = function () {
			return element(by.model("vm.checkOutInfo.creditcheck.ssn"));
		};

		this.getDateOfBirthFieldHighlight = function () {
			return element(by.model("vm.checkOutInfo.creditcheck.dob"));
		};

		//Credit check fields
		this.getIdTypeLabel = function () {
			return element(by.id("idTypeLabel"));
		};

		this.getIdTypeField = function () {
			return element(by.model("vm.checkOutInfo.creditcheck.idType"));
		};

		this.getIdNumberField = function () {
			return element(by.model("vm.checkOutInfo.creditcheck.idNumber"));
		};

		this.getExpirationDateField = function () {
			return element(by.model("vm.checkOutInfo.creditcheck.expiryDate"));
		};

		this.getSelectYourStateField = function () {
			return element(by.model("vm.checkOutInfo.creditcheck.state"));
		};

		this.getSocialSecurityField = function () {
			return element(by.model("vm.checkOutInfo.creditcheck.ssn"));
		};

		this.getDateOfBirthField = function () {
			return element(by.model("vm.checkOutInfo.creditcheck.dob"));
		};

		//Credit Check Repeater page objects
		this.getIdTypeFieldDropdown = function () {
			return element(by.cssCustomSelector("vm.checkOutInfo.creditcheck.idType", "select", "ng-model")).all(by.tagName("option"));
		};

		this.getIdTypeDropdown = function () {
			return element(by.id("idType"));
		};

		this.selectIdTypeOption = function (RepeaterElement) {
			return RepeaterElement;
		};

		this.getStateFieldDropdown = function () {
			return element(by.cssCustomSelector("vm.checkOutInfo.creditcheck.state", "select", "ng-model")).all(by.tagName("option"));
		};

		this.selectStateType = function (RepeaterElement) {
			return RepeaterElement;
		};

		this.getSelectedIDType = function () {
			return element(by.cssCustomSelector("vm.checkOutInfo.creditcheck.idType", "select", "ng-model")).element(by.cssCustomSelector("selected", "option", "selected"));
		};

		this.getSelectedState = function () {
			return element(by.cssCustomSelector("vm.checkOutInfo.creditcheck.state", "select", "ng-model")).element(by.cssCustomSelector("selected", "option", "selected"));
		};

		this.getCreditCheckPageProgressBarIcon = function () {
			return element.all(by.repeater('section in vm.arrayOfSections')).get(2).element(by.css('button[ng-click="vm.setSectionToBeDisplayed(section.name)"]'));
		};

	};
	module.exports = function () {
		return new CreditCheck();
	};
}());