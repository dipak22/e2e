'use strict';
(function () {

    var ConfirmationPage = function () {

        this.getAuthFailButton1 = function () {
            return element(by.css('[class="btn-black ng-binding"]'));
        };

        this.getAuthFailButton2 = function () {
            return element(by.css('[class="btn-white ng-binding"]'));
        };

        this.getFraudCheckMessage = function () {
            return element(by.css(".AuthFailWrapper")).element(by.tagName('h6'));
        };

        this.getFraudCheckPage = function () {
            return element(by.css(".AuthFailWrapper")).element(by.tagName('h6'));
        };

        this.getFraudCheckIcon = function () {
            return element(by.css(".AuthFailWrapper")).element(by.tagName('img'));
        };

        this.getConfirmationUserName = function () {
            return element(by.id('confirmationHeader'));
        };

    };

    module.exports = function () {
        return new ConfirmationPage();
    };

}());