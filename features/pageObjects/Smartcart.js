'use strict';
//var CustomLocators = require('../pageObjects/CustomLocators.js');
//var CustomLocatorObject = new CustomLocators();
(function () {

	var Smartcart = function () {

		//notification message popup

		//Add a Phone in Cart Page
		this.addAPhoneButtonElement = function () {
			return element(by.id('cart_cta_device'));
		};

		this.addAnAccessoryButtonElement = function () {
			return element(by.css('[ng-click="vm.myCartCtrl.showAccessories()"]'));
		};

		//Add a Phone in Empty Cart page
		this.addAPhoneButtonEmptyCart = function () {
			return element(by.css('div[ng-click="vm.myCartCtrl.addLine()"]'));
		};

		this.addAnAccessoryEmptyCart = function () {
			return element(by.css('div[ng-click="vm.myCartCtrl.showAccessories()"]'));
		};

		this.getEmptyCartHeader = function () {
			return element(by.css('h2[ng-bind-html="vm.myCartCtrl.keyValues.emptyCart.emptyCartHeader"]'));
		};

		this.addATabletOrWerableButton = function () {
			return element(by.id('cart_cta_tablet'));
		};

		this.getCartTitleElement = function () {
			return element(by.css('div[ng-bind-html="myCartCtrl.keyValues.cartHeader.cartTitle | parseHtml"]'));
		};

		this.deviceTileHeaderElement = function () {
			return element(by.css('[ng-bind-html="myCartCtrl.keyValues.cartRowItems.deviceTitle | parseHtml"]'));
		};

		this.getAddNewPhoneLabelElement = function () {
			return element(by.css('button[ng-click="myCartCtrl.creditCheck(\'Select Phone\',selectedLine,$index)"]'));
		};

		this.getAddDeviceIconElement = function () {
			return element.all(by.css('[ng-click="myCartCtrl.editProduct(selectedLine)"]'));
		};

		this.getAddDeviceIconElements = function (index) {
			return element.all(by.css('[ng-click="myCartCtrl.editProduct(selectedLine)"]')).get(index - 1);
		};

		this.creditSelectorModalHeaderElement = function () {
			return element(by.css('[ng-bind-html="creditSelectorModalCtrl.keyValues.creditSelector.header | parseHtml"]'));
		};

		this.getAwesomeCreditClassLabelElement = function () {
			return element.all(by.repeater('creditClass in creditSelectorModalCtrl.keyValues.class')).get(0).element(by.css('[ng-bind-html="creditClass.label | parseHtml"]'));
		};

		this.getCartCreditClassLink = function () {
			return element.all(by.css('[ng-bind-html="myCartCtrl.cartCommonOperationsService.model.structCartData.selectedCreditClass.label || myCartCtrl.keyValues.class[0].label | parseHtml"]')).get(0);
		};

		this.getBuildMyOrderButtonElement = function () {
			// return element(by.css('button[ng-click="creditSelectorModalCtrl.buildMyOrder()"]'));
			return element(by.css('button[ng-click="creditSelectorModalCtrl.buildMyOrder()"]'));
		};

		this.getMiniBrowseTitleElement = function () {
			//return element(by.css('[ng-if="deviceSelectorModalCtrl.mostPopularDevices"]')).element(by.tagName('h4'));
			// return element(by.cssCustomSelector("deviceSelectorModalCtrl.mostPopularDevices","div","ng-if")).element(by.tagName('h2'));
			return element(by.css(".modal-header")).element(by.tagName('h4'));
		};

		this.getBrowseDevices = function () {
			return element.all(by.css('[ng-repeat-start="product in vm.model.newProducts track by $index"]'));
		};

		/*CART PROMOTIONS*/
		this.getPromoNotificationBanner1 = function () {
			return element.all(by.css('ng-repeat="message in notificationMsg"'));
		};

		this.getPromoNotificationBanner2 = function () {
			return element(by.id("cart027"));
		};

		/***********************/

		this.getDeviceColumnTitle = function () {
			//return element(by.cssCustomSelector('!selectedLine.lineItemDetails.deviceDetails.sku && selectedLine.simKitDetails.simKitId != \'SIM_KIT_BYOD\' && !selectedLine.mvcAttrs.editProductInProgress','p','ng-if'));
			return element(by.cssCustomSelector('!selectedLine.lineItemDetails.deviceDetails.sku && !selectedLine.simKitDetails.isOnlySimAdded && !selectedLine.mvcAttrs.editProductInProgress', 'p', 'ng-if'));
		};

		this.getDeviceDefaultImage = function () {
			return element(by.cssCustomSelector("!selectedLine.mvcAttrs.editProductInProgress", "a", "ng-if")).element(by.tagName("img"));
		};

		this.getPlanColumnTitle = function () {
			//return element(by.cssCustomSelector("!selectedLine.mvcAttrs.editPlanInProgress","div","ng-show")).element(by.cssCustomSelector("myCartCtrl.keyValues.cartRowItems.plansTitle | parseHtml","span","ng-bind-html"));
			return element(by.cssCustomSelector("!selectedLine.mvcAttrs.editPlanInProgress", "div", "ng-show")).element(by.cssCustomSelector("myCartCtrl.keyValues.cartRowItems.plansTitle | parseHtml", "p", "ng-bind-html"));
			//return element(by.cssCustomSelector("!selectedLine.mvcAttrs.editPlanInProgress","div","ng-show")).element(by.cssCustomSelector("vm.keyValues.cartRowItems.plansTitle | parseHtml","p","ng-bind-html"));
		};
		this.getProtectionColumnTitle = function () {
			return element(by.cssCustomSelector("myCartCtrl.keyValues.cartRowItems.servicesTitle | parseHtml", "span", "ng-bind-html"));
			//return element(by.cssCustomSelector("vm.keyValues.cartRowItems.servicesTitle | parseHtml","span","ng-bind-html"));
		};

		this.getStickyBannerCostWrapper = function () {
			//return element(by.cssCustomSelector("","div","sticky-price")).element(by.cssCustomSelector("price-lockup-medium","div","size-class"));
			return element(by.id("cart_stickybanner_id")).element(by.cssCustomSelector("price-lockup-medium", "div", "size-class"));
		};

		this.getAddedDeviceName = function () {
			//return element(by.cssCustomSelector("!selectedLine.mvcAttrs.editProductInProgress","div","ng-show")).element(by.tagName("h5"));
			return element.all(by.css('div[ng-show="!selectedLine.mvcAttrs.editProductInProgress"]')).get(0).element(by.tagName("h5"));
			//return element(by.id('cartDeviceSection_2')).element(by.tagName("h5"));
		};

		this.getAddedDeviceNames = function (index) {
			//return element(by.cssCustomSelector("!selectedLine.mvcAttrs.editProductInProgress","div","ng-show")).element(by.tagName("h5"));
			return element.all(by.css('div[ng-show="!selectedLine.mvcAttrs.editProductInProgress"]')).get(index - 1).element(by.tagName("h5"));
			//return element(by.id('cartDeviceSection_2')).element(by.tagName("h5"));
		};

		this.getAddedDeviceColorAndMemory = function () {
			//return element(by.cssCustomSelector("!selectedLine.mvcAttrs.editProductInProgress","div","ng-show")).element(by.cssCustomSelector("ng-binding","p","class"));
			return element(by.cssCustomSelector("!selectedLine.mvcAttrs.editProductInProgress", "div", "ng-show")).element(by.cssCustomSelector("selectedLine.lineItemDetails.deviceDetails.colorSwatch.length > 0", "img", "ng-if"));
		};

		this.getCartProductCostWrapper = function () {
			return element(by.cssCustomSelector("selectedLine in myCartCtrl.cartCommonOperationsService.model.structCartData.lineItems | orderBy: '1*lineNo'", "div", "ng-repeat")).element(by.cssCustomSelector("price-lockup-xsmall", "div", "size-class"));
			//return element(by.cssCustomSelector("selectedLine in vm.cartControllerHelperForCommonOperationsService.model.structCartData.lineItems | orderBy: '1*lineNo'","div","ng-repeat")).element(by.cssCustomSelector("price-lockup-xsmall","div","size-class"));
		};

		this.stickyBannerCreditInitial = function () {
			return element(by.css('a[ng-click="myCartCtrl.openCreditClassSelectorModal()"]'));
		};

		this.getCreditClass = function () {
			return element.all(by.repeater('creditClass in creditSelectorModalCtrl.keyValues.class'));
		};

		this.getCreditClassLabel = function (RepeaterElement) {
			//return RepeaterElement.element(by.cssCustomSelector("creditClassTextTitle ng-binding","span","class"));
			return RepeaterElement.element(by.cssCustomSelector("creditClass.label | parseHtml", "span", "ng-bind-html"));
		};

		this.getAverageCreditClassLabel = function () {
			return element.all(by.repeater('creditClass in creditSelectorModalCtrl.keyValues.class')).get(1).element(by.cssCustomSelector("creditClass.label | parseHtml", "span", "ng-bind-html"));
		};

		this.getCreditClassSubText = function (RepeaterElement) {
			return RepeaterElement.element(by.cssCustomSelector("creditClass.description | parseHtml", "p", "ng-bind-html"));
		};

		this.ContactUSText = function () {
			return element(by.css('[ng-bind-html="myCartCtrl.keyValues.footerText.footerTitle | parseHtml"]'));
		};

		this.ContactUsSubText = function () {
			return element(by.css('[ng-bind-html="myCartCtrl.keyValues.footerText.footerSubTitle | parseHtml"]'));
		};

		this.CallUsText = function () {
			return element(by.css('[ng-bind-html="myCartCtrl.keyValues.footerText.callUsText | parseHtml"]'));
		};

		this.CallUsContactNumber = function () {
			return element(by.cssCustomSelector("myCartCtrl.authorValue.rootPage.invoca.invocaCallusvalue | parseHtml", "span", "ng-bind-html"))
		};

		this.getAddPromoLabel = function () {
			return element(by.css('[ng-click="myCartCtrl.showPromoCodeIP=true"]'));
		};

		this.getCheckoutButton = function () {
			element(by.cssCustomSelector("LPMcloseButton", "img", "class")).isPresent().then(function (presence) {
				console.log(presence);
				if (presence == true) {
					element(by.cssCustomSelector("LPMcloseButton", "img", "class")).click().then(function () {
						console.log("unexpected alert window is closed");
					})
				}
				else {
					console.log("unexpected alert window is not present");
				}
			});
			return element(by.cssCustomSelector("!myCartCtrl.cartCommonOperationsService.model.structCartData.flags.isOrderEligible", 'button', 'ng-disabled'));
		};

		this.getAddNewPhoneLabel = function () {
			return element(by.css('button[ng-click="myCartCtrl.creditCheck(\'Select Phone\',selectedLine,$index)"]'));
		};

		this.getEditDeviceButtonInCart = function () {
			return element(by.cssCustomSelector("myCartCtrl.editProduct(selectedLine)", "a", "ng-enter"));
		};

		this.getSelectNewPhoneButton = function () {
			return element(by.css('button[ng-click="myCartCtrl.openDeviceSelectorModal(\'Select Phone\',selectedLine,$index)"]'));
		};

		this.getEditCurrentPhoneButton = function () {
			return element(by.cssCustomSelector("myCartCtrl.openMiniPDPEditModal('Edit Phone',selectedLine,$index)", "button", "ng-click"));
		};

		this.getProtectionAndMoreIcon = function () {
			element(by.cssCustomSelector("LPMcloseButton", "img", "class")).isPresent().then(function (presence) {
				console.log(presence);
				if (presence == true) {
					element(by.cssCustomSelector("LPMcloseButton", "img", "class")).click().then(function () {
						console.log("unexpected alert window is closed");
					})
				}
				else {
					console.log("unexpected alert window is not present");
				}
			});
			return element(by.cssCustomSelector("myCartCtrl.zipCodeCheck(selectedLine,$index)", "div", "ng-click"));
		};

		this.CreditSelectorModalHeader = function () {
			return element(by.css('[ng-bind-html="creditSelectorModalCtrl.keyValues.creditSelector.header | parseHtml"]'));
		};

		this.getCreditModalLabel = function () {
			return element(by.cssCustomSelector("creditSelectorModalCtrl.keyValues.creditSelector.header | parseHtml", "h2", "ng-bind-html"));
		};

		this.CreditSelectorSubText = function () {
			return element(by.cssCustomSelector("creditSelectorModalCtrl.keyValues.creditSelector.subHeader | parseHtml", "p", "ng-bind-html"));
		};

		this.getBuildMyOrderButton = function () {
			return element(by.css('button[ng-click="creditSelectorModalCtrl.buildMyOrder()"]'));
		};

		this.getMiniBrowseDeviceList = function () {
			return element.all(by.repeater('device in deviceSelectorModalCtrl.mostPopularDevices  track by $index | limitTo:8'));
		};

		this.getMiniBrowseDeviceApple = function () {
			return element.all(by.repeater('device in deviceSelectorModalCtrl.mostPopularDevices  track by $index | limitTo:8')).get(2).element(by.tagName('img'));
		};

		this.getMiniBrowseDeviceAppleiphone7 = function () {
			return element.all(by.repeater('device in deviceSelectorModalCtrl.mostPopularDevices  track by $index | limitTo:8')).get(3).element(by.tagName('img'));
		};

		this.getMiniBrowseDeviceName = function (RepeaterElement) {
			return RepeaterElement.element(by.css('.selector-device-name'));
		};

		this.getMiniBrowseDeviceImage = function (RepeaterElement) {
			return RepeaterElement.element(by.tagName('img'));
		};

		this.getMiniPDPLabelButton = function () {
			return element(by.cssCustomSelector('miniPDPModalCtrl.selectedProduct.SKU && !miniPDPModalCtrl.showModel', 'div', 'ng-if')).element(by.tagName('h3'));
		};

		this.getMemoryList = function () {
			return element.all(by.repeater("product in miniPDPModalCtrl.filteredColorProductData | unique:'Memory'"));
		};

		this.getColorList = function () {
			return element.all(by.repeater("product in miniPDPModalCtrl.filteredProductData"));
		};

		this.selectMemoryValue = function (RepeaterElement) {
			return RepeaterElement;
		};

		this.selectMemory = function () {
			return element.all(by.repeater("product in miniPDPModalCtrl.filteredColorProductData | unique:'Memory'"));
		};

		this.selectColorValue = function (RepeaterElement) {
			return RepeaterElement;
		};

		this.selectColor = function () {
			return element.all(by.repeater('product in miniPDPModalCtrl.filteredProductData'));
		};

		this.getPaymentSelectDropDownList = function () {
			return element(by.model('miniPDPModalCtrl.paymentOption')).all(by.tagName('option'));
		};

		this.selectPaymentOption = function (RepeaterElement) {
			return RepeaterElement;
		};

		this.getDeviceFullPriceMiniPDP = function () {
			return element(by.cssCustomSelector('{"modalID":"cartMiniPDP","modalName":"cart Mini PDP","dynamicContent":"no"}', "div", "pdl-modal-data")).element(by.cssCustomSelector("price-lockup price-lockup-medium", "div", "class")).element(by.cssCustomSelector("price-lockup-large", "div", "size-class"));
		};

		this.getDeviceMonthlyPaymentsMiniPDP = function () {
			return element(by.cssCustomSelector('{"modalID":"cartMiniPDP","modalName":"cart Mini PDP","dynamicContent":"no"}', "div", "pdl-modal-data")).element(by.cssCustomSelector("deviceSelectorModalCtrl.currentCreditClass == 'creditClass1' || deviceSelectorModalCtrl.currentCreditClass == 'creditClass2' && deviceSelectorModalCtrl.selectedProduct.pricing.Upfront != deviceSelectorModalCtrl.selectedProduct.pricing.DiscountedPrice", "div", "ng-if"));
		};

		this.getminiPDPHeader = function () {
			return element(by.css('div[class="modal-content"]')).element(by.tagName('h3'));
		}

		this.getAddToCartButtonMiniPDP = function () {
			return element(by.css('button[ng-click="miniPDPModalCtrl.addToCart()"]'));
		};

		//Accessories related page objects
		this.getAccessoriesHeader = function () {
			return element(by.cssCustomSelector("vm.keyValuePair.accessories.accessoriesTitle | parseHtml", "span", "ng-bind-html"))
		};

		this.getAccMiniBrowseHeader = function () {
			return element(by.id('dialogTitle'));
		};

		this.getAddAccessoriesBoxHeader = function () {
			return element(by.cssCustomSelector("vm.openAccessorySelectorModal('selectAccessaory',acc.index);$event.stopPropagation();", "div", "ng-click")).element(by.tagName('p'));
		};

		this.getAccessoriesAddIcon = function () {
			return element(by.css('div[ng-click="vm.openAccessorySelectorModal(\'selectAccessaory\',acc.index);$event.stopPropagation();"]'));
			//return element(by.css('[ng-click="vm.openAccessoryMiniBrowse()"]'));
		};

		this.getAccessoriesModalHeader = function () {
			return element(by.css('h3[ng-bind-html="vm.keyValues.accessoriesModal.accessoriesHeader | parseHtml"]'));
		};

		this.selectFirstTabAccessory = function () {
			return element(by.id('tab-tablets-and-devices')).element(by.tagName('h5'));
		};

		this.selectSecondTabAccessory = function () {
			return element(by.id('tab-phones')).element(by.tagName('h5'));
		};

		this.selectThirdTabAccessory = function () {
			return element(by.id('tab-accessories')).element(by.tagName('h5'));
		};

		this.addToCart = function () {
			return element(by.css('button[ng-click="vm.addToCart()"]'));
		};

		this.getCasesTab = function () {
			return element(by.css('li[ng-class="{active: vm.paneIndex === 0}"]')).element(by.tagName('a'));
		};

		this.getHeadsetsTab = function () {
			return element(by.css('li[ng-class="{active: vm.paneIndex === 1}"]')).element(by.tagName('a'));
		};

		this.getChargersTab = function () {
			return element(by.css('li[ng-class="{active: vm.paneIndex === 2}"]')).element(by.tagName('a'));
		};

		this.getCasesItemsRepeater = function () {
			return element(by.css('div[id="tab-tablets-and-devices"]')).all(by.cssCustomSelector('accessory in tab.products', "a", "ng-repeat-start"));
		};

		this.getFirstTabAccessoryImage = function () {
			//return element.all(by.repeater('tab in vm.tabs')).get(0).element(by.tagName('img'));
			return element(by.id('tab-tablets-and-devices')).element(by.tagName('img'));
		};

		this.getSecondTabAccessoryImage = function () {
			//return element.all(by.repeater('tab in vm.tabs')).get(0).element(by.tagName('img'));
			return element(by.id('tab-phones')).element(by.tagName('img'));
		};

		this.getThirdTabAccessoryImage = function () {
			//return element.all(by.repeater('tab in vm.tabs')).get(0).element(by.tagName('img'));
			return element(by.id('tab-accessories')).element(by.tagName('img'));
		};

		this.getCaseName = function (RepeaterElement) {
			return RepeaterElement.element(by.tagName('h5'));
		};

		this.getCaseTotalPriceDollar = function (RepeaterElement) {
			return RepeaterElement.element(by.cssCustomSelector("totalPrice", "money", "price")).element(by.cssCustomSelector("dollarClass", "span", "ng-class"));
		};

		this.getCaseTotalPriceCents = function (RepeaterElement) {
			return RepeaterElement.element(by.cssCustomSelector("totalPrice", "money", "price")).element(by.cssCustomSelector("pennyClass", "span", "ng-class"));
		};

		this.getSelectedAccessoryModalHeader = function () {
			return element(by.css('.accessories-pdp-modal-content')).element(by.tagName('h5'));
		};

		this.getSelectedItemQuantityListAccessoriesModal = function () {
			return element(by.cssCustomSelector("i for i in [1,2,3,4,5]", "select", "ng-options")).all(by.tagName("option"));
		};

		this.selectItemQuantityInAccessoriesModal = function (RepeaterElement) {
			return RepeaterElement;
		};

		this.getSelectedAccessoriesTotalPrice = function () {
			return element(by.cssCustomSelector("vm.paymentOption == vm.keyValues.accessoryminipdp.paymentOption2", "li", "ng-if"));
		};

		this.getSelectedAccessoriesOriginalDollarPrice = function () {
			return element(by.cssCustomSelector("vm.paymentOption == 'FULL' && vm.model.selectedAccessory.FullPrice != vm.getCreditAmount.DiscountedPrice", "div", "ng-if")).element(by.cssCustomSelector("dollarClass", "span", "ng-class"));
		};

		this.getSelectedAccessoriesOriginalCentPrice = function () {
			return element(by.cssCustomSelector("vm.paymentOption == 'FULL' && vm.model.selectedAccessory.FullPrice != vm.getCreditAmount.DiscountedPrice", "div", "ng-if")).element(by.cssCustomSelector("pennyClass", "span", "ng-class"));
		};

		this.getSelectedAccessoriesDiscountedDollarPrice = function () {
			return element(by.cssCustomSelector("vm.paymentOption == 'FULL' && vm.model.selectedAccessory.FullPrice != vm.getCreditAmount.DiscountedPrice", "div", "ng-if")).element(by.cssCustomSelector("dollarClass", "span", "ng-class"));
		};

		this.getSelectedAccessoriesDiscountedCentPrice = function () {
			return element(by.cssCustomSelector("vm.paymentOption == 'FULL' && vm.model.selectedAccessory.FullPrice != vm.getCreditAmount.DiscountedPrice", "div", "ng-if")).element(by.cssCustomSelector("pennyClass", "span", "ng-class"));
		};

		this.getAddToCartAccessoriesModal = function () {
			return element(by.cssCustomSelector("vm.addToCart()", "button", "ng-click"));
		};

		this.getAccessoriesEditButtonInCart = function () {
			return element(by.cssCustomSelector("vm.showEditAccessory(acc)", "a", "ng-enter"));
		};

		this.getAccessoriesQuantityLabelInCart = function () {
			return element(by.cssCustomSelector("vm.showQuantity(acc.quantity)", "span", "ng-show")).element(by.cssCustomSelector("vm.keyValuePair.accessories.quantityText | parseHtml", "span", "ng-bind-html"));
		};

		this.getSelectedAccessoriesQuantityInCart = function () {
			return element(by.cssCustomSelector("vm.showQuantity(acc.quantity)", "span", "ng-show")).element(by.cssCustomSelector("vm.keyValuePair.accessories.quantityText | parseHtml", "span", "ng-bind-html")).element(by.xpath('following-sibling::span'));
		};

		this.getSelectedAccessoriesNameInCart = function () {
			return element(by.cssCustomSelector("acc.familyName && !acc.edit && !acc.deleteConfirm", "div", "ng-if")).element(by.cssCustomSelector("small name ng-binding", "p", "class"));
		};

		this.getSelectedAccessoriesDollarPriceInCart = function () {
			return element(by.cssCustomSelector("!vm.checkPaymentOptionType(acc.paymentOption,vm.keyValuePair)", "div", "ng-if")).element(by.css('.cost-wrapper')).element(by.cssCustomSelector("dollarClass", "span", "ng-class"));
		};

		this.getSelectedAccessoriesCentsPriceInCart = function () {
			return element(by.cssCustomSelector("!vm.checkPaymentOptionType(acc.paymentOption,vm.keyValuePair)", "div", "ng-if")).element(by.css('.cost-wrapper')).element(by.cssCustomSelector("pennyClass", "span", "ng-class"));
		};

		this.getCancelButtonInEditAccessoriesModal = function () {
			return element(by.cssCustomSelector("vm.cancelEdit(acc)", "a", "ng-click"));
		};

		this.getAccessoriesEditButtonInEditModal = function () {
			return element(by.cssCustomSelector("vm.openAccessorySelectorModal('editAccessaory',acc.index,acc);$event.stopPropagation();", "button", "ng-click"));
		};

		this.getAccessoriesDeleteButtonInEditModal = function () {
			return element(by.cssCustomSelector("vm.showDeleteAccessory(acc)", "button", "ng-click"));
		};

		this.getAccessoriesDeleteButtonConfimationInEditModal = function () {
			return element(by.id('cnfdelBtnAcc'));
		};

		this.getAddedAccessoryNames = function (index) {
			return element.all(by.css('p[class="small name ng-binding"]')).get(index - 1);
		}

		this.getAddedAccessoryName = function () {
			return element(by.css('p[class="small name ng-binding"]'));
		}

		// this.getSelectedItemQuantityInAccessoriesModal = function(){
		// 	return element(by.cssCustomSelector("vm.selectCount(vm.qty)", "select", "ng-change"));
		// };

		//protection and more modal

		this.getProtectionModalNextButton = function () {
			return element(by.cssCustomSelector("setLocationFPOModalCtrl.validateZipCode(zipCodeForm)", "button", "ng-click"));
		};

		this.getProtectionModalCloseIcon = function () {
			return element(by.cssCustomSelector("button", "setLocationFPOModalCtrl.cancel()", "ng-click"))
		};

		this.getProtectionModalHeader = function () {
			return element(by.cssCustomSelector("location-fpo-modal", "div", "window-class")).element(by.tagName('h4'));
		};

		this.getProtectionModalHeaderSubText = function () {
			return element(by.cssCustomSelector("setLocationFPOModalCtrl.keyValues.zipCodeModal.descriptionText | parseHtml", "p", "ng-bind-html"));
		};

		this.getProtectionModalInputTagLabel = function () {
			return element(by.cssCustomSelector("setLocationFPOModalCtrl.keyValues.zipCodeModal.zipCodeField | parseHtml", "label", "ng-bind-html"));
		};

		this.getProtectionModalInput = function () {
			return element(by.cssCustomSelector("setLocationFPOModalCtrl.zipCode", "input", "ng-model"));
		};

		this.getServicesModalHeader = function () {
			return element(by.cssCustomSelector("devicePASelctor.keyValues.services.serviceModalHeader | parseHtml", "h4", "ng-bind-html"));
		};

		this.getServicesModalUpdateButton = function () {
			return element(by.cssCustomSelector("devicePASelctor.save()", "button", "ng-click"));
		};

		this.getServicesModalDeviceProtectionTab = function () {
			return element.all(by.cssCustomSelector("devicePASelctor.selectServiceCategory(category.type)", "li", "ng-click")).get(0);
		};

		this.getServicesModalCommunicationAndDataTab = function () {
			return element.all(by.cssCustomSelector("devicePASelctor.selectServiceCategory(category.type)", "li", "ng-click")).get(1);
		};

		this.getServicesModalEntertainmentAndMoreTab = function () {
			return element.all(by.cssCustomSelector("devicePASelctor.selectServiceCategory(category.type)", "li", "ng-click")).get(2);
		};

		this.getServiceCategories = function () {
			return element(by.css('div[id="device-service-panel"]')).all(by.repeater('superCategory in devicePASelctor.serviceCategories'));
		};

		this.getServicesLabelName = function (RepeaterElement) {
			return RepeaterElement.element(by.css('h5[ng-hide="superCategory.items.length<=1"]'));
		};

		this.getAllServices = function (RepeaterElement) {
			return RepeaterElement.all(by.repeater("plan in devicePASelctor.protectionPlans.lineItems | filter:{serviceCategory:category.name} | orderBy : 'orderGroup'"));
		};

		this.getAllServices1 = function () {
			return element(by.css('div[id="serices_list_panel"]')).all(by.repeater("category in superCategory.items"));
		};

		this.getServiceName = function (RepeaterElement) {
			return RepeaterElement.element(by.css(".service-name"));
		};

		this.getFirstServiceName = function () {
			return element(by.cssCustomSelector("plan in devicePASelctor.protectionPlans.lineItems | filter:{serviceCategory:devicePASelctor.selectedServiceCategory} | orderBy : 'orderGroup'", "div", "ng-repeat")).element(by.css(".service-name"));
		};

		this.getAllAddedServices = function () {
			return element(by.cssCustomSelector("selectedLine.lineItemDetails.serviceDetails.length > 0", "div", "ng-if")).all(by.cssCustomSelector("service in selectedLine.lineItemDetails.serviceDetails", "p", "ng-repeat"));
		};

		this.getAddedServiceName = function (RepeaterElement) {
			return RepeaterElement;
		};

		this.needMoreServicesLink = function (index) {
			return element.all(by.css('a[ng-click="vm.myCartCtrl.zipCodeCheck(vm.lineItem,$index)"]')).get(index - 1);
		};

		this.zipCode = function () {
			return element(by.id('dialogDiscription'));
		};

		this.setZipCode = function () {
			return element(by.id('zipcode'));
		};

		this.nextButton = function () {
			return element(by.css('button[ng-click="setLocationFPOModalCtrl.validateZipCode(zipCodeForm)"]'));
		};

		this.serviceHeader = function () {
			return element(by.id('dialogTitle'));
		};

		this.bundleServiceCategory = function () {
			return element.all(by.repeater("category in devicePASelctor.serviceCategories")).get(0);
		};

		this.alacarteServiceCategory = function () {
			return element.all(by.repeater("category in devicePASelctor.serviceCategories")).get(1);
		};

		this.serviceslist = function () {
			return element.all(by.repeater("plan in devicePASelctor.protectionPlans.lineItems | filter:{serviceCategory:category.name} | orderBy : 'orderGroup'"));
		};

		this.selectService = function (name) {
			return element.all(by.repeater("plan in devicePASelctor.protectionPlans.lineItems | filter:{serviceCategory:category.name} | orderBy : 'orderGroup'")).element(by.cssContainingText('.service-name', name));
		}

		//////////////////////////*************************Added element on the cart Page e2e006*******************************************/////////////////////////////////////////////////
		this.needmoreoptions = function () {
			return element(by.css('[ng-click="vm.myCartCtrl.zipCodeCheck(vm.lineItem,$index)"]'))
		};

		this.nextonzipcode = function () {
			element(by.css('[ng-click="setLocationFPOModalCtrl.validateZipCode(zipCodeForm)"]'));
		};

		///////////////////////////*****************end of added element e2e006 feature file*********************///////////////////////////////
		this.getServicePriceMonthlyLabel = function (RepeaterElement) {
			return RepeaterElement.element(by.cssCustomSelector("devicePASelctor.keyValues.pricing.monthlyPriceLabel | parseHtml", "span", "ng-bind-html"));
		};

		this.getServiceModalLegalText = function () {
			return element(by.cssCustomSelector("devicePASelctor.keyValues.services.serviceLegalText | parseHtml", "p", "ng-bind-html"));
		};

		this.closeServicesModal = function () {
			return element(by.cssCustomSelector("devicePASelctor.close()", "button", "ng-click"));
		};

		//BYOD related page objects
		this.getAddBYODButton = function () {
			return element(by.css('button[ng-bind-html="selectedLine.lineType==\'HANDSET\' ? myCartCtrl.keyValues.defaultRowContent.useYourOwnPhone : myCartCtrl.keyValues.defaultRowContent.useYourOwnTablet | parseHtml"]'));
		};

		this.getBYODHeader = function () {
			return element(by.css('.product-name-mini-pdp'));
		};

		this.getBYODDescription = function () {
			return element(by.cssCustomSelector("ownPhone.phoneDetails.Description | parseHtml", "p", "ng-bind-html"));
		};

		this.getBYODLegalText = function () {
			return element(by.cssCustomSelector("ownPhone.keyValues.byod.byodLegalText | parseHtml", "p", "ng-bind-html"));
		};

		this.getBYODPricingHeader = function () {
			return element(by.cssCustomSelector("ownPhone.keyValues.byod.byodPricingHeader | parseHtml", "p", "ng-bind-html"));
		};

		this.getBYODFullPriceHeader = function () {
			return element(by.cssCustomSelector("ownPhone.keyValues.byod.byodFullPrice | parseHtml", "p", "ng-bind-html"));
		};

		this.getBYODPricing = function () {
			return element(by.cssCustomSelector("own-phone-modal", "div", "window-class")).element(by.cssCustomSelector("price-lockup-large", "div", "size-class"));
		};

		this.getBYODOriginalPrice = function () {
			return element(by.cssCustomSelector("own-phone-modal", "div", "window-class")).element(by.cssCustomSelector("price-lockup-large", "div", "size-class")).element(by.id("money_219"));
		};

		this.getBYODTodayPrice = function () {
			return element(by.cssCustomSelector("own-phone-modal", "div", "window-class")).element(by.cssCustomSelector("price-lockup-large", "div", "size-class")).element(by.id("money_222"));
		};

		this.getBYODOriginalPriceLabel = function () {
			return element(by.cssCustomSelector("own-phone-modal", "div", "window-class")).element(by.cssCustomSelector("price-lockup-large", "div", "size-class")).element(by.cssCustomSelector("leftText", "span", "ng-bind-html"));
		};

		this.getBYODTodayPriceLabel = function () {
			return element(by.cssCustomSelector("own-phone-modal", "div", "window-class")).element(by.cssCustomSelector("price-lockup-large", "div", "size-class")).element(by.cssCustomSelector("rightText", "span", "ng-bind-html"));
		};

		this.getBYODImage = function () {
			return element(by.cssCustomSelector("own-phone-modal", "div", "window-class")).element(by.cssCustomSelector("image in slickerData track by $index", "span", "ng-repeat")).element(by.tagName("img"));
		};

		this.getBYODAddToCart = function () {
			return element(by.css('button[ng-click="ownPhone.addTocart()"]'));
		};

		this.getAddBYODButtonMiniBrowse = function () {
			return element(by.cssCustomSelector("deviceSelectorModalCtrl.openOwnPhoneModal()", "button", "ng-click"));
		};

		this.getBYODCloseButton = function () {
			return element(by.cssCustomSelector("ownPhone.cancel()", "button", "ng-click"));
		};

		this.getaddedDeviceNameLine1 = function () {
			return element.all(by.css('div[ng-show="!selectedLine.mvcAttrs.editProductInProgress"]')).get(0).element(by.tagName("h5"));
			//return element(by.id('cartDeviceSection_1')).element(by.tagName("h5"));
		};

		this.getaddedDeviceNameLine2 = function () {
			return element.all(by.css('div[ng-show="!selectedLine.mvcAttrs.editProductInProgress"]')).get(1).element(by.tagName("h5"));
			//return element(by.id('cartDeviceSection_2')).element(by.tagName("h5"));
		};

		this.getaddedDeviceNameLine3 = function () {
			return element.all(by.css('div[ng-show="!selectedLine.mvcAttrs.editProductInProgress"]')).get(2).element(by.tagName("h5"));
			//return element(by.id('cartDeviceSection_2')).element(by.tagName("h5"));
		};

		this.getAddedBYODName2 = function () {
			return element(by.id('cartDeviceSection_2')).element(by.tagName("h5"));
		};

		//MAG-6811 Accessories Pricing Breakdown Modal

		this.PricingBreakdownLink = function () {
			return element(by.cssCustomSelector("myCartCtrl.openPriceBreakdownModal()", "a", "ng-click"));
		};

		this.getPricingBreakdownHeader = function () {
			return element(by.cssCustomSelector("priceBreakdownCtrl.keyValues.pricingBreakdown.priceBreakdown | parseHtml", "h3", "ng-bind-html"));
		};

		this.getPricingBreakdownAccessoriesHeader = function () {
			return element(by.cssCustomSelector("vm.cartObj.accessories.accessoryDetails.length>0", "h5", "ng-if"));
		};

		this.PricingBreakdownAccessoriesName = function () {
			return element(by.css('[ng-click="item.mvcAttrs.isCollapsed=!item.mvcAttrs.isCollapsed"]')).element(by.tagName('p'));
		};

		this.PricingBreakdownAccessoriesQty = function () {
			return element(by.css('.col-lg-11 col-md-11 accessories-padding-top')).element(by.tagName('p'));
		};

		this.PricingBreakdownAccessoriesTodayPrice = function () {
			return element(by.cssCustomSelector("dollarClass", "span", "ng-class"));
		};

		this.PricingBreakdownAccessoriesTodayDollar = function () {
			return element(by.cssCustomSelector("symbolClass", "sup", "ng-class"));
		};

		this.PricingBreakdownAccessoriesTodaySuperScript = function () {
			return element(by.cssCustomSelector("pennyClass", "sup", "ng-class"));
		};

		this.PricingBreakdownAccessoriesTodayLabel = function () {
			return element(by.cssCustomSelector('.cost-header paragraph-bold xsmall ng-binding')).element(by.tagName('span'));
		};

		this.PricingBreakdownClose = function () {
			return element(by.cssCustomSelector("priceBreakdownCtrl.close()", "button", "ng-click"));
		};

		this.getPriceBreakDownModalTotalSubheader = function () {
			return element(by.cssCustomSelector("priceBreakdownCtrl.keyValues.pricingBreakdown.totalSubheader | parseHtml", "div", "ng-bind-html"));
		};

		this.PricingBreakdownSingleDeviceOrderHeader = function () {
			return element(by.cssCustomSelector("(key,groupItem) in vm.cartObj.lineItems | groupBy : 'orderGroup'", "span", "ng-repeat")).element(by.tagName("h5"));
		};

		this.PricingBreakdownSingleDevicePlanName = function () {
			return element(by.cssCustomSelector("(item.lineType=='HANDSET'?vm.breakdownAuthorValue.planNameDevice: (item.lineItemDetails.deviceDetails.isWearable?vm.breakdownAuthorValue.wearablePlanName:vm.breakdownAuthorValue.tabletPlanName)) || item.lineItemDetails.planDetails.familyName | parseHtml", "span", "ng-bind-html"));
		};

		this.PricingBreakdownSingleDeviceAutopayText = function () {
			return element(by.cssCustomSelector("vm.breakdownAuthorValue.autoPayText | parseHtml", "span", "ng-bind-html"));
		};

		this.PricingBreakDownSingleDeviceSIMKit = function () {
			return element(by.cssCustomSelector("item.lineItemDetails.deviceDetails.isProductAdded && item.simKitDetails.displayName", "li", "ng-if")).element(by.cssCustomSelector("SIM Starter Kit", "span", "aria-label"));
		};

		this.PricingBreakDownSingleDeviceName = function () {
			return element(by.cssCustomSelector("!item.simKitDetails.isBYODAdded", "div", "ng-if")).element(by.tagName("p"));
		};

		//Red lines related page objects
		this.getCartBannerText = function () {
			return element(by.cssCustomSelector("myCartCtrl.keyValues.cartHeader.bannerText | parseHtml", "p", "ng-bind-html"));
		};

		this.getPlanName = function () {
			return element(by.cssCustomSelector("!selectedLine.mvcAttrs.editPlanInProgress", "div", "ng-show")).element(by.tagName("h5"));
		};

		this.getProtectionText = function () {
			return element(by.cssCustomSelector("!selectedLine.lineItemDetails.deviceDetails.sku && selectedLine.simKitDetails.simKitId != 'SIM_KIT_BYOD'", "p", "ng-if"));
		};

		this.getCartItemList = function () {
			return element.all(by.repeater('selectedLine in myCartCtrl.structCartData.lineItems | orderBy: \'1*lineNo\''));
		};

		this.getCartItemNumber = function (RepeaterElement) {
			return RepeaterElement.element(by.tagName('h3')).element(by.cssCustomSelector('product-number', 'class', 'span'));
		};

		this.getCartItemNumberHeader = function (RepeaterElement) {
			return RepeaterElement.element(by.tagName('h3'));
		};

		this.getDuplicateIcon = function (lineItem) {
			//return element(by.cssCustomSelector("myCartCtrl.duplicateLine(selectedLine)", "a", "ng-click"));
			return lineItem.element(by.css('a[ng-click="myCartCtrl.duplicateLine(selectedLine)"]'));
		};

		this.getDuplicateText = function () {
			return element(by.cssCustomSelector("myCartCtrl.duplicateLine(selectedLine)", "a", "ng-click")).element(by.cssCustomSelector("{'text-gray-dark disableLink' : myCartCtrl.cartCommonOperationsService.checkDuplicateDisable(selectedLine)}", "span", "ng-class"));
		};

		this.getRemoveIcon = function (lineItem) {
			//return element(by.cssCustomSelector("myCartCtrl.cartServicesService.removeLine(selectedLine)", "a", "ng-click")).element(by.tagName("i"));
			return lineItem.element(by.css('a[ng-click="myCartCtrl.cartServicesService.removeLine(selectedLine)"]'));
		};

		this.getRemoveText = function () {
			return element(by.id('cart_remove_line_0'));
		};

		this.getLineNumber = function () {
			return element.all(by.css('span[class="product-number"]'));
		};

		this.getFooterLegalText = function () {
			return element(by.cssCustomSelector("myCartCtrl.keyValues.footerText.videoLegalText | parseHtml", "p", "ng-bind-html"));
		};

		this.getTermsOfUseLink = function () {
			return element(by.cssCustomSelector("vm.footerKeyValues.termsOfUseText!==undefined", "div", "ng-if")).element(by.tagName("a"));
		};

		this.getTermsandConditionsLink = function () {
			return element(by.cssCustomSelector("vm.footerKeyValues.termsAndConditionsText!==undefined", "div", "ng-if")).element(by.tagName("a"));
		};

		this.getReturnPolicyLink = function () {
			return element(by.cssCustomSelector("vm.footerKeyValues.returnPolicyText!==undefined", "div", "ng-if")).element(by.tagName("a"));
		};

		this.getPrivacyPolicyLink = function () {
			return element(by.cssCustomSelector("text-left footer-link inline-flex pull-left p-xs-b-35 p-lg-b-0 col-lg-5 col-md-5", "div", "class")).element(by.tagName("a"));
		};

		this.getCopyrightText = function () {
			return element(by.cssCustomSelector("myCartCtrl.authorValue.page", "common-footer", "key-value")).element(by.tagName("span"));
		};

		this.getFooterDivision = function () {
			return element(by.cssCustomSelector("checkout padding-vertical-large p-xs-b-15 p-lg-b-20 footer-lift", "div", "class"));
		};

		this.getTermsOfUseDivision = function () {
			return element(by.xpath('html/body/div[3]/div/div[1]/div[3]/div/div/div[1]/div[1]/div[1]'));
		};

		this.getTermsandConditionsDivision = function () {
			return element(by.xpath('html/body/div[3]/div/div[1]/div[3]/div/div/div[1]/div[1]/div[2]'));
		};

		this.getReturnPolicyDivision = function () {
			return element(by.xpath('html/body/div[3]/div/div[1]/div[3]/div/div/div[1]/div[1]/div[3]'));
		};

		this.getPrivacyPolicyDivision = function () {
			return element(by.cssCustomSelector("test-footer", "div", "class")).element(by.cssCustomSelector("padding-horizontal-small border", "div", "class"));
		};

		this.getCopyrightDivision = function () {
			return element(by.xpath('html/body/div[3]/div/div[1]/div[3]/div/div/div[2]'));
		};

		this.getOpenCreditModalLinkInCart = function () {
			return element(by.cssCustomSelector("myCartCtrl.openCreditClassSelectorModal()", "a", "ng-click"));
		};

		//MAG-3346 & 5958 PageObject
		this.getDefaultPlanName = function () {
			return element(by.cssCustomSelector("!selectedLine.mvcAttrs.editPlanInProgress", "div", "ng-show")).element(by.tagName('h4'));
		};

		this.getSmartFitLink = function () {
			return element(by.cssCustomSelector("myCartCtrl.openLearnMoreModal(selectedLine)", "a", "ng-click"));
		};

		this.closeSmartfitModal = function () {
			return element(by.cssCustomSelector("learnMoreModalCtrl.cancel()", "button", "ng-click"));
		};

		// MAG-6526 Page Objects //

		this.clickDeviceTile = function () {
			return element(by.css('[ng-click="myCartCtrl.editProduct(selectedLine)"]'));
		};

		this.getAddNewPhoneButton = function () {
			return element(by.cssCustomSelector("myCartCtrl.creditCheck('Select Phone',selectedLine,$index)", "button", "ng-click"));
		};

		this.getBYODButton = function () {
			return element(by.cssCustomSelector("myCartCtrl.creditCheck('Select Phone',selectedLine,$index,'BYOD')", "button", "ng-click"));
		};

		this.getDeviceCancelLink = function () {
			return element(by.cssCustomSelector("myCartCtrl.editProductClose(selectedLine);$event.stopPropagation();", "a", "ng-click"));
		};

		this.clickPlansTile = function () {
			return element(by.css('[ng-click="myCartCtrl.editPlanOpen(selectedLine)"]'));
		};

		this.getPlansHeader = function () {
			return element(by.cssCustomSelector("myCartCtrl.keyValues.cartRowItems.plansTitle | parseHtml", "span", "ng-bind-html"));
		};

		this.getAddPlansButton = function () {
			return element(by.cssCustomSelector("myCartCtrl.addPlan(selectedLine)", "button", "ng-click"));
		};

		this.PlansCancelButton = function () {
			return element(by.cssCustomSelector("myCartCtrl.cancelAddPlan(selectedLine)", "button", "ng-click"));
		};

		this.PlansWhatsSimpleChoice = function () {
			return element(by.cssCustomSelector("myCartCtrl.openLearnMoreModal(selectedLine)", "a", "ng-click"));
		};

		this.PlansOurMostPopular = function () {
			return element(by.cssCustomSelector("myCartCtrl.openPlanModal(selectedLine)", "a", "ng-click"));
		};

		this.closePlanSelector = function () {
			return element(by.cssCustomSelector("vm.cancel()", "button", "ng-click"));
		};

		// MAG-1111 & 1112 Page Objects //

		this.ClickCartAddNewPhoneButton = function () {
			return element(by.cssCustomSelector("vm.cartObj.addLine()", "button", "ng-click"));
		};

		this.getAddPromoCodeLabel = function () {
			return element(by.cssCustomSelector("vm.showPromoInput=true", "a", "ng-click"));
		};

		this.EnterPromoCodeLabel = function () {
			return element(by.cssCustomSelector("vm.keyValues.promoCodeInput | parseHtml", "p", "ng-bind-html"));
		};

		this.PromoCodeField = function () {
			return element(by.cssCustomSelector("vm.promoCode", "input", "ng-model"));
		};

		this.getPromoApplyButton = function () {
			return element(by.cssCustomSelector("vm.applyPromocode(promoForm.promoInput)", "button", "ng-click"));
		};

		this.getErrorMessge = function () {
			return element(by.cssCustomSelector("promoForm.promoInput.$error.promoInvalid", "p", "ng-show"));
		};

		this.getGalaxyS7Device = function () {
			return element(by.cssCustomSelector("/content/dam/t-mobile/en-p/cell-phones/samsung-galaxy-s7/gold/carousel-samsung-galaxy-s7-gold-platinum-1.jpg", "img", "ng-src"));
		};

		this.SelectMemoryColor = function () {
			return element(by.cssCustomSelector("/content/dam/t-mobile/en-p/cell-phones/samsung-galaxy-s7/black/Samsung-Galaxy-S7-Black-Onyx.gif", "img", "ng-src"));
		};

		this.getPromoCodeDescritption = function () {
			return element(by.cssCustomSelector("xsmall p-lg-l-5 m-lg-l-20 p-xs-l-0 m-xs-l-0 p-xs-b-0 p-lg-b-10 ng-binding", "p", "class"));
		};

		this.getAwesomecredit = function () {
			return element(by.cssCustomSelector("indicator", "div", "class"));
		};
		// MAG-1111 & 1112 End  of page Objects//

		//******************Price Breakdown validations start****************//

		this.getPriceBreakDownLink = function () {
			//return element(by.cssCustomSelector("myCartCtrl.openPriceBreakdownModal()", "a", "ng-click"));
			return element(by.css('a[ng-click="myCartCtrl.openPriceBreakdownModal()"]'));
		};

		this.getPriceBreakDownModalHeader = function () {
			return element(by.id('dialogTitle'));
		};

		this.getPriceBreakDownModalCloseIcon = function () {
			//return element(by.cssCustomSelector("priceBreakdownCtrl.close()", "button", "ng-click"));
			return element(by.css('button[ng-click="priceBreakdownCtrl.close()"]'));
		};

		this.getOrderGroupRepeaterInPriceBreakdownModal = function () {
			return element(by.cssCustomSelector("(key,groupItem) in vm.cartObj.lineItems | groupBy : 'orderGroup' ", "span", "ng-repeat"));
		};

		this.getOrderGroupRepeaterHeaderInPriceBreakdownModal = function () {
			return element(by.cssCustomSelector("(key,groupItem) in vm.cartObj.lineItems | groupBy : 'orderGroup' ", "span", "ng-repeat"));
		};

		this.getPriceBreakDownDeviceName = function () {
			return element(by.id('deviceFamilyName'));
		};

		this.getPriceBreakDownTradeinValue = function () {
			return element(by.css('li[ng-if="item.tradeInDetails && (item.isTradeInStandardFlow || vm.creditClass3 || item.poFull)"]')).element(by.tagName('p'));
		};

		this.getPriceBreakDownPromoTradeinValue = function () {
			return element(by.css('span[ng-class="{\'text-strike-through\': getStrikeThrough()}"]'));
		};

		this.getPriceBreakDownTradeinHeader = function () {
			return element(by.css('p[ng-bind-html="vm.breakdownAuthorValue.standardTradeInDeviceHeader | parseHtml"]'));
		};

		this.getPriceBreakdownTradeinLegaltext = function () {
			return element(by.css('p[ng-bind-html="vm.breakdownAuthorValue.standardTradeInDeviceInfo | parseHtml"]'));
		};

		this.getPriceBreakDownTradeinDeviceModalName = function () {
			return element(by.css('p[class="p-t-10 p-b-10 small text-gray-dark trade-details-width ng-binding"]'));
		};

		this.getPriceBreakDownPromoTradeinDeviceModalName = function () {
			return element(by.css('class="p-t-10 small text-gray-dark trade-details-width ng-binding"'));
		};

		this.getPricebreakDownTradeinValueLink = function () {
			return element(by.id('tradeInValueModalLink')).element(by.tagName('a'));
		};

		//******************Price Breakdown validations End****************//
		//************ Family Match Banner PageObjects **************//

		this.getFamilyPromtionType = function () {
			return element(by.cssCustomSelector("vm.authorValue.familybanner.promotionType | parseHtml", "p", "ng-bind-html"));
		};

		this.getFamilyPromtionName = function () {
			return element(by.cssCustomSelector("vm.authorValue.familybanner.promotionName | parseHtml", "span", "ng-bind-html"));
		};

		this.getFamilyPromotionDetail = function () {
			return element(by.cssCustomSelector("vm.authorValue.familybanner.promotionDetails", "span", "ng-bind-html"));
		};

		this.getFamilyPromtionIcon = function () {
			return element(by.cssCustomSelector("vm.authorValue.familybanner.promotionIndicatorIcon", "p", "ng-class"));
		};

		this.getFamilyPromtionLegalText = function () {
			return element(by.cssCustomSelector("vm.authorValue.familybanner.promotionLegal", "p", "ng-bind-html"));
		};

		//
		//Pageobjects added for authorability related elements//

		this.getBasedOnLabel = function () {
			return element(by.cssCustomSelector("myCartCtrl.keyValues.stickyBanner.basedOn | parseHtml", "span", "ng-bind-html"));
		};

		this.getCreditSelectorLegalText = function () {
			return element(by.cssCustomSelector("creditSelectorModalCtrl.keyValues.creditSelector.finalLegalText | parseHtml", "p", "ng-bind-html"));
		};

		this.getBrowseAllCTA = function () {
			return element(by.cssCustomSelector("deviceSelectorModalCtrl.browseAllCtaMethod($event)", "a", "ng-click"));
		};

		this.getMiniBrowseLegalText = function () {
			return element(by.cssCustomSelector("device in deviceSelectorModalCtrl.mostPopularDevices  track by $index | limitTo:8", "div", "ng-repeat")).element(by.cssCustomSelector("(($index + 1) % 4 === 0)||$last", "p", "ng-show"));
		};

		this.getMiniBrowseCloseButton = function () {
			return element(by.cssCustomSelector("deviceSelectorModalCtrl.close()", "button", "ng-click"));
		};

		this.getCallUsImage = function () {
			return element(by.cssCustomSelector("pdlevent", "div", "pdl-click-call-data")).element(by.tagName("img"));
		};

		this.getMiniPDPMemoryLabel = function () {
			return element(by.cssCustomSelector("miniPDPModalCtrl.authorValue.rootPage.deviceAuthoring.memoryTitle | parseHtml", "label", "ng-bind-html"));
		};

		this.getMiniPDPColorLabel = function () {
			return element(by.cssCustomSelector("miniPDPModalCtrl.authorValue.rootPage.deviceAuthoring.colorTitle | parseHtml", "span", "ng-bind-html"));
		};

		this.getMiniPDPPaymentOptionLabel = function () {
			return element(by.cssCustomSelector("miniPDPModalCtrl.authorValue.rootPage.deviceAuthoring.payment_title_label | parseHtml", "label", "ng-bind-html"));
		};

		this.getMiniPDPCreditDisclaimer = function () {
			return element(by.cssCustomSelector("miniPDPModalCtrl.authorValue.rootPage.generalAuthoring.credit_class_description_text | parseHtml", "span", "ng-bind-html"));
		};

		this.getMiniPDPSIMStarterKit = function () {
			return element(by.cssCustomSelector("sim toolkit modal", "a", "dialog-description")).element(by.tagName("span"));
		};

		this.getMiniPDPFinancingHeader = function () {
			return element(by.cssCustomSelector("miniPDPModalCtrl.keyValues.miniPdp.financingPricingHeader | parseHtml", "p", "ng-bind-html"));
		};

		this.getMiniPDPFullPriceHeader = function () {
			return element(by.cssCustomSelector("miniPDPModalCtrl.authorValue.rootPage.generalAuthoring.total_price_title | parseHtml", "p", "ng-bind-html"));
		};

		this.getMiniPDPFullPriceTodayLabel = function () {
			return element(by.cssCustomSelector("miniPDPModalCtrl.paymentOption==miniPDPModalCtrl.getPaymentOption('full').id || miniPDPModalCtrl.selectedCreditClass.class=='creditClass3'", "div", "ng-show")).element(by.cssCustomSelector("price-lockup-large", "div", "size-class")).element(by.cssCustomSelector("leftText", "span", "ng-bind-html"));
		};

		this.getMiniPDPFullPrice = function () {
			return element(by.cssCustomSelector("miniPDPModalCtrl.paymentOption==miniPDPModalCtrl.getPaymentOption('full').id || miniPDPModalCtrl.selectedCreditClass.class=='creditClass3'", "div", "ng-show")).element(by.cssCustomSelector("price-lockup-large", "div", "size-class")).element(by.id("money_1468"));
		};

		this.getMiniPDPFinancialLegalText = function () {
			return element(by.cssCustomSelector("miniPDPModalCtrl.authorValue.rootPage.generalAuthoring.globalpdplegaltext | parseHtml", "p", "ng-bind-html"));
		};

		this.getMiniPDPPriceBreakDownLabel = function () {
			return element(by.cssCustomSelector("miniPDPModalCtrl.authorValue.rootPage.generalAuthoring.price_breakdown_title | parseHtml", "p", "ng-bind-html"));
		};

		this.getMiniPDPTodayPriceLabel = function () {
			return element(by.cssCustomSelector("miniPDPModalCtrl.paymentOption==miniPDPModalCtrl.getPaymentOption('monthly').id", "div", "ng-show")).element(by.cssCustomSelector("price-lockup-large", "div", "size-class")).element(by.cssCustomSelector("leftText", "span", "ng-bind-html"));
		};

		this.getMiniPDPMonthlyPriceLabel = function () {
			return element(by.cssCustomSelector("miniPDPModalCtrl.paymentOption==miniPDPModalCtrl.getPaymentOption('monthly').id", "div", "ng-show")).element(by.cssCustomSelector("price-lockup-large", "div", "size-class")).element(by.cssCustomSelector("rightText", "span", "ng-bind-html"));
		};

		this.getMiniPDPTotalPriceLabel = function () {
			return element(by.cssCustomSelector("miniPDPModalCtrl.paymentOption==miniPDPModalCtrl.getPaymentOption('monthly').id", "div", "ng-show")).element(by.cssCustomSelector("price-lockup-large", "div", "size-class")).element(by.cssCustomSelector("getShowTotalPricing()", "div", "ng-show")).element(by.tagName("span"));
		};

		this.getMiniPDPTodayPrice = function () {
			return element(by.cssCustomSelector("miniPDPModalCtrl.paymentOption==miniPDPModalCtrl.getPaymentOption('monthly').id", "div", "ng-show")).element(by.cssCustomSelector("price-lockup-large", "div", "size-class")).element(by.id("money_1481"));
		};

		this.getMiniPDPMonthlyPrice = function () {
			return element(by.cssCustomSelector("miniPDPModalCtrl.paymentOption==miniPDPModalCtrl.getPaymentOption('monthly').id", "div", "ng-show")).element(by.cssCustomSelector("price-lockup-large", "div", "size-class")).element(by.id("money_1484"));
		};

		this.getMiniPDPTotalPrice = function () {
			return element(by.cssCustomSelector("miniPDPModalCtrl.paymentOption==miniPDPModalCtrl.getPaymentOption('monthly').id", "div", "ng-show")).element(by.cssCustomSelector("price-lockup-large", "div", "size-class")).element(by.cssCustomSelector("getShowTotalPricing()", "div", "ng-show")).element(by.id("money_1490"));
		};

		this.getMiniPDPHelpTextLabel = function () {
			return element(by.cssCustomSelector("miniPDPModalCtrl.authorValue.rootPage.generalAuthoring.helpText", "label", "ng-bind-html"));
		};

		this.getMiniPDPHelpTextLink = function () {
			return element(by.cssCustomSelector("miniPDPModalCtrl.authorValue.rootPage.generalAuthoring.helpTextAndNumber", "a", "ng-if"));
		};

		this.getMiniPDPHelpTextNumber = function () {
			return element(by.cssCustomSelector("miniPDPModalCtrl.authorValue.rootPage.generalAuthoring.helpTextAndNumber", "a", "ng-if")).element(by.tagName("span"));
		};

		this.getMiniPDPAlreadyCustomerLabel = function () {
			return element(by.cssCustomSelector("p-t-20 m-t-10 small", "class", "div")).element(by.tagName("span"));
		};

		this.getMiniPDPLoginLink = function () {
			return element(by.cssCustomSelector("p-t-20 m-t-10 small", "class", "div")).element(by.tagName("a"));
		};

		this.getEditDeviceCancelLink = function () {
			return element(by.cssCustomSelector("myCartCtrl.keyValues.defaultRowContent.cancelText | parseHtml", "a", "ng-bind-html"));
		};

		//Refundable deposit page objects

		this.getRefundableDepositHeader = function () {
			return element(by.cssCustomSelector("myCartCtrl.keyValues.cartRowItems.refundableDepositHeader | parseHtml", "p", "ng-bind-html"));
		};

		this.getRefundableDepositText = function () {
			return element(by.cssCustomSelector("myCartCtrl.keyValues.cartRowItems.refundableDepositLegalText | parseHtml", "p", "ng-bind-html"));
		};

		this.getRefundableDepositPriceDollars = function () {
			return element(by.cssCustomSelector("myCartCtrl.structCartData.selectedCreditClass.class=='creditClass3' && myCartCtrl.checkRefundableDepositAmount(myCartCtrl.structCartData.refundableDesposit)", "div", "ng-if")).element(by.cssCustomSelector("{'text-strike-through':showAsDiscount === 'true'}", "span", "ng-class"));
		};

		this.getRefundableDepositPriceCents = function () {
			return element(by.cssCustomSelector("myCartCtrl.structCartData.selectedCreditClass.class=='creditClass3' && myCartCtrl.checkRefundableDepositAmount(myCartCtrl.structCartData.refundableDesposit)", "div", "ng-if")).element(by.cssCustomSelector("{'text-strike-through':showAsDiscount, 'no-spin': spin === 'false'}", "span", "ng-class"));
		};

		this.getPlanDetails = function () {
			return element(by.cssCustomSelector("myCartCtrl.keyValues.cartRowItems.planDetailsDevice | parseHtml", "p", "ng-bind-html"));
		};

		this.getLearnMoreModalHeader = function () {
			return element(by.cssCustomSelector("learnMoreModalCtrl.keyValues.learnModal.learnMoreTitle | parseHtml", "h3", "ng-bind-html"));
		};

		this.getLearnMoreModalText = function () {
			return element(by.cssCustomSelector("learnMoreModalCtrl.keyValues.learnModal.learnMoreText", "p", "ng-bind-html"));
		};

		this.getLearnMoreModalSection1Image = function () {
			return element(by.cssCustomSelector("{'main-content col-lg-6 col-md-6 col-sm-6 col-xs-12': !learnMoreModalCtrl.selectedLine.lineItemDetails.deviceDetails.isWearable, 'main-content col-lg-12 col-md-12 col-sm-12 col-xs-12': learnMoreModalCtrl.selectedLine.lineItemDetails.deviceDetails.isWearable}", "div", "ng-class")).element(by.tagName("img"));
		};

		this.getLearnMoreModalSection1Title = function () {
			return element(by.cssCustomSelector("learnMoreModalCtrl.keyValues.learnModal.section1Title | parseHtml", "h5", "ng-bind-html"));
		};

		this.getLearnMoreModalSection1Detail = function () {
			return element(by.cssCustomSelector("learnMoreModalCtrl.keyValues.learnModal.section1Desc | parseHtml", "p", "ng-bind-html"));
		};

		this.getLearnMoreModalSection2Image = function () {
			return element(by.cssCustomSelector("!learnMoreModalCtrl.selectedLine.lineItemDetails.deviceDetails.isWearable", "div", "ng-if")).element(by.tagName("img"));
		};

		this.getLearnMoreModalSection2Title = function () {
			return element(by.cssCustomSelector("learnMoreModalCtrl.keyValues.learnModal.section2Title | parseHtml", "h5", "ng-bind-html"));
		};

		this.getLearnMoreModalSection2Detail = function () {
			return element(by.cssCustomSelector("learnMoreModalCtrl.keyValues.learnModal.section2Desc | parseHtml", "p", "ng-bind-html"));
		};

		this.getLearnMoreModalSection3Image = function () {
			return element(by.xpath("html/body/div[6]/div/div/div/div/div/div[2]/div/div[3]/img"));
		};

		this.getLearnMoreModalSection3Title = function () {
			return element(by.cssCustomSelector("learnMoreModalCtrl.keyValues.learnMoreModal.section3Title | parseHtml", "h5", "ng-bind-html"));
		};

		this.getLearnMoreModalSection3Detail = function () {
			return element(by.cssCustomSelector("learnMoreModalCtrl.keyValues.learnMoreModal.section3Text | parseHtml", "p", "ng-bind-html"));
		};

		this.getLearnMoreModalLegalText = function () {
			return element(by.cssCustomSelector("learnMoreModalCtrl.keyValues.learnModal.learnLegalText | parseHtml ", "div", "ng-bind-html"))
		};

		this.getDeviceLegalText = function () {
			return element(by.cssCustomSelector("myCartCtrl.keyValues.cartRowItems.deviceLegalText | parseHtml", "span", "ng-bind-html")).element(by.tagName('p'));
		};

		this.getAverageCreditNote = function () {
			return element(by.cssCustomSelector("creditSelectorModalCtrl.keyValues.creditSelector.averageNote | parseHtml", "p", "ng-bind-html"));
		};

		this.getAverageCreditRecommendedFlag = function () {
			return element(by.cssCustomSelector("creditSelectorModalCtrl.keyValues.creditSelector.averageRecommendedFlag | parseHtml", "span", "ng-bind-html"));
		};

		this.getAverageCreditLegalText = function () {
			return element(by.cssCustomSelector("creditSelectorModalCtrl.keyValues.creditSelector.averageLegal | parseHtml", "p", "ng-bind-html"));
		};

		this.getNoCreditCheckNote = function () {
			return element(by.cssCustomSelector("creditSelectorModalCtrl.keyValues.creditSelector.noCreditCheckCopy | parseHtml", "p", "ng-bind-html"));
		};

		this.getZipCodeModalErrorText = function () {
			return element(by.cssCustomSelector("setLocationFPOModalCtrl.keyValues.zipCodeModal.errorText | parseHtml", "p", "ng-bind-html"));
		};

		this.getZipCodeModalErrorIcon = function () {
			return element(by.cssCustomSelector("{'error':setLocationFPOModalCtrl.invalidZipCode && setLocationFPOModalCtrl.zipCode}", "div", "ng-class")).element(by.tagName('i'));
		};

		this.getCartSubTitle = function () {
			return element(by.cssCustomSelector("myCartCtrl.keyValues.cartHeader.bannerText | parseHtml", "p", "ng-bind-html"));
		};

		this.getRowItemLegalText = function () {
			return element(by.cssCustomSelector("myCartCtrl.keyValues.cartRowItems.rowLegalText | parseHtml", "p", "ng-bind-html")).element(by.tagName("p"));
		};

		this.getProtectionSectionHeader = function () {
			return element(by.cssCustomSelector("myCartCtrl.keyValues.cartRowItems.servicesTitle | parseHtml", "span", "ng-bind-html"));
		};

		this.getPlanSectionHeader = function () {
			return element(by.css(".recommended-text-wrapper")).element(by.cssCustomSelector("myCartCtrl.keyValues.cartRowItems.plansTitle | parseHtml", "span", "ng-bind-html"));
		};

		this.getMiniPDPBackButton = function () {
			return element(by.cssCustomSelector("miniPDPModalCtrl.back()", "a", "ng-click"));
		};

		this.getMiniPDPCloseIcon = function () {
			return element(by.cssCustomSelector("miniPDPModalCtrl.close()", "button", "ng-click"));
		};

		this.getProtectionSectionEditButton = function () {
			return element(by.cssCustomSelector("selectedLine.lineItemDetails.serviceDetails[0].familyName", "a", "ng-if")).element(by.cssCustomSelector("myCartCtrl.keyValues.cartRowItems.editIconText | parseHtml", "span", "ng-bind-html"));
		};

		this.getEditBYODDevice = function () {
			return element(by.cssCustomSelector("myCartCtrl.openOwnPhoneModal(selectedLine)", "button", "ng-click"));
		};

		this.getAwesomeCredit = function () {
			return element.all(by.repeater('creditClass in creditSelectorModalCtrl.keyValues.class')).get(0);
		};

		this.getAddedDeviceMemory = function () {
			return element(by.cssCustomSelector("selectedLine.lineItemDetails.deviceDetails.memory.length>0", "span", "ng-if"));
		};

		this.getAddedDeviceColor = function () {
			return element(by.cssCustomSelector("selectedLine.lineItemDetails.deviceDetails.colorSwatch", "span", "ng-if"));
		};

		this.closeAccessoryErrorModal = function () {
			return element(by.cssCustomSelector("vm.close()", "button", "ng-click"));
		};

		this.getAddAnAccessoryButtonMainCart = function () {
			return element(by.cssCustomSelector("vm.cartObj.showAccessories()", "button", "ng-click"))
		};

		this.getAddATabletButtonMainCart = function () {
			return element(by.cssCustomSelector("vm.cartObj.addLineByLineType('TABLET')", "button", "ng-click"))
		};

		this.getAddNewLineForPhoneButton = function () {
			return element(by.cssCustomSelector("vm.cartObj.addLine()", "button", "ng-click"));
		};

		this.getAllLineItemsInCart = function () {
			return element.all(by.repeater("selectedLine in myCartCtrl.cartCommonOperationsService.model.structCartData.lineItems | orderBy: '1*lineNo'"));
		};

		this.getCartLineItemNumber = function (RepeaterElement) {
			return RepeaterElement.element(by.cssCustomSelector("product-number", "span", "class"));
		};

		this.getAddDeviceIconInRepeater = function (RepeaterElement) {
			return RepeaterElement.element(by.cssCustomSelector("myCartCtrl.editProduct(selectedLine)", "div", "ng-click"));
		};

		this.getAddedDeviceNameInRepeater = function (RepeaterElement) {
			return RepeaterElement.element(by.cssCustomSelector("!selectedLine.mvcAttrs.editProductInProgress", "div", "ng-show")).element(by.tagName("h5"));
		};

		this.getAddedDeviceColorInRepeater = function (RepeaterElement) {
			return RepeaterElement.element(by.cssCustomSelector("!selectedLine.mvcAttrs.editProductInProgress", "div", "ng-show")).element(by.cssCustomSelector("selectedLine.lineItemDetails.deviceDetails.colorSwatch", "span", "ng-if"));
		};

		this.getAddedDeviceMemoryInRepeater = function (RepeaterElement) {
			return RepeaterElement.element(by.cssCustomSelector("!selectedLine.mvcAttrs.editProductInProgress", "div", "ng-show")).element(by.cssCustomSelector("selectedLine.lineItemDetails.deviceDetails.memory.length>0", "span", "ng-if"));
		};

		/* New Pageobjects for Sprint 19 changes*/

		this.getAddProtectionButton = function () {
			return element(by.cssCustomSelector("phpModalCtrl.authorValue.cart.phpModal.addProtection", "button", "ng-bind-html"));
		};

		this.getProceedToCheckoutButton = function () {
			return element(by.cssCustomSelector("phpModalCtrl.keyValues.cart.phpModal.discardCta", "button", "ng-bind-html"));
		};

		this.getBYODCheckoutButton = function () {
			return element(by.cssCustomSelector("!myCartCtrl.structCartData.flags.isPHPFlow", "div", "ng-show")).element(by.cssCustomSelector("myCartCtrl.keyValues.stickyBanner.checkoutButtonText | parseHtml", "div", "ng-bind-html"));
		};

		this.CheckoutButton = function () {
			return element(by.cssCustomSelector("myCartCtrl.keyValues.stickyBanner.checkoutButtonText | parseHtml", "div", "ng-bind-html"));
		};

		this.getVoiceRequiredMessageBelowCheckoutButton = function () {
			return element(by.cssCustomSelector("myCartCtrl.keyValues.stickyBanner.checkoutDisableMsg", "p", "ng-bind-html"));
		};

		this.getMaxLineReachedText = function () {
			return element(by.cssCustomSelector("myCartCtrl.cartCommonOperationsService.model.deviceLimitReached ? myCartCtrl.keyValues.defaultRowContent.deviceLimitReached : (myCartCtrl.cartCommonOperationsService.model.lineMaxHandsetLimitReached ? myCartCtrl.keyValues.defaultRowContent.handsetLimitReached : (myCartCtrl.cartCommonOperationsService.model.lineMaxTabletLimitReached ? myCartCtrl.keyValues.defaultRowContent.tabletLimitReached : '') )| parseHtml", "div", "ng-bind-html"));
		};

		this.getPhpModalCloseButton = function () {
			return element(by.css('button[ng-click="phpModalCtrl.close()"]'));
		};

		/* End of PHP Modal Pageobjects */

		this.getUpgradeDataCheckBox = function () {
			return element(by.cssCustomSelector("myCartCtrl.cartPlansService.toggleSelectedPlan(selectedLine)", "input", "ng-click"));
		};

		this.getUpgradeDataCheckBoxText = function () {
			return element(by.cssCustomSelector("selectedLine.lineType=='HANDSET' ? myCartCtrl.keyValues.cartRowItems.dataUpgrade : myCartCtrl.keyValues.cartRowItems.dataUpgradeTablet | parseHtml", "p", "ng-bind-html"));
		};

		this.getMainCartLineNumber = function () {
			return element(by.cssCustomSelector("product-number", "span", "class"));
		};

		this.getMainCartRowTodayPriceLabel = function () {
			return element(by.cssCustomSelector("selectedLine in myCartCtrl.cartCommonOperationsService.model.structCartData.lineItems | orderBy: '1*lineNo'", "div", "ng-repeat")).element(by.cssCustomSelector("price-lockup-xsmall", "div", "size-class")).element(by.cssCustomSelector("leftText", "span", "ng-bind-html"));
		};

		this.getMainCartRowMonthlyPriceLabel = function () {
			return element(by.cssCustomSelector("selectedLine in myCartCtrl.cartCommonOperationsService.model.structCartData.lineItems | orderBy: '1*lineNo'", "div", "ng-repeat")).element(by.cssCustomSelector("price-lockup-xsmall", "div", "size-class")).element(by.cssCustomSelector("rightText", "span", "ng-bind-html"));
		};

		this.getCartAutopayText = function () {
			return element(by.cssCustomSelector("selectedLine in myCartCtrl.cartCommonOperationsService.model.structCartData.lineItems | orderBy: '1*lineNo'", "div", "ng-repeat")).element(by.cssCustomSelector("price-lockup-xsmall", "div", "size-class")).element(by.cssCustomSelector("text-black xsmall ng-binding", "p", "class"));
		};

		this.getAccessoryRowTodayPrice = function () {
			return element(by.cssCustomSelector("vm.model.isFRPPayment", "div", "ng-if")).element(by.id("money_143"));
		};

		this.getAccessoryRowTodayPriceLabel = function () {
			return element(by.cssCustomSelector("vm.model.isFRPPayment", "div", "ng-if")).element(by.cssCustomSelector("leftText", "span", "ng-bind-html"));
		};

		this.getFooterVoiceLegalText = function () {
			return element(by.cssCustomSelector("myCartCtrl.keyValues.footerText.voiceLegalText | parseHtml", "p", "ng-bind-html"));
		};

		this.getFooterFamilyPlanLegalText = function () {
			return element(by.cssCustomSelector("myCartCtrl.keyValues.footerText.familyPlanLegalText | parseHtml", "p", "ng-bind-html"));
		};

		this.getFooterDataStashLegalText = function () {
			return element(by.cssCustomSelector("myCartCtrl.keyValues.footerText.dataStashLegalText | parseHtml", "p", "ng-bind-html"));
		};

		this.getFooterMonthlyLegalText = function () {
			return element(by.cssCustomSelector("myCartCtrl.keyValues.footerText.monthlyLegalText | parseHtml", "p", "ng-bind-html"));
		};

		this.getSpanishLink = function () {
			return element(by.cssCustomSelector("vm.toggleLanguage('es')", "a", "ng-click"));
		};

		this.getStickyBannerPriceWrapper = function () {
			return element(by.id("cart_stickybanner_id")).element(by.cssCustomSelector("price-lockup-medium", "div", "size-class"));
		};

		this.getStickyBannerTodayPriceLabel = function () {
			return element(by.id("cart_stickybanner_id")).element(by.cssCustomSelector("price-lockup-medium", "div", "size-class")).element(by.cssCustomSelector("leftText", "span", "ng-bind-html"));
		};

		this.getStickyBannerTodayPrice = function () {
			return element(by.id("cart_stickybanner_id")).element(by.cssCustomSelector("price-lockup-medium", "div", "size-class")).element(by.cssCustomSelector("left-amount", "span", "ng-bind-html"));
		};

		this.getStickyBannerMonthlyPriceLabel = function () {
			return element(by.id("cart_stickybanner_id")).element(by.cssCustomSelector("price-lockup-medium", "div", "size-class")).element(by.cssCustomSelector("rightText", "span", "ng-bind-html"));
		};

		this.getStickyBannerMonthlyPrice = function () {
			return element(by.id("cart_stickybanner_id")).element(by.cssCustomSelector("price-lockup-medium", "div", "size-class")).element(by.cssCustomSelector("right-amount", "span", "ng-bind-html"));
		};

		this.getStickyBannerAutopayLabel = function () {
			return element(by.id("cart_stickybanner_id")).element(by.cssCustomSelector("price-lockup-medium", "div", "size-class")).element(by.cssCustomSelector("rightSubText", "span", "ng-bind-html"));
		};

		this.getTMobileLogo = function () {
			return element(by.cssCustomSelector("myCartCtrl.openExitPathConfirmationModal()", "img", "ng-click"));
		};

		this.getAccessoryNotificationCloseIcon = function () {
			return element(by.cssCustomSelector("close(message.messageKey)", "div", "ng-click")).element(by.tagName("a"));
		};

		/*this.getEmptyCartHeader = function () {
			return element(by.cssCustomSelector("myCartCtrl.keyValues.emptyCart.emptyCartHeader", "h1", "ng-bind-html"));
		};*/

		this.getEmptyCartSubHeader = function () {
			return element(by.cssCustomSelector("myCartCtrl.keyValues.emptyCart.emptyCartSubHeader", "h4", "ng-bind-html"));
		};

		this.getEmptyCartImage = function () {
			return element(by.cssCustomSelector("myCartCtrl.keyValues.emptyCart.emptyCartImage", "img", "ng-if"));
		};

		this.getCloseIconForExitPathConfirmationModal = function () {
			return element(by.cssCustomSelector("exitPathConfirmation.cancel()", "button", "ng-click"));
		};

		this.getStayHereButtonInExitPathConfirmationModal = function () {
			return element(by.cssCustomSelector("exitPathConfirmation.cancel()", "button", "ng-click"));
		};

		this.getGoToHomePageButtonInExitPathConfirmationModal = function () {
			return element(by.cssCustomSelector("exitPathConfirmation.keyValues.exitPage.homePageCTA | parseHtml", "button", "ng-bind-html"));
		};

		this.getUpgradeDataCheckBoxText = function () {
			return element(by.cssCustomSelector("selectedLine.lineType=='HANDSET' ? myCartCtrl.keyValues.cartRowItems.dataUpgrade : myCartCtrl.keyValues.cartRowItems.dataUpgradeTablet | parseHtml", "p", "ng-bind-html"));
		};

		this.getUserFilters = function () {
			return element(by.css('a[href="#tab-userfilters"]'));
		};

		this.getNewFilter = function () {
			return element(by.css('input[id="newFilter"]'));
		};

		this.getClickAddFilter = function () {
			return element(by.css('span[class="i18n_add_filter_button"]'));
		}

		this.CheckoutButton = function () {
			return element(by.css('button[ng-bind-html="myCartCtrl.keyValues.stickyBanner.checkoutButtonText | parseHtml"]'));
			//return element(by.css("myCartCtrl.keyValues.stickyBanner.checkoutButtonText | parseHtml","div","ng-bind-html"));
		};

		/************************Trade-in Device Page Objects**************************/
		this.getTradeinCTAElement = function () {
			return element(by.css('[ng-click="vm.tradeInOpenPopUp(vm.selectedLine);"]'));
		};

		this.getTQTModalHeader = function () {
			return element(by.css('[ng-bind-html="tradeInCtrl.authorValue.tradeInTool.header | parseHtml"]'));
		};

		this.getCarrierTitle = function () {
			return element(by.id('carrierOptions'));
		};

		this.getMakeTitle = function () {
			return element(by.id('makeOptions'));
		};

		this.getModelTitle = function () {
			return element(by.id('deviceOptions'));
		};

		this.getImeiTitle = function () {
			return element(by.css('[ng-bind-html="tradeInCtrl.authorValue.tradeInTool.imeiTitle | parseHtml"]'));
		};

		this.getDeviceConditionTitle = function () {
			return element(by.css('[ng-bind-html="tradeInCtrl.authorValue.tradeInTool.deviceConditionLabel | parseHtml"]'));
		};

		this.getDeviceConditionGoodTitle = function () {
			return element(by.css('[ng-bind-html="tradeInCtrl.authorValue.tradeInTool.deviceConditionGoodLabel | parseHtml"]'));
		};

		this.getDeviceConditionDamageTitle = function () {
			return element(by.css('[ng-bind-html="tradeInCtrl.authorValue.tradeInTool.deviceConditionDamageLabel | parseHtml"]'));
		};

		this.getTradeinCTA = function () {
			return element(by.id('submitTradeInCTA'));
		};

		this.getEstimateCTAText = function () {
			return element(by.id('submitTradeInCTA'));
		};

		this.getSkipTradeinCTA = function () {
			return element(by.id('skipTradeInCTA'));
		};

		this.getCarrier = function () {
			return element.all(by.model('tradeInCtrl.model.formData.carrier')).all(by.tagName('option'));
		};

		this.getCarrierName = function () {
			//return element(by.cssContainingText('option', carrierName));
			return element(by.name('carrier'));
		};

		this.getMake = function () {
			return element(by.model('tradeInCtrl.model.formData.make')).all(by.tagName('option'));
		};

		this.getModel = function () {
			return element(by.model('tradeInCtrl.model.formData.deviceModel')).all(by.tagName('option'));
		};

		this.imeiApplyButton = function () {
			return element(by.id('imeiApply'));
		};

		this.imei = function () {
			return element(by.css('input[ng-model="tradeInCtrl.model.formData.imei"]'));
		};

		this.tqtTooltip = function () {
			return element(by.css('button[ng-click="vm.togglePopover()"]'));
		};

		this.getGoodDeviceCondition = function () {
			return element(by.id('deviceConditionGood'));
		};

		this.getDamageDeviceCondition = function () {
			return element(by.id('deviceConditionDamage'));
		};

		this.getConfirmationModalHeader = function () {
			return element(by.id('dialogDiscription'));
		};

		this.getConfirmationModalSubheader = function () {
			return element(by.css('p[ng-bind-html="vm.authorValue.tradeInValueScreen.subHeader | parseHtml"]'));
		};

		this.getTradeinOptions = function () {
			return element(by.css('div[class="col-lg-12 col-md-12 col-sm-12 col-xs-12 radio p-t-10 m-b-0"]')).all(by.tagName('label'));
		};

		this.promotionFlow = function () {
			return element(by.id('promotionFlow'));
		};

		this.standardFlow = function () {
			return element(by.id('standardFlow'));
		};

		this.promotionTradeinEstimate = function () {
			return element(by.id('promotionFlow')).all(by.css('span[class="ng-binding"]')).get(0);
		};

		this.standardTradeinEstimate = function () {
			return element(by.id('standardFlow')).all(by.css('span[class="ng-binding"]')).get(1);
		};

		this.standardTradeinLegalText = function () {
			return element(by.id('standardFlow')).element(by.css('div[ng-if="vm.isMobileView && vm.authorValue.tradeInValueScreen.standardTradeInLegalText"]'));
			//return element(by.id('standardFlow')).element(by.className('ng-binding'));
		};

		this.getaddedTradeinModelName = function (lineItem) {
			return lineItem.element(by.css('div[ng-if="vm.selectedLine.isTradeInStandardFlow || vm.creditClass3 || vm.poFull"]')).all(by.className('ng-binding')).get(0);
		};

		this.getaddedPromoTradeinModelName = function (lineItem) {
			return lineItem.element(by.css('div[ng-if="vm.selectedLine.isTradeInPromoFlow"]')).all(by.className('ng-binding')).get(0);
		};

		this.getaddedTradeinIMEINumber = function (lineItem) {
			return lineItem.element(by.css('div[ng-if="vm.selectedLine.isTradeInStandardFlow || vm.creditClass3 || vm.poFull"]')).all(by.className('ng-binding')).get(1);
		};

		this.getaddedPromoTradeinIMEINumber = function (lineItem) {
			return lineItem.element(by.css('div[ng-if="vm.selectedLine.isTradeInPromoFlow"]')).all(by.className('ng-binding')).get(1);
		};

		this.getaddedTradeinValue = function (lineItem) {
			return lineItem.element(by.css('div[ng-if="vm.selectedLine.isTradeInStandardFlow || vm.creditClass3 || vm.poFull"]')).all(by.className('ng-binding')).get(2);
		};

		this.getaddedPromoTradeinValue = function (lineItem) {
			return lineItem.element(by.css('div[ng-if="vm.selectedLine.isTradeInPromoFlow"]')).all(by.className('ng-binding')).get(2);
		};

		this.getCartLineItem = function (index) {
			return element.all(by.repeater('selectedLine in myCartCtrl.cartCommonOperationsService.model.structCartData.lineItems | orderBy: \'1*lineNo\'')).get(index - 1);
		};

		this.tradeinAcceptButton = function () {
			return element(by.id('trade_in_accept'));
		};

		this.tradeinDeclineButton = function () {
			return element(by.id('trade_in_decline'));
		};

		this.tradeinNotification = function () {
			return element(by.className('nudge bg-magenta note-bar'));
		};

		this.getTradeinHeader = function () {
			return element(by.css('span[ng-bind-html="vm.myCartCtrl.keyValues.tradeIn.tradeInTitle | parseHtml"]'));
		};

		this.getTradeinRemoveLink = function (lineItem) {
			return lineItem.element(by.css('div[ng-if="vm.selectedLine.tradeInDetails"]')).element(by.tagName('a'));
		};

		this.gettradeinValueLink = function () {
			return element(by.id('standardTradeInDetailsModal')).all(by.tagName('a')).get(3);
		};
		this.getPromotradeinValueLink = function () {
			return element(by.css('a[ng-bind-html="vm.myCartCtrl.keyValues.tradeIn.promoDetails | parseHtml"]'));
		};

	};

	module.exports = function () {
		return new Smartcart();
	};

}());