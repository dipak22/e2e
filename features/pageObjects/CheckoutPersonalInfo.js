'use strict';
(function () {

	var CheckoutPersonalInfo = function () {

		this.getPersonalInfoHeader = function () {
			return element(by.css(".creditheading"));
		};

		this.getConsentCheckbox = function () {
			return element(by.id('infoAndShippingpersonalDisclaimerLabel'));
		};

		this.getPersonaInfoNextButton = function () {
			return element(by.id('infoNextButton'));
		};

		this.getExpertBarText = function () {
			return element(by.css('h6[ng-bind-html="::vm.authorValue.checkout.expertBar.class1Text || \'Expert 1 text\'"]'));
		};

		this.getExpertBarImage = function () {
			return element(by.css('img[src="/content/dam/t-mobile/expert_bar/expert.png"]'));
		};

		this.getTMobileLogo = function () {
			return element(by.css('img[ng-if="!vm.authorValue.checkout.checkoutHeader.hideModal"]'));
		};

		this.getStayHereButtonFromPathConfirmationModal = function () {
			return element(by.cssCustomSelector("vm.dismiss()", "button", "ng-click"));
		};

		this.getGoToHomepageButtonFromPathConfirmationModal = function () {
			return element(by.cssCustomSelector("vm.close('checkOutHeader')", "button", "ng-click"));
		};

		this.getEntrustLogo = function () {
			return element(by.css('img[aria-label="Entrust Logo Image"]'));
		};

		this.getEntrustText = function () {
			return element(by.css('.legal-justify'));
		};

		this.getContactUsText = function () {
			return element(by.css('h5[ng-bind-html="vm.authorValue.contactUsTitle | parseHtml"]'));
		};

		this.getContactUsSubText = function () {
			return element(by.css('p[ng-bind-html="vm.authorValue.contactUsDesc | parseHtml"]'));
		};

		this.getCallUsImage = function () {
			return element(by.cssValueBasedSelector("tmo_tfn_number", "a", "class")).element(by.tagName('img'));
		};

		this.getCallUsText = function () {
			return element(by.css('span[ng-bind-html="vm.authorValue.contactUsCallText | parseHtml"]'));
		};

		this.getCallUsNumber = function () {
			return element(by.css('span[ng-bind-html="vm.authorValue.contactUsCallValue | parseHtml"]'));
		};

		this.getPersonalInfoProgressBar = function () {
			return element(by.id('page-name-1'));
		};

		this.getFirstNameErrorMessage = function () {
			return element(by.id('personalInfoFormfirstNameErrorMessage')).element(by.tagName('p'));
		};

		this.getLastNameErrorMessage = function () {
			return element(by.id('personalInfoFormlastNameErrorMessage')).element(by.tagName('p'));
		};

		this.getEmailAddressErrorMessage = function () {
			return element(by.id('personalInfoFormemailErrorMessage')).element(by.tagName('p'));
		};

		this.getConfirmEmailAddressErrorMessage = function () {
			return element(by.id('confirmEmailErrorMessage')).element(by.tagName('p'));
		};

		this.getPhoneNumberErrorMessage = function () {
			return element(by.id('personalInfoFormphoneNumberErrorMessage')).element(by.tagName('p'));
		};

		this.getFirstName = function () {
			return element(by.name('firstName'));
		};

		this.getLastName = function () {
			return element(by.name('lastName'));
		};

		this.getMiddleName = function () {
			return element(by.name('middleName'));
		};

		this.getEmailAddress = function () {
			return element(by.name('email'));
		};

		this.getConfirmEmailAddress = function () {
			return element(by.name('confirmEmail'));
		};

		this.getPhoneNumber = function () {
			return element(by.name('phoneNumber'));
		};

		this.getCarrierDropDown = function () {
			return element(by.model('vm.checkOutInfo.personalInfo.carrier'));
		};

		this.getConsentCheckboxOnError = function () {
			return element(by.css('div[class="indicator required checkboxError"]'));
		};

		this.getStayHereButton = function () {
			return element(by.css('button[ng-click="vm.dismiss()"]'));
		};

		this.getGoToHomePageButton = function () {
			return element(by.css('button[ng-click="vm.close(\'checkOutHeader\')"]'));
		};

		this.getPersonalInfoProgressBarIcon = function () {
			return element.all(by.repeater('section in vm.arrayOfSections')).get(0).element(by.css('button[ng-click="vm.setSectionToBeDisplayed(section.name)"]'));
		}

		this.getConsentCheckboxInput = function () {
			return element(by.id('personalInfoFormpersonalDisclaimer'));
		};

		this.getTermsOfUseFooterLink = function () {
			return element(by.cssCustomSelector("vm.footerKeyValues.termsOfUseText", "a", "ng-bind-html"));
		};

		this.getTermsAndConditionsFooterLink = function () {
			return element(by.cssCustomSelector("vm.footerKeyValues.termsAndConditionsText", "a", "ng-bind-html"));
		};

		this.getReturnPolicyFooterLink = function () {
			return element(by.cssCustomSelector("vm.footerKeyValues.returnPolicyText", "a", "ng-bind-html"));
		};

		this.getPrivacyPolicyFooterLink = function () {
			return element(by.cssCustomSelector("vm.footerKeyValues.privacyPolicyText", "a", "ng-bind-html"));
		};

		this.getLanguageToggleFooterLink = function () {
			return element(by.cssCustomSelector("vm.userPreferredLanguage=='en'? vm.footerKeyValues.localizationSpanishText:vm.footerKeyValues.localizationEnglishText", "a", "ng-bind-html"));
		};

		this.getFooterCopyRightText = function () {
			return element(by.cssCustomSelector("vm.footerKeyValues.copyrightText", "span", "ng-bind-html"));
		};


	};
	module.exports = function () {
		return new CheckoutPersonalInfo();
	};
}());