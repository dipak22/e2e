'use strict';
(function () {

	var myTMODetails = function () {

		/**************************************************************************************************************************
		 *                                           	   My TMO                                                         *
		 * ************************************************************************************************************************/

		this.getmyTMOHeader = function () {
			return element(by.name('signinform'));
		};
		
		this.setUserName = function(){
			return element(by.id('username'));
		};
		
		this.setPassword = function(){
			return element(by.id('password'));
		};
		
		this.login = function(){
			return element(by.css('input[type="submit"]'));
		};
		
		this.getLinesPageHeader = function(){
			return element(by.css('div[class="ui_subhead"]'));
		};
		
		this.myLine = function(){
			return element(by.id('nickname2'));
		};
		

		
	};

	module.exports = function () {
		return new myTMODetails();
	};
}());