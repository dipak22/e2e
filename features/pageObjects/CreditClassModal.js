'use strict';
(function () {

	var CreditClassModal = function () {

		this.getAwesomeCreditClass = function () {
			return element.all(by.cssCustomSelector("creditClass in creditSelectorModalCtrl.keyValues.class", "div", "ng-repeat")).get(0).element(by.cssCustomSelector("creditClass.label | parseHtml", "span", "ng-bind-html"));
		};

		this.getAverageCreditClass = function () {
			return element.all(by.cssCustomSelector("creditClass in creditSelectorModalCtrl.keyValues.class", "div", "ng-repeat")).get(1).element(by.cssCustomSelector("creditClass.label | parseHtml", "span", "ng-bind-html"));
		};

		this.getNoCreditClass = function () {
			return element.all(by.cssCustomSelector("creditClass in creditSelectorModalCtrl.keyValues.class", "div", "ng-repeat")).get(2).element(by.cssCustomSelector("creditClass.label | parseHtml", "span", "ng-bind-html"));
		};

		this.getCreditClassModalCloseButton = function () {
			return element(by.cssCustomSelector("creditSelectorModalCtrl.cancel()", "button", "ng-click"));
		};

		this.getBuildMyOrderButton = function () {
			return element(by.cssCustomSelector("creditSelectorModalCtrl.buildMyOrder()", "button", "ng-click"));
		};

		this.getPriceWrapperFromCreditClassModal = function () {
			return element(by.css(".modal-content")).element(by.cssCustomSelector("price-lockup-large", "div", "size-class"));
		};

		this.getSelectedCreditClass = function () {
			return element.all(by.cssCustomSelector("creditClass in creditSelectorModalCtrl.keyValues.class", "div", "ng-repeat")).filter(function (elem, index) {
				//return elem.element(by.cssCustomSelector("checked","input","checked")).isPresent().then(function(Value) {
				return elem.element(by.tagName("input")).getAttribute("checked").then(function (Value) {
					return Value === "true";
				});
			}).get(0).element(by.css(".credit-main-text"));
		};

		this.getSkipCreditCheckLink = function () {
			return element(by.cssCustomSelector("creditSelectorModalCtrl.cartSettings.creditClassSelector.hideCreditClass3", "div", "ng-if")).element(by.tagName("a"));
		};

		this.getSkipCreditCheckModalLink = function () {
			return element(by.cssCustomSelector("creditClass.class =='creditClass3' && creditSelectorModalCtrl.keyValues.creditSelector.skip_label_popup", "span", "ng-if")).element(by.tagName("a"));
		};

		this.getSkipCreditCheckModalBackButton = function () {
			return element(by.cssValueBasedSelector("modal-back", "p", "class")).element(by.cssCustomSelector("button", "a", "role"));
		};
	};

	module.exports = function () {
		return new CreditClassModal();
	};

}());