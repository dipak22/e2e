'use strict';
(function () {

	var Common = function () {

		/**************************************************************************************************************************
		 *                                           	Smart Cart Page                                                           *
		 * ************************************************************************************************************************/
		//modal fading
		this.addPhoneButtonMainCart = function () {
			return element(by.css('[ng-click="vm.myCartCtrl.addLine()"]'));
		};

		this.addAPhoneButton = function () {
			return element(by.css('[ng-click="vm.myCartCtrl.addLine()"]'));
		};

		this.addAnAccessoryButton = function () {
			return element(by.css('[ng-click="vm.myCartCtrl.showAccessories()"]'));
		};

		this.getCartTitle = function () {
			//return element(by.css('.my-cart-wrapper')).element(by.tagName('h1'));
			return element(by.css('.my-cart-wrapper')).element(by.css('[ng-bind-html="myCartCtrl.keyValues.cartHeader.cartTitle | parseHtml"]'));
		};

		this.backdropStaleness = function () {
			return element(by.css('div[class="modal-backdrop fade  in"]'));
			// return element(by.cssCustomSelector("screen-lock","div","class"));
		};

		this.screenLock = function () {
			return element(by.cssCustomSelector("screen-lock", "div", "class"));
		};

		/**************************************************************************************************************************
		 *                                           	Checkout                                                           *
		 * ************************************************************************************************************************/
		this.viewOrderCheckout = function () {
			return element(by.id('viewOrderDetailsLink'));
		};

		this.getViewOrderDetailsTitleElement = function () {
			//return element(by.css('[ng-if="deviceSelectorModalCtrl.mostPopularDevices"]')).element(by.tagName('h4'));
			// return element(by.cssCustomSelector("deviceSelectorModalCtrl.mostPopularDevices","div","ng-if")).element(by.tagName('h2'));
			return element(by.id("lineNumber"));
		};

	};

	module.exports = function () {
		return new Common();
	};
}());