'use strict';
(function () {

	var CheckoutShippingInfo = function () {

		this.getPaymentHeader = function () {
			return element(by.id("paymentHeader"));
		};

		this.getCreditHeader = function () {
			return element(by.id("creditHeader"));
		};

		this.getStandardShippingOptionSelected = function () {
			return element(by.css('label[id="2-4 business days"]')).element(by.css('div[class="indicator pull-left"]'));
		};

		this.getStandardShippingOption = function () {
			return element.all(by.css('[class="indicator pull-left"]')).get(0);
			//return element(by.id("2-4 business daysRadio"));
		};

		this.getOvernightShippingOption = function () {
			return element.all(by.css('[class="indicator pull-left"]')).get(1);
      		//return element(by.id("1 business dayRadio"));
		};

		this.getSaturdayShippingOption = function () {
			return element.all(by.css('[class="indicator pull-left"]')).get(2);
			//return element(by.id("Saturday deliveryRadio"));
		};

		this.getAutoPayOnHeading = function () {
			return element(by.cssCustomSelector("vm.checkOutInfo.flags.isAutoPay && vm.checkOutInfo.reviewCart.autoPayDiscount != '$0'", "h3", "ng-if"));
		};

		this.getAutoPayOffHeading = function () {
			return element(by.cssCustomSelector("!vm.checkOutInfo.flags.isAutoPay && vm.checkOutInfo.reviewCart.autoPayDiscount != '$0'", "h3", "ng-if"));
		};

		this.getAutoPayOnLabel = function () {
			return element.all(by.cssCustomSelector("vm.checkOutInfo.flags.isAutoPay", "p", "ng-if")).get(0);
		};

		this.getAutoPayOffLabel = function () {
			return element.all(by.cssCustomSelector("!vm.checkOutInfo.flags.isAutoPay", "p", "ng-if")).get(0);
		};

		this.getAutoPayOnContent = function () {
			return element(by.cssCustomSelector("::vm.authorValue.checkout.shippingInfo.autoPayDescription2", "p", "bind-html-compile"));
		};

		this.getAutoPaySwitchButton = function () {
			return element(by.css('label[for="autoPaySwitch"]'));
		};

		this.getAutoPaySwitchButtonColor = function () {
			return element(by.css('label[for="autoPaySwitch"]')).element(by.css('span[class="onoffswitch-inner"]'));
		};

		this.getPaymentCreditNextButton = function () {
			return element(by.id('paymentCreditNextButton'));
		};
		this.getselectcheckboxe = function () {
			return element(by.id("e911CheckboxLabel"));
		};

		this.getShippingAddressLine1Label = function () {
			return element(by.id("shipAddress1Label"));
		};

		this.getShippingAddressLine2Label = function () {
			return element(by.id("shipAddress2Label"));
		};

		this.getShippingCityLabel = function () {
			return element(by.id("shipCityLabel"));
		};

		this.getShippingStateLabel = function () {
			return element(by.id("shipStateLabel"));
		};

		this.getShippingZipLabel = function () {
			return element(by.id("shipZipLabel"));
		};

		this.getBillingAddressLine1Label = function () {
			return element(by.id("billAddress1Label"));
		};

		this.getBillingAddressLine2Label = function () {
			return element(by.id("billAddress2Label"));
		};

		this.getBillingCityLabel = function () {
			return element(by.id("billCityLabel"));
		};

		this.getBillingStateLabel = function () {
			return element(by.id("billStateLabel"));
		};

		this.getBillingZipLabel = function () {
			return element(by.id("billZipLabel"));
		};

		this.getE911AddressLine1Label = function () {
			return element(by.id("e911Address1Label"));
		};

		this.getE911AddressLine2Label = function () {
			return element(by.id("e911Address2Label"));
		};

		this.getE911CityLabel = function () {
			return element(by.id("e911CityLabel"));
		};

		this.getE911StateLabel = function () {
			return element(by.id("e911StateLabel"));
		};

		this.getE911ZipLabel = function () {
			return element(by.id("e911ZipLabel"));
		};

		this.getNameOnCardLabel = function () {
			return element(by.id("nameOnCard"));
		};

		this.getCreditCardNumberLabel = function () {
			return element(by.id("creditCardNumber"));
		};
		this.getCreditCardNumberInput = function () {
			return element(by.id("creditCardNumber"));
		};

		this.getCardExpiryDateLabel = function () {
			return element(by.id("expiryDate"));
		};
		this.getCardExpiryDateInput = function () {
			return element(by.id("cardExpiryDate"));
		};

		this.getCVVLabel = function () {
			return element(by.id("securityCode"));
		};

		this.getCVVInput = function () {
			return element(by.name("securityCode"));
		};

		this.getShippingAddressLine1 = function () {
			return element(by.id('shipAddress1'));
		};

		this.getShippingAddressLine2 = function () {
			return element(by.id('shipAddress2'));
		};

		this.getShippingCity = function () {
			return element(by.id('shipCity'));
		};

		this.getShippingState = function () {
			return element(by.id('shipState'));
		};

		this.selectShippingState = function () {
			return element(by.name("shipState")).all(by.tagName('option'));
		};

		this.selectShippingStateInput = function () {
			return element(by.name("shipState"));
		};

		this.selectState = function (RepeaterElement) {
			return RepeaterElement;
		};

		this.getShippingZip = function () {
			return element(by.css('input[name="shipZip"]'));
		};

		this.getBillingAddressLine1 = function () {
			return element(by.name("billAddress1"));
		};

		this.getBillingAddressLine2 = function () {
			return element(by.name("billAddress2"));
		};

		this.getBillingCity = function () {
			return element(by.name("billCity"));
		};

		this.getAllBillingStates = function () {
			return element(by.name("billState")).all(by.tagName('option'));
		};

		this.selectBillingState = function (RepeaterElement) {
			return RepeaterElement;
		};

		this.getBillingState = function (RepeaterElement) {
			return element(by.name("billState"));
		};

		this.getBillingZip = function () {
			return element(by.name("billZip"));
		};

		this.getE911AddressLine1 = function () {
			return element(by.name("e911Address1"));
		};

		this.getE911AddressLine2 = function () {
			return element(by.name("e911Address2"));
		};

		this.getE911City = function () {
			return element(by.name("e911City"));
		};

		this.getAllE911States = function () {
			return element(by.name("e911State")).all(by.tagName('option'));
		};

		this.selectE911State = function (RepeaterElement) {
			return RepeaterElement;
		};

		this.getE911State = function (RepeaterElement) {
			return element(by.name("e911State"));
		};

		this.getE911Zip = function () {
			return element(by.name("e911Zip"));
		};

		this.getNameOnCard = function () {
			return element(by.css('input[name="nameOnCard"]'));
		};

		this.getCreditCardNumber = function () {
			return element(by.css('input[name="creditCardNumber"]'));
		};

		this.getCardExpiryDate = function () {
			return element(by.name("expiryDate"));
		};

		this.getCVV = function () {
			return element(by.css('input[name="securityCode"]'));
		};

		this.getShippingAddressLine1ErrorMessage = function () {
			return element(by.css('div[id="shipAddress1Message"]')).element(by.tagName('p'));
		};

		this.getShippingCityErrorMessage = function () {
			return element(by.id("cityerror")).element(by.tagName('p'));
		};

		this.getShippingStateErrorMessage = function () {
			return element(by.id("staterror")).element(by.tagName('p'));
		};

		this.getShippingZipErrorMessage = function () {
			return element(by.id("ziperror")).element(by.tagName('p'));
		};

		this.getBillingAddressLine1ErrorMessage = function () {
			return element(by.id("billAddress1Message")).element(by.tagName('p'));
		};

		this.getBillingCityErrorMessage = function () {
			//return element(by.id("cityerror")).element(by.tagName('p'));
			return element(by.name("billCity")).element(by.xpath('following-sibling::div')).element(by.tagName('p'));
		};

		this.getBillingStateErrorMessage = function () {
			//return element(by.id("staterror")).element(by.tagName('p'));
			return element(by.name("billState")).element(by.xpath('following-sibling::div')).element(by.tagName('p'));
		};

		this.getBillingZipErrorMessage = function () {
			//return element(by.id("ziperror")).element(by.tagName('p'));
			return element(by.name("billZip")).element(by.xpath('following-sibling::div')).element(by.tagName('p'));
		};

		this.getE911AddressLine1ErrorMessage = function () {
			return element(by.id("e911Address1Message")).element(by.tagName('p'));
		};

		this.getE911CityErrorMessage = function () {
			//return element(by.id("cityerror")).element(by.tagName('p'));
			return element(by.name("e911City")).element(by.xpath('following-sibling::div')).element(by.tagName('p'));
		};

		this.getE911StateErrorMessage = function () {
			//return element(by.id("staterror")).element(by.tagName('p'));
			return element(by.name("e911State")).element(by.xpath('following-sibling::div')).element(by.tagName('p'));
		};

		this.getE911ZipErrorMessage = function () {
			//return element(by.id("ziperror")).element(by.tagName('p'));
			return element(by.name("e911Zip")).element(by.xpath('following-sibling::div')).element(by.tagName('p'));
		};

		this.getCreditCardNumberErrorMessage = function () {
			return element(by.css('div[id="creditCardNumbererror"]')).element(by.tagName('p'));
		};

		this.getCardExpiryDateErrorMessage = function () {
			return element(by.css('div[id="expiryDateerror"]')).element(by.tagName('p'));
		};

		this.getCVVErrorMessage = function () {
			return element(by.css('div[id="securityCodeerror"]')).element(by.tagName('p'));
		};

		this.clickShippingState = function () {
			return element(by.name("shipState"));
		};

		this.getBillingCheckBoxIndicator = function () {
			return element(by.css('label[id="billCheckBox"]')).element(by.css('div[class="indicator"]'));
		};

		this.getE911CheckBoxIndicator = function () {
			return element(by.css('label[id="e911CheckBox"]')).element(by.css('div[class="indicator"]'));
		};

		this.getBillingCheckBox = function () {
			return element(by.name("billDisclaimer"));
		};

		this.getE911CheckBox = function () {
			return element(by.name("e911Disclaimer"));
		};

		this.getSelectedShippingState = function () {
			return element(by.name("shipState")).element(by.cssCustomSelector("selected", 'option', "selected"));
		};

		this.getSelectedShippingOption = function () {
			return element.all(by.repeater("selectOpt in vm.checkOutInfo.initData.selectedShippingOptions | orderBy: 'costValue' ")).filter(function (elem, index) {
				//return elem.element(by.cssCustomSelector("checked","input","checked")).isPresent().then(function(Value) {
				return elem.element(by.tagName("input")).getAttribute("checked").then(function (Value) {
					return Value === "true";
				});
			}).get(0).element(by.cssCustomSelector("selectOpt.name", "div", "on")).element(by.tagName("p"));
		};

		this.getShippingInfoProgressBarIcon = function () {
			return element.all(by.repeater('section in vm.arrayOfSections')).get(1).element(by.css('button[ng-click="vm.setSectionToBeDisplayed(section.name)"]'));
		};

		/*this.getStandardShippingOption = function () {
			return element.all(by.repeater("selectOpt in vm.checkOutInfo.initData.selectedShippingOptions | orderBy: 'costValue' ")).get(0).element(by.tagName("label"));
		};

		this.getOvernightShippingOption = function () {
			return element.all(by.repeater("selectOpt in vm.checkOutInfo.initData.selectedShippingOptions | orderBy: 'costValue' ")).get(1).element(by.tagName("label"));
		};

		this.getSaturdayShippingOption = function () {
			return element.all(by.repeater("selectOpt in vm.checkOutInfo.initData.selectedShippingOptions | orderBy: 'costValue' ")).get(2).element(by.tagName("label"));
		};*/

		this.getStandardShippingOptionValue = function () {
			return element.all(by.repeater("selectOpt in vm.checkOutInfo.initData.selectedShippingOptions | orderBy: 'costValue' ")).get(0).element(by.cssCustomSelector("selectOpt.name", "div", "on")).element(by.tagName("p"));
		};

		this.getOvernightShippingOptionValue = function () {
			return element.all(by.repeater("selectOpt in vm.checkOutInfo.initData.selectedShippingOptions | orderBy: 'costValue' ")).get(1).element(by.cssCustomSelector("selectOpt.name", "div", "on")).element(by.tagName("p"));
		};

		this.getSaturdayShippingOptionValue = function () {
			return element.all(by.repeater("selectOpt in vm.checkOutInfo.initData.selectedShippingOptions | orderBy: 'costValue' ")).get(2).element(by.cssCustomSelector("selectOpt.name", "div", "on")).element(by.tagName("p"));
		};

		this.getVisaCardLogo = function () {
			return element(by.cssValueBasedSelector("visa-image", "i", "class"));
		};

		this.getMasterCardLogo = function () {
			return element(by.cssValueBasedSelector("mc-image", "i", "class"));
		};

		this.getAMEXCardLogo = function () {
			return element(by.cssValueBasedSelector("amex-image", "i", "class"));
		};

		this.getDiscoverCardLogo = function () {
			return element(by.cssValueBasedSelector("disc-image", "i", "class"));
		};

		this.getStandardShippingToolTip = function () {
			return element(by.id("standardToolTip"));
		};

		this.getOvernightShippingToolTip = function () {
			return element(by.id("overnightToolTip"));
		};

		this.getSameShippingAdressWillBeUsedTextToolTip = function () {
			return element(by.id("standardToolTip1"));
		};

		this.getBillDisclaimerCheckboxToolTip = function () {
			return element(by.id("standardToolTip2"));
		};

		this.getE911DisclaimerCheckboxToolTip = function () {
			return element(by.id("standardToolTip2"));
		};

		this.getCreditCardNumberToolTip = function () {
			return element(by.id("ccNumberToolTip"));
		};

		this.getExpiryDateToolTip = function () {
			return element(by.id("expiryDateToolTip"));
		};

		this.getCVVNumberToolTip = function () {
			return element(by.id("cvvNumberToolTip"));
		};

		this.getToolTipText = function () {
			return element(by.css(".popover-content"));
		};


	};
	module.exports = function () {
		return new CheckoutShippingInfo();
	};
}());
