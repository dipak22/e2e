'use strict';
(function () {

    var Checkout = function () {

        this.getInfoShippingProgressBarLink = function(){
            return element((by.id('progressBtn1')).by.cssCustomSelector('bg-brand-magenta', 'class', 'button'));
        };
        
        this.getFirstName = function () {
            //return element(by.name('firstName'));
            element(by.xpath('//*[@id="fsrOverlay"]/div/div/div/div/div')).isPresent().then(function (presence) {
                console.log(presence);
                if (presence == true) {
                    element(by.xpath('//*[@id="fsrOverlay"]/div/div/div/div/div/div[2]/div[2]/a')).click().then(function () {
                        console.log("unexpected alert window is closed");
                    })
                }
                else {
                    console.log("unexpected alert window is not present");
                }
            })
            return element(by.css('input[name="firstName"]'));
        };

        this.getLastName = function () {
            return element(by.css('input[id="personalInfoFormlastName"]'));
        };

        this.getLastName1 = function () {
            return element(by.css('input[id="personalInfoFormlastName"]'));
        };

        this.getEmailAddress = function () {
            return element(by.css('input[id="personalInfoFormemail"]'));
        };

        this.getConfirmEmailAddress = function () {
            return element(by.css('input[id="personalInfoFormconfirmEmail"]'));
        };

        this.getPhoneNumber = function () {
            return element(by.id('personalInfoFormphoneNumber'));
        };

        this.getConsentEdited = function () {
            return element(by.id("infoAndShippingpersonalDisclaimerLabel"));
        };

        this.getPersonalInfoNextButton = function () {
            browser.sleep(1000);
            element(by.xpath('//*[@id="fsrOverlay"]/div/div/div/div/div')).isPresent().then(function (presence) {
                console.log(presence);
                if (presence == true) {
                    element(by.xpath('//*[@id="fsrOverlay"]/div/div/div/div/div/div[2]/div[2]/a')).click().then(function () {
                        console.log("unexpected alert window is closed");
                    })
                }
                else {
                    console.log("unexpected alert window is not present");
                }
            })
            return element(by.cssCustomSelector('userForm.isSubmitted && userForm.$invalid', 'button', 'ng-disabled'));
        };

        this.getCheckboxAsterisk = function () {
            return element(by.cssCustomSelector("indicator required", "div", "class"));
        };

        this.getShippingAddress1 = function () {
            return element(by.name("shipAddress1"));
        };

        this.getShippingAddress2 = function () {
            return element(by.name("shipAddress2"));
        };

        this.getShippingCity = function () {
            return element(by.name("shipCity"));
        };

        this.clickShippingState = function () {
            return element(by.name("shipState"));
        };

        this.selectShippingState = function () {
            return element(by.name("shipState")).all(by.tagName('option'));
        };

        this.getShippingState = function () {
            return element(by.name("shipState"));
        }

        this.selectState = function (RepeaterElement) {
            return RepeaterElement;
        };

        this.getShippingZip = function () {
            return element(by.name("shipZip"));
        };

        this.getBillingAddress1 = function () {
            return element(by.name("billAddress1"));
        };

        this.getBillingAddress2 = function () {
            return element(by.name("billAddress2"));
        };

        this.getBillingCity = function () {
            return element(by.name("billCity"));
        };

        this.clickBillingState = function () {
            return element(by.name("billState"));
        };

        this.selectBillingState = function () {
            return element(by.name("billState")).all(by.tagName('option'));
        };

        this.getBillingState = function (RepeaterElement) {
            return RepeaterElement;
        };

        this.getBillingZip = function () {
            return element(by.name("billZip"));
        };

        this.getCheckSameAddressForBilling = function () {
            return element(by.id('billCheckBox')).element(by.css(".indicator"));
        }

        this.getNameOnCard = function () {
            return element(by.name('nameOnCard'));
        };

        this.getCreditCardNumber = function () {
            return element(by.name('creditCardNumber'));
        };

        this.getCardExpiryDate = function () {
            return element(by.name('expiryDate'));
        };

        this.getCardCVV = function () {
            return element(by.name('securityCode'));
        };

        this.clickShippingNext = function () {
            return element(by.cssCustomSelector('shipForm.isSubmitted && !shipForm.$valid', 'button', 'ng-disabled'));
        }

        this.clickIDType = function () {
            return element(by.model('vm.checkOutInfo.creditcheck.idType'));
        }

        this.selectIDType = function () {
            return element(by.model('vm.checkOutInfo.creditcheck.idType')).all(by.tagName('option'));
        }

        this.getCreditIDType = function () {
            return element(by.model('vm.checkOutInfo.creditcheck.idType'));
        }

        this.selectID = function (RepeaterElement) {
            return RepeaterElement;
        };

        this.getIDNumber = function () {
            return element(by.name('idnumber'));
        }

        this.getCreditCheckCardExpiry = function () {
            return element(by.name('expiryDate'));
        }

        this.clickCreditCheckState = function () {
            return element(by.model('vm.checkOutInfo.creditcheck.state'));
        }

        this.selectCreditCheckState = function () {
            return element(by.model('vm.checkOutInfo.creditcheck.state')).all(by.tagName('option'));
        }

        this.getCreditCheckState = function () {
            return element(by.model('vm.checkOutInfo.creditcheck.state'));
        }

        this.selectCreditState = function (RepeaterElement) {
            return RepeaterElement;
        };

        this.getCreditCheckSSN = function () {
            return element(by.name('ssn'));
        }

        this.getCreditCheckDOB = function () {
            return element(by.name('dob'));
        }

        this.clickCreditCheckNext = function () {
            return element(by.cssCustomSelector('creditForm.isSubmitted && !creditForm.$valid', 'button', 'ng-disabled'));
        }

        this.CreditCheckStateAsterisk = function () {
            return element(by.cssCustomSelector("{'required' : (vm.checkOutInfo.creditcheck.idType =='State issued ID') || (vm.checkOutInfo.creditcheck.idType=='Driver license')}", "label", "ng-class"));
        }

        this.getReviewShippingDetail = function () {
            return element(by.xpath('//*[@id="top"]/div/div/div[4]/div[1]/div'));
        }

        this.getReviewPaymentDetail = function () {
            return element(by.xpath('//*[@id="top"]/div/div/div[4]/div[2]/div/p[1]'));
        }

        this.getReviewCardDetail = function () {
            return element(by.xpath('//*[@id="top"]/div/div/div[4]/div[2]/div/p[2]'));
        }

        this.getSubmitOrderButton = function () {
            browser.executeScript('window.scrollTo(0,1000);');
            return element(by.css('button[pdl-checkout-step="termsCondition"]'));
        }

        this.getTermsAgreeButton = function () {
            return element(by.css('button[ng-click="vm.close()"]'));
        }

        this.getConfirmationsection = function () {
            return element(by.css('div[ng-show="vm.sectionToBeDisplayed == \'confirmation\'"]'));
        }

        this.getConfirmationRecapHeader = function () {
            return element(by.xpath('//*[@id="tmobileApp"]/div/div[4]/div[4]/div/h3')).getText();
        }

        this.getConfirmationDeviceName = function () {
            return element(by.tagName('figcaption')).getText();
        }

        this.getuserconsentfield = function () {
            return element(by.cssCustomSelector('row p-b-35 p-lg-b-50 checkboxtab', 'div', 'class'));
        }

        this.getuserconsentdivision = function () {
            return element(by.cssCustomSelector('row p-b-35 p-lg-b-50 checkboxtab', 'div', 'class')).element(by.tagName('p'));
        }

        this.getNextCTAdivision = function () {
            return element(by.xpath('html/body/div[4]/div/div[1]/div[3]/div[3]/div[1]/div[3]/div/div/div[2]/form/div[5]/div/button'))
        }

        this.getprivacypolicydiv = function () {
            return element(by.xpath('html/body/div[4]/div/div[1]/div[3]/div[3]/div[2]/div/div'))
        }

        this.getuserconsentcheckboxdivision = function () {
            return element(by.cssCustomSelector('checkbox1', 'label', 'for')).element(by.cssCustomSelector('indicator required', 'div', 'class'));
        }

        this.getpersonalinfofirstnamediv = function () {
            return element(by.cssCustomSelector('{ \'show-errors\' : userForm.firstName.$invalid && userForm.isSubmitted, \'row-valid\': userForm.firstName.$valid}', 'div', 'ng-class'));
        }

        this.getentrustlogo = function () {
            return element(by.cssCustomSelector('vm.sectionToBeDisplayed != \'confirmation\'', 'div', 'ng-show')).element(by.cssCustomSelector('pull-left', 'span', 'class'));
        };

        this.getentrusttext = function () {
            return element(by.cssCustomSelector('vm.sectionToBeDisplayed != \'confirmation\'', 'div', 'ng-show')).element(by.cssCustomSelector('padding-small pull-left legal width-100', 'p', 'class'));
        };

        this.getpersonalinfobladedivision = function () {
            return element(by.cssCustomSelector("vm.sectionToBeDisplayed == 'shippingInfoSection' || vm.sectionToBeDisplayed == 'creditCheckInfoSection' || vm.sectionToBeDisplayed == 'reviewOrderInfoSection'", 'div', 'ng-show'));

        };
        this.getshippingsectionheaderdivision = function () {
            return element(by.cssCustomSelector('checkout shipInfo bg-white', 'div', 'class')).element(by.tagName('h4'));
        }

        this.getshippingoptiondivision = function () {
            return element(by.cssCustomSelector('2-4 business days', 'label', 'for'));
        }

        this.getshippingoptiondivisionnew = function () {
            return element(by.xpath('html/body/div[4]/div/div[1]/div[3]/div[3]/div[1]/div[3]/div/div/form/div[2]/div[1]/div[1]/div'));
        }

        this.getshippingaddress1field = function () {
            return element(by.cssCustomSelector('{\'show-errors\' : shipForm.add1.$invalid && shipForm.isSubmitted, \'row-valid\': shipForm.add1.$valid}', 'div', 'ng-class')).element(by.tagName('input'));
        }

        this.getshippingaddress2field = function () {
            return element(by.cssCustomSelector("{'show-errors' : shipForm.add2.$invalid && (shipForm.add2.$dirty || shipForm.add2.$touched || shipForm.isSubmitted), 'row-valid': shipForm.add2.$valid && vm.checkOutInfo.shipInfo.add2 && vm.checkOutInfo.shipInfo.add2 != ''}", 'div', 'ng-class')).element(by.tagName('input'));
        }

        this.getcityfield = function () {
            return element(by.cssCustomSelector('{\'show-errors\' : shipForm.city.$invalid && shipForm.isSubmitted, \'row-valid\': shipForm.city.$valid}', 'div', 'ng-class')).element(by.tagName('input'));
        }

        this.getstatedivision = function () {
            return element(by.cssCustomSelector('{\'show-errors\' : shipForm.state.$invalid && shipForm.isSubmitted, \'row-valid\': shipForm.state.$valid}', 'div', 'ng-class'));
        }

        this.getzipdivision = function () {
            return element(by.cssCustomSelector('{\'show-errors\' : shipForm.zip.$invalid && shipForm.isSubmitted, \'row-valid\': shipForm.zip.$valid}', 'div', 'ng-class'));
        }

        this.getuseshippingaddressforbillingcheckboxdivision = function () {
            return element(by.xpath('html/body/div[4]/div/div[1]/div[3]/div[3]/div[1]/div[3]/div/div/form/div[2]/div[5]/div/label/div[2]/p[2]'));
        }

        this.getnameoncardfield = function () {
            return element(by.cssCustomSelector('{ \'show-errors\' : shipForm.nameOnCard.$invalid && (shipForm.nameOnCard.$dirty ||    shipForm.nameOnCard.$touched || shipForm.isSubmitted), \'row-valid\': shipForm.nameOnCard.$valid}', 'div', 'ng-class')).element(by.tagName('input'));
        }

        this.getcreditcardnumberdivision = function () {
            return element(by.cssCustomSelector('{ \'show-errors\':!shipForm.creditCardNumber.$valid && shipForm.isSubmitted, \'row-valid\': shipForm.creditCardNumber.$valid}', 'div', 'ng-class'));
        }
        this.getexpirationdatefield = function () {
            return element(by.cssCustomSelector('{ \'show-errors\' : shipForm.expiryDate.$invalid && shipForm.isSubmitted, \'row-valid\': shipForm.expiryDate.$valid}', 'div', 'ng-class')).element(by.tagName('input'));
        }

        this.getcvvdivision = function () {
            return element(by.cssCustomSelector('{ \'show-errors\' : shipForm.securityCode.$invalid && shipForm.isSubmitted, \'row-valid\': shipForm.securityCode.$valid}', 'div', 'ng-class'));
        }

        this.getchancetoreviewordertextdivision = function () {
            return element(by.xpath('html/body/div[4]/div/div[1]/div[3]/div[3]/div[1]/div[3]/div/div/form/div[2]/div[9]/label/p'));
        }
        this.getshippingCTAsection = function () {
            return element(by.xpath('html/body/div[4]/div/div[1]/div[3]/div[3]/div[1]/div[3]/div/div/form/div[2]/div[10]/button'));
        }

        this.getshippinginfoheader = function () {
            return element(by.xpath('html/body/div[3]/div/div[1]/div[3]/div[3]/div[1]/div[3]/div/div/div/h4'));
        };

        this.getAllShippingOptions = function () {
            return element.all(by.repeater("selectOpt in vm.checkOutInfo.initData.selectedShippingOptions | orderBy: 'costValue' "));
        };

        this.getShippingOptionLabel = function (RepeaterElement) {
            return RepeaterElement.element(by.cssCustomSelector('label-copy small paragraph-bold pull-left ng-binding', 'div', 'class'));
        };

        this.getshippingoptionsubtext = function (RepeaterElementshippingoption) {
            return RepeaterElementshippingoption.element(by.tagName('p'));
        };

        this.getshippingaddress1label = function () {
            return element(by.cssCustomSelector('{\'show-errors\' : shipForm.add1.$invalid && shipForm.isSubmitted, \'row-valid\': shipForm.add1.$valid}', 'div', 'ng-class')).element(by.cssCustomSelector('add1', 'label', 'for'));
        };

        this.getshippingaddress2label = function () {
            return element(by.cssCustomSelector("{'show-errors' : shipForm.add2.$invalid && (shipForm.add2.$dirty || shipForm.add2.$touched || shipForm.isSubmitted), 'row-valid': shipForm.add2.$valid && vm.checkOutInfo.shipInfo.add2 && vm.checkOutInfo.shipInfo.add2 != ''}", 'div', 'ng-class')).element(by.cssCustomSelector('add2', 'label', 'for'));
        }

        this.getshippingcitylabel = function () {
            return element(by.cssCustomSelector('{\'show-errors\' : shipForm.city.$invalid && shipForm.isSubmitted, \'row-valid\': shipForm.city.$valid}', 'div', 'ng-class')).element(by.cssCustomSelector('city', 'label', 'for'));
        }

        this.getshippingstatelabel = function () {
            return element(by.cssCustomSelector('{\'show-errors\' : shipForm.state.$invalid && shipForm.isSubmitted, \'row-valid\': shipForm.state.$valid}', 'div', 'ng-class')).element(by.cssCustomSelector('state', 'label', 'for'));
        }
        this.getshippingziplabel = function () {
            return element(by.cssCustomSelector('{\'show-errors\' : shipForm.zip.$invalid && shipForm.isSubmitted, \'row-valid\': shipForm.zip.$valid}', 'div', 'ng-class')).element(by.cssCustomSelector('zip', 'label', 'for'));
        }

        this.getnameoncardlabel = function () {
            return element(by.cssCustomSelector('{ \'show-errors\' : shipForm.nameOnCard.$invalid && (shipForm.nameOnCard.$dirty ||    shipForm.nameOnCard.$touched || shipForm.isSubmitted), \'row-valid\': shipForm.nameOnCard.$valid}', 'div', 'ng-class')).element(by.cssCustomSelector('nameOnCard', 'label', 'for'));
        }

        this.getcreditcardnumberlabel = function () {
            return element(by.cssCustomSelector("{ 'show-errors':shipForm.creditCardNumber.$invalid && shipForm.isSubmitted, 'row-valid': shipForm.creditCardNumber.$valid}", 'div', 'ng-class')).element(by.cssCustomSelector('creditCardNumber', 'label', 'for'));
        }

        this.getexpirationdatelabel = function () {
            return element(by.cssCustomSelector('{ \'show-errors\' : shipForm.expiryDate.$invalid && shipForm.isSubmitted, \'row-valid\': shipForm.expiryDate.$valid}', 'div', 'ng-class')).element(by.cssCustomSelector('expiryDate', 'label', 'for'));
        }
        this.getcvvlabel = function () {
            return element(by.cssCustomSelector('{ \'show-errors\' : shipForm.securityCode.$invalid && shipForm.isSubmitted, \'row-valid\': shipForm.securityCode.$valid}', 'div', 'ng-class')).element(by.cssCustomSelector('securityCode', 'label', 'for'));
        }

        this.getReviewOrderFooter = function () {
            return element(by.xpath('.//*[@id=\'tmobileApp\']/div[2]/div/div/p')).getText();
        };
        this.getSIMStarterkitLabel = function () {
            return element(by.xpath('.//*[@id=\'collapse0\']/div/div[4]/div[1]/p/span')).getText();
        };
        this.getMonthlySuperScript = function () {
            return element(by.xpath('.//*[@id=\'top\']/div/div[2]/div[3]/div[5]/div[2]/div/ul/li[3]/sup[2]')).getText();
        };

        this.getCheckOutFooter = function () {
            return element(by.cssCustomSelector("vm.sectionToBeDisplayed != 'confirmation'", "div", "ng-show")).element(by.tagName('p'));
        };

        this.getFraudCheckMessage = function () {
            return element(by.css(".AuthFailWrapper")).element(by.tagName("h6")).element(by.tagName("p"));
        };

        this.getFraudCheckIcon = function () {
            return element(by.css(".AuthFailWrapper")).element(by.tagName("img"));
        };

        this.getAuthFailGotItButton = function () {
            return element(by.cssCustomSelector("vm.close()", "button", "ng-click"));
        };

        this.getAuthFailCancelButton = function () {
            return element(by.cssCustomSelector("vm.close('authFailModal')", "button", "ng-click"));
        };

        this.getAverageCreditConfirmation = function () {
            var currentdatetime = new Date();
            console.log("Hours:" + currentdatetime.getHours() + ", Minutes:" + currentdatetime.getMinutes());
            if (currentdatetime.getHours() >= '8' && currentdatetime.getHours() <= '19') {
                if (currentdatetime.getHours() == '19' && currentdatetime.getMinutes() <= '30') {
                    return element(by.cssCustomSelector("vm.checkOutInfo.personalInfo.creditClass=='BUILDING' && vm.isOffHour == 'No' && !vm.checkOutInfo.reviewCart.isOnlyAccessory ", "div", "ng-show")).element(by.tagName('img'));
                }
                else if (currentdatetime.getHours() == '19' && currentdatetime.getMinutes() > '30') {
                    return element(by.cssCustomSelector("vm.checkOutInfo.personalInfo.creditClass=='BUILDING' && vm.isOffHour == 'Yes' && !vm.checkOutInfo.reviewCart.isOnlyAccessory ", "div", "ng-show")).element(by.tagName('img'));
                }
                else {
                    return element(by.cssCustomSelector("vm.checkOutInfo.personalInfo.creditClass=='BUILDING' && vm.isOffHour == 'No' && !vm.checkOutInfo.reviewCart.isOnlyAccessory ", "div", "ng-show")).element(by.tagName('img'));
                }
            }
            else {
                return element(by.cssCustomSelector("vm.checkOutInfo.personalInfo.creditClass=='BUILDING' && vm.isOffHour == 'Yes' && !vm.checkOutInfo.reviewCart.isOnlyAccessory ", "div", "ng-show")).element(by.tagName('img'));
            }
        };

        this.getConfirmationEmailIcon = function () {
            var currentdatetime = new Date();
            console.log("Hours:" + currentdatetime.getHours() + ", Minutes:" + currentdatetime.getMinutes());
            if (currentdatetime.getHours() >= '8' && currentdatetime.getHours() <= '19') {
                if (currentdatetime.getHours() == '19' && currentdatetime.getMinutes() <= '30') {
                    return element(by.cssCustomSelector("vm.authorValue.checkout.showboxprime", "div", "ng-if")).element(by.tagName('img'));
                }
                else if (currentdatetime.getHours() == '19' && currentdatetime.getMinutes() > '30') {
                    return element(by.cssCustomSelector("!vm.authorValue.checkout.offhoursshowboxprime", "div", "ng-if")).element(by.tagName('img'));
                }
                else {
                    return element(by.cssCustomSelector("vm.authorValue.checkout.showboxprime", "div", "ng-if")).element(by.tagName('img'));
                }
            }
            else {
                return element(by.cssCustomSelector("!vm.authorValue.checkout.offhoursshowboxprime", "div", "ng-if")).element(by.tagName('img'));
            }
        };

        this.getNoCreditConfirmationIcon = function () {
            return element(by.cssCustomSelector('vm.checkOutInfo.personalInfo.creditClass==\'NONE\' && vm.isOffHour == \'No\' && !vm.checkOutInfo.reviewCart.isOnlyAccessory ', 'div', 'ng-show')).element(by.tagName('img'));
        }

        this.getAwesomeExpertHeader = function () {
            return element(by.cssCustomSelector("(vm.checkOutInfo.personalInfo.creditClass=='GOOD'  || vm.checkOutInfo.reviewCart.flags.isOnlyAccessory)", "div", "ng-if")).element(by.tagName('h6'));
        }

        this.getAwesomeExpertImage = function () {
            return element(by.cssCustomSelector("(vm.checkOutInfo.personalInfo.creditClass=='GOOD'  || vm.checkOutInfo.reviewCart.flags.isOnlyAccessory)", "div", "ng-if")).element(by.tagName('img'));
        }

        this.getAverageExpertHeader = function () {
            return element(by.cssCustomSelector("(vm.checkOutInfo.personalInfo.creditClass=='BUILDING' && !vm.checkOutInfo.reviewCart.flags.isOnlyAccessory)", "div", "ng-if")).element(by.tagName('h6'));
        }

        this.getAverageExpertImage = function () {
            return element(by.cssCustomSelector("(vm.checkOutInfo.personalInfo.creditClass=='BUILDING' && !vm.checkOutInfo.reviewCart.flags.isOnlyAccessory)", "div", "ng-if")).element(by.tagName('img'));
        }

        this.getNoCreditExpertHeader = function () {
            return element(by.cssCustomSelector('vm.checkOutInfo.personalInfo.creditClass==\'NONE\'', 'div', 'ng-if')).element(by.tagName('h6'));
        }
        this.getNoCreditExpertImage = function () {
            return element(by.cssCustomSelector('vm.checkOutInfo.personalInfo.creditClass==\'NONE\'', 'div', 'ng-if')).element(by.tagName('img'));
        }

        /* E911 Address PageObjects */
        this.getE911Address1 = function () {
            // element(by.name('e911Address1')).clear();
            return element(by.name("e911Address1"));
        };

        this.getE911Address2 = function () {
            // element(by.name('e911Address2')).clear();
            return element(by.name("e911Address2"));
        };

        this.getE911City = function () {
            element(by.name('e911City')).clear();
            return element(by.name("e911City"));
        };

        this.clickE911State = function () {
            return element(by.name("e911State"));
        };

        this.selectE911State = function () {
            return element(by.name("e911State")).all(by.tagName('option'));
        };

        this.getE911State = function (RepeaterElement) {
            return RepeaterElement;
        };

        this.getE911Zip = function () {
            return element(by.name("e911Zip"));
        };

        this.getCheckSameAddressForE911 = function () {
            return element(by.id('e911CheckBox')).element(by.css(".indicator"));
        };

        this.getLabelOfFirstName = function () {
            return element(by.id("firstNameLabel"));
        };

        this.getLabelOfMiddleInitial = function () {
            return element(by.id("middle-Name-Label"));
        };

        this.getLabelOfLastName = function () {
            return element(by.id("lastNameLabel"));
        };

        this.getLabelOfEmail = function () {
            return element(by.id("emailLabel"));
        };

        this.getLabelOfConfirmEmail = function () {
            return element(by.id("confirmEmailLabel"));
        };

        this.getLabelOfPhoneNumber = function () {
            return element(by.id("phoneNumberLabel"));
        };

        this.getLabelOfCarrier = function () {
            return element(by.id("carrierLabel"));
        };

        this.getLabelOfShippingAddress1 = function () {
            return element(by.id("shipAddress1"));
        };

        this.getLabelOfShippingAddress2 = function () {
            return element(by.id("shipAddress2"));
        };

        this.getLabelOfShippingCity = function () {
            return element(by.id("shipCity"));
        };

        this.getLabelOfShippingState = function () {
            return element(by.id("shipState"));
        };

        this.getLabelOfShippingZip = function () {
            return element(by.id("shipZip"));
        };

        this.getLabelOfBillingAddress1 = function () {
            return element(by.id("billAddress1"));
        };

        this.getLabelOfBillingCity = function () {
            return element(by.id("billCity"));
        };

        this.getLabelOfBillingState = function () {
            return element(by.id("billState"));
        };

        this.getLabelOfBillingZip = function () {
            return element(by.id("billZip"));
        };

        this.getLabelOfCreditCard = function () {
            return element(by.id("creditCardNumber"));
        };

        this.getLabelOfCreditCardExpiry = function () {
            return element(by.id("expiryDate"));
        };

        this.getLabelOfCreditCardCVV = function () {
            return element(by.id("securityCode"));
        };

        this.getLabelOfIDType = function () {
            return element(by.id("idTypeLabel"));
        };

        this.getLabelOfIDNumber = function () {
            return element(by.id("idNumberLabel"));
        };

        this.getLabelOfIDExpiry = function () {
            return element(by.id("expiryDateLabel"));
        };

        this.getLabelOfIDState = function () {
            return element(by.id("stateLabel"));
        };

        this.getLabelOfSSN = function () {
            return element(by.id("ssnLabel"));
        };

        this.getLabelOfDOB = function () {
            return element(by.id("dobLabel"));
        };

        //checkout fields error messages

        this.getErrorOfFirstName = function () {
            return element(by.id("firstNameErrorMessage")).element(by.tagName("p"));
        };

        this.getErrorOfLastName = function () {
            return element(by.id("lastNameErrorMessage")).element(by.tagName("p"));
        };

        this.getErrorOfEmail = function () {
            return element(by.id("emailErrorMessage")).element(by.tagName("p"));
        };

        this.getErrorOfConfirmEmail = function () {
            return element(by.id("confirmEmailErrorMessage")).element(by.tagName("p"));
        };

        this.getErrorOfPhoneNumber = function () {
            return element(by.id("phoneNumberErrorMessage")).element(by.tagName("p"));
        };

        this.getErrorOfShippingAddress1 = function () {
            return element(by.id("shipAddress1Message")).element(by.tagName("p"));
        };

        this.getErrorOfShippingCity = function () {
            return element(by.id("cityerror")).element(by.tagName("p"));
        };

        this.getErrorOfShippingState = function () {
            return element(by.id("staterror")).element(by.tagName("p"));
        };

        this.getErrorOfShippingZip = function () {
            return element(by.id("ziperror")).element(by.tagName("p"));
        };

        this.getErrorOfBillingAddress1 = function () {
            return element(by.id("billAddress1Message")).element(by.tagName("p"));
        };

        this.getErrorOfBillingCity = function () {
            return element.all(by.id("cityerror")).get(1).element(by.tagName("p"));
        };

        this.getErrorOfBillingState = function () {
            return element.all(by.id("staterror")).get(1).element(by.tagName("p"));
        };

        this.getErrorOfBillingZip = function () {
            return element.all(by.id("ziperror")).get(1).element(by.tagName("p"));
        };

        this.getErrorOfCreditCard = function () {
            return element(by.id("creditCardNumbererror")).element(by.tagName("p"));
        };

        this.getErrorOfCreditCardExpiry = function () {
            return element(by.id("expiryDateerror")).element(by.tagName("p"));
        };

        this.getErrorOfCreditCardCVV = function () {
            return element(by.id("securityCodeerror")).element(by.tagName("p"));
        };

        this.getErrorOfIDType = function () {
            return element(by.id("idTypeErrorMessage")).element(by.tagName("p"));
        };

        this.getErrorOfIDNumber = function () {
            return element(by.id("idNumberErrorMessage")).element(by.tagName("p"));
        };

        this.getErrorOfIDExpiry = function () {
            return element(by.id("expiryDateerror")).element(by.tagName("p"));
        };

        this.getErrorOfIDSSN = function () {
            return element(by.id("ssnerror")).element(by.tagName("p"));
        };

        this.getErrorOfIDDOB = function () {
            return element(by.id("doberror")).element(by.tagName("p"));
        };

        this.getPersonalInfoNavBlade = function () {
            return element(by.cssCustomSelector("vm.setSectionToBeDisplayed('personalInfoSection')", "a", "ng-click"));
        };

        this.getShippingAndPaymentInfoNavBlade = function () {
            return element(by.cssCustomSelector("vm.setSectionToBeDisplayed('shippingInfoSection')", "a", "ng-click"));
        };

        this.getCreditCheckNavBlade = function () {
            return element(by.cssCustomSelector("vm.setSectionToBeDisplayed('creditCheckInfoSection')", "a", "ng-click"));
        };

        this.getSelectedIDTypeInCreditCheck = function () {
            return element(by.cssCustomSelector("vm.checkOutInfo.creditcheck.idType", "select", "ng-model")).element(by.cssCustomSelector("selected", "option", "selected"));
        };

        this.getSelectedCreditStateInCreditCheck = function () {
            return element(by.cssCustomSelector("vm.checkOutInfo.creditcheck.state", "select", "ng-model")).element(by.cssCustomSelector("selected", "option", "selected"));
        };

        this.getSelectedShippingState = function () {
            return element(by.cssCustomSelector("vm.formData.state", "select", "ng-model")).element(by.cssCustomSelector("selected", "option", "selected"));
        };

        this.getSelectedBillingState = function () {
            return element(by.name("billState")).element(by.cssCustomSelector("selected", "option", "selected"));
        };

        this.getSelectIDTypeTag = function () {
            return element(by.cssCustomSelector("vm.checkOutInfo.creditcheck.idType", "select", "ng-model"));
        };

        this.getPersonalInfoHeader = function () {
            return element(by.css('.personalinfo')).element(by.tagName("h4"));
        };

        this.getConsentText = function () {
            return element(by.cssCustomSelector("::vm.authorValue.checkout.userConsent[vm.userConsentIndex].consentText", "p", "ng-bind-html")).element(by.tagName("p"));
        };

        this.getMiddleName = function () {
            return element(by.id("middleName"));
        };

        this.getEntrustText = function () {
            return element(by.cssCustomSelector("::vm.authorValue.checkout.legaltext || 'Your information will not be used for any other purpose. For details about how we protect your information; see the T-Mobile <a>Privacy Policy</a>'", "div", "ng-bind-html"));
        };

        this.getCallUsHeader = function () {
            return element(by.css('.contactUs-mainBlock')).element(by.tagName("h5"));
        };

        this.getCallUsSubHeader = function () {
            return element(by.css('.contactUs-mainBlock')).element(by.tagName("h5")).element(by.xpath('following-sibling::p'));
        };

        this.getCallUsIcon = function () {
            return element(by.css('.contactUs-mainBlock')).element(by.tagName("img"));
        };

        this.getCallUsText = function () {
            return element(by.css('.contactUs-mainBlock')).element(by.tagName("img")).element(by.xpath('following-sibling::span'));
        };

        this.getCallUsNumber = function () {
            return element(by.css('.contactUs-mainBlock')).element(by.tagName("img")).element(by.xpath('following-sibling::span')).element(by.xpath('following-sibling::span'));
        };

        this.getPersonalInfoLegalFooter = function () {
            return element(by.cssCustomSelector("::vm.authorValue.checkout.legalfooterPersonal || 'Legal Footer:Personal Information Page'", "div", "ng-bind-html"));
        };

        this.getCopyrightText = function () {
            return element(by.css('.text-center-mob')).element(by.tagName("span"));
        };

        this.getTermsOfUseText = function () {
            return element(by.cssCustomSelector("vm.footerKeyValues.termsOfUseText!==undefined", "div", "ng-if")).element(by.tagName("a"));
        };

        this.getTermsAndConditionText = function () {
            return element(by.cssCustomSelector("vm.footerKeyValues.termsAndConditionsText!==undefined", "div", "ng-if")).element(by.tagName("a"));
        };

        this.getReturnPolicyText = function () {
            return element(by.cssCustomSelector("vm.footerKeyValues.returnPolicyText!==undefined", "div", "ng-if")).element(by.tagName("a"));
        };

        this.getPrivacyPolicyText = function () {
            return element.all(by.cssCustomSelector("padding-horizontal-small small", "div", "class")).last().element(by.tagName("a"));
        };

        this.getEspanolText = function () {
            return element.all(by.cssCustomSelector("padding-horizontal-small small", "div", "class")).last().element(by.xpath('following-sibling::div')).element(by.tagName("a"));
        };

        this.getShippingAndPaymentHeader = function () {
            return element(by.css('.shipInfo')).element(by.tagName("h4"));
        };

        /* E911 Address PageObjects */
        this.getE911Address1 = function () {
            element(by.name('e911Address1')).clear();
            return element(by.name("e911Address1"));
        };
        this.getE911Address2 = function () {
            element(by.name('e911Address2')).clear();
            return element(by.name("e911Address2"));
        };
        this.getE911City = function () {
            element(by.name('e911City')).clear();
            return element(by.name("e911City"));
        };
        this.clickE911State = function () {
            return element(by.name("e911State"));
        };
        this.selectE911State = function () {
            return element(by.name("e911State")).all(by.tagName('option'));
        };
        this.getE911State = function (RepeaterElement) {
            return RepeaterElement;
        };
        this.getE911Zip = function () {
            element(by.name('e911Zip')).clear();
            return element(by.name("e911Zip"));
        };
        this.getCheckSameAddressForE911 = function () {
            return element(by.id('e911CheckBox')).element(by.css(".indicator"));
        }

        this.getPersonalInfoHeader = function () {
            return element(by.cssCustomLocator("personalInfo", "div", "pdl-checkout-step")).element(by.tagName("h4"));
        };

        this.getShippingPaymentHeader = function () {
            return element(by.cssCustomSelector("vm.sectionToBeDisplayed == 'shippingInfoSection'", "div", "ng-if")).element(by.tagName("h4"));
        };

        this.getCreditCheckHeader = function () {
            return element(by.cssCustomSelector("vm.sectionToBeDisplayed == 'creditCheckInfoSection' && vm.checkOutInfo.personalInfo.creditClass!=='NONE'", "div", "ng-if")).element(by.tagName("h4"));
        };

        this.getCreditCheckSubHeader = function () {
            return element(by.cssCustomSelector("vm.sectionToBeDisplayed == 'creditCheckInfoSection' && vm.checkOutInfo.personalInfo.creditClass!=='NONE'", "div", "ng-if")).element(by.tagName("p"));
        };

        this.getCarrierDropdown = function () {
            return element(by.model("vm.checkOutInfo.personalInfo.carrier"));
        };

        this.getuserconsenttext = function () {
            return element(by.cssCustomSelector("::vm.authorValue.checkout.userConsent[vm.userConsentIndex].consentText", "p", "ng-bind-html"));
        };

        this.getPersonalInfoSectionBladeText = function () {
            return element(by.cssCustomSelector("vm.setSectionToBeDisplayed('personalInfoSection')", "a", "ng-click")).element(by.cssCustomSelector("ng-binding", "span", "class"));
        };

        this.getPersonalInfoSectionBladeCarat = function () {
            return element(by.cssCustomSelector("vm.setSectionToBeDisplayed('personalInfoSection')", "a", "ng-click")).element(by.cssValueBasedSelector("glyphicon-chevron-down", "span", "class"));
        };

        this.getShippingPaymentSectionBladeText = function () {
            return element(by.cssCustomSelector("vm.setSectionToBeDisplayed('shippingInfoSection')", "a", "ng-click")).element(by.cssCustomSelector("ng-binding", "span", "class"));
        };

        this.getShippingPaymentSectionBladeCarat = function () {
            return element(by.cssCustomSelector("vm.setSectionToBeDisplayed('shippingInfoSection')", "a", "ng-click")).element(by.cssValueBasedSelector("glyphicon-chevron-down", "span", "class"));
        };

        this.getCreditCheckSectionBladeText = function () {
            return element(by.cssCustomSelector("vm.setSectionToBeDisplayed('creditCheckInfoSection')", "a", "ng-click")).element(by.cssCustomSelector("ng-binding", "span", "class"));
        };

        this.getCreditCheckSectionBladeCarat = function () {
            return element(by.cssCustomSelector("vm.setSectionToBeDisplayed('creditCheckInfoSection')", "a", "ng-click")).element(by.cssValueBasedSelector("glyphicon-chevron-down", "span", "class"));
        };

        this.getSameShipAddressBillAddressText = function () {
            return element(by.id("billCheckBox")).element(by.cssCustomSelector("::vm.addressAuthorValue.checkboxText", "p", "ng-bind-html"));
        };

        this.getSameShipAddressE911AddressText = function () {
            return element(by.id("e911CheckBox")).element(by.cssCustomSelector("::vm.addressAuthorValue.checkboxText", "p", "ng-bind-html"));
        };

        // Sprint 23 Pageobjects

        this.getAutoPayHeader = function () {
            return element(by.cssCustomSelector("text-center", "h3", "class")).element(by.tagName('strong'));
        };

        this.getToggleONName = function () {
            return element(by.cssCustomSelector("vm.checkOutInfo.autoPay", "p", "ng-if"));
        };

        this.getToggleONLabel = function () {
            return element(by.cssCustomSelector("vm.checkOutInfo.autoPay", "h3", "ng-if"));
        };

        this.getToggleOFFName = function () {
            return element(by.cssCustomSelector("!vm.checkOutInfo.autoPay", "p", "ng-if"));
        };

        this.getToggleOFFLabel = function () {
            return element(by.cssCustomSelector("!vm.checkOutInfo.autoPay", "h3", "ng-if"));
        };

        this.getAutopaySwitch = function () {
            // return element(by.id("autoPaySwitch"));
            return element(by.cssCustomSelector("autoPaySwitch", "label", "for"));
        };

        this.getDetailsModalLink = function () {
            //return element(by.cssCustomSelector("::vm.authorValue.checkout.shippingInfo.autoPayDescription","p","bind-html-compile")).element(by.tagName('a'));
            return element(by.cssCustomSelector("Link to Autopay modal:", "a", "dialog-description"))
        };

        this.getModalCloseButton = function () {
            return element(by.cssCustomSelector("$dismiss()", "button", "ng-click"));
        };

        this.getTermsandConditionsModalLink = function () {
            return element(by.cssCustomSelector("Link to terms and condition modal:", "a", "dialog-description"));
        };

        this.getConfirmationUserName = function () {
            return element(by.css('div[ng-if="vm.confirmationString.greetLine && vm.confirmationString.greetDesc"]')).element(by.tagName('h2'));
        }

        this.getSaturdayShippingOption = function () {
            var currentdatetime = new Date();
            console.log("Day of the Week:" + currentdatetime.getDay());
            if (currentdatetime.getDay() > 3 && currentdatetime.getDay() < 6) {
                return element.all(by.repeater("selectOpt in vm.checkOutInfo.initData.selectedShippingOptions | orderBy: 'costValue' ")).get(2).element(by.cssCustomSelector('label-copy small paragraph-bold pull-left ng-binding', 'div', 'class'));
            }
            else {
                return element.all(by.repeater("selectOpt in vm.checkOutInfo.initData.selectedShippingOptions | orderBy: 'costValue' ")).get(1).element(by.cssCustomSelector('label-copy small paragraph-bold pull-left ng-binding', 'div', 'class'));
            }
        };


    };
    module.exports = function () {
        return new Checkout();
    };


}());