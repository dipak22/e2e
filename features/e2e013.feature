#features/e2e013.feature
Feature: Running Cucumber with Protractor
    As u user, I want to place an order with 2 tablet line having same SKU
    and one with Full payment and other with the monthly payment mode
    and similarly two phone lines having the same SkU and different payment modes

  Scenario: Launch internet device browse page and select credit class
    When I navigate to T-Mobile Tablets page
    Then I select Average Credit

  Scenario: Select Apple iPad from main browse page
    When I select "Apple iPad Pro 12.9-inch" from main browse
    Then I navigate to Product detail page of "Apple iPad Pro 12.9-inch"

  Scenario: Change Credit Type, Memory, Payment Option and Color on PDP and look for reviews,legal text, Sim Kit
    Given I am on Product detail page of "Apple iPad Pro 12.9-inch"
    Then I should see review stars on main PDP
    And I should see total ratings on main PDP
    Then I select Awesome credit from main PDP
    Then I select color swatch from main PDP
    Then I select memory from main PDP
    Then I should see EIP legal text on main PDP
    Then I should see sim kit on main PDP

  Scenario: Add device to cart
    And I add "Apple iPad Pro 12.9-inch" to cart by clicking on 'Add to cart' button
    Then I validate the selected device is added to cart

  Scenario: Duplicate the added internet device
    Then I duplicate the added device line
    Then validate the duplicate internet device is added to cart

  Scenario: Edit the internet device to change the payment type to full and select
    And I click on edit button for internet device
    And I look for payment option dropdown
    And I select full price from payment option
    Then I click on add to cart button on mini pdp

  Scenario: Add cell phones as third line
    And I click on add a phone button
    Then I click on plus button for phones
    And I click on 'Add a new phone' button to view the mini browse
    Then I select a cell phone from mini browse
    And I add the selected cell phone to cart.

  Scenario: Duplicate the added cell phone
    And I duplicate the added  cell phone line
    Then validate the duplicate cell phone is added to cart

  Scenario: Edit the cell-phones to change the payment type to full and select
    And I click on edit button for phone line
    And I look for payment option dropdown
    And I select full price from payment option
    Then validate full price payment option is selected
    Then I click on add to cart button on mini pdp

  Scenario: Navigate to checkout
    Given I am in a loaded cart page
    When I click on the 'Checkout' button
    Then the browser will navigate to the checkout section

  Scenario: Checkout - Personal Information
    Given I see the Personal Information section
    And I enter "SDET" in the first name
    And I enter "T" in the middle initial
    And I enter "TMO" in the last name
    And I enter "qat.tmo@gmail.com" in the email address
    And I enter "4254254250" in the phone number

  Scenario: Checkout - Shipping Information
    Given I see the Shipping section
    And I enter "10 main st" in the shipping address
    And I enter "Beverly Hills" in the city
    And I select "CA" in the State
    And I enter "90210" in the zip
    And I click on user-consent checkbox
    And I click on 'Next' button
    Then the browser will navigate to the Payment Information page

  Scenario: Checkout - Payment Information
    Given I am on the Payment and Credit Page
    And I enter "4444444444444448" in the credit card
    And I enter "0122" in the Expiry date
    And I enter "111" in the CVV

  Scenario: Checkout - Credit Information
    Given I see the Credit check section
    And I select "Passport" in the id
    And I enter "A1234567890" in the id
    And I enter "0122" in the expiry date
    And I enter "001110000" in the ssn
    And I enter "12/24/1984" in the dob
    And I click on the 'Agree and Next' button
    Then the browser will navigate to Review order page

  Scenario: Review Order details and submit
    Given I am on review order page
    When I click on 'I Agree Disclaimer' Label for Terms and Conditions
    And I click on 'Submit Order' Button
    Then I should see the order confirmation page
