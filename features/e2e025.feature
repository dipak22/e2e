#features/e2e025.feature
Feature: Running Cucumber with Protractor
As a user I want to add three different accessories to the cart
and edit the cart from review section
and need to delete the first accessory
and add a new accessory

	Scenario: Launch t-mobile site and navigate to Accessories browse page
    Given I navigate to T-Mobile Accessories page
    Then I select Average Credit

	Scenario: Select Belkin MIXIT™ Metallic Car Charger - Gold from main browse page
    When I select "Belkin MIXIT™ Metallic Car Charger - Gold" accessory from main browse
    Then I navigate to Product detail page of "Belkin MIXIT™ Metallic Car Charger - Gold"

	Scenario: Add device to cart
    Given I am on Product detail page of "Belkin MIXIT™ Metallic Car Charger - Gold"
    And I click on 'Add to cart' button in PDP Page
    Then I see the selected accessory is added to the cart on first tile

    Scenario: Adding Second accessory from Speakers Category to the Cart
    Given I am in Cart page
    When I click on Accessory tile
    Then I should see Accessory mini Browse
    When I select first tab in accessory mini browse
    And I select accessory which is under first tab in mini browse
    Then I should see accessory product description page
    When I click on 'Add to Cart' Button on accessory mini PDP
    Then I see the selected accessory is added to the cart on tile "2"

	Scenario: Adding Third accessory from Chargers Category to the Cart
    Given I am in Cart page
    When I click on Accessory tile
    Then I should see Accessory mini Browse
    When I select third tab in accessory mini browse
    And I select accessory which is under third tab in mini browse
    Then I should see accessory product description page
    When I click on 'Add to Cart' Button on accessory mini PDP
    Then I see the selected accessory is added to the cart on tile "3"

	Scenario: Navigate to checkout
    Given I am in a loaded cart page
    When I click on the 'Checkout' button
    Then the browser will navigate to the checkout section

	Scenario: Checkout - Personal Information
    Given I see the Personal Information section
    And I enter "SDET" in the first name
    And I enter "T" in the middle initial
    And I enter "TMO" in the last name
    And I enter "qat.tmo@gmail.com" in the email address
    And I enter "4254254250" in the phone number

	Scenario: Checkout - Shipping Information
    Given I see the Shipping section
    And I enter "10 main st" in the shipping address
    And I enter "Beverly Hills" in the city
    And I select "CA" in the State
    And I enter "90210" in the zip
    And I click on user-consent checkbox
    And I click on 'Next' button
    Then the browser will navigate to the Payment Information page
        
	Scenario: Checkout - Payment Information
    Given I am on the Payment and Credit Page
    And I enter "4444444444444448" in the credit card
    And I enter "0122" in the Expiry date
    And I enter "111" in the CVV
    And I click on the 'Continue' button
    Then the browser will navigate to Review order page

	Scenario: Edit the cart from Review Order details page
    Given I am on review order page
    When I click on Edit in Cart link in review order page
    Then I should be taken back to cart page

	Scenario: Delete the second accessory from cart
    And I click on edit accessory option for accessory two
    And I click on delete button for accessory
    Then I remove the second accessory from cart

	Scenario: Adding another accessory to cart
    Given I am in Cart page
    When I click on Accessory tile
    Then I should see Accessory mini Browse
    When I select second tab in accessory mini browse
    And I select accessory which is under second tab in mini browse
    Then I should see accessory product description page
    When I click on 'Add to Cart' Button on accessory mini PDP
    Then I see the selected accessory is added to the cart on tile "2"

	Scenario: Navigate to checkout
    Given I am in a loaded cart page
    When I click on the 'Checkout' button
    Then the browser will navigate to the checkout section

	Scenario: Check and re-enter required checkout form fields
    Given I am on Info and Shipping page
    And I click on user-consent checkbox
    And I click on 'Next' button
    Then the browser will navigate to the Payment Information page
        
	Scenario: Checkout - Payment Information
    Given I am on the Payment and Credit Page
    And I enter "4444444444444448" in the credit card
    And I enter "0122" in the Expiry date
    And I enter "111" in the CVV
    And I click on the 'Continue' button
    Then the browser will navigate to Review order page

	Scenario: Review Order details and submit
    Given I am on review order page
    When I click on 'I Agree Disclaimer' Label for Terms and Conditions
    And I click on 'Submit Order' Button
    Then I should see the order confirmation page
