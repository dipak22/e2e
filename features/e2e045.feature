Feature:
    As a user
	When I click on "Login to MyTMO" link on a Mobile Internet Devices PDP, Product details should be passed in cookies and should navigate to My TMO website

	Scenario: Launch t-mobile site and navigate to MI browse page
    Given I navigate to T-Mobile Tablets page
    Then I select Awesome Credit

	Scenario: Select Apple iPad mini 4 from main browse page
    When I select "Apple iPad mini 4" from main browse
    Then I navigate to Product detail page of "Apple iPad mini 4"
	
	Scenario: Navigating to MyTMO site
    Given I am on Product detail page of "Apple iPad mini 4"
    When I click on "Login to T-Mobile" link on PDP
	Then I see My TMO Homepage
	
	Scenario: Login to MyTMO Application
	Given I see My TMO Homepage
	When I enter "4047297321" in Username field
	And I enter "TM0Test1" in Password field
	And I click on Login Button
	Then I see MyTMO Lines Page
	
	Scenario: Viewing line details
	Given I see MyTMO Lines Page
	When I select my line
	
	Scenario: Verifying Cookie details
	Given I see MyTMO line selector page
	When I get the data from cookie
	Then I see "888462376952" as SKU in Cookies
	And I see "Tablet" as Product Type in Cookies
	And I see "EIP" as paymentoption in Cookies
	#And I see "Save 300 on your Iphone" as Promo Name in Cookies
	And I see "g-25F4ED7F9071445DBF0DA3DED3FBC45C" as Product Family in Cookies
	
	Scenario: Launch t-mobile site and navigate to MI browse page
    Given I navigate to T-Mobile Tablets page
    Then I select Average Credit

	Scenario: Select Samsung Gear S3 frontier from main browse page
    When I select "Samsung Gear S3 frontier" from main browse
    Then I navigate to Product detail page of "Samsung Gear S3 frontier"
	
	Scenario: Validating Upgrade and Add a Line Buttons on PDP
    Given I am on Product detail page of "Samsung Gear S3 frontier"
	Then I should see 'Upgrade' and 'Add a Line' buttons in Product detail Page
	And I see 'Upgrade' button in disabled state
	
	Scenario: Navigation to MyTMO using Add a Line Button
	Given I am on Product detail page of "Samsung Gear S3 frontier"
	When I click on 'Add a Line' button on PDP
	Then I see MyTMO line selector page
	
	Scenario: Verifying Cookie details
	Given I see MyTMO line selector page
	When I get the data from cookie
	Then I see "888462376952" as SKU in Cookies
	And I see "Tablet" as Product Type in Cookies
	And I see "EIP" as paymentoption in Cookies
	And I see "Save 300 on your Iphone" as Promo Name in Cookies
	And I see "g-25F4ED7F9071445DBF0DA3DED3FBC45C" as Product Family in Cookies
	
	
