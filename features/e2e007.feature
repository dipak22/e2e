Feature: Running Cucumber with Protractor #e2e007 Feature
  As a user I want to add an accessory whose cost is greater than $69
  and phone line with Zip based services and
  need to remove the existing accessory from cart
  and add two accessories where the cost of first accessory is less than $69
  and second is greater than than $69

  Scenario: Navigate to Accessories browse page and select credit class
    Given I navigate to T-Mobile Accessories page
    Then I select Average Credit

  Scenario: Apply sorting
    Then I click on Sorting Drop Down in Accessory browse page
    And I select sort value as Price High To Low
    And I validate Price High To Low is selected in sort options

  Scenario: Select "Beats Solo3 Wireless On-Ear Headphones - Rose Gold" from main browse page
    When I select "Beats Solo3 Wireless On-Ear Headphones - Rose Gold" accessory from main browse
    Then I navigate to Product detail page of "Beats Solo3 Wireless On-Ear Headphones - Rose Gold"
    Then I select Average credit from main PDP
    Then I add selected accessory to cart

  Scenario: On cart page Add phone
    Given I am in Cart page
    When I click on Add a Phone Button for adding new line
    And I click on Device Image on Device tile in the first line
    When I click on add a new phone button
    Then I should see new line added to cart
    When I select a Phone from Mini Browse
    #Then I should see Product name on Mini PDP of Phone
    When I click on 'Add to cart' button

  Scenario: Remove the already added accessory and add another accessory
    Given I am in a loaded cart page
    When  I click on edit accessory option for accessory one
    Then  I should see option for delete and edit
    Then  I should see again a confirmation to delete the accessorry added to cart

  Scenario: On Cart Page Add an Accessory from mini PDP
    Given I am in Cart page
    When I click on Accessory tile
    Then I should see Accessory mini Browse
    When I select third tab in accessory mini browse
    Then I select accessory which is under third tab in mini browse
      #Then I should see accessory product description page
    Then I click on 'Add to Cart' Button on accessory mini PDP

  Scenario: Add another accessory to the cart from Mini PDP
    Given I am in Cart page
    When I click on Accessory tile
    Then I should see Accessory mini Browse
    When I select second tab in accessory mini browse
    Then I select accessory which is under second tab in mini browse
      #Then I should see accessory product description page
    Then I click on 'Add to Cart' Button on accessory mini PDP

  Scenario: Navigate to checkout
    Given I am in a loaded cart page
    When I click on the 'Checkout' button
    Then the browser will navigate to the checkout section

  Scenario: Checkout - Personal Information
    Given I see the Personal Information section
    And I enter "SDET" in the first name
    And I enter "T" in the middle initial
    And I enter "TMO" in the last name
    And I enter "qat.tmo@gmail.com" in the email address
    And I enter "4254254250" in the phone number

  Scenario: Checkout - Shipping Information
    Given I see the Shipping section
    And I enter "10 main st" in the shipping address
    And I enter "Beverly Hills" in the city
    And I select "CA" in the State
    And I enter "90210" in the zip
    And I click on user-consent checkbox
    And I click on 'Next' button
    Then the browser will navigate to the Payment Information page

  Scenario: Checkout - Payment Information
    Given I am on the Payment and Credit Page
    And I enter "4444444444444448" in the credit card
    And I enter "0122" in the Expiry date
    And I enter "111" in the CVV

  Scenario: Checkout - Credit Information
    Given I see the Credit check section
    And I select "Passport" in the id
    And I enter "A1234567890" in the id
    And I enter "0122" in the expiry date
    And I enter "001110000" in the ssn
    And I enter "12/24/1984" in the dob
    And I click on the 'Agree and Next' button
    Then the browser will navigate to Review order page

  Scenario: Review Order details and submit
    Given I am on review order page
    When I click on 'I Agree Disclaimer' Label for Terms and Conditions
    And I click on 'Submit Order' Button
    Then I should see the order confirmation page
