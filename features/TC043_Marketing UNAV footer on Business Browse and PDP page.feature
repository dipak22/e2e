#features/UNAV-footer.feature
Feature: This feature with cover the scenario for marketing UNAV footer on business browse page and PDP page,need to make sure marketing toggle is on before
  we execute these steps

  Scenario: Navigate to cell phones page and check all the UNAV footer component is available
    Given I am on "http://qat.business.t-mobile.com/cell-phones"
    Then I should see "CONNECT WITH AN @WORK EXPERT" business link1
    Then I should see "REACH T-MOBILE BUSINESS CARE" business link2
    Then I should see "ADVANTAGE PROGRAM" business link3

  Scenario: Navigate to PDP page and check all the UNAV footer component is avilable
    Given I am on "http://qat.business.t-mobile.com/cell-phone/apple-iphone-7-plus?color=black"
    Then I should see "CONNECT WITH AN @WORK EXPERT" business link1
    Then I should see "REACH T-MOBILE BUSINESS CARE" business link2
    Then I should see "ADVANTAGE PROGRAM" business link3
