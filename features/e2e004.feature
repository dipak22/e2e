#features/e2e004.feature
Feature: Ordering a phone with a compatible accessory
  As a user I want to add a Phone and a Compatible accessory of cost greater than $69 with Quantity as 1,
  And from Checkout review section, I need to edit the cart and delete the phone line and Change the Shippping option to overnight shipping Shipping.

Scenario: Navigate to cell-phones browse page and select credit class
    Given I navigate to T-Mobile cell-phones page
    Then I select Average Credit

Scenario: Select 'Apple iPhone 7 Plus' from main browse page
    When I select "Apple iPhone 7 Plus" from main browse
    Then I navigate to Product detail page of "Apple iPhone 7 Plus"

  Scenario: Change Credit Type, Memory, Payment Option and Color on PDP nd look for reviews,legal text, Sim Kit
    Given I am on Product detail page of "Apple iPhone 7 Plus"
    Then I should see review stars on main PDP
    And I should see total ratings on main PDP
    Then I select Awesome credit from main PDP
    Then I select color swatch from main PDP
    Then I select memory from main PDP
    Then I select full price as payment option from main PDP
    Then I should see sim kit on main PDP

  Scenario: Add device to cart
    And I add "Apple iPhone 7 Plus" to cart by clicking on 'Add to cart' button
    Then I validate the selected device is added to cart

Scenario: Add accessory form the mini Browse
    Given I am in Cart page
    When I click on Accessory tile
    Then I should see Accessory mini Browse
    When I select second tab in accessory mini browse
    Then I select accessory which is under second tab in mini browse
    Then I should see accessory product description page
    When I click on add to cart button on mini pdp accessory
    Then I see the selected accessory is added to the cart

Scenario: Navigate to checkout
    Given I am in a loaded cart page
    When I click on the 'Checkout' button
    Then the browser will navigate to the checkout section

Scenario: Checkout - Personal Information
    Given I see the Personal Information section
    And I enter "SDET" in the first name
    And I enter "T" in the middle initial
    And I enter "TMO" in the last name
    And I enter "qat.tmo@gmail.com" in the email address
    And I enter "4254254250" in the phone number

Scenario: Checkout - Shipping Information
    Given I see the Shipping section
    And I enter "10 main st" in the shipping address
    And I enter "Beverly Hills" in the city
    And I select "CA" in the State
    And I enter "90210" in the zip
    And I click on user-consent checkbox
    And I click on 'Next' button
    Then the browser will navigate to the Payment Information page

Scenario: Checkout - Payment Information
    Given I am on the Payment and Credit Page
    And I enter "4444444444444448" in the credit card
    And I enter "0122" in the Expiry date
    And I enter "111" in the CVV

Scenario: Checkout - Credit Information
    Given I see the Credit check section
    And I select "Passport" in the id
    And I enter "A1234567890" in the id
    And I enter "0122" in the expiry date
    And I enter "001110000" in the ssn
    And I enter "12/24/1984" in the dob
    And I click on the 'Agree and Next' button
    Then the browser will navigate to Review order page

Scenario: Edit the cart from Review Order details page
    Given I am on review order page
    When I click on edit cart on review page
    Then I should be taken back to cart page

Scenario: On cart page remove the already added Phone line
    When I Click on Remove line link in Cart Row

Scenario: Navigate to checkout
    Given I am in a loaded cart page
    When I click on the 'Checkout' button
    Then the browser will navigate to the checkout section

Scenario: Check and re-enter required checkout form fields
    Given I see the Shipping section
    Then I select Overnight shipping option
    And I click on user-consent checkbox
    And I click on 'Next' button
    Then the browser will navigate to the Payment Information page

Scenario: Checkout - Payment Information
    Given I am on the Payment and Credit Page
    And I enter "4444444444444448" in the credit card
    And I enter "0122" in the Expiry date
    And I enter "111" in the CVV
    And I click on the 'Agree and Next' button
    Then the browser will navigate to Review order page

Scenario: Review Order details and submit
    Given I am on review order page
    When I click on 'I Agree Disclaimer' Label for Terms and Conditions
    And I click on 'Submit Order' Button
    Then I should see the order confirmation page
