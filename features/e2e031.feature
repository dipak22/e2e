#features/e2e031.feature
Feature: Running Cucumber with Protractor
  As a user I want to add a phone BYOD, Tablet BYOD
  and add multiple accessories to the cart
  and submit the order


	Scenario: Launch t-mobile site and navigate to cell-phones browse page
    Given I navigate to T-Mobile cell-phones page
    Then I select Average Credit

	Scenario: Select T-Mobile 3-in-1 SIM Starter Kit from main browse page
    When I select "T-Mobile 3-in-1 SIM Starter Kit" from main browse
    Then I navigate to Product detail page of "T-Mobile 3-in-1 SIM Starter Kit"

	Scenario: Add device to cart
    Given I am on Product detail page of "T-Mobile 3-in-1 SIM Starter Kit"
    And I click on 'Add to cart' button in PDP Page
    Then I validate the selected device is added to Cart

	Scenario: Adding MI Sim kit to Cart in Second Line
    Given I am in Cart Page
    When I click on Add a Tablet Button
    And I click on Add Device Icon in device tile for adding Tablet
    And I click on Bring your own Tablet button in device tile
    Then I should see Sim kit Mini PDP
    And I should see Tablet Sim Kit Product name on Mini PDP
    When I click on Add to Cart Button on SIM kit Product Description Page
    Then I should see MI Sim kit added to cart in second line

	Scenario: Adding Accessory from Speakers Category to the Cart
    Given I am in Cart Page
    When I click on Accessory tile
    Then I should see Accessory mini Browse
    When I select first tab in accessory mini browse
    And I select accessory which is under first tab in mini browse
    Then I should see accessory product description page
    When I click on 'Add to Cart' Button on accessory mini PDP
    Then I see the selected accessory is added to the cart on tile "1"

	Scenario: Adding Accessory from Chargers Category to the Cart
    Given I am in Cart Page
    When I click on Accessory tile
    Then I should see Accessory mini Browse
    When I select third tab in accessory mini browse
    And I select accessory which is under third tab in mini browse
    Then I should see accessory product description page
    When I click on 'Add to Cart' Button on accessory mini PDP
    Then I see the selected accessory is added to the cart on tile "2"

	Scenario: Navigate to checkout
    Given I am in a loaded cart page
    When I click on the 'Checkout' button
    Then the browser will navigate to the checkout section

	Scenario: Checkout - Personal Information
    Given I see the Personal Information section
    And I enter "SDET" in the first name
    And I enter "T" in the middle initial
    And I enter "TMO" in the last name
    And I enter "qat.tmo@gmail.com" in the email address
    And I enter "4254254250" in the phone number

	Scenario: Checkout - Shipping Information
    Given I see the Shipping section
    And I enter "10 main st" in the shipping address
    And I enter "Beverly Hills" in the city
    And I select "CA" in the State
    And I enter "90210" in the zip
    And I click on user-consent checkbox
    And I click on 'Next' button
    Then the browser will navigate to the Payment Information page
        
	Scenario: Checkout - Payment Information
    Given I am on the Payment and Credit Page
    And I enter "4444444444444448" in the credit card
    And I enter "0122" in the Expiry date
    And I enter "111" in the CVV

	Scenario: Checkout - Credit Information
    Given I see the Credit check section
    And I select "Passport" in the id
    And I enter "A1234567890" in the id
    And I enter "0122" in the expiry date
    And I enter "001110000" in the ssn
    And I enter "12/24/1984" in the dob
    And I click on the 'Agree and Next' button
    Then the browser will navigate to Review order page

	Scenario: Review Order details and submit
    Given I am on review order page
    When I click on 'I Agree Disclaimer' Label for Terms and Conditions
    And I click on 'Submit Order' Button
    Then I should see the order confirmation page
