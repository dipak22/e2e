#features/e2e038.feature
Feature:
	As a user,
	I want to see the Promotion tradein information is switched to Standard tradein information and vice versa,
	When I switch the Credit Class between Credit and No Credit and Payment options between Monthly and Full Payments.
	
	Scenario: Launch cell-phones browse page
    Given I navigate to T-Mobile cell-phones page
	Then I select Average Credit

	Scenario: Select 'LG G Stylo™ - Certified Pre-Owned' from main browse page
    When I select "LG G Stylo™ - Certified Pre-Owned" from Main Browse
    Then I navigate to Product detail page of "LG G Stylo™ - Certified Pre-Owned"
	
	Scenario: Add device to cart
    Given I am on Product detail page of "LG G Stylo™ - Certified Pre-Owned"
	#When I select "32 GB" as memory of the device from memory variants dropdown list
	#And I select color as "GOLD"
    And I add "LG G Stylo™ - Certified Pre-Owned" to cart by clicking on 'Add to cart' button
    Then I validate the selected device is added to cart
	
	Scenario: Navigate to TQT Tool
	Given I am in Cart page
	When I Click on Trade in Button
	Then I should see TQT Modal
	
	Scenario: Promotion Tradein Flow Validation with Phone and Device Condition as Good
	Given I am in TQT Modal
	When I Select Carrier as "T-Mobile"
	And I Select Make as "Samsung"
	And I Select Model as "SM-G550T Galaxy On5 8GB Black - T-Mobile"
	And I enter "357759080728132" in IMEI field and click on Apply button
	And I select Device Condition as Good
	And I click on Get Estimate Button
	Then I should see Confirmation Modal
	
	Scenario: Selecting Promotion Tradein option and cliking on Agree and Next Button.
    Given I am in TQT Confirmation Modal
    When I select promotion trade in option
	And I click on Agree & Continue Button in Promo Flow
	Then I should see Promo Notification banner
	
	Scenario: Validating Tradein Information on Device Tile
	When I accept trade in and returned to the cart 
	Then I see Tradein Header on device tile
	And I see the Model Name of the traded device on line "1" in Promo Flow
	And I see the IMEI Number of the traded device on line "1" in Promo Flow
	And I see the Trade in Promo Applied text on line "1" in Promo Flow
	And I see trade in value link on line "1" in Promo Flow
	And I see Remove link to remove tradein on line "1"
	
	Scenario: Clicking on Price Breakdown link on Sticky Banner
	Given I am in Cart page
	When I click on PriceBreakdown link on Sticky Banner
	Then I should see PriceBreakdown Modal
	
	Scenario: Validating the Tradein Information on PriceBreakdown
	When I am in PriceBreakdown Modal
	Then I see my added devices in the modal
	And I see my promo trade in device value
	And I see my promo trade in device model name
	And I see my promo trade in device header and legal text
	And I close the PriceBreakdown Modal
	
	Scenario: Changing the credit class from Awesome Credit to No Credit
	Given I am in Cart page
	When I click on credit class on sticky banner
	Then I should see credit class modal
	When I select No credit radio button
	And I click on 'Done' button
	Then I should see credit class changed in sticky banner
	
	Scenario: Validating Standard Tradein Information on Device Tile
	Given I am in Cart page 
	Then I see Tradein Header on device tile
	And I see the Model Name of the traded device on line "1" in Standard Flow
	And I see the IMEI Number of the traded device on line "1" in Standard Flow
	And I see the Trade in value of the traded device on line "1" in Standard Flow
	And I see trade in value link on line "1"
	And I see Remove link to remove tradein on line "1"
	
	Scenario: Clicking on Price Breakdown link on Sticky Banner
	Given I am in Cart page
	When I click on PriceBreakdown link on Sticky Banner
	Then I should see PriceBreakdown Modal
	
	Scenario: Validating the Tradein Information on PriceBreakdown
	When I am in PriceBreakdown Modal
	Then I see my added devices in the modal
	And I see my trade in device value
	And I see my trade in device model name
	And I see my trade in device header and legal text
	And I see trade in value link in the modal
	And I close the PriceBreakdown Modal
	
	Scenario: Changing the credit class from No Credit to Awesome Credit
	Given I am in Cart page
	When I click on credit class on sticky banner
	Then I should see credit class modal
	When I select Awesome credit radio button
	And I click on 'Done' button
	Then I should see credit class changed in sticky banner
	
	Scenario: Validating Tradein Information on Device Tile
	Given I am in Cart page 
	Then I see Tradein Header on device tile
	And I see the Model Name of the traded device on line "1" in Promo Flow
	And I see the IMEI Number of the traded device on line "1" in Promo Flow
	And I see the Trade in Promo Applied text on line "1" in Promo Flow
	And I see trade in value link on line "1" in Promo Flow
	And I see Remove link to remove tradein on line "1"
	
	Scenario: Navigate to checkout
    Given I am in a loaded cart page
    When I click on the 'Checkout' button
    Then the browser will navigate to the checkout section

	Scenario: Checkout - Personal Information
    Given I see the Personal Information section
    And I enter "SDET" in the first name
    And I enter "T" in the middle initial
    And I enter "TMO" in the last name
    And I enter "qat.tmo@gmail.com" in the email address
    And I enter "4254254250" in the phone number

	Scenario: Checkout - Shipping Information
    Given I see the Shipping section
    And I enter "10 main st" in the shipping address
    And I enter "Beverly Hills" in the city
    And I select "CA" in the State
    And I enter "90210" in the zip
    And I click on user-consent checkbox
    And I click on 'Next' button
    Then the browser will navigate to the Payment Information page
        
	Scenario: Checkout - Payment Information
    Given I am on the Payment and Credit Page
    And I enter "4444444444444448" in the credit card
    And I enter "0122" in the Expiry date
    And I enter "111" in the CVV

	Scenario: Checkout - Credit Information
    Given I see the Credit check section
    And I select "Passport" in the id
    And I enter "A1234567890" in the id
    And I enter "0120" in the expiry date
    And I enter "001110000" in the ssn
    And I enter "12/24/1984" in the dob
    And I click on the 'Agree and Next' button
    Then the browser will navigate to Review order page

	Scenario: Removal of Trade-In details in Review order page
    Given I am on review order page
	Then I should not see trade-in details
    And I should not see the trade-in Terms & Conditions
    
	Scenario: Review Order details and submit
    When I click on 'I Agree Disclaimer' Label for Terms and Conditions
    And I click on 'Submit Order' Button
    Then I should see the order confirmation page
    Then Clear the browser cache
	
	Scenario: Changing the Payment option to Full Payments
	Given I am in Cart page
	When I click on edit link for editing the device on line "1"
	Then I should see Mini PDP Modal of the device
	When I select "Full Payments" in Payment options dropdown list
	And I click on 'Add to cart' button
	Then I should be taken back to cart page
	
	Scenario: Validating Standard Tradein Information on Device Tile
	Given I am in Cart page 
	Then I see Tradein Header on device tile
	And I see the Model Name of the traded device on line "1" in Standard Flow
	And I see the IMEI Number of the traded device on line "1" in Standard Flow
	And I see the Trade in value of the traded device on line "1" in Standard Flow
	And I see trade in value link on line "1"
	And I see Remove link to remove tradein on line "1"
	
	Scenario: Navigate to checkout
    Given I am in a loaded cart page
    When I click on the 'Checkout' button
    Then the browser will navigate to the checkout section

	Scenario: Checkout - Personal Information
    Given I see the Personal Information section
    And I enter "SDET" in the first name
    And I enter "T" in the middle initial
    And I enter "TMO" in the last name
    And I enter "qat.tmo@gmail.com" in the email address
    And I enter "4254254250" in the phone number

	Scenario: Checkout - Shipping Information
    Given I see the Shipping section
    And I enter "10 main st" in the shipping address
    And I enter "Beverly Hills" in the city
    And I select "CA" in the State
    And I enter "90210" in the zip
    And I click on user-consent checkbox
    And I click on 'Next' button
    Then the browser will navigate to the Payment Information page

	Scenario: Checkout - Payment Information
    Given I am on the Payment and Credit Page
    And I enter "4444444444444448" in the credit card
    And I enter "0122" in the Expiry date
    And I enter "111" in the CVV

	Scenario: Checkout - Credit Information
    Given I see the Credit check section
    And I select "Passport" in the id
    And I enter "A1234567890" in the id
    And I enter "0122" in the expiry date
    And I enter "001110000" in the ssn
    And I enter "12/24/1984" in the dob
    And I click on the 'Agree and Next' button
    Then the browser will navigate to Review order page

	Scenario: Review Order details and submit
    Given I am on review order page
    Then I should see appropriate device details
    When I click on 'I Agree Disclaimer' Label for Terms and Conditions
    And I click on 'Submit Order' Button
    Then I should see the order confirmation page
    Then Clear the browser cache
	
	
	
	
	
	
	
	
	
	