#features/UNAV-footer.feature
Feature: This feature with cover the scenario for marketing UNAV footer on consumer browse page and PDP page,need to make sure marketing toggle is on before
  we execute these steps

  Scenario: Navigate to cell phones page and check all the UNAV footer component is available
    Given I am on "http://qat.digital.t-mobile.com/cell-phones"
    Then I should see "Contact US" Link1
    Then I should see "Support" Link2
    Then I should see "T-MOBILE" Link3

  Scenario: Navigate to PDP page and check all the UNAV footer component is avilable
    Given I am on "http://qat.digital.t-mobile.com/cell-phone/apple-iphone-7-plus?color=black"
    Then I should see "Contact US" Link1
    Then I should see "Support" Link2
    Then I should see "T-MOBILE" Link3
