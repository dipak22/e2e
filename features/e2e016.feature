#features/e2e016.feature
Feature: Add a phone and change the credit class in the cart
    As a user
    I want to add a phone and change the credit class in the cart

Scenario: Navigate to cell-phones browse page and select credit class
    Given I navigate to T-Mobile cell-phones page
    Then I select Average Credit

Scenario: Select 'Apple iPhone 7 Plus' from main browse page
    When I select "Apple iPhone 7 Plus" from main browse
    Then I navigate to Product detail page of "Apple iPhone 7 Plus"

Scenario: Add device to cart
    Given I am on Product detail page of "Apple iPhone 7 Plus"
    And I add "Apple iPhone 7 Plus" to cart by clicking on 'Add to cart' button
    Then I validate the selected device is added to cart

Scenario: Change the credit class
    When I click on creditclass link
    Then I should see cart credit class modal
    And I change the credit class to Awesome
    And I click on Done button to update the creditclass
    Then I validate the cart credit type is changed to Awesome

Scenario: Navigate to checkout
    Given I am in a loaded cart page
    When I click on the 'Checkout' button
    Then the browser will navigate to the checkout section

Scenario: Checkout - Personal Information
    Given I see the Personal Information section
    And I enter "SDET" in the first name
    And I enter "T" in the middle initial
    And I enter "TMO" in the last name
    And I enter "qat.tmo@gmail.com" in the email address
    And I enter "4254254250" in the phone number

Scenario: Checkout - Shipping Information
    Given I see the Shipping section
    And I enter "10 main st" in the shipping address
    And I enter "Beverly Hills" in the city
    And I select "CA" in the State
    And I enter "90210" in the zip
    And I click on user-consent checkbox
    And I click on 'Next' button
    Then the browser will navigate to the Payment Information page
        
Scenario: Checkout - Payment Information
    Given I am on the Payment and Credit Page
    And I enter "4444444444444448" in the credit card
    And I enter "0122" in the Expiry date
    And I enter "111" in the CVV

Scenario: Checkout - Credit Information
    Given I see the Credit check section
    And I select "Passport" in the id
    And I enter "A1234567890" in the id
    And I enter "0120" in the expiry date
    And I enter "001110000" in the ssn
    And I enter "12/24/1984" in the dob
    And I click on the 'Agree and Next' button
    Then the browser will navigate to Review order page

Scenario: Review Order details and submit
    Given I am on review order page
    Then I should see appropriate device details
    When I click on 'I Agree Disclaimer' Label for Terms and Conditions
    And I click on 'Submit Order' Button
    Then I should see the order confirmation page
    
Scenario: Clean-up
    And Clear the browser cache