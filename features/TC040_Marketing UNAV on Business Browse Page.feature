#features/UNAVHeaderBusinessSite.feature
Feature: This feature with cover the scenario for marketing UNAV header on browse page for business site,
  Please make sure marketing toggle is ON before we execute these steps

  Scenario: Navigate to browse page and check all the UNAV header component are available for business site

    Given I am on "http://qat.business.t-mobile.com/cell-phones"
    Then I should see tmobile work logo
    Then I should see plans link for business site
    Then I should see coverage link for business site
    Then I should see devices link for business site
    Then I should see get started modal for business site
    Then I should see search icon for business site
    Then I should see consumer link for business site
    Then I should see espanol link
    Then I should see store locator for business site
    Then I should see lets talk option for business site
    Then I should see get help option for business site
    Then click on hamburger menu
    And I should see mytmobile link under hamburger
    And I should see devices link under hamburger for business site
    And I should see phones link under hamburger for business site
    And I should see tablets and devices link under hamburger for business site
    And I should see accessories link under hamburger for business site
    And I should see plans link under hamburger for business site
    And I should see TMO One unlimited plan link under hamburger for business site
    And I should see simple choice plan link under hamburger for business site
    And I should see coverage under hamburger for business site
    And I should see our coverage option under hamburger for business site
    And I should see map option under hamburger for business site
    And I should see resources option under hamburger for business site
    And I should see success stories option under hamburger for business site
    And I should see get expert advice option under hamburger for business site
    And click on close button
