Feature:
    As a user
	When I click on "Login to MyTMO" link on a Phone PDP, Product details should be passed in cookies and should navigate to My TMO website

	Scenario: Launch t-mobile site and navigate to cell-phones browse page
    Given I navigate to T-Mobile cell-phones page
    Then I select Awesome Credit

	Scenario: Select Apple iPhone seven Plus from main browse page
    When I select "Apple iPhone 7 Plus" from main browse
    Then I navigate to Product detail page of "Apple iPhone 7 Plus"

	Scenario: Navigating to MyTMO site
    Given I am on Product detail page of "Apple iPhone 7 Plus"
    When I click on "Login to T-Mobile" link on PDP
	Then I see My TMO Homepage
	
	Scenario: Login to MyTMO Application
	Given I see My TMO Homepage
	When I enter "4047297321" in Username field
	And I enter "TM0Test1" in Password field
	And I click on Login Button
	Then I see MyTMO Lines Page
	
	Scenario: Viewing line details
	Given I see MyTMO Lines Page
	When I select my line
		
	Scenario: Verifying Cookie details
	#Given I see MyTMO line selector page
	When I get the data from cookie
	Then I see "190198157379" as SKU in Cookies
	And I see "Handset" as Product Type in Cookies
	And I see "EIP" as paymentoption in Cookies
	And I see "$0_PROMOtion" as Promo Name in Cookies
	And I see "g-3DF495D6C11B473EAA5986855F9DE1C0" as Product Family in Cookies
	
	Scenario: Launch t-mobile site and navigate to cell-phones browse page
    Given I navigate to T-Mobile cell-phones page
    Then I select Average Credit
	
	Scenario: Select Apple iPhone 6s from main browse page
    When I select "Apple iPhone 6s" from main browse
    Then I navigate to Product detail page of "Apple iPhone 6s"
	
	Scenario: Validating Upgrade and Add a Line Buttons on PDP
    Given I am on Product detail page of "Apple iPhone 6s"
	Then I should see 'Upgrade' and 'Add a Line' buttons in Product detail Page
	
	Scenario: Navigation to MyTMO using Upgrade Button
	Given I am on Product detail page of "Apple iPhone 6s"
	When I click on 'Upgrade' button on PDP
	Then I see MyTMO line selector page
	
	Scenario: Verifying Cookie details
	Given I see MyTMO line selector page
	When I get the data from cookie
	Then I see "190198058553" as SKU in Cookies
	And I see "Handset" as Product Type in Cookies
	And I see "EIP" as paymentoption in Cookies
	#And I see "Save 300 on your Iphone" as Promo Name in Cookies
	And I see "g-5DAAAFA1CA0A4AA5875A6BF68A29E110" as Product Family in Cookies
	
	Scenario: Launch t-mobile site and navigate to cell-phones browse page
    Given I navigate to T-Mobile cell-phones page
    Then I select No Credit
	
	Scenario: Select Samsung Galaxy S8 plus from main browse page
    When I select "Samsung Galaxy S8 plus" from main browse
    Then I navigate to Product detail page of "Samsung Galaxy S8 plus"
	
	Scenario: Navigation to MyTMO using Add a Line Button
	Given I am on Product detail page of "Samsung Galaxy S8 plus"
	When I click on 'Add a Line' button on PDP
	Then I see MyTMO line selector page
	
	Scenario: Verifying Cookie details
	Given I see MyTMO line selector page
	When I get the data from cookie
	Then I see "610214653208" as SKU in Cookies
	And I see "Handset" as Product Type in Cookies
	And I see "FULL" as paymentoption in Cookies
	#And I see "Save 300 on your Iphone" as Promo Name in Cookies
	And I see "g-12FF392EA318486E893C583EDAE13D13" as Product Family in Cookies
	
	
	