#features/e2e026.feature
Feature: Running Cucumber with Protractor
  As a user I want to add a phone BYOD and
  edit the cart from review section and
  need to replace the BYOD with a phone line

	Scenario: Launch t-mobile site and navigate to cell-phones browse page
    Given I navigate to T-Mobile cell-phones page
    Then I select Awesome Credit	

    Scenario: Select BYOD cell phones SIM from main browse page
    When I select "T-Mobile 3-in-1 SIM Starter Kit" from main browse
    Then I navigate to Product detail page of "T-Mobile 3-in-1 SIM Starter Kit"

	Scenario: Add device to cart
    Given I am on Product detail page of "T-Mobile 3-in-1 SIM Starter Kit"
    And I click on 'Add to cart' button in PDP Page
    Then I validate the selected device is added to Cart

	Scenario: Navigate to checkout
    Given I am in a loaded cart page
    When I click on the 'Checkout' button
    Then the browser will navigate to the checkout section

	Scenario: Checkout - Personal Information
    Given I see the Personal Information section
    And I enter "SDET" in the first name
    And I enter "T" in the middle initial
    And I enter "TMO" in the last name
    And I enter "qat.tmo@gmail.com" in the email address
    And I enter "4254254250" in the phone number

	Scenario: Checkout - Shipping Information
    Given I see the Shipping section
    And I enter "10 main st" in the shipping address
    And I enter "Beverly Hills" in the city
    And I select "CA" in the State
    And I enter "90210" in the zip
    And I click on user-consent checkbox
    And I click on 'Next' button
    Then the browser will navigate to the Payment Information page
        
	Scenario: Checkout - Payment Information
    Given I am on the Payment and Credit Page
    And I enter "4444444444444448" in the credit card
    And I enter "0122" in the Expiry date
    And I enter "111" in the CVV

	Scenario: Checkout - Credit Information
    Given I see the Credit check section
    And I select "Passport" in the id
    And I enter "A1234567890" in the id
    And I enter "0122" in the expiry date
    And I enter "001110000" in the ssn
    And I enter "12/24/1995" in the dob
    And I click on the 'Agree and Next' button
    Then the browser will navigate to Review order page

	Scenario: Edit in Cart in Review order
    When I click on Edit in Cart link in review order page
    Then I should be taken back to cart page
	
	Scenario: Remove existing line and add new line
    When I Click on Remove line link in Cart Row
    Then I should see empty cart page
    When I click on Add a phone button in Empty Cart Page
    Then I should see cart page

	Scenario: Adding Phone to the Cart
	Given I am in Cart page
	When I click on Add Device Icon in device tile
	And I click on 'Add a new phone' button in device tile
	And I select "Apple iPhone 7 Plus"
	And I click on 'Add to cart' button
	Then I validate the selected device is added to Cart

	Scenario: Navigate to checkout
    Given I am in a loaded cart page
    When I click on the 'Checkout' button
    Then the browser will navigate to the checkout section

	Scenario: Check and re-enter required checkout form fields
    Given I am on Info and Shipping page
    And I click on user-consent checkbox
    And I click on 'Next' button
    Then the browser will navigate to the Payment Information page
        
	Scenario: Checkout - Payment Information
    Given I am on the Payment and Credit Page
    And I enter "4444444444444448" in the credit card
    And I enter "0122" in the Expiry date
    And I enter "111" in the CVV

	Scenario: Checkout - Credit Information
    Given I see the Credit check section
    And I select "Passport" in the id
    And I enter "A1234567890" in the id
    And I enter "0122" in the expiry date
    And I enter "001110000" in the ssn
    And I enter "12/24/1984" in the dob
    And I click on the 'Agree and Next' button
    Then the browser will navigate to Review order page

	Scenario: Review Order details and submit
    Given I am on review order page
	Then I should see appropriate device details
    When I click on 'I Agree Disclaimer' Label for Terms and Conditions
    And I click on 'Submit Order' Button
    Then I should see the order confirmation page
