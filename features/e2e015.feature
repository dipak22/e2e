#features/e2e015.feature
Feature: Running Cucumber with Protractor
  As a user I want to add a tablet
  and phone line to the cart,
  and change the color, memory and payment options

  Scenario: Launch internet device browse page and select credit class
    When I navigate to T-Mobile Tablets page
    And I am on internet devices browse page
    Then I select Average Credit

  Scenario: Select Apple iPad from main browse page
    When I select "Apple iPad Pro 12.9-inch" from main browse
    Then I navigate to Product detail page of "Apple iPad Pro 12.9-inch"

  Scenario: Change Credit Type, Memory, Payment Option and Color on PDP
    Given I am on Product detail page of "Apple iPad Pro 12.9-inch"
    Then I should see review stars on main PDP
    And I should see total ratings on main PDP
    Then I select Awesome credit from main PDP
    Then I select color swatch from main PDP
    Then I select memory from main PDP
    Then I should see sim kit on main PDP

  Scenario: Add device to cart
    And I add "Apple iPad Pro 12.9-inch" to cart by clicking on 'Add to cart' button
    Then I validate the selected device is added to cart

  Scenario: Edit the internet device to change the  payment type, color swatch and memory
    And I click on edit button for line 1
    And I look for memory option dropdown
    And I select different memory from memory option
    And I select new color swatch from mini PDP
    And I look for payment option dropdown
    And I select full price from payment option
    Then I click on add to cart button on mini pdp

  Scenario: Add cell phones as second line
    And I click on add a phone button
    Then I click on plus button for phones for line two
    And I click on 'Add a new phone' button for line 2 to view the mini browse
    Then I select a cell phone from mini browse
    And I add the selected cell phone to cart.

  Scenario: Edit the cell-phones to change the  payment type, color swatch and memory
    And I click on edit button for line two
    And I look for memory option dropdown
    And I select different memory from memory option
    And I select new color swatch from mini PDP
    And I look for payment option dropdown
    And I select full price from payment option
    Then validate full price payment option is selected
    Then I click on add to cart button on mini pdp

  Scenario: Navigate to checkout
    Given I am in a loaded cart page
    When I click on the 'Checkout' button
    Then the browser will navigate to the checkout section

  Scenario: Checkout - Personal Information
    Given I see the Personal Information section
    And I enter "SDET" in the first name
    And I enter "T" in the middle initial
    And I enter "TMO" in the last name
    And I enter "qat.tmo@gmail.com" in the email address
    And I enter "4254254250" in the phone number

  Scenario: Checkout - Shipping Information
    Given I see the Shipping section
    And I enter "10 main st" in the shipping address
    And I enter "Beverly Hills" in the city
    And I select "CA" in the State
    And I enter "90210" in the zip
    And I click on user-consent checkbox
    And I click on 'Next' button
    Then the browser will navigate to the Payment Information page

  Scenario: Checkout - Payment Information
    Given I am on the Payment and Credit Page
    And I enter "4444444444444448" in the credit card
    And I enter "0122" in the Expiry date
    And I enter "111" in the CVV

  Scenario: Checkout - Credit Information
    Given I see the Credit check section
    And I select "Passport" in the id
    And I enter "A1234567890" in the id
    And I enter "0122" in the expiry date
    And I enter "001110000" in the ssn
    And I enter "12/24/1984" in the dob
    And I click on the 'Agree and Next' button
    Then the browser will navigate to Review order page

  Scenario: Review Order details and submit
    Given I am on review order page
    When I click on 'I Agree Disclaimer' Label for Terms and Conditions
    And I click on 'Submit Order' Button
    Then I should see the order confirmation page
