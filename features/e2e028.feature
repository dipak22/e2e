#features/e2e028feature
Feature: Running Cucumber with Protractor
  As a user I want to place a 8 line order with Phone BYOD with services
  ,Device in monthly installments and One Plus service
  ,Device with full payment and service
  ,Tablet BYOD, a tablet with Full payment,
  ,Tablet with monthly payment, a wearable with Monthly payments
  ,a wearable with Full payment and Multiple accessories


  Scenario: Launch t-mobile site Navigate to Cell - Phones Browse page and Select Credit Class as Average
    Given I navigate to T-Mobile cell-phones page
    Then I select Average Credit

  Scenario: Select BYOD cell phones SIM from main browse page
    Then I select BYOD cell phones SIM from browse page

  Scenario: Add device to cart
    #Given I am on Product detail page of BYOD cell phones SIM
    And I add BYOD cell phone SIM to cart
    #Then I validate the BYOD cell phones SIM is added to cart


  Scenario: Add service to line one
    And I add an service to the line one

  Scenario: Add second line to cart with monthly payment
    #And I click on add a phone button
    And I click on Add a Phone Button for adding new line
    Then I click on plus button for new phone line
    And I click on 'Add a new phone' button to view the mini phone browse
    Then I select a cell phone from mini browse
    And I add the selected cell phone to cart


  Scenario: Add service to line two
    And I add an service to the line two

  Scenario: Add third line to cart with full payment
    #And I click on add a phone button
    And I click on Add a Phone Button for adding new line
    Then I click on plus button for new phone line three
    And I click on 'Add a new phone' button for line 3 to view the mini browse
    Then I select a cell phone from mini browse for line three
    And I look for payment option dropdown
    And I select full price from payment option
    #Then validate full price payment option is selected
    And I add the selected cell phone to cart


  Scenario: Add service to line three
    And I add an service to the line three

  Scenario: Add BYOD internet device as 4th line
    #Then I click on Add a Tablet Button
    Then I click on Add a Tablet Button for new tab line
    #And I click on plus button for new tablet line
	And I click on plus button on tablets empty line
	And I click on BYOD Tablet button to view the BYOD tablet mini PDP
    And I add the selected BYOD tab to cart

  Scenario: Add a tablet with Full payment through mini Browse as 5th line
    #Then I click on Add a Tablet Button
    Then I click on Add a Tablet Button for new tab line
    And I click on plus button for adding a new tablet line number five
    And I click on Add Tablet button on cart to view the mini browse for line five
    And I select a tab from mini browse for line five
    And I look for payment option dropdown
    And I select full price from payment option
    #Then validate full price payment option is selected
    And I add the selected tab to cart

  Scenario: Add a tablet with monthly payment through mini Browse as 6th line
    Then I click on Add a Tablet Button for new tab line
    And I click on plus button for adding a new tablet line number six
    And I click on Add Tablet button on cart to view the mini browse for line six
    And I select a tab from mini browse for line six
    And I add the selected tab to cart

  Scenario: Add a wearable with monthly payment as 7th line
    Then I click on Add a Tablet Button for new tab line
    And I click on plus button for adding a new tablet line number seven
    And I click on Add Tablet or wearable button on cart to view the mini browse for line seven
    And I select a wearable from mini browse for line seven
    And I add the selected tab to cart

  Scenario: Add a wearable with full payment as 8th line
    Then I duplicate the wearable line seventh
    And I click on edit button for line eight
    And I look for payment option dropdown
    And I select full price from payment option
    Then I click on add to cart button on mini pdp

  Scenario: Adding accessory one to cart
    And I click on plus button on tile for accessory one
    Then I select accessory one from mini browse
    And I add the newly selected accessory to cart

  Scenario: Adding accessory two to cart
    And I click on plus button on tile for accessory two
    Then I select accessory two from mini browse
    And I add the newly selected accessory to cart

  Scenario: Navigate to checkout
    Given I am in a loaded cart page
    When I click on the 'Checkout' button
    Then the browser will navigate to the checkout section

  Scenario: Checkout - Personal Information
    Given I see the Personal Information section
    And I enter "SDET" in the first name
    And I enter "T" in the middle initial
    And I enter "TMO" in the last name
    And I enter "qat.tmo@gmail.com" in the email address
    And I enter "4254254250" in the phone number

  Scenario: Checkout - Shipping Information
    Given I see the Shipping section
    And I enter "10 main st" in the shipping address
    And I enter "Beverly Hills" in the city
    And I select "CA" in the State
    And I enter "90210" in the zip
    And I click on user-consent checkbox
    And I click on 'Next' button
    Then the browser will navigate to the Payment Information page

  Scenario: Checkout - Payment Information
    Given I am on the Payment and Credit Page
    And I enter "4444444444444448" in the credit card
    And I enter "0122" in the Expiry date
    And I enter "111" in the CVV

  Scenario: Checkout - Credit Information
    Given I see the Credit check section
    And I select "Passport" in the id
    And I enter "A1234567890" in the id
    And I enter "0122" in the expiry date
    And I enter "001110000" in the ssn
    And I enter "12/24/1984" in the dob
    And I click on the 'Agree and Next' button
    Then the browser will navigate to Review order page

  Scenario: Review Order details and submit
    Given I am on review order page
    #Then I should see appropriate device details
    When I click on 'I Agree Disclaimer' Label for Terms and Conditions
    And I click on 'Submit Order' Button
    Then I should see the order confirmation page

Scenario: Clean-up
    And Clear the browser cache
