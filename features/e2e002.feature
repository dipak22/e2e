#features/e2e002.feature
Feature: Accessory only order with quantity as 1
    As a user
    I want to place an Accessory only order with quantity as one with Awesome credit

Scenario: Launch t-mobile site and navigate to cart page
    Given I navigate to T-Mobile Cart page
    Then I should see "Add an accessory" in the homepage for accessories
    When I click on 'Add an accessory' button in Empty Cart Page
    Then I should see cart title in cart page

Scenario: Add a new Accessory from mini PDP whose cost is less than $69
    Given I am in Cart page
    When I click on Accessory tile
    Then I should see Accessory mini Browse
    When I select third tab in accessory mini browse
    And I select "T-Mobile 3.4A Lightning Tip Vehicle Power Charger - Blue" accessory from Chargers
    And I click on 'Add to Cart' Button on accessory mini PDP
    Then I see the selected accessory is added to the cart on tile "1"

Scenario: Navigate to checkout
    Given I am in a loaded cart page
    When I click on the 'Checkout' button
    Then the browser will navigate to the checkout section

Scenario: Checkout - Personal Information
    Given I see the Personal Information section
    And I enter "SDET" in the first name
    And I enter "T" in the middle initial
    And I enter "TMO" in the last name
    And I enter "qat.tmo@gmail.com" in the email address
    And I enter "4254254250" in the phone number

Scenario: Checkout - Shipping Information
    Given I see the Shipping section
    And I enter "10 main st" in the shipping address
    And I enter "Beverly Hills" in the city
    And I select "CA" in the State
    And I enter "90210" in the zip
    And I click on user-consent checkbox
    And I click on 'Next' button
    Then the browser will navigate to the Payment Information page
        
Scenario: Checkout - Payment Information
    Given I am on the Payment and Credit Page
    And I enter "4444444444444448" in the credit card
    And I enter "0122" in the Expiry date
    And I enter "111" in the CVV
    And I click on the 'Continue' button
    Then the browser will navigate to Review order page

Scenario: Review Order details and submit
    Given I am on review order page
    Then I should see appropriate accessory details added to Cart
    When I click on 'I Agree Disclaimer' Label for Terms and Conditions
    And I click on 'Submit Order' Button
    Then I should see the order confirmation page
    
Scenario: Clean-up
    And Clear the browser cache