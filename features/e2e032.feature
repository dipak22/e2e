Feature: Running scenario E2E032:
    As a user I want to place an order with 5 lines and accessories with No credit
	and change the credit class to Awesome and add one more phone line
 
	Scenario: Launch t-mobile site and navigate to internet-devices browse page
    Given I navigate to T-Mobile Tablets page
    Then I select No Credit

	Scenario: Select Apple iPad mini 4 from main browse page
    When I select "Apple iPad mini 4" from main browse
    Then I navigate to Product detail page of "Apple iPad mini 4"

	Scenario: Add device to cart
    Given I am on Product detail page of "Apple iPad mini 4"
    And I click on 'Add to cart' button in PDP Page
    #Then I validate the selected device is added to Cart

	Scenario: Adding cell phone in second line
	Given I am in Cart page
	When I click on Add a Phone Button for adding new line
    And I click on Device Image on Device tile in line "2"
	And I click on 'Add a new phone' button in device tile
	And I select "Apple iPhone 6s Plus" in mini browse
	And I click on 'Add to cart' button
	Then I should see selected device added to the cart in line "2"

	Scenario: Adding cell phone in third line
	Given I am in Cart page
	When I click on Add a Phone Button for adding new line
    And I click on Device Image on Device tile in line "3"
	And I click on 'Add a new phone' button in device tile
	And I select "Apple iPhone 7 Plus" in mini browse
	And I click on 'Add to cart' button
	Then I should see selected device added to the cart in line "3"

	Scenario: Adding tablet to the Cart in fourth line
    Given I am in Cart page
	When I click on Add a Tablet Button
	And I click on Device Image on Device tile in line "4"
	And I click on Add a Tablet button in device tile
	And I select "Samsung Galaxy Tab E" tablet in mini browse
	When I click on 'Add to cart' button
	Then I should see selected device added to the cart in line "4"

	Scenario: Adding Wearable to the Cart in fifth line
	Given I am in Cart Page
	When I click on Add a Tablet Button
	And I click on Device Image on Device tile in line "5"
	And I click on Add a Tablet button in device tile
	And I select "Samsung Gear S3 frontier" wearable in mini browse
	When I click on 'Add to cart' button
	Then I should see selected device added to the cart in line "5"

	Scenario: Adding First accessory from Speakers Category to the Cart
    Given I am in Cart page
    When I click on Accessory tile
    Then I should see Accessory mini Browse
    When I select first tab in accessory mini browse
    And I select accessory which is under first tab in mini browse
    #Then I should see accessory product description page
    When I click on 'Add to Cart' Button on accessory mini PDP
    Then I see the selected accessory is added to the cart on tile "1"
	
	Scenario: Changing the credit class from No Credit to Awesome credit
	Given I am in Cart page
	When I click on credit class on sticky banner
	Then I should see credit class modal
	When I select Awesome credit radio button
	And I click on 'Done' button
	Then I should see credit class changed in sticky banner

	Scenario: Adding cell phone in sixth line
	Given I am in Cart page
	When I click on Add a Phone Button for adding New Line
    And I click on Device Image on Device tile in line "6"
	And I click on 'Add a new phone' button in device tile
	And I select "Apple iPhone 7" in mini browse
	And I click on 'Add to cart' button
	Then I should see selected device added to the cart in line "6"

	Scenario: Navigate to checkout
    Given I am in a loaded cart page
    When I click on the 'Checkout' button
    Then the browser will navigate to the checkout section

	Scenario: Checkout - Personal Information
    Given I see the Personal Information section
    And I enter "SDET" in the first name
    And I enter "T" in the middle initial
    And I enter "TMO" in the last name
    And I enter "qat.tmo@gmail.com" in the email address
    And I enter "4254254250" in the phone number

	Scenario: Checkout - Shipping Information
    Given I see the Shipping section
    And I enter "10 main st" in the shipping address
    And I enter "Beverly Hills" in the city
    And I select "CA" in the State
    And I enter "90210" in the zip
    And I click on user-consent checkbox
    And I click on 'Next' button
    Then the browser will navigate to the Payment Information page

	Scenario: Checkout - Payment Information
    Given I am on the Payment and Credit Page
    And I enter "4444444444444448" in the credit card
    And I enter "0122" in the Expiry date
    And I enter "111" in the CVV
    
	Scenario: Checkout - Credit Information
    Given I see the Credit check section
    And I select "Passport" in the id
    And I enter "A1234567890" in the id
    And I enter "0122" in the expiry date
    And I enter "001110000" in the ssn
    And I enter "12/24/1984" in the dob
    And I click on the 'Agree and Next' button
    Then the browser will navigate to Review order page

	Scenario: Review Order details and submit
    Given I am on review order page
    When I click on 'I Agree Disclaimer' Label for Terms and Conditions
    And I click on 'Submit Order' Button
    Then I should see the order confirmation page