#features/e2e012.feature
Feature:  As a User of TMNG Application
  I would like to purchase MI Sim kit and Phone Sim kit
  In order to complete my shopping experiences
	
	Scenario: Launch t-mobile site and navigate to internet-devices browse page
    Given I navigate to T-Mobile Tablets page
    Then I select Average Credit

	Scenario: Select T-Mobile 3-in-1 SIM Starter kit from main browse page
    When I select "T-Mobile 3-in-1 Mobile Internet SIM Kit" from main browse
    Then I navigate to Product detail page of "T-Mobile 3-in-1 Mobile Internet SIM Kit"

	Scenario: Add device to cart
    Given I am on Product detail page of "T-Mobile 3-in-1 Mobile Internet SIM Kit"
    And I click on 'Add to cart' button in PDP Page
    Then I validate the selected device is added to Cart	

	Scenario: Adding a line to the Cart
    Given I am in Cart page
    When I click on Add a Phone Button for adding new line
    Then I should see new line added to cart

	Scenario: Adding Phone Sim kit to Cart
    When I click on Device Image on Device tile in the second line
    And I click on Bring your own device Button
    Then I should see Sim kit Mini PDP
    And I should see Sim Starter kit on Mini PDP
    When I click on Add to Cart Button on SIM kit Product Description Page
    Then I should see Sim Starter kit added to cart

	Scenario: Navigate to checkout
    Given I am in a loaded cart page
    When I click on the 'Checkout' button
    Then the browser will navigate to the checkout section

	Scenario: Checkout - Personal Information
    Given I see the Personal Information section
    And I enter "SDET" in the first name
    And I enter "T" in the middle initial
    And I enter "TMO" in the last name
    And I enter "qat.tmo@gmail.com" in the email address
    And I enter "4254254250" in the phone number

	Scenario: Checkout - Shipping Information
    Given I see the Shipping section
    And I enter "10 main st" in the shipping address
    And I enter "Beverly Hills" in the city
    And I select "CA" in the State
    And I enter "90210" in the zip
    And I click on user-consent checkbox
    And I click on 'Next' button
    Then the browser will navigate to the Payment Information page
        
	Scenario: Checkout - Payment Information
    Given I am on the Payment and Credit Page
    And I enter "4444444444444448" in the credit card
    And I enter "0122" in the Expiry date
    And I enter "111" in the CVV

	Scenario: Checkout - Credit Information
    Given I see the Credit check section
    And I select "Passport" in the id
    And I enter "A1234567890" in the id
    And I enter "0120" in the expiry date
    And I enter "001110000" in the ssn
    And I enter "12/24/1995" in the dob
    And I click on the 'Agree and Next' button
    Then the browser will navigate to Review order page

	Scenario: Review Order details and submit
    Given I am on review order page
	Then I should see SIM kit details in review order page
    When I click on 'I Agree Disclaimer' Label for Terms and Conditions
    And I click on 'Submit Order' Button
    Then I should see the order confirmation page