#features/e2e035.feature
Feature: 
    As a user,
    I want to see the standard tradein value for my device which got issues,
    so that I can tradein my existing device and purchase a new device.
	
	Scenario: Launch cell-phones browse page
    Given I navigate to T-Mobile cell-phones page
	Then I select No Credit

	Scenario: Select 'Samsung Galaxy S8' from main browse page
    When I select "Samsung Galaxy S8" from Main Browse
    Then I navigate to Product detail page of "Samsung Galaxy S8"
	
	Scenario: Add device to cart
    Given I am on Product detail page of "Samsung Galaxy S8"
    And I add "Samsung Galaxy S8" to cart by clicking on 'Add to cart' button
    Then I validate the selected device is added to cart
	
	Scenario: Navigate to TQT Tool
	Given I am in Cart page
	When I Click on Trade in Button
	Then I should see TQT Modal
	
	Scenario: Standard Tradein Flow Validation with Phone and Device Condition as Damage
	Given I am in TQT Modal
	When I Select Carrier as "ATT"
	And I Select Make as "Samsung"
	And I Select Model as "SGH-D807 - ATT"
	And I enter "357759080728132" in IMEI field and click on Apply button
	And I select Device Condition as Damage
	And I click on Get Estimate Button
	Then I should see Confirmation Modal
	
	Scenario: Selecting Standard Tradein option and clicking on Agree and Next Button.
    Given I am in TQT Confirmation Modal
    When I select standard trade in option
	And I click on Agree & Continue Button in Standard Flow
	#Then I should see Standard Notification Banner
	
	Scenario: Validating Tradein Information on Device Tile
	When I accept trade in and returned to the cart 
	Then I see Tradein Header on device tile
	And I see the Model Name of the traded device on line "1" in Standard Flow
	And I see the IMEI Number of the traded device on line "1" in Standard Flow
	And I see the Trade in value of the traded device on line "1" in Standard Flow
	And I see trade in value link on line "1"
	And I see Remove link to remove tradein on line "1"
	
	Scenario: Clicking on Price Breakdown link on Sticky Banner
	Given I am in Cart page
	When I click on PriceBreakdown link on Sticky Banner
	Then I should see PriceBreakdown Modal
	
	Scenario: Validating the Tradein Information on PriceBreakdown
	When I am in PriceBreakdown Modal
	Then I see my added devices in the modal
	And I see my trade in device value
	And I see my standard trade in device model name
	And I see my trade in device header and legal text
	#And I see trade in value link in the modal
	And I close the PriceBreakdown Modal
	
	Scenario: Navigate to checkout
    Given I am in a loaded cart page
    When I click on the 'Checkout' button
    Then the browser will navigate to the checkout section

	Scenario: Checkout - Personal Information
    Given I see the Personal Information section
    And I enter "SDET" in the first name
    And I enter "T" in the middle initial
    And I enter "TMO" in the last name
    And I enter "qat.tmo@gmail.com" in the email address
    And I enter "4254254250" in the phone number

	Scenario: Checkout - Shipping Information
    Given I see the Shipping section
    And I enter "10 main st" in the shipping address
    And I enter "Beverly Hills" in the city
    And I select "CA" in the State
    And I enter "90210" in the zip
    And I click on user-consent checkbox
    And I click on 'Next' button
    Then the browser will navigate to the Payment Information page
        
	Scenario: Checkout - Payment Information
    Given I am on the Payment and Credit Page
    And I enter "4444444444444448" in the credit card
    And I enter "0122" in the Expiry date
    And I enter "111" in the CVV

	Scenario: Checkout - Credit Information
    Given I see the Credit check section
    And I select "Passport" in the id
    And I enter "A1234567890" in the id
    And I enter "0120" in the expiry date
    And I enter "001110000" in the ssn
    And I enter "12/24/1984" in the dob
    And I click on the 'Agree and Next' button
    Then the browser will navigate to Review order page

	Scenario: Review Trade-In details and its Terms & Conditions
    Given I am on review order page
	Then I see my traded-in device model
    And I see the traded-in device value
	And I see the trade in value link in order review page
    And I see the trade-in Terms & Conditions text
    When I click on 'Terms and Conditions' link in trade-in Terms & Conditions text
    Then it should open 'DEVICE RECOVERY PROGRAM Terms and Conditions' modal
    When I close the 'DEVICE RECOVERY PROGRAM Terms and Conditions' modal

	Scenario: Review Order details and submit
    When I click on 'I Agree Disclaimer' Label for Terms and Conditions
    And I click on 'Submit Order' Button
    Then I should see the order confirmation page