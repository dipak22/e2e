#features/e2e011.feature
Feature: Accessory only order with edit cart
    As a user,
    I want to add Phone BYOD and accessory whose cost is greater than $69,
    And edit the cart from Review section and modify (Edit / remove) the existing accessory,
    And add new accesosry such that cost is less than $69

Scenario: Navigate to Accessories browse page and apply high to low sorting
    Given I navigate to T-Mobile Accessories page
    And I click on Sorting Drop Down in Accessory browse page
    And I select sort value as Price High To Low
    Then I validate Price High To Low is selected in sort options

Scenario: Select "Beats Solo3 Wireless On-Ear Headphones - Rose Gold" from main browse page
    When I select "Beats Solo3 Wireless On-Ear Headphones - Rose Gold" accessory from main browse
    Then I navigate to Product detail page of "Beats Solo3 Wireless On-Ear Headphones - Rose Gold" accessory

Scenario: Add accessory to cart
    Given I am on Product detail page of "Beats Solo3 Wireless On-Ear Headphones - Rose Gold"
    And I click on 'Add to cart' button in PDP Page
    Then I see the selected accessory is added to the cart on first tile

Scenario: Adding Phone SIM Kit to Cart
    When I click on add a phone button
    And I click on Device Image on Device tile in the first line
    And I click on Bring your own device Button
    Then I should see "T-Mobile� 3-in-1 SIM Starter Kit" Mini PDP
    When I click on Add to Cart Button on SIM kit Product Description Page
    Then I should see Sim kit added to cart

Scenario: Navigate to checkout
    Given I am in a loaded cart page
    When I click on the 'Checkout' button
    Then the browser will navigate to the checkout section

Scenario: Checkout - Personal Information
    Given I see the Personal Information section
    And I enter "SDET" in the first name
    And I enter "T" in the middle initial
    And I enter "TMO" in the last name
    And I enter "qat.tmo@gmail.com" in the email address
    And I enter "4254254250" in the phone number

Scenario: Checkout - Shipping Information
    Given I see the Shipping section
    And I enter "10 main st" in the shipping address
    And I enter "Beverly Hills" in the city
    And I select "CA" in the State
    And I enter "90210" in the zip
    And I click on user-consent checkbox
    And I click on 'Next' button
    Then the browser will navigate to the Payment Information page
        
Scenario: Checkout - Payment Information
    Given I am on the Payment and Credit Page
    And I enter "4444444444444448" in the credit card
    And I enter "0122" in the Expiry date
    And I enter "111" in the CVV

Scenario: Checkout - Credit Information
    Given I see the Credit check section
    And I select "Passport" in the id
    And I enter "A1234567890" in the id
    And I enter "0122" in the expiry date
    And I enter "001110000" in the ssn
    And I enter "12/24/1984" in the dob
    And I click on the 'Agree and Next' button
    Then the browser will navigate to Review order page

Scenario: Edit the cart from Review Order details page
    Given I am on review order page
    When I click on Edit in Cart link in review order page
    Then I should be taken back to cart page

Scenario: Change the quantity of the existing accessory
    And I click on edit accessory option for accessory one
    And I select a edit button from edited tile
    Then I look for quantity option dropdown
    And I change the quantity
    And I click on 'Add to Cart' Button on accessory mini PDP
    
Scenario: Add a new Accessory from mini PDP whose cost is less than $69
    Given I am in Cart page
    When I click on Accessory tile
    Then I should see Accessory mini Browse
    When I select third tab in accessory mini browse
    And I select "T-Mobile 3.4A Lightning Tip Vehicle Power Charger - Blue" accessory from Chargers
    And I click on 'Add to Cart' Button on accessory mini PDP
    Then I see the selected accessory is added to the cart on tile "2"

Scenario: Navigate to checkout
    Given I am in a loaded cart page
    When I click on the 'Checkout' button
    Then the browser will navigate to the checkout section

Scenario: Check and re-enter required checkout form fields
    Given I am on Info and Shipping page
    And I click on user-consent checkbox
    And I click on 'Next' button
    Then the browser will navigate to the Payment Information page
        
Scenario: Checkout - Payment Information
    Given I am on the Payment and Credit Page
    And I enter "4444444444444448" in the credit card
    And I enter "0122" in the Expiry date
    And I enter "111" in the CVV

Scenario: Checkout - Credit Information
    Given I see the Credit check section
    And I select "Passport" in the id
    And I enter "A1234567890" in the id
    And I enter "0122" in the expiry date
    And I enter "001110000" in the ssn
    And I enter "12/24/1984" in the dob
    And I click on the 'Agree and Next' button
    Then the browser will navigate to Review order page

Scenario: Review Order details and submit
    Given I am on review order page
    When I click on 'I Agree Disclaimer' Label for Terms and Conditions
    And I click on 'Submit Order' Button
    Then I should see the order confirmation page

Scenario: Clean-up
    And Clear the browser cache