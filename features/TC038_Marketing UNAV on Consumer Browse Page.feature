#features/UNAVheader.feature
Feature: This feature with cover the scenario for marketing UNAV header on browse page,need to make sure marketing toggle is on beafore
  we execute these steps

  Scenario: Navigate to cell phones page and check all the UNAV header component are available

    Given I am on "http://qat.digital.t-mobile.com/cell-phones"
    Then I should see tmobile logo link
    Then I should see deals link
    Then I should see phones link
    Then I should see plans link
    Then I should see mytmobile link
    Then I should see cart icon
    Then I should see search icon
    Then I should see business link
    Then I should see espanol link
    Then I should see store locator
    Then I should see lets talk option
    Then click on hamburger menu
    And I should see mytmobile link under hamburger
    And I should see plans link under hamburger
    And I should see tmobile one link under hamburger
    And I should see prepaid plans link under hamburger
    And I should see phones link under hamburger
    And I should see tablets and devices link under hamburger
    And I should see mobile hotspots link under hamburger
    And I should see wearable tech link under hamburger
    And I should see accessories link under hamburger
    And I should see bring your own phone link under hamburger
    And I should see prepaid phones link under hamburger
    And I should see bring your own tablet link under hamburger
    And I should see deals link under hamburger
    And I should see accessory finder link under hamburger
    And I should see tmobile magenta gear under hamburger
    And I should see coverage under hamburger
    And I should see LTE comparison map under hamburger
    And click on close button
