var outputDir = './report';
var Cucumber = require('Cucumber');
var fs = require('fs');
var CucumberHtmlReport = require('cucumber-html-reporter');

module.exports = function () {

    
    this.BeforeFeature(function(event, callback) {
        if('robots-txt' === event.getName()) {
            browser.ignoreSynchronization = true;
        }
        callback();
    });

    this.After(function (scenario, callback) {
        if (scenario.isFailed()) {
            browser.takeScreenshot().then(function (png) {
                var decodedImage = new Buffer(png, 'base64').toString('binary');
                scenario.attach(decodedImage, 'image/png');
                callback();
            }, function (err) {
                callback(err);
            });
        } else {
            callback();
        }
    });

    this.registerHandler('AfterFeatures', function (event, callback) {
        console.log("Inside afterFeatures");
        var options = {
            theme: 'bootstrap',
            jsonDir: './report',
            output: './report/cucumber_report.html',
            reportSuiteAsScenarios: true,
            launchReport: true,
            metadata: {
                "Test Environment": "REGRESSION",
                "Browser": "Chrome 59.0.3071.115",
                "Platform": "Windows Server 2012 R2",
                "Parallel": "Scenarios",
                "Executed": "Remote"
            }
        };

        CucumberHtmlReport.generate(options);

        browser.close()
            .then(function () {
                callback();
            });
    });
}
