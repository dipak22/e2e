Feature: ibaOptOutHappyPath

I want to verify the iba Opt-Out flow

    Scenario: Verifying the iba Opt-Out happy path redirection
    When I go to "/iba"
    Then I see Turn Off button

    Scenario: Verifying the Turn off CTA + Modal screen
    When I go to "/iba"
    And click on Turn off CTA
    Then I see modal window overtakes the iba screen
    And I see the modal title
    And I see the Turn off CTA button on modal
    And I see the Leave on CTA button on modal
    