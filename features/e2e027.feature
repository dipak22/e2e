#features/e2e027.feature
Feature:
	As a user I want to add a Phone to the cart and
	replace the Phone with BYOD using edit in cart option in review order page and
	submit the order by adding MI BYOD in second line and services in both the lines.

	Scenario: Launch t-mobile site and navigate to cell-phones browse page
    Given I navigate to T-Mobile cell-phones page
    Then I select Average Credit

	Scenario: Select Apple iPhone seven Plus from main browse page
    When I select "Apple iPhone 7 Plus" from main browse
    Then I navigate to Product detail page of "Apple iPhone 7 Plus"

	Scenario: Add device to cart
    Given I am on Product detail page of "Apple iPhone 7 Plus"
    And I click on 'Add to cart' button in PDP Page
    Then I validate the selected device is added to Cart

	Scenario: Navigate to checkout
    Given I am in a loaded cart page
    When I click on the 'Checkout' button
    Then the browser will navigate to the checkout section

	Scenario: Checkout - Personal Information
    Given I see the Personal Information section
    And I enter "SDET" in the first name
    And I enter "T" in the middle initial
    And I enter "TMO" in the last name
    And I enter "qat.tmo@gmail.com" in the email address
    And I enter "4254254250" in the phone number

	Scenario: Checkout - Shipping Information
    Given I see the Shipping section
    And I enter "10 main st" in the shipping address
    And I enter "Beverly Hills" in the city
    And I select "CA" in the State
    And I enter "90210" in the zip
    And I click on user-consent checkbox
    And I click on 'Next' button
    Then the browser will navigate to the Payment Information page

	Scenario: Checkout - Payment Information
    Given I am on the Payment and Credit Page
    And I enter "4444444444444448" in the credit card
    And I enter "0122" in the Expiry date
    And I enter "111" in the CVV

	Scenario: Checkout - Credit Information
    Given I see the Credit check section
    And I select "Passport" in the id
    And I enter "A1234567890" in the id
    And I enter "0120" in the expiry date
    And I enter "001110000" in the ssn
    And I enter "12/24/1984" in the dob
    And I click on the 'Agree and Next' button
    Then the browser will navigate to Review order page

	Scenario: Edit in Cart in Review order
    When I click on Edit in Cart link in review order page
    Then I should be taken back to cart page

	Scenario: Remove existing line and add new line
    When I Click on Remove line link in Cart Row
    Then I should see empty cart page
    When I click on Add a phone button in Empty Cart Page
    Then I should see cart page

	Scenario: Adding Phone Sim kit to Cart in first Line
    Given I am in Cart page
    When I click on Device Image on Device tile in the first line
    And I click on Bring your own device Button
    Then I should see Sim kit Mini PDP
    And I should see Product name on Mini PDP
    When I click on Add to Cart Button on SIM kit Product Description Page
    Then I should see Sim kit added to cart

	Scenario: Adding MI Sim kit to Cart in Second Line
    Given I am in Cart Page
    When I click on Add a Tablet Button
    And I click on Add Device Icon in device tile for adding Tablet
    And I click on Bring your own Tablet button in device tile
    Then I should see Sim kit Mini PDP
    And I should see Tablet Sim Kit Product name on Mini PDP
    When I click on Add to Cart Button on SIM kit Product Description Page
    Then I should see MI Sim kit added to cart in second line

	Scenario: Navigate to checkout
    Given I am in a loaded cart page
    When I click on the 'Checkout' button
    Then the browser will navigate to the checkout section

	Scenario: Checkout - Personal Information
    Given I see the Personal Information section
    And I click on user-consent checkbox
    And I click on 'Next' button
    Then the browser will navigate to the Payment Information page

	Scenario: Checkout - Payment Information
    Given I am on the Payment and Credit Page
    And I enter "4444444444444448" in the credit card
    And I enter "0122" in the Expiry date
    And I enter "111" in the CVV

	Scenario: Checkout - Credit Information
    Given I see the Credit check section
    And I select "Passport" in the id
    And I enter "A1234567890" in the id
    And I enter "0122" in the expiry date
    And I enter "001110000" in the ssn
    And I enter "12/24/1984" in the dob
    And I click on the 'Agree and Next' button
    Then the browser will navigate to Review order page

	Scenario: Review Order details and submit
    Given I am on review order page
	Then I should see appropriate SIM kit details in review order page
    When I click on 'I Agree Disclaimer' Label for Terms and Conditions
    And I click on 'Submit Order' Button
    Then I should see the order confirmation page
