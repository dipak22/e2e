#features/e2e020.feature
Feature: Attempt to submit an order with Auth fail credentials and re-submit order after correcting the credentials
    As a user
    I want to submit the order with Authorization fail credentials
    And re-submit the order after correcting the credentials

Scenario: Launch t-mobile site and navigate to cart page
    Given I navigate to T-Mobile Cart page
    Then I should see "Add a phone" in the homepage
    When I click on Add a phone button in Empty Cart Page
    Then I should see cart title in cart page

Scenario: Navigate to credit class modal
    Given I am in cart page
    When I click on Add Device Icon in device tile
    And I click on 'Add a new phone' button in device tile
    Then I should see credit class modal

Scenario: Select Awesome Credit and navigate to mini browse modal
    Given I am in credit class modal
    When I select Awesome credit radio button
    And I click on 'Done' button
    Then I should see mini browse page

Scenario: Select device from mini browse
    Given I am on mini browse page
    When I select "Apple iPhone 7 Plus"
    Then I should see a button to add device to cart
    When I click on 'Add to cart' button
    Then I validate the selected device is added to Cart

Scenario: Navigate to checkout
    Given I am in a loaded cart page
    When I click on the 'Checkout' button
    Then the browser will navigate to the checkout section

Scenario: Checkout - Personal Information
    Given I see the Personal Information section
    And I enter "SDET" in the first name
    And I enter "T" in the middle initial
    And I enter "TMO" in the last name
    And I enter "qat.tmo@gmail.com" in the email address
    And I enter "4254254250" in the phone number

Scenario: Checkout - Shipping Information
    Given I see the Shipping section
    And I enter "10 main st" in the shipping address
    And I enter "Beverly Hills" in the city
    And I select "CA" in the State
    And I enter "90210" in the zip
    And I click on user-consent checkbox
    And I click on 'Next' button
    Then the browser will navigate to the Payment Information page
        
Scenario: Checkout - Payment Information
    Given I am on the Payment and Credit Page
    And I enter "5434347823433412" in the credit card
    And I enter "0122" in the Expiry date
    And I enter "111" in the CVV

Scenario: Checkout - Credit Information
    Given I see the Credit check section
    And I select "Passport" in the id
    And I enter "A1234567890" in the id
    And I enter "0120" in the expiry date
    And I enter "001110000" in the ssn
    And I enter "12/24/1984" in the dob
    And I click on the 'Agree and Next' button
    Then the browser will navigate to Review order page

Scenario: Checkout - Authorization fail check
    Given I am on review order page
    Then I should see appropriate device details
    When I click on 'I Agree Disclaimer' Label for Terms and Conditions
    And I click on 'Submit Order' Button
    Then I should see the Authorization fail modal

Scenario: Authorization fail to Payment section
    When I am on Authorization fail modal
    And I click on 'Got it' button
    Then the browser will navigate to the Payment Information page

Scenario: Enter valid Payment details
    Given I am on the Payment and Credit Page
    And I clear credit card number
    And I enter "4444444444444448" in the credit card
    And I clear credit card expiry date
    And I enter "0122" in the Expiry date
    And I clear CVV
    And I enter "111" in the CVV
    And I click on the 'Agree and Next' button

Scenario: Review and Submit Order
    Given I am on review order page
    Then I should see appropriate device details
    When I click on 'I Agree Disclaimer' Label for Terms and Conditions
    And I click on 'Submit Order' Button
    Then I should see the order confirmation page
    
Scenario: Clean-up
    And Clear the browser cache