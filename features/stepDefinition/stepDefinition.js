//features/stepDefinitions/stepDefinition.js
'use strict';
var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);
var expect = chai.expect;
var EC = protractor.ExpectedConditions;

var cartPage = require('../pageObjects/Smartcart.js');
var commonFile = require('../pageObjects/Common.js');
var checkoutpersonalinfo = require('../pageObjects/CheckoutPersonalInfo.js');
var shippingpayment = require('../pageObjects/CheckoutShippingInfo.js');
var creditpage = require('../pageObjects/CreditCheck.js');
var revieworderpage = require('../pageObjects/ReviewOrder.js');
var browseDevicePage = require('../pageObjects/BrowseDevices.js');
var orderConfirmation = require('../pageObjects/ConfirmationPage.js');
var upperFunnelDetails = require('../pageObjects/UpperFunnel.js');
var myTMODetails = require('../pageObjects/myTMO.js');
var SelectWrapper = require('../pageObjects/select-wrapper.js');
var helperFile = require('../pageObjects/helperFunction.js');

var helper = new helperFile();
var Smartcart = new cartPage();
var BrowseDevices = new browseDevicePage();
var Common = new commonFile();
var personalinfo = new checkoutpersonalinfo();
var shipping = new shippingpayment();
var credit = new creditpage();
var review = new revieworderpage();
var confirmationPage = new orderConfirmation();
var upperFunnel = new upperFunnelDetails();
var myTMO = new myTMODetails();

var firstName;

var phoneNameLine1;
var phoneColorLine1;
var phoneTodayPriceLine1;
var phoneMonthlyPriceLine1;

var phoneNameLine2;
var phoneColorLine2;
var phoneTodayPriceLine2;
var phoneMonthlyPriceLine2;

var phoneNameLine3;
var phoneColorLine3;
var phoneTodayPriceLine3;
var phoneMonthlyPriceLine3;

var tabletNameLine1;
var tabletColorLine1;
var tabletTodayPriceLine1;
var tabletMonthlyPriceLine1;

var tabletNameLine2;
var tabletColorLine2;
var tabletTodayPriceLine2;
var tabletMonthlyPriceLine2;

var tabletNameLine3;
var tabletColorLine3;
var tabletTodayPriceLine3;
var tabletMonthlyPriceLine3;

var wearableNameLine1;
var wearableColorLine1;
var wearableTodayPriceLine1;
var wearableMonthlyPriceLine1;

var wearableNameLine2;
var wearableColorLine2;
var wearableTodayPriceLine2;
var wearableMonthlyPriceLine2;

var wearableNameLine3;
var wearableColorLine3;
var wearableTodayPriceLine3;
var wearableMonthlyPriceLine3;

var ufAccessoryName;
var accessoryNameLine1;
var accessoryColorLine1;
var accessoryTodayPriceLine1;
var accessoryMonthlyPriceLine1;

var accessoryNameLine2;
var accessoryColorLine2;
var accessoryTodayPriceLine2;
var accessoryMonthlyPriceLine2;

var accessoryNameLine3;
var accessoryColorLine3;
var accessoryTodayPriceLine3;
var accessoryMonthlyPriceLine3;
var accessoryName;
var accessoryImage;

var SIMkitName;
var SIMkitName1;
var SIMStarterkitName;

var tradeinModelName;
var tradeinIMEI;
var standardTradeinValue;
var promotionTradeinValue;
var tradeinValueLink;
var cartLineCount;
var myTMOCookieData;


//*********************************************Common Steps- Clear Cache, Go to URL etc************************************

module.exports = function () {
    this.setDefaultTimeout(60 * 2000);

    function checkRepeaterCount(ArrayElement) {
        ArrayElement.then(function (elementsLookup) {
            ArrayElement.count().then(function (elementCount) {
                console.log("Element Count is" + elementCount);
                if (elementCount > 0) {
                    return;
                } else {
                    checkRepeaterCount(ArrayElement);
                }
            });
        });
    }

    function updateSendKeys(ele, data) {
        ele.sendKeys(data);
        ele.getAttribute("value").then(function (attributeValue) {
            attributeValue = attributeValue.replace(/\D+/g, '');
            data = data.replace(/\D+/g, '');
            if (data == attributeValue) {
                return;
            }
            else {
                ele.clear();
                updateSendKeys(ele, data);
            }
        });
    }

    // DEVICE COLOR SELECTOR
    function selectColorFromArray(elementArray, data) {
        console.log("I am inside the function");
        elementArray.then(function (colorArray) {
            colorArray.forEach(function (colorElement, colorCounter) {
                console.log(colorCounter);
                colorElement.element(by.tagName('img')).getAttribute('alt').then(function (colorValue) {
                    //colorValue = colorValue = ' color';
                    console.log(colorValue);
                    if (colorValue.indexOf(data) > 0) {
                        console.log("Desired color");
                        colorElement.click();
                    }
                });
            })
        });
    }

    this.defineStep(/^I go to "([^"]*)"$/, function (site) {
        browser.get(site);
        browser.manage().window().maximize();
    });

    this.defineStep(/^I wait to load page$/, function () {
        browser.waitForAngular();
        browser.driver.sleep(1000);
    });

    this.defineStep(/^Clear the browser cache$/, function () {
        browser.manage().deleteAllCookies();
        browser.executeScript('window.sessionStorage.clear();');
        browser.executeScript('window.localStorage.clear();');
    });

    this.defineStep(/^Clear the cache and cookies$/, function () {
        browser.manage().deleteAllCookies();
        browser.executeScript('window.sessionStorage.clear();');
        browser.executeScript('window.localStorage.clear();');
    });

    //*********************************************************HOME PAGE**************************************************************

    //This will be removed
    this.defineStep(/^I click on hamburger menu$/, function () {
        var clickHamBurger = element.all(by.css('[class="hamburger"]'));
        return clickHamBurger.click();
    });

    //This will be removed
    this.defineStep(/^I click on cell\-phone link from nav$/, function () {
        var goToAccessoryBrowse = element.all(by.css('[data-analytics-id="12f22de70746fb51b867a59b79c467ce9fc12c51-link3-side"]'));
        return goToAccessoryBrowse.click();
    });

    this.defineStep(/^I click on cell phones link$/, function () {
        var goToCBrowse = element(by.css('[data-analytics-id="12f22de70746fb51b867a59b79c467ce9fc12c51-link3-side"]')); //element.all should be followed by get() hence removing this .all as already the locaters are unique/////
        return goToCBrowse.click();
    });

    this.defineStep(/^I click on Accessories link$/, function () {
        var goToCBrowse = element(by.css('[data-analytics-id="12f22de70746fb51b867a59b79c467ce9fc12c51-link3-side-sublink4"]'));
        return goToCBrowse.click();
    });

    this.defineStep(/^I click on internet devices link$/, function () {
        var goToIBrowse = element(by.css('[data-analytics-id="12f22de70746fb51b867a59b79c467ce9fc12c51-link3-side-sublink1"]'));
        return goToIBrowse.click();
    });

    //*********************************************************CELL-PHONES PLP*********************************************************

    this.defineStep(/^I should navigate to cell\-phones browse page$/, function () {
        var checkCP = element(by.id('credit-class-selector')).element(by.css('h6[role="heading"]'));
        checkCP.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return checkCP.getText().then(function (CPOne) {
            expect(CPOne).to.equal('Please select your credit.');
        });
    });

    this.defineStep(/^I am on cell\-phones browse page$/, function () {
        var checkCP = element(by.id('credit-class-selector')).element(by.css('h6[role="heading"]'));
        return checkCP.getText().then(function (CPOne) {
            expect(CPOne).to.equal('Please select your credit.');
        });
    });

    this.defineStep(/^I select Awesome Credit$/, function () {
        browser.sleep(2000);
        return upperFunnel.getAwesomeCredit().getText().then(function (creditText) {
            browser.wait(EC.visibilityOf(upperFunnel.getAwesomeCredit()), 30000);
            browser.executeScript("arguments[0].scrollIntoView(false);", upperFunnel.getAwesomeCredit().getWebElement());
            upperFunnel.getAwesomeCredit().click();
        });
    });

    this.defineStep(/^I select Average Credit$/, function () {
        browser.sleep(2000);
        return upperFunnel.getAverageCredit().getText().then(function (creditText) {
            browser.wait(EC.visibilityOf(upperFunnel.getAverageCredit()), 30000);
            browser.executeScript("arguments[0].scrollIntoView(false);", upperFunnel.getAverageCredit().getWebElement());
            upperFunnel.getAverageCredit().click();
        });
    });

    this.defineStep(/^I select No Credit$/, function () {
        browser.sleep(2000);
        return upperFunnel.getNoCredit().getText().then(function (creditText) {
            browser.wait(EC.visibilityOf(upperFunnel.getNoCredit()), 30000);
            browser.executeScript("arguments[0].scrollIntoView(false);", upperFunnel.getNoCredit().getWebElement());
            upperFunnel.getNoCredit().click();
        });
    });

    //---Below code will be refactored for the older features-----

    this.defineStep(/^I click credit one$/, function () {
        browser.sleep(2000);
        return upperFunnel.getAwesomeCredit().getText().then(function (creditText) {
            browser.wait(EC.visibilityOf(upperFunnel.getAwesomeCredit()), 30000);
            browser.executeScript("arguments[0].scrollIntoView(false);", upperFunnel.getAwesomeCredit().getWebElement());
            upperFunnel.getAwesomeCredit().click();
        });
    });

    this.defineStep(/^I click credit two$/, function () {
        browser.sleep(2000);
        return upperFunnel.getAverageCredit().getText().then(function (creditText) {
            browser.wait(EC.visibilityOf(upperFunnel.getAverageCredit()), 30000);
            browser.executeScript("arguments[0].scrollIntoView(false);", upperFunnel.getAverageCredit().getWebElement());
            upperFunnel.getAverageCredit().click();
        });
    });

    this.defineStep(/^I click credit three$/, function () {
        browser.sleep(2000);
        return upperFunnel.getNoCredit().getText().then(function (creditText) {
            browser.wait(EC.visibilityOf(upperFunnel.getNoCredit()), 30000);
            browser.executeScript("arguments[0].scrollIntoView(false);", upperFunnel.getNoCredit().getWebElement());
            upperFunnel.getNoCredit().click();
        });
    });

    this.defineStep(/^I look for product one on cell phone browse page$/, function () {
        browser.sleep(5000);
        var Cellphonebrowse = element.all(by.css('[ng-bind="vm.product.productName"]')).get(0);
        return Cellphonebrowse.getText().then(function (CellPhoneDeviceOne) {
            expect(CellPhoneDeviceOne).to.equal('Apple iPhone 7 Plus');
        });
    });

    this.defineStep(/^I look for product two on cell phone browse page$/, function () {
        browser.sleep(5000);
        var Cellphonebrowse = element.all(by.css('[ng-bind="vm.product.productName"]')).get(1);
        return Cellphonebrowse.getText().then(function (CellPhoneDeviceTwo) {
            expect(CellPhoneDeviceTwo).to.equal('Samsung Galaxy S7');
        });
    });

    this.defineStep(/^I look for product three on cell phone browse page$/, function () {
        browser.sleep(5000);
        var Cellphonebrowse = element.all(by.css('[ng-bind="vm.product.productName"]')).get(2);
        return Cellphonebrowse.getText().then(function (CellPhoneDeviceThree) {
            expect(CellPhoneDeviceThree).to.equal('Apple iPhone 6s');
        });
    });

    this.defineStep(/^I select no credit from browse page$/, function () {
        var checkNoCredit = element.all(by.css('[aria-labeledby="No credit"]'));
        browser.sleep(5000);
        return checkNoCredit.click();
    });

    this.defineStep(/^I select Apple iPhone seven from browse page$/, function () {
        var selectPhone = element.all(by.css('[class="g-BCE0E79BD93D4E2D9ED56676048B628B-img"]')).get(0);
        selectPhone.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        browser.sleep(8000);
        return selectPhone.click();

    });

    this.defineStep(/^I select Apple iPhone seven plus from browse page$/, function () {
        var selectPhone = element(by.css('[class="g-3DF495D6C11B473EAA5986855F9DE1C0-img"]'));
        selectPhone.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectPhone.click();
    });

    //Updated for e2e008
    //////updated for e2e008///////////
    this.defineStep(/^I select any device with carousel from cell phone browse page$/, function () {
        browser.manage().window().maximize();
        var selectCPhone = element.all(by.binding('vm.product.productName')).get(0); //updated the master steps file
        selectCPhone.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        browser.sleep(8000);
        return selectCPhone.click();
        //var phonename = selectCPhone.getText();
        //console.log("abc:", phonename);
    });

    this.defineStep(/^I select BYOD cell phones SIM from browse page$/, function () {
        browser.manage().window().maximize();
        var selectBYODPhone = element.all(by.css('[class="i-21225F913860490989C4738E867119BB-img"]')).get(0);
        browser.wait(EC.invisibilityOf(element(by.css('.screen-lock'))), 20000);
        browser.wait(EC.elementToBeClickable(selectBYODPhone), 20000);
        selectBYODPhone.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        selectBYODPhone.click();
        return browser.sleep(8000);
    });

    //*******************************************************INTERNET DEVICES PLP****************************************************

    this.defineStep(/^I look for product one on internet devices browse page$/, function () {
        var Internetbrowse = element.all(by.css('[ng-bind="vm.product.productName"]')).get(0);
        return Internetbrowse.getText().then(function (InternetDeviceOne) {
            expect(InternetDeviceOne).to.equal('Apple 9.7 inch iPad Pro');
        });
    });

    this.defineStep(/^I look for product two on internet devices browse page$/, function () {
        var Internetbrowse = element.all(by.css('[ng-bind="vm.product.productName"]')).get(0);
        return Internetbrowse.getText().then(function (InternetDeviceTwo) {
            expect(InternetDeviceTwo).to.equal('Apple 9.7 inch iPad Pro');
        });
    });

    this.defineStep(/^I look for product three on internet devices browse page$/, function () {
        var Internetbrowse = element.all(by.css('[ng-bind="vm.product.productName"]')).get(0);
        return Internetbrowse.getText().then(function (InternetDeviceThree) {
            expect(InternetDeviceThree).to.equal('Apple 9.7 inch iPad Pro');
        });
    });

    this.defineStep(/^I select Apple iPad from browse page$/, function () {
        var selectIPad = element.all(by.css('[class="g-D6BCBDB807BA4EC2BB407412AF85DB67-img"]'));
        browser.manage().window().maximize();
        selectIPad.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        browser.sleep(8000);
        return selectIPad.click();
    });

    this.defineStep(/^I select any device with carousel from internet devices browse page$/, function () {
        browser.manage().window().maximize();
        browser.sleep(8000);
        var selectID = element.all(by.css('[ng-bind="vm.product.productName"]')).get(0);
        selectID.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        browser.sleep(8000);
        return selectID.click();
    });

    this.defineStep(/^I select BYOD internet\-devices SIM from browse page$/, function () {
        var selectBYODTablet = element.all(by.css('[class="i-DC14CC31D84F4BB7A9547DB4DDD19DF8-img"]')).get(0);
        browser.wait(EC.invisibilityOf(element(by.css('.screen-lock'))), 20000);
        browser.wait(EC.elementToBeClickable(selectBYODTablet), 20000);
        selectBYODTablet.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        selectBYODTablet.click();
        return browser.sleep(8000);
    });

    this.defineStep(/^I should navigate to internet devices browse page$/, function () {
        browser.driver.sleep(12000);
        var checkID = element(by.id('credit-class-selector')).element(by.css('h6[role="heading"]'));
        checkID.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return checkID.getText().then(function (IEOne) {
            expect(IEOne).to.equal('Please select your credit.');
        });
    });

    this.defineStep(/^I am on internet devices browse page$/, function () {
        var checkID = element(by.id('credit-class-selector')).element(by.css('h6[role="heading"]'));
        return checkID.getText().then(function (IEOne) {
            expect(IEOne).to.equal('Please select your credit.');
        });
    });

    this.defineStep(/^I select Apple iPad$/, function () {
        var selectID = element.all(by.css('[class="g-D6BCBDB807BA4EC2BB407412AF85DB67-img"]')).get(0);
        selectID.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        selectID.click();
    });

    this.defineStep(/^I select iPad$/, function () //new function
    {
        var productclick = element.all(by.binding('vm.product.productName')).get(1); // locater to select the product from the browse page
        productclick.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            productclick.getText().then(function (productname) {
                console.log("selected product name is", productname);
                productclick.click(); //click on the product selected from the browse page
            });
        });
    });

    this.defineStep(/^I add iPad to cart$/, function () {
        browser.sleep(8000);
        var selectIPad = element(by.css('[ng-click="vm.addDeviceToCart(vm.productDetailsService.model.selectedProduct,vm.productDetailsService.model.productCategoryType)"]'));
        browser.wait(EC.invisibilityOf(element(by.css('.screen-lock'))), 20000);
        browser.wait(EC.elementToBeClickable(selectIPad), 20000);
        selectIPad.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        selectIPad.click()
        return browser.sleep(8000);
    });
    //end of updates
    //************************************************************ACCESSORIES PLP************************************************

    this.defineStep(/^I should navigate to accessories browse page$/, function () {
        var checkAcc = element(by.id('credit-class-selector')).element(by.css('h6[role="heading"]'));
        return checkAcc.getText().then(function (CAOne) {
            expect(CAOne).to.equal('Please select your credit.');
        });
    });

    this.defineStep(/^I select accessory from browse page$/, function () {
        var selectAccessory = element.all(by.binding('vm.product.productName')).get(10);
        selectAccessory.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            selectAccessory.getText().then(function (accessoryname) {
                accessoryNameLine1 = accessoryname;
                console.log("category selected is", accessoryname); //print the name of the accessory selected
                selectAccessory.click(); //click on the accessory product selected
            });
        });
    });

    this.defineStep(/^I click on Sorting Drop Down in Accessory browse page$/, function () {
        var selectSort = element.all(by.id('sort')).get(0);
        selectSort.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            return selectSort.click();
        });
    });

    this.defineStep(/^I select sort value as Price High To Low$/, function () {
        return browser.wait(EC.visibilityOf(upperFunnel.selectHTL()), 30000).then(function (isVisible) {
            if (isVisible) {
                upperFunnel.selectHTL().click();
            }
        });
    });

    this.defineStep(/^I validate Price High To Low is selected in sort options$/, function () {
        var checkSort = element.all(by.css('[class="text ng-binding"]')).get(0);
        return checkSort.getText().then(function (Sorted) {
            expect(Sorted).to.equal('Price high to low');
        });
    });

    this.defineStep(/^I select the first accessory from browse page$/, function () {
        var selectHTLA = element.all(by.css('[ng-bind="vm.product.productName"]')).get(0);
        selectHTLA.getText().then(function (accessoryName) {
            accessoryNameLine1 = accessoryName;
        });
        selectHTLA.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectHTLA.click();
    });

    //-----Below code will be refactored for older fatures ------
    this.defineStep(/^I am on accessories browse page$/, function () {
        var checkAccessory = element(by.id('credit-class-selector')).element(by.css('h6[role="heading"]'));
        return checkAccessory.getText().then(function (AccessoryOne) {
            expect(AccessoryOne).to.equal('Please select your credit.');
        });
    });

    this.defineStep(/^I look for product one on accessories browse page$/, function () {
        var Accessorybrowse = element.all(by.css('[ng-bind="vm.product.productName"]')).get(0);
        return Accessorybrowse.getText().then(function (AccessoryOne) {
            expect(AccessoryOne).to.equal('LG Aristo T-Mobile Flex Protective Cover - Black');
        });
    });

    this.defineStep(/^I look for product two on accessories browse page$/, function () {
        var Accessorybrowse = element.all(by.css('[ng-bind="vm.product.productName"]')).get(2);
        return Accessorybrowse.getText().then(function (AccessoryTwo) {
            expect(AccessoryTwo).to.equal('Wired Selfie Stick - Black & White');
        });
    });

    this.defineStep(/^I look for product three on accessories browse page$/, function () {
        var Accessorybrowse = element.all(by.css('[ng-bind="vm.product.productName"]')).get(3);
        return Accessorybrowse.getText().then(function (AccessoryThree) {
            expect(AccessoryThree).to.equal('Apple Lightning to 3.5MM Headphone Adapter');
        });
    });

    this.defineStep(/^I navigate to the page two$/, function () {
        browser.manage().window().maximize();
        var goToPage = element.all(by.css('[ng-click="vm.updateSelection(num, $index)"]')).get(1);
        goToPage.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return goToPage.click();
    });

    this.defineStep(/^I select USB cable from browse page$/, function () {
        var selectAccessory = element.all(by.css('[class="i-C34A405FEFA24E61834D7408C3446A55-img"]'));
        selectAccessory.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        browser.sleep(8000);
        return selectAccessory.click();
    });

    this.defineStep(/^I select any accessory with carousel from accessories browse page$/, function () {
        browser.manage().window().maximize();
        browser.sleep(8000);
        var selectAcc = element.all(by.css('[ng-bind="vm.product.productName"]')).get(0);
        selectAcc.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        browser.sleep(8000);
        return selectAcc.click();
    });

    //********************************************************PDP Redesigning****************************************//

    this.defineStep(/^I select memory from main PDP$/, function () {
        browser.sleep(1000);
        //phoneMemoryLine1 = memoryName;
        return upperFunnel.getMemoryUP().getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            browser.sleep(1000);
            upperFunnel.getMemoryUP().click();
        });
    });

    this.defineStep(/^I select color swatch from main PDP$/, function () {
        browser.sleep(3000);
        //phoneColorLine1 = colorName;
        return upperFunnel.getSwatchUP().getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            browser.sleep(3000);
            upperFunnel.getSwatchUP().click();
        });
    });


    /**this.defineStep(/^I select Awesome credit from main PDP$/, function () {
        var selectCredit1 = element(by.css('[ng-bind="vm.detailCreditAuthoringData.awesomeBlockTxt"]'));
        selectCredit1.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectCredit1.click();
    });**/

    this.defineStep(/^I select Awesome credit from main PDP$/, function () {
        return element(by.css('[ng-bind="vm.detailCreditAuthoringData.awesomeBlockTxt"]')).getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            element(by.css('[ng-bind="vm.detailCreditAuthoringData.awesomeBlockTxt"]')).click();
        });
    });

    this.defineStep(/^I select Average credit from main PDP$/, function () {
        var selectCredit2 = element(by.css('[ng-bind="vm.detailCreditAuthoringData.avgBlockTxt"]'));
        return selectCredit2.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            selectCredit2.click();
        });
    });

    this.defineStep(/^I select No credit from main PDP$/, function () {
        var selectCredit3 = element(by.css('[ng-bind="vm.detailCreditAuthoringData.noCreditTxt"]'));
        return selectCredit3.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            selectCredit3.click();
        });
    });

    this.defineStep(/^I select monthly price as payment option from main PDP$/, function () {
        return upperFunnel.getPaymentEIPUP().getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            upperFunnel.getPaymentEIPUP().click();
        });
    });

    this.defineStep(/^I select full price as payment option from main PDP$/, function () {
        return upperFunnel.getPaymentFRPUP().getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            upperFunnel.getPaymentFRPUP().click();
        });
    });


    		this.defineStep(/^I should see review stars on main PDP$/, function () {
            return upperFunnel.checkRStars().getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            upperFunnel.checkRStars().getText().then(function (Rstars) {
            expect(Rstars).to.not.equal('');
            });
    		});
    		});

    	    this.defineStep(/^I should see total ratings on main PDP$/, function () {
            return upperFunnel.checkRating().getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            upperFunnel.checkRating().getText().then(function (Rating) {
            expect(Rating).to.not.equal('');
            });
    		});
    		});

    		this.defineStep(/^I should see EIP legal text on main PDP$/, function () {
            return upperFunnel.checkEIPLegal().getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            upperFunnel.checkEIPLegal().getText().then(function (legalText) {
            expect(legalText).to.not.equal('');
            });
    		});
    		});

    	    this.defineStep(/^I should see sim kit on main PDP$/, function () { /*This is for New SIM Kit from Sprint 23*/
            return upperFunnel.checkSIMKit().getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            upperFunnel.checkSIMKit().getText().then(function (simKit) {
            expect(simKit).to.not.equal('');
            });
    		});
    		});




    //*******************************************PRODUCT DESCRIPTION PAGE - CELLPHONES/INTERNET DEVICES***************************************

    this.defineStep(/^I navigate to Product detail page of "([^"]*)"$/, function (phoneName) {
        phoneNameLine1 = phoneName;
        browser.sleep(2000);
        browser.ignoreSynchronization = true;
        return browser.wait(EC.visibilityOf(upperFunnel.getMainPDPDetails()), 50000).then(function () {
            upperFunnel.getMainPDPDetails().getText().then(function (data) {
                expect(data).to.equal(phoneName);
            });
        });
    });

    this.defineStep(/^I navigate to Product detail page of "([^"]*)" accessory$/, function (accessoryName) {
        accessoryNameLine1 = accessoryName;
        return browser.wait(EC.visibilityOf(upperFunnel.getAccessoryMainPDPDetails()), 30000).then(function (isVisible) {
            if (isVisible) {
                upperFunnel.getAccessoryMainPDPDetails().getText().then(function (title) {
                    expect(title).to.equal(accessoryName);
                });
            }
        });
    });

    this.defineStep(/^I select "([^"]*)" as memory of the device from memory variants dropdown list$/, function (memorySize) {
        var myMemory = new SelectWrapper(by.id('selectedMemory'));
        return myMemory.selectByText(memorySize);
    });

    this.defineStep(/^I select device color as "([^"]*)"$/, function (colorName) {
        return selectColorFromArray(upperFunnel.getColorSwatch(), colorName);
    });

    this.defineStep(/^I select color as "([^"]*)"$/, function (colorName) {
        var colorSwatch = element(by.css('img["alt="' + colorName + ' color"]'));
        return colorSwatch.click();
    });

    this.defineStep(/^I select color as "([^"]*)" in Mini PDP$/, function (colorName) {
        var colorSwatch = element.all(by.repeater('product in miniPDPModalCtrl.filteredProductData'));
        selectColorFromArray(colorSwatch, colorName);
        console.log("I am going to launch selectColorFromArray function");
        browser.sleep(5000)

    });

    this.defineStep(/^I am on Product detail page of "([^"]*)"$/, function (phoneName) {
        phoneNameLine1 = phoneName;
        return upperFunnel.getMainPDPDetails().getText().then(function (data) {
            expect(data).to.equal(phoneName);
        });
    });

    this.defineStep(/^I add "([^"]*)" to cart by clicking on add to cart button$/, function (phoneName) {
        var selectPhone = element(by.css('[ng-click="vm.addDeviceToCart(vm.productDetailsService.model.selectedProduct,vm.productDetailsService.model.productCategoryType)"]'));
        selectPhone.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectPhone.click()
        return browser.sleep(8000);
    });

    this.defineStep(/^I am on Product detail page of Apple iPhone seven Plus$/, function () {
        var checkPDP = element(by.id('productDescription')).element(by.css('h3[role="heading"]'));
        return checkPDP.getText().then(function (CPOne) {
            expect(CPOne).to.equal('Apple iPhone 7 Plus');
        });
    });

    this.defineStep(/^I click on 'Add to cart' button in PDP Page$/, function () {
        return upperFunnel.getAddToCartButton().getText().then(function (buttonData) {
            browser.sleep(2000);
            browser.executeScript("arguments[0].scrollIntoView(false);", upperFunnel.getAddToCartButton().getWebElement());
            browser.sleep(1000);
            upperFunnel.getAddToCartButton().getLocation().then(function (ElementLocation) {
                var yAxisValue = ElementLocation.y - 250;
                browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
                upperFunnel.getAddToCartButton().click();
            });
        });
    });

    this.defineStep(/^I add Apple iPhone seven Plus to cart by clicking on 'Add to cart' button$/, function () {
        var selectPhone = element(by.css('[ng-click="vm.addDeviceToCart(vm.productDetailsService.model.selectedProduct,vm.productDetailsService.model.productCategoryType)"]'));
        selectPhone.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        selectPhone.click()
        return browser.sleep(8000);
    });

    this.defineStep(/^I add "([^"]*)" to cart by clicking on 'Add to cart' button$/, function (phoneName) {
        phoneNameLine1 = phoneName;
        return upperFunnel.getAddToCartButton().getText().then(function (buttonData) {
            browser.sleep(2000);
            browser.executeScript("arguments[0].scrollIntoView(false);", upperFunnel.getAddToCartButton().getWebElement());
            browser.sleep(1000);
            upperFunnel.getAddToCartButton().getLocation().then(function (ElementLocation) {
                var yAxisValue = ElementLocation.y - 250;
                browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
                upperFunnel.getAddToCartButton().click();
            });
        });
    });

    this.defineStep(/^I select "([^"]*)" from main browse$/, function (phoneName) {
        phoneNameLine1 = phoneName;
        return browser.wait(EC.visibilityOf(element(by.cssContainingText('.product-name', phoneName))), 50000).then(function () {
            element(by.cssContainingText('.product-name', phoneName)).getLocation().then(function (ElementLocation) {
                var yAxisValue = ElementLocation.y - 450;
                browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
                element(by.cssContainingText('.product-name', phoneName)).click();
            });
        });
    });

    this.defineStep(/^I select "([^"]*)" from Main browse$/, function (phoneName) {
        browser.sleep(10000);
        phoneNameLine1 = phoneName;
        var selectPhone = element(by.cssContainingText('.product-name', phoneName));
        browser.wait(EC.visibilityOf(selectPhone), 50000);
        //var selectPhone = element(by.xpath('//a[@class="m-b-5 product-name text-center regular block ng-binding" = phoneName]'));
        selectPhone.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectPhone.click();
    });

    this.defineStep(/^I select "([^"]*)" from Main Browse$/, function (phoneName) {
        phoneNameLine1 = phoneName;
        Smartcart.getBrowseDevices().count().then(function (value) {
            var devicelist = value;
            Smartcart.getBrowseDevices().then(function (browseDevices) {
                //browser.sleep(5000);
                browseDevices.forEach(function (deviceElement, deviceCount) {
                    deviceElement.getLocation().then(function (ElementLocation) {
                        var yAxisValue = ElementLocation.y - 250;
                        browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
                    });
                    //browser.wait(EC.visibilityOf(deviceElement.element(by.binding('vm.product.productName'))),50000);
                    deviceElement.element(by.binding('vm.product.productName')).getText().then(function (deviceName) {
                        if (deviceName == phoneNameLine1) {
                            browser.wait(EC.visibilityOf(Smartcart.getBrowseDevices().get(deviceCount).element(by.tagName('img'))), 50000);
                            return Smartcart.getBrowseDevices().get(deviceCount).element(by.tagName('img')).click();
                        }
                    });
                });
            });

        });
    });

    this.defineStep(/^I select "([^"]*)" accessory from main browse$/, function (accessoryName) {
        ufAccessoryName = accessoryName;
        return browser.executeScript(EC.visibilityOf(element(by.cssContainingText('.product-name', accessoryName))), 50000).then(function () {
            element(by.cssContainingText('.product-name', accessoryName)).getLocation().then(function (ElementLocation) {
                var yAxisValue = ElementLocation.y - 250;
                browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
                element(by.cssContainingText('.product-name', accessoryName)).click();
            });
        });
    });

    this.defineStep(/^I am on Product detail page of BYOD cell phones SIM$/, function () {
        var checkBYODPDP = element(by.id('productDescription')).element(by.css('h3[role="heading"]'));
        return checkBYODPDP.getText().then(function (IE) {
            expect(IE).to.equal('T-Mobile 3-in-1 SIM Starter Kit');
        });
    });

    //----- Below code will be refactored for the older features-----------------------

    this.defineStep(/^I look for magenta banner on the page$/, function () {
        browser.manage().window().maximize();
        browser.sleep(5000);
        var getMagentaBannerElement = element.all(by.id('promo-banner')).get(0);
        return getMagentaBannerElement.getCssValue('background-color').then(function (bannerColor) {
            expect(bannerColor).to.equal('rgba(226, 0, 116, 1)');
        });
    });

    this.defineStep(/^I try to click the available hyperlink modal$/, function () {
        browser.sleep(5000);
        var selectModal = element.all(by.css('[class="ng-binding ng-isolate-scope"]')).get(0);
        return selectModal.click();
    });

    this.defineStep(/^I close the opened modal$/, function () {
        browser.sleep(5000);
        var closeModal = element.all(by.css('[ng-click="$dismiss()"]'));
        return closeModal.click();
    });

    this.defineStep(/^I verify the whether the link has bold text style$/, function () {
        browser.sleep(5000);
        var getBannerLinkStyleElement = element.all(by.css('[class="ng-binding ng-isolate-scope"]')).get(0);
        return getBannerLinkStyleElement.getCssValue('font-weight').then(function (textStyle) {
            expect(textStyle).to.equal('bold');
        });
    });

    this.defineStep(/^I verify the link text has white color\.$/, function () {
        browser.sleep(5000);
        var getBannerLinkColorElement = element.all(by.css('[class="ng-binding ng-isolate-scope"]')).get(0);
        return getBannerLinkColorElement.getCssValue('text-decoration-color').then(function (linkColor) {
            expect(linkColor).to.equal('rgb(255, 255, 255)');
        });
    });

    this.defineStep(/^I add Apple iPhone seven to cart$/, function () {
        var selectCellPhonePDP = element(by.css('[ng-click="vm.addDeviceToCart(vm.productDetailsService.model.selectedProduct,vm.productDetailsService.model.productCategoryType)"]'));
        browser.wait(EC.invisibilityOf(element(by.css('.screen-lock'))), 20000);
        browser.wait(EC.elementToBeClickable(selectCellPhonePDP), 20000);
        selectCellPhonePDP.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        selectCellPhonePDP.click()
        return browser.sleep(8000);
    });

    this.defineStep(/^I add Apple iPad to cart$/, function () {
        browser.sleep(8000);
        var selectIPad = element(by.css('[ng-click="vm.addDeviceToCart(vm.productDetailsService.model.selectedProduct,vm.productDetailsService.model.productCategoryType)"]'));
        browser.wait(EC.invisibilityOf(element(by.css('.screen-lock'))), 20000);
        browser.wait(EC.elementToBeClickable(selectIPad), 20000);
        selectIPad.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        selectIPad.click()
        return browser.sleep(8000);
    });

    this.defineStep(/^I navigate to the 'Carousel Section' on cell phone PDP page to check for labels has non empty text$/, function () {
        browser.sleep(8000);
        var carouselCP = element(by.css('[ng-if="accessoriesCarouselCtrl.showCarousal==true"]')).element(by.tagName('h3'));
        //var carouselCP = element(by.css('[ng-bind="vm.recomemendedHeading"]')).get(0);
        browser.wait(EC.invisibilityOf(element(by.css('.screen-lock'))), 20000);
        browser.wait(EC.elementToBeClickable(carouselCP), 20000);
        carouselCP.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        browser.sleep(8000);
        return carouselCP.getText().then(function (CarouselCPDP) {
            expect(CarouselCPDP).to.not.equal('');
        });
    });

    this.defineStep(/^I navigate to the 'Carousel Section' on internet device PDP page to check for labels has non empty text$/, function () {
        browser.sleep(8000);
        var carouselID = element(by.css('[ng-if="accessoriesCarouselCtrl.showCarousal==true"]')).element(by.tagName('h3'));
        //var carouselID = element(by.css('[ng-bind="vm.recomemendedHeading"]')).get(0);
        browser.wait(EC.invisibilityOf(element(by.css('.screen-lock'))), 20000);
        browser.wait(EC.elementToBeClickable(carouselID), 20000);
        carouselID.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        browser.sleep(8000);
        return carouselID.getText().then(function (CarouselIDPDP) {
            expect(CarouselIDPDP).to.not.equal('');
        });
    });

    this.defineStep(/^I add BYOD cell phone SIM to cart$/, function () {
        var selectBYOD = element(by.css('[ng-click="vm.addDeviceToCart(vm.productDetailsService.model.selectedProduct,vm.productDetailsService.model.productCategoryType)"]'));
        browser.wait(EC.invisibilityOf(element(by.css('.screen-lock'))), 20000);
        browser.wait(EC.elementToBeClickable(selectBYOD), 20000);
        selectBYOD.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        selectBYOD.click();
        return browser.sleep(8000);
    });

    this.defineStep(/^I add BYOD internet\-devices SIM to cart$/, function () {
        var selectBYODTab = element.all(by.css('[ng-click="vm.addDeviceToCart(vm.productDetailsService.model.selectedProduct,vm.productDetailsService.model.productCategoryType)"]'));
        //browser.wait(EC.invisibilityOf(element(by.css('.screen-lock'))),20000);
        //browser.wait(EC.elementToBeClickable(selectBYODTab),20000);
        selectBYODTab.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        selectBYODTab.click();
        return browser.sleep(8000);
    });

    //****************************Added enhancements for e2e013*********************************************

    this.defineStep(/^I click on 'Edit Current Phone' button for line 2 to view the mini PDP$/, function () {
        var selectGoToPDP2 = element.all(by.css('[class="btn btn-primary ng-binding"]')).get(1);
        selectGoToPDP2.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectGoToPDP2.click();
    });

    this.defineStep(/^I click on 'Edit Current Phone' button for line 4 to view the mini PDP$/, function () {
        browser.driver.sleep(5000);
        var selectGoToPDP4 = element.all(by.css('[class="btn btn-primary ng-binding"]')).get(3);
        selectGoToPDP4.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        selectGoToPDP4.click();
        return browser.driver.sleep(3000);
    });


    //*************************************Added for e2e010*******************************************************

    this.defineStep(/^I add a phone to the cart with monthly payment$/, function () {
        var select = element(by.id('pricing'));
        // var select = element(by.id('pricing'));
        select.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            select.$('[value="string:monthly"]').click(); //for monthly payment
            //select.$('[value="string:full"]').click();  //for full payment
        });
        var addtocart = element(by.id('add-to-bag-btn')); //locater for add to cart CTA
        addtocart.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            addtocart.click(); // click on the add to cart CTA
        });
    });

    //*******************************************Added for e2e024*******************************//

    this.defineStep(/^I click on 'Browse All' Phones$/, function () {
        browser.driver.sleep(3000);
        var selectGoToMBrowse = element(by.css('[ng-click="deviceSelectorModalCtrl.browseAllCtaMethod($event)"]'));
        selectGoToMBrowse.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectGoToMBrowse.click();
    });

    this.defineStep(/^on Cart page deselect Phone Service$/, function () {
        browser.driver.sleep(3000);
        var deselectPhoneService = element.all(by.css('[ng-bind-html="service.familyName"]')).get(0);
        deselectPhoneService.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        deselectPhoneService.click();
        return browser.sleep(8000);
    });

    this.defineStep(/^on Cart page deselect Tablet Services$/, function () {
        browser.driver.sleep(8000);
        var deselectTabletService = element.all(by.css('[ng-bind-html="service.familyName"]')).get(2);
        deselectTabletService.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        deselectTabletService.click();
        return browser.sleep(8000);
    });

    this.defineStep(/^I look for created empty line three$/, function () {
        browser.driver.sleep(8000);
        var LineThree = element.all(by.css('[ng-bind-html="myCartCtrl.keyValues.cartRowItems.deviceTitle | parseHtml"]')).get(2);
        return LineThree.getText().then(function (CPDeviceHeader) {
            expect(CPDeviceHeader).to.not.equal('');
            browser.driver.sleep(8000);
        });
    });

    //****************************************************PRODUCT DESCRIPTION PAGE - ACCESSORIES***********************************************

    this.defineStep(/^I should navigate to Product detail page of selected accessory$/, function () {
        var checkPDP = element(by.id('productDescription')).element(by.css('h3[role="heading"]'));
        browser.wait(EC.invisibilityOf(element(by.css('.screen-lock'))), 20000);
        browser.wait(EC.elementToBeClickable(checkPDP), 20000);
        return checkPDP.getText().then(function (CP) {
            expect(CP).to.equal(accessoryNameLine1);
        });
    });
    //////////////////////////****Newly added step related to e2e007 scenario*******//////////////////////////////////////////////////////////////////////

    this.defineStep(/^add it to the cart with quantity greater than "([^"]*)" and cost greater than "([^"]*)"$/, function (arg1, arg2) {
        var select = element(by.name('accessoryQuantity')); //select from the quantity drop down
        select.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            select.$('[value="number:3"]').click(); //click on the option to be selected by value
            var AddtoCart = element(by.id('add-to-bag-btn')); //locater for Add to cart CTA
            AddtoCart.getLocation().then(function (ElementLocation) {
                var yAxisValue = ElementLocation.y - 250;
                browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
                AddtoCart.click(); //click on the add to cart cta
            });
        });
    });

    this.defineStep(/^I add selected accessory to cart$/, function () {
        var selectAccessoryP = element(by.css('[ng-click="vm.addDeviceToCart(vm.productDetailsService.model.selectedProduct,vm.productDetailsService.model.productCategoryType)"]'));
        browser.wait(EC.invisibilityOf(element(by.css('.screen-lock'))), 20000);
        browser.wait(EC.elementToBeClickable(selectAccessoryP), 20000);
        return selectAccessoryP.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            selectAccessoryP.click();
        });
    });

    this.defineStep(/^I add USB cable to cart$/, function () {
        var selectAccessoryPDP = element.all(by.css('[class="btn btn-primary ng-binding ng-scope"]'));
        browser.sleep(8000);
        return selectAccessoryPDP.click()
    });

    this.defineStep(/^I navigate to the 'Carousel Section' on accessory PDP page to check for labels has non empty text$/, function () {
        browser.sleep(8000);
        var carouselAccPD = element(by.css('[ng-if="accessoriesCarouselCtrl.showCarousal==true"]')).element(by.tagName('h3'));
        carouselAccPD.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        browser.sleep(8000);
        return carouselAccPD.getText().then(function (carouselAccPDP) {
            expect(carouselAccPDP).to.not.equal('');
            browser.sleep(8000);
        });
    });

    //******************************************Adding MI SIM kit**********************************************************************************
    this.defineStep(/^I am on "([^"]*)"$/, function (url) {
        return browser.get(url);
    });

    this.defineStep(/^I navigate to "([^"]*)"$/, function (url) {
        browser.get(url);
        browser.manage().window().maximize();
    });

    this.defineStep(/^I navigate to T-Mobile home page$/, function () {
        browser.get("/");
        browser.manage().window().maximize();
    });

    this.defineStep(/^I navigate to T-Mobile cell-phones page$/, function () {
        browser.get("/cell-phones");
        browser.manage().window().maximize();
    });

    this.defineStep(/^I navigate to T-Mobile Tablets page$/, function () {
        browser.get("/internet-devices");
        browser.manage().window().maximize();
    });

    this.defineStep(/^I navigate to T-Mobile Accessories page$/, function () {
        browser.get("/accessories");
        browser.manage().window().maximize();
    });

    this.defineStep(/^I navigate to T-Mobile Cart page$/, function () {
        browser.get("/cart");
        browser.manage().window().maximize();
    });

    this.defineStep(/^I should see Awesome Credit is selected$/, function () {
        browser.manage().window().maximize();
        var checkAwesomeCredit = element.all(by.repeater('creditClass in vm.model.creditClassData')).get(0).element(by.css('.creditClassText'));
        return checkAwesomeCredit.getText().then(function (awesometext) {
            console.log(awesometext);
        });
    });

    this.defineStep(/^I select T-Mobile SIM starter kit$/, function () {
        var selectBYODTablet = element.all(by.css('[class="i-DC14CC31D84F4BB7A9547DB4DDD19DF8-img"]')).get(0);
        browser.wait(EC.invisibilityOf(element(by.css('.screen-lock'))), 20000);
        browser.wait(EC.elementToBeClickable(selectBYODTablet), 20000);
        selectBYODTablet.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        selectBYODTablet.click();
    });

    //***********************************Adding device from home Page****************************************************************/

    this.defineStep(/^I click on phones link from nav$/, function () {
        var goToAccessoryBrowse = element.all(by.css('[data-analytics-id="12f22de70746fb51b867a59b79c467ce9fc12c51-link3-side"]'));
        return goToAccessoryBrowse.click();
    });

    this.defineStep(/^I validate the selected device is added to cart$/, function () {
        //browser.wait(EC.visibilityOf(Smartcart.getAddedDeviceName()),50000);
        //return Smartcart.getAddedDeviceName().getText().then(function (header) {
        //expect(phoneNameLine1).to.include(header);
        //});
        browser.wait(EC.visibilityOf(Smartcart.getAddedDeviceName()), 50000);
        var IDeviceheader = element(by.id('cartDeviceSection_1')).element(by.css('h5[role="heading"]'));
        return IDeviceheader.getText().then(function (CellPhoneDeviceHeader) {
            expect(CellPhoneDeviceHeader).to.not.equal('');
        });
    });

    this.defineStep(/^I validate the selected device is added to Cart$/, function () {
        browser.wait(EC.visibilityOf(Smartcart.getAddedDeviceName()), 50000);
        return Smartcart.getAddedDeviceName().getText().then(function (header) {
            expect(phoneNameLine1).to.include(header);
        });
    });

    this.defineStep(/^I see the changes reflected in cart for the device$/, function () {
        return browser.executeScript(EC.visibilityOf(Smartcart.getAddedDeviceName()), 50000).then(function () {
            Smartcart.getAddedDeviceName().getText().then(function (header) {
                expect(phoneNameLine1).to.include(header);
            });
        });
    });

    this.defineStep(/^I see the same device on the next line$/, function () {
        browser.sleep(10000);
        return browser.executeScript(EC.visibilityOf(Smartcart.getaddedDeviceNameLine2()), 50000).then(function () {
            Smartcart.getaddedDeviceNameLine2().getText().then(function (header) {
                expect(phoneNameLine1).to.include(header);
            });
        });
    });

    //***************************************************************************************************************************************

    this.defineStep(/^I should see T-Mobile Phone SIM Starter kit Product Description Page$/, function () {
        var cellHeader = element(by.id('productDescription')).element(by.css('h3[role="heading"]'));
        browser.wait(EC.invisibilityOf(element(by.css('.screen-lock'))), 20000);
        browser.wait(EC.elementToBeClickable(cellHeader), 20000);
        return cellHeader.getText().then(function (IDCellHeader) {
            expect(IDCellHeader).to.equal('T-Mobile 3-in-1 SIM Starter Kit');
        });
    });

    this.defineStep(/^I should see T-Mobile MI device SIM Starter kit Product Description Page$/, function () {
        //browser.sleep(8000);
        var cellHeader = element(by.id('productDescription')).element(by.css('h3[role="heading"]'));
        browser.wait(EC.invisibilityOf(element(by.css('.screen-lock'))), 20000);
        browser.wait(EC.elementToBeClickable(cellHeader), 20000);
        return cellHeader.getText().then(function (IDCellHeader) {
            expect(IDCellHeader).to.equal('T-Mobile 3-in-1 Mobile Internet SIM Kit');
        });
    });

    this.defineStep(/^I Click on Add to Cart Button$/, function () {
        browser.manage().window().maximize();
        var selectBYOD = element(by.id('add-to-bag-btn'));
        browser.wait(EC.invisibilityOf(element(by.css('.screen-lock'))), 20000);
        browser.wait(EC.elementToBeClickable(selectBYOD), 20000);
        selectBYOD.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            selectBYOD.click();
            return browser.sleep(2000);
        });
    });

    this.defineStep(/^I should see device added to the cart$/, function () {
        return browser.getTitle().then(function (browsertitle) {
            console.log(browsertitle); //get the title from the parameter
            var cartTitle = element(by.css('[ng-bind-html="myCartCtrl.keyValues.cartHeader.cartTitle | parseHtml"]')); //read the cart titile of the Cart page
            return cartTitle.getText().then(function (cartTitle) {
                if (cartTitle == browsertitle) {
                    console.log("cart page has same titile", browsertitle);
                } else {
                    console.log("cart page has different titile", cartTitle);
                }
            });
        });
    });

    this.defineStep(/^I should see same product I added to cart$/, function () {
        var tabletname = element(by.css('[class="p-t-20 p-b-20 ng-binding"]')); //only this is the locator to find the lement on the UI
        return tabletname.getText().then(function (tabletnametext) {
            console.log("the product i have chosen is ", tabletnametext); //print the tabletname added to the cart
        });
    });

    /************************************************************************************************/
    /*********************************************SMARTCART******************************************/
    /************************************************************************************************/
    //******************************************Empty Cart********************************************/
    this.defineStep(/^I should see "([^"]*)" in the homepage$/, function (task) {
        return Smartcart.addAPhoneButtonEmptyCart().getText().then(function (data) {
            expect(data).to.equal("Add a phone", "Add a phone button text is different");
        });
    });

    this.defineStep(/^I should see "([^"]*)" in the homepage for accessories$/, function (task) {
        return Smartcart.addAnAccessoryEmptyCart().getText().then(function (data) {
            expect(data).to.equal(task, "Add an accessory");
        });
    });

    this.defineStep(/^I click on 'Add a phone' button$/, function () {
        return Smartcart.addAPhoneButtonElement().click();
    });

    this.When(/^I click on 'Add an accessory' button in Empty Cart Page$/, function () {
        return Smartcart.addAnAccessoryEmptyCart().click();
    });

    this.defineStep(/^I click on Add a phone button in Empty Cart Page$/, function () {
        browser.sleep(10000);
        return Smartcart.addAPhoneButtonEmptyCart().click();
        browser.wait(EC.visibilityOf(Smartcart.addAPhoneButtonEmptyCart()), 30000);
        Smartcart.addAPhoneButtonEmptyCart().getLocation().then(function (buttonLocation) {
            var x = buttonLocation.x;
            var y = buttonLocation.y - 200;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
        });

    });

    this.defineStep(/^I click on Add an accessory button$/, function () {
        return Smartcart.addAnAccessoryButtonElement().click();
    });

    this.defineStep(/^I should see cart title in cart page$/, function () {
        return browser.wait(EC.visibilityOf(Smartcart.getCartTitleElement()), 30000).then(function (isVisible) {
            if (isVisible) {
                Smartcart.getCartTitleElement().getText().then(function (title) {
                    expect(title).to.not.be.null;
                });
            }
        });
    });

    this.defineStep(/^the browser will navigate to cart page$/, function () {
        return Smartcart.getCartTitleElement().getText().then(function (title) {
            expect(title).to.not.be.null;
        });
    });

    this.defineStep(/^I validate the selected accessory is added to cart$/, function () {
        var Accheader = element.all(by.repeater('acc in vm.cartObj.accessories.accessoryDetails')).get(0).element(by.css('[class="small name ng-binding"]'));
        return Accheader.getText().then(function (AccTileHeader) {
            expect(AccTileHeader).to.equal(accessoryNameLine1);
        });
    });
    /*var toCart = element.all(by.css('[class="small name ng-binding"]')).get(0);
        toCart.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return toCart.getText().then(function (IE) {
            expect(IE).to.not.equal('');
        });
    });*/

    this.defineStep(/^I click on plus button on tile for accessory$/, function () {
        var selectAccessoryTile = element(by.css('[ng-if="vm.keyValuePair.accessories.addAccessoriesImage"]'));
        selectAccessoryTile.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        selectAccessoryTile.click();
        return browser.sleep(2000);
    });

    this.defineStep(/^I select an accessory from mini browse to add as an second accessory$/, function () {
        var accTwo = element.all(by.css('[alt="Product image"]')).get(1);
        accTwo.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        accTwo.click();
    });

    this.defineStep(/^I select an accessory from mini browse to add as an third accessory$/, function () {
        var accThree = element.all(by.css('[alt="Product image"]')).get(2);
        accThree.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        accThree.click();
    });

    this.defineStep(/^I add the selected accessory to cart$/, function () {
        var accToCart = element(by.css('[ng-click="vm.addToCart()"]'));
        accToCart.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return accToCart.click();
    });

    this.defineStep(/^I click on edit accessory option for accessory one$/, function () {
        browser.sleep(2000);
        //var editAcc = element.all(by.css('[ng-bind-html="vm.keyValuePair.accessories.editAccessories | parseHtml"]')).get(0);
        return browser.executeScript("arguments[0].scrollIntoView();", element.all(by.id('acctile')).get(0).getWebElement()).then(function() {
            element.all(by.id('acctile')).get(0).click();
        });
    });

    this.defineStep(/^I select a edit button from edited tile$/, function () {
        browser.sleep(2000);
        return browser.executeScript("arguments[0].scrollIntoView();", element(by.id('addBtnAcc')).getWebElement()).then(function() {
            element(by.id('addBtnAcc')).click();
        });
    });

    this.defineStep(/^I look for quantity option dropdown$/, function () {
        var quantityOpt = element(by.id('accessory-quantity'));
        quantityOpt.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return quantityOpt.getText().then(function (quantityText) {
            expect(quantityText).to.not.be.null;
        });
    });

    this.defineStep(/^I change the quantity$/, function () {
        var selectAcc = element(by.id('accessory-quantity')).element(by.css('[value="number:2"]'));
        selectAcc.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectAcc.click();
    });

    this.defineStep(/^I click on edit accessory option for accessory two$/, function () {
        /*var editAcc = element.all(by.css('[ng-bind-html="vm.keyValuePair.accessories.editAccessories | parseHtml"]')).get(1);
        browser.wait(EC.visibilityOf(editAcc), 30000);
        editAcc.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return editAcc.click(); */

       return  Smartcart.getAccessoriesEditButtonInCart().click();
    });

    this.defineStep(/^I click on delete button for accessory$/, function () {
        /*var editAcc = element(by.css('[ng-click="vm.showDeleteAccessory(acc);$event.stopPropagation();"]'));
        editAcc.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return editAcc.click(); */

        return Smartcart.getAccessoriesDeleteButtonInEditModal.click();
    });

    this.defineStep(/^I remove the second accessory from cart$/, function () {
        var delAcc = element(by.css('[ng-click="vm.deleteAccessory(acc)"]'));
        delAcc.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        delAcc.click();
        return browser.sleep(2000);
    });

    this.defineStep(/^I select another accessory from mini browse$/, function () {
        var accThree = element.all(by.css('[alt="Product image"]')).get(4);
        accThree.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        accThree.click();
    });

    //***********************************Selecing Credit class in Cart*********************************************************************************************

    this.defineStep(/^I select Average Credit on Cart$/, function () {
        var clickAverageCredit = element(by.id('crd_id_1'));
        clickAverageCredit.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return clickAverageCredit.click();
    });

    this.defineStep(/^I select Average Credit on Cart$/, function () {
        var clickAverageCredit = element(by.id('crd_id_0'));
        clickAverageCredit.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return clickAverageCredit.click();
    });

    this.defineStep(/^I select No Credit on Cart$/, function () {
        var clickAverageCredit = element(by.id('crd_id_2'));
        clickAverageCredit.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return clickAverageCredit.click();
    });

    //////////////////////////////******************************************end********************************////////////////////////////////////////////	/////
    //********************************Adding New line in Cart ****************************************************/

    this.defineStep(/^I am in Cart page$/, function () {
        browser.sleep(2000);
        return browser.wait(EC.visibilityOf(Smartcart.getCartTitleElement()), 30000).then(function (isVisible) {
            if (isVisible) {
                Smartcart.getCartTitleElement().getText().then(function (title) {
                    expect(title).to.not.be.null;
                });
            }
        });
    });

    this.defineStep(/^I click on Add a Phone Button for adding new line$/, function () {
        return Smartcart.addAPhoneButtonElement().getLocation().then(function (buttonLocation) {
            var x = buttonLocation.x;
            var y = buttonLocation.y - 200;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
            Smartcart.addAPhoneButtonElement().click();
        });
    });

    this.defineStep(/^I click on Add a Phone Button for adding New Line$/, function () {
        browser.sleep(10000);
        browser.wait(EC.visibilityOf(Smartcart.addAPhoneButtonElement()), 50000);
        Smartcart.addAPhoneButtonElement().getLocation().then(function (buttonLocation) {
            var x = buttonLocation.x;
            var y = buttonLocation.y - 200;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
            return Smartcart.addAPhoneButtonElement().click();
        });
    });

    this.defineStep(/^I should see new line added to cart$/, function () {
        return Smartcart.getCartTitleElement().getText().then(function (title) {
            expect(title).to.not.be.null;
        });
    });
    ////////////////////////////updated for "add a phone button" on cart page when trying to add a phone on phone tile//////////////////////////////
    this.defineStep(/^I click on add a new phone button$/, function () {
        //var getAddNewPhoneButton = element(by.css('[ng-click="myCartCtrl.creditCheck('Select Phone',selectedLine,$index)"]')); // throwing an error for arguement list and cssCustomselector is not a defined locater
        var getAddNewPhoneButton = element(by.buttonText('Add a new phone')); //working fine Text can be changed accrodingly the environment
        getAddNewPhoneButton.getLocation().then(function (deviceIconLocation) {
            var x = deviceIconLocation.x;
            var y = deviceIconLocation.y - 200;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
            getAddNewPhoneButton.click();
        });
    });

    ///////////////////////////////////////***************************end************************************//////////////////////////////

    ///////////////////******************When On mini Browse newly added******************///////////////////////////////////

    this.defineStep(/^I select a Phone from Mini Browse$/, function () {
        var minibrowsephoneselect = element.all(by.css('[ng-click="deviceSelectorModalCtrl.openMiniPDPModal(device)"]')).get(3);
        return minibrowsephoneselect.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            minibrowsephoneselect.click();
        });
    });

    ///////////////////////***************************end***************************************////////////////////////////////////

    this.defineStep(/^I should see device added to cart$/, function () {
        return Smartcart.getCartTitleElement().getText().then(function (title) {
            expect(title).to.not.be.null;
        });
    });

    //-----------------------------------------Adding ID to cart---------------------------------------------------------------

    this.defineStep(/^I should navigate to Product detail page of Apple iPad$/, function () {
        var checkPDP = element(by.id('productDescription')).element(by.css('h3[role="heading"]'));
        browser.wait(EC.invisibilityOf(element(by.css('.screen-lock'))), 20000);
        browser.wait(EC.elementToBeClickable(checkPDP), 20000);
        return checkPDP.getText().then(function (IE) {
            expect(IE).to.equal('Apple 9.7 inch iPad Pro');
        });
    });

    this.defineStep(/^I should navigate to Product detail page of "([^"]*)" internet-device$/, function (pdpDetails) {
        return upperFunnel.getMainPDPDetails().getText().then(function (pdpHeader) {
            expect(pdpHeader).to.equal(pdpDetails);
            browser.wait(EC.invisibilityOf(element(by.css('.screen-lock'))), 20000);
            browser.wait(EC.elementToBeClickable(checkPDP), 20000);
        });
    });

    this.defineStep(/^I should navigate to Product detail page of "([^"]*)" phone$/, function (pdpDetails) {
        return upperFunnel.getMainPDPDetails().getText().then(function (pdpHeader) {
            expect(pdpHeader).to.equal(pdpDetails);
            browser.wait(EC.invisibilityOf(element(by.css('.screen-lock'))), 20000);
            browser.wait(EC.elementToBeClickable(checkPDP), 20000);
        });
    });

    this.defineStep(/^I am on Product detail page of "([^"]*)" internet-device$/, function () {
        return upperFunnel.getMainPDPDetails().getText().then(function (pdpHeader) {
            expect(pdpHeader).to.equal(pdpDetails);
            browser.wait(EC.invisibilityOf(element(by.css('.screen-lock'))), 20000);
            browser.wait(EC.elementToBeClickable(checkPDP), 20000);
        });
    });

    this.defineStep(/^I add "([^"]*)" internet-device to cart$/, function () {
        selectPhone.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return upperFunnel.getAddToCartButton().click();
    });

    this.defineStep(/^I am on Product detail page of Apple iPad$/, function () {
        var checkPDP = element(by.id('productDescription')).element(by.css('h3[role="heading"]'));
        return checkPDP.getText().then(function (IE) {
            expect(IE).to.equal('Apple 9.7 inch iPad Pro');
        });
    });

    //*****************************************Adding BYOD to Cart****************************************************************************************
    this.defineStep(/^I click on Device Image on Device tile in the first line$/, function () {
        return Smartcart.getAddDeviceIconElement().get(0).getLocation().then(function (deviceIconLocation) {
            var x = deviceIconLocation.x;
            var y = deviceIconLocation.y - 200;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
            Smartcart.getAddDeviceIconElement().get(0).click();
        });
    });

    this.defineStep(/^I click on Device Image on Device tile in the second line$/, function () {
        browser.sleep(10000);
        return browser.wait(EC.visibilityOf(Smartcart.getAddDeviceIconElement().get(1)), 50000).then(function () {
            Smartcart.getAddDeviceIconElement().get(1).getLocation().then(function (deviceIconLocation) {
                var x = deviceIconLocation.x;
                var y = deviceIconLocation.y - 200;
                browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
                Smartcart.getAddDeviceIconElement().get(1).click();
            });
        });

    });

    this.defineStep(/^I click on Device Image on Device tile in line "([^"]*)"$/, function (linenum) {
        browser.sleep(10000);
        browser.wait(EC.visibilityOf(Smartcart.getAddDeviceIconElements(linenum)), 50000);
        Smartcart.getAddDeviceIconElements(linenum).getLocation().then(function (deviceIconLocation) {
            var x = deviceIconLocation.x;
            var y = deviceIconLocation.y - 200;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
        });
        return Smartcart.getAddDeviceIconElements(linenum).click();
    });

    this.defineStep(/^I click on Bring your own device Button$/, function () {
        return Smartcart.getAddBYODButton().click();
    });

    this.defineStep(/^I click on Bring your own Tablet button in device tile$/, function () {
        return Smartcart.getAddBYODButton().click();
    });

    this.defineStep(/^I should see "([^"]*)" Mini PDP$/, function (simkit) {
        browser.sleep(2000);
        SIMkitName = simkit;
        return Smartcart.getBYODHeader().getText().then(function (bYODHeader) {
            expect(bYODHeader).to.equal(simkit);
        });
    });

    this.defineStep(/^I should see Sim kit Mini PDP$/, function () {
        return Smartcart.getBYODHeader().getText().then(function (bYODHeader) {
            expect(bYODHeader).to.not.be.null;
        });
    });

    this.defineStep(/^I should see Product name on Mini PDP$/, function () {
        browser.sleep(5000);
        browser.wait(EC.visibilityOf(Smartcart.getBYODHeader()), 20000);
        return Smartcart.getBYODHeader().getText().then(function (bYODHeader) {
            SIMkitName = bYODHeader;
            expect(bYODHeader).to.equal(SIMkitName);
        });
    });

    this.defineStep(/^I should see Sim Starter kit on Mini PDP$/, function () {
        browser.sleep(5000);
        return browser.wait(EC.visibilityOf(Smartcart.getBYODHeader()), 20000).then(function () {
            Smartcart.getBYODHeader().getText().then(function (bYODHeader) {
                SIMStarterkitName = bYODHeader;
                expect(bYODHeader).to.equal(SIMStarterkitName);
            });
        });
    });

    this.defineStep(/^I should see Tablet Sim Kit Product name on Mini PDP$/, function () {
        browser.sleep(5000);
        return browser.wait(EC.visibilityOf(Smartcart.getBYODHeader()), 20000).then(function () {
            Smartcart.getBYODHeader().getText().then(function (bYODHeader) {
                SIMkitName1 = bYODHeader;
                expect(bYODHeader).to.equal(SIMkitName1);
            });
        });
    });

    this.defineStep(/^I should see Product image displayed on this page$/, function () {
        return Smartcart.getBYODImage().isDisplayed().then(function (imagePresent) {
            if (imagePresent) {
                console.log('SIM Kit Image is displayed');
            } else {
                console.log('SIM Kit Image is not displayed');
            }
        })
    });

    this.defineStep(/^Check for Product Price$/, function () { });

    this.defineStep(/^I click on Add to Cart Button on SIM kit Product Description Page$/, function () {
        browser.sleep(5000);
        return Smartcart.getBYODAddToCart().getLocation().then(function (buttonLocation) {
            var x = buttonLocation.x;
            var y = buttonLocation.y - 200;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
            Smartcart.getBYODAddToCart().click();
        });

    });

    this.defineStep(/^I should see Sim kit added to cart in Line 2$/, function () {
        browser.wait(EC.visibilityOf(Smartcart.getaddedDeviceNameLine2()), 30000);
        return Smartcart.getaddedDeviceNameLine2().getText().then(function (simheader) {
            expect(simheader).to.equal(SIMkitName1);
        });
    });

    this.defineStep(/^I should see Sim kit added to cart$/, function () {
        browser.wait(EC.visibilityOf(Smartcart.getaddedDeviceNameLine1()), 30000);
        return Smartcart.getaddedDeviceNameLine1().getText().then(function (simheader) {
            expect(simheader).to.equal(SIMkitName);
        });
    });

    this.defineStep(/^I should see Sim Starter kit added to cart$/, function () {
        return browser.wait(EC.visibilityOf(Smartcart.getaddedDeviceNameLine2()), 30000).then(function () {
            Smartcart.getaddedDeviceNameLine2().getText().then(function (simheader) {
                expect(simheader).to.equal(SIMStarterkitName);
            });
        });
    });

    this.defineStep(/^I should see MI Sim kit added to cart$/, function () {
        browser.wait(EC.visibilityOf(Smartcart.getaddedDeviceNameLine1()), 30000);
        return Smartcart.getaddedDeviceNameLine1().getText().then(function (simheader) {
            expect(simheader).to.equal(SIMkitName);
        });
    });

    this.defineStep(/^I should see MI Sim kit added to cart in second line$/, function () {
        return browser.wait(EC.visibilityOf(Smartcart.getaddedDeviceNameLine2()), 30000).then(function () {
            Smartcart.getaddedDeviceNameLine2().getText().then(function (simheader) {
                expect(simheader).to.equal(SIMkitName1);
            });
        });
    });

    //*********************************************Adding Tablet to Cart****************************************************************************************

    this.defineStep(/^I click on Add a Tablet Button$/, function () {
        browser.sleep(10000);
        return browser.wait(EC.visibilityOf(Smartcart.addATabletOrWerableButton()), 50000).then(function () {
            Smartcart.addATabletOrWerableButton().getLocation().then(function (ctaLocation) {
                var x = ctaLocation.x;
                var y = ctaLocation.y - 200;
                browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
                Smartcart.addATabletOrWerableButton().click();
            });
        });
    });

    this.defineStep(/^I select "([^"]*)" in mini browse$/, function (deviceName) {
        browser.sleep(10000);
        var minibrowseDevice = element(by.cssContainingText('.selector-device-name', deviceName));
        phoneNameLine2 = deviceName;
        return minibrowseDevice.click();
    });

    this.defineStep(/^I should see phone added to the cart in second line$/, function () {
        return browser.wait(EC.visibilityOf(Smartcart.getaddedDeviceNameLine2()), 30000).then(function () {
            Smartcart.getaddedDeviceNameLine2().getText().then(function (header) {
                expect(phoneNameLine2).to.include(header);
            });
        });
    });

    this.defineStep(/^I should see selected device added to the cart in line "([^"]*)"$/, function (linenum) {
        return browser.wait(EC.visibilityOf(Smartcart.getAddedDeviceNames(linenum)), 30000).then(function () {
            Smartcart.getAddedDeviceNames(linenum).getText().then(function (header) {
                expect(phoneNameLine2).to.include(header);
            });
        });
    });

    this.defineStep(/^I click on Add Device Icon in device tile for adding Tablet$/, function () {
        browser.sleep(15000);
        return Smartcart.getAddDeviceIconElement().get(1).getLocation().then(function (AddDeviceIconLocation) {
            var x = AddDeviceIconLocation.x;
            var y = AddDeviceIconLocation.y - 200;
            console.log("x and y is " + x + " " + y);
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
            Smartcart.getAddDeviceIconElement().get(1).click();
        });
    });

    this.defineStep(/^I select "([^"]*)" tablet in mini browse$/, function (deviceName) {
        browser.sleep(10000);
        tabletNameLine1 = deviceName;
       return  element(by.cssContainingText('.selector-device-name', deviceName)).getLocation().then(function (Location) {
            var x = Location.x;
            var y = Location.y - 200;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
            element(by.cssContainingText('.selector-device-name', deviceName)).click();
        });
    });

    this.defineStep(/^I click on Add a Tablet button in device tile$/, function () {
        return Smartcart.getAddNewPhoneLabelElement().click();
    });

    this.defineStep(/^I should see Tablet added to the cart in second line$/, function () {
        browser.wait(EC.visibilityOf(Smartcart.getaddedDeviceNameLine2()), 30000);
        return Smartcart.getaddedDeviceNameLine2().getText().then(function (header) {
            expect(tabletNameLine1).to.include(header);
        });
    });

    //************************************************Adding Wearable to Cart***********************************************************/

    this.defineStep(/^I click on Add Device Icon in device tile for adding Wearable$/, function () {
        browser.sleep(15000);
        Smartcart.getAddDeviceIconElement().get(2).getLocation().then(function (AddDeviceIconLocation) {
            var x = AddDeviceIconLocation.x;
            var y = AddDeviceIconLocation.y - 200;
            console.log("x and y is " + x + " " + y);
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
        });
        return Smartcart.getAddDeviceIconElement().get(2).click();
    });

    this.defineStep(/^I select "([^"]*)" wearable in mini browse$/, function (deviceName) {
        browser.sleep(10000);
        wearableNameLine1 = deviceName;
       return element(by.cssContainingText('.selector-device-name', deviceName)).getLocation().then(function (Location) {
            var x = Location.x;
            var y = Location.y - 200;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
            element(by.cssContainingText('.selector-device-name', deviceName)).click();
        });
    });

    this.defineStep(/^I should see wearable added to cart in third line$/, function () {
        browser.wait(EC.visibilityOf(Smartcart.getaddedDeviceNameLine3()), 30000);
        return Smartcart.getaddedDeviceNameLine3().getText().then(function (header) {
            expect(wearableNameLine1).to.include(header);
        });
    });

    //**********************************************Adding Services to Cart************************************************************************/

    this.defineStep(/^I click on Need more services link on service tile in line "([^"]*)"$/, function (linenum) {
        browser.wait(EC.visibilityOf(Smartcart.needMoreServicesLink(linenum)), 30000);
        Smartcart.needMoreServicesLink(linenum).getLocation().then(function (linkLocation) {
            var x = linkLocation.x;
            var y = linkLocation.y - 200;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
        });
        return Smartcart.needMoreServicesLink(linenum).click();
    });

    this.defineStep(/^I should see zip code modal$/, function () {
        browser.wait(EC.visibilityOf(Smartcart.zipCode()), 30000);
        return Smartcart.zipCode().getText().then(function (zipCodeHeader) {
            expect(zipCodeHeader).to.not.be.null;
        });
    });

    this.defineStep(/^I enter zipcode as "([^"]*)" in zipcode field$/, function (zipCode) {
        Smartcart.setZipCode().clear();
        browser.sleep(5000);
        return Smartcart.setZipCode().sendKeys(zipCode);
    });

    this.defineStep(/^I click on 'Next' button on zipcode modal$/, function () {
        Smartcart.nextButton().click();
    });

    this.defineStep(/^I should see services modal$/, function () {
        browser.wait(EC.visibilityOf(Smartcart.serviceHeader()), 30000);
        return Smartcart.serviceHeader().getText().then(function (serviceHeader) {
            expect(serviceHeader).to.not.be.null;
        })
    });

    this.defineStep(/^I am in Services Modal$/, function () {
        browser.wait(EC.visibilityOf(Smartcart.serviceHeader()), 30000);
        return Smartcart.serviceHeader().getText().then(function (serviceHeader) {
            expect(serviceHeader).to.not.be.null;
        })
    });

    this.defineStep(/^I select service category as "([^"]*)"$/, function (serviceCategory) {
        if (serviceCategory == 'BUNDLED SERVICES') {
            return Smartcart.bundleServiceCategory().click();
        }
        else {
            return Smartcart.alacarteServiceCategory().click();
        }
    });

    this.defineStep(/^I Select "([^"]*)" service$/, function (serviceName) {
        browser.wait(EC.visibilityOf(Smartcart.serviceslist(serviceName)), 30000);
        var sevicesCount = Smartcart.getserviceslist().count();
        for (i = 0; i <= servicesCount; i++) {
            Smartcart.serviceslist().getAttribute('plan-data');
            console.log(plan - data);
        }
        /*if(Smartcart.serviceslist(serviceName).getText() == serviceName)
        {
        } */
    });

    this.defineStep(/^I see selected services added to the cart$/, function () {

    });

    //**********************************************Adding accessory to Cart***************************************************************/

    this.defineStep(/^I click on Accessory tile$/, function () {
        browser.sleep(3000);
        /*browser.executeScript("arguments[0].scrollIntoView(false);", Smartcart.getAccessoriesAddIcon().getWebElement());
        browser.sleep(2000);
        Smartcart.getAccessoriesAddIcon().getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 100;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            return Smartcart.getAccessoriesAddIcon().click();
        }); */

        return browser.executeScript("arguments[0].scrollIntoView(false);", Smartcart.getAccessoriesAddIcon().getWebElement()).then(function () {
            browser.sleep(2000);
            Smartcart.getAccessoriesAddIcon().getLocation().then(function (ElementLocation) {
                var yAxisValue = ElementLocation.y - 100;
                browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
                Smartcart.getAccessoriesAddIcon().click();
            });
        });
    });

    this.defineStep(/^I should see Accessory mini Browse$/, function () {
       return Smartcart.getAccMiniBrowseHeader().getText().then(function (accHeader) {
            expect(accHeader).to.not.be.null;
        });
    });

    this.defineStep(/^I select first tab in accessory mini browse$/, function () {
        return browser.wait(EC.visibilityOf(Smartcart.getCasesTab()), 30000).then(function () {
            Smartcart.getCasesTab().click();
        });
    });

    this.defineStep(/^I select second tab in accessory mini browse$/, function () {
        return browser.wait(EC.visibilityOf(Smartcart.getHeadsetsTab()), 30000).then(function () {
            Smartcart.getHeadsetsTab().click();
        });
    });

    this.defineStep(/^I select third tab in accessory mini browse$/, function () {
        return browser.wait(EC.visibilityOf(Smartcart.getChargersTab()), 30000).then(function () {
            Smartcart.getChargersTab().click();
        });
    });

    /* this.defineStep(/^I select "([^"]*)" accessory in mini browse$/, function (deviceName) {
         var accminibrowseDevice = element(by.cssContainingText('.accessory-name', deviceName));
         return accminibrowseDevice.click();
     }); */

    this.defineStep(/I select accessory which is under first tab in mini browse$/, function () {
        return browser.wait(EC.visibilityOf(Smartcart.selectFirstTabAccessory()), 30000).then(function () {
            Smartcart.selectFirstTabAccessory().getText().then(function (accessoryTitle) {
                accessoryName = accessoryTitle;
            });
            Smartcart.getFirstTabAccessoryImage().click();
        });
    });

    this.defineStep(/I select accessory which is under second tab in mini browse$/, function () {
        return browser.wait(EC.visibilityOf(Smartcart.selectSecondTabAccessory()), 30000).then(function () {
            Smartcart.selectSecondTabAccessory().getText().then(function (accessoryTitle) {
                accessoryName = accessoryTitle;
            });
            Smartcart.getSecondTabAccessoryImage().click();
        });
    });

    this.defineStep(/I select accessory which is under third tab in mini browse$/, function () {
        browser.sleep(2000);
        return Smartcart.selectThirdTabAccessory().getText().then(function (accessoryTitle) {
            accessoryName = accessoryTitle;
            Smartcart.getThirdTabAccessoryImage().click();
        });
    });

    this.defineStep(/I select "([^"]*)" accessory from Speakers$/, function (acc) {
        accessoryName = acc;
        browser.sleep(2000);
        var selectingDevice = element(by.cssContainingText('.accessory-name', acc));
        return selectingDevice.click();
    });

    this.defineStep(/I select "([^"]*)" accessory from Chargers$/, function (acc) {
        accessoryName = acc;
        browser.sleep(2000);
        var selectingDevice = element(by.cssContainingText('.accessory-name', acc));
        return selectingDevice.click();
    });

    this.defineStep(/^I should see "([^"]*)" accessory Mini PDP$/, function (acc) {
        browser.wait(EC.visibilityOf(Smartcart.getSelectedAccessoryModalHeader()), 30000);
        Smartcart.getSelectedAccessoryModalHeader().getText().then(function (accHeader) {
            expect(accessoryName).to.include(accHeader);
        });
    });

    this.defineStep(/^I should see accessory product description page$/, function () {
        return browser.wait(EC.visibilityOf(Smartcart.getSelectedAccessoryModalHeader()), 30000).then(function () {
            Smartcart.getSelectedAccessoryModalHeader().getText().then(function (accHeader) {
                expect(accessoryName).to.include(accHeader);
            });
        });
    });

    this.defineStep(/^I click on 'Add to Cart' Button on accessory mini PDP$/, function () {
        return browser.wait(EC.visibilityOf(Smartcart.addToCart()), 30000).then(function () {
            Smartcart.addToCart().getLocation().then(function (buttonLocation) {
                var x = buttonLocation.x;
                var y = buttonLocation.y - 250;
                browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
                Smartcart.addToCart().click();
            });
        });
    });

    this.defineStep(/^I see the selected accessory is added to the cart$/, function () {
        browser.wait(EC.visibilityOf(Smartcart.getAddedAccessoryName()), 30000);
        return Smartcart.getAddedAccessoryName().getText().then(function (accN) {
            expect(accessoryName).to.equal(accN);
        });
    });

    this.defineStep(/^I see the selected accessory is added to the cart on first tile$/, function () {
        return browser.wait(EC.visibilityOf(Smartcart.getAddedAccessoryName()), 30000).then(function () {
            Smartcart.getAddedAccessoryName().getText().then(function (accHeader) {
                expect(ufAccessoryName).to.equal(accHeader);
            });
        });
    });

    this.defineStep(/^I see the selected accessory is added to the cart on tile "([^"]*)"$/, function (tileNum) {
        return browser.wait(EC.visibilityOf(Smartcart.getAddedAccessoryNames(tileNum)), 30000).then(function (isVisible) {
            if (isVisible) {
                Smartcart.getAddedAccessoryNames(tileNum).getText().then(function (accHeader) {
                    expect(accessoryName).to.include(accHeader);
                });
            }
        });
    });

    //************************************************** My TMO Deeplinking feature**************************************************************************/

    this.defineStep(/^I click on "([^"]*)" link on PDP$/, function (link) {
        browser.wait(EC.visibilityOf(upperFunnel.loginMyTMo(), 30000));
        upperFunnel.loginMyTMo().getLocation().then(function (buttonLocation) {
            var x = buttonLocation.x;
            var y = buttonLocation.y - 250;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
        });
        return upperFunnel.loginMyTMo().click();
    });

    this.defineStep(/^I see My TMO Homepage$/, function () {
        browser.wait(EC.visibilityOf(myTMO.getmyTMOHeader(), 30000));
        myTMO.getmyTMOHeader().getLocation().then(function (Location) {
            var x = Location.x;
            var y = Location.y - 250;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
        });
        return myTMO.getmyTMOHeader().getText().then(function (headerText) {
            expect(headerText).to.include('Log in with T-Mobile ID');
        });
    });

    this.defineStep(/^I enter "([^"]*)" in Username field$/, function (id) {
        return myTMO.setUserName().sendKeys(id);
    });

    this.defineStep(/^I enter "([^"]*)" in Password field$/, function (pwd) {
        return myTMO.setPassword().sendKeys(pwd);
    });

    this.defineStep(/^I click on Login Button$/, function () {
        return myTMO.login().click();
    });


    this.defineStep(/^I see MyTMO Lines Page$/, function () {
        browser.wait(EC.visibilityOf(myTMO.getLinesPageHeader(), 30000));
        return myTMO.getLinesPageHeader().getText().then(function (Header) {
            expect(Header).to.not.be.null;
        });
    });


    this.defineStep(/^I select my line$/, function () {
        browser.wait(EC.visibilityOf(myTMO.myLine(), 30000));
        return myTMO.myLine().click();
    });

    this.defineStep(/^I get the data from cookie$/, function () {
        browser.sleep(10000);
        return browser.manage().getCookie('tmo2mytmo').then(function (cookies) {
            myTMOCookieData = cookies.value;
            myTMOCookieData = JSON.parse(myTMOCookieData);
            console.log(myTMOCookieData);
        });
    });

    this.defineStep(/^I see "([^"]*)" as SKU in Cookies$/, function (SKU) {
        return browser.manage().getCookie('tmo2mytmo').then(function (cookies) {
            myTMOCookieData = cookies.value;
            myTMOCookieData = JSON.parse(myTMOCookieData);
            //myTMOSKU = helper.getTMOCookie();
            console.log(myTMOCookieData.sku);
            expect(SKU).to.equal(myTMOCookieData.sku);
        });
    });


    this.defineStep(/^I see "([^"]*)" as Product Type in Cookies$/, function (productType) {
        return browser.manage().getCookie('tmo2mytmo').then(function (cookies) {
            myTMOCookieData = cookies.value;
            myTMOCookieData = JSON.parse(myTMOCookieData);
            console.log(myTMOCookieData.productType);
            expect(productType).to.equal(myTMOCookieData.productType);
        });
    });

    this.defineStep(/^I see "([^"]*)" as Promo Name in Cookies$/, function (promoName) {
        return browser.manage().getCookie('tmo2mytmo').then(function (cookies) {
            myTMOCookieData = cookies.value;
            myTMOCookieData = JSON.parse(myTMOCookieData);
            console.log(myTMOCookieData.promoName);
            expect(promoName).to.equal(myTMOCookieData.promoName);
        });
    });

    this.defineStep(/^I see "([^"]*)" as paymentoption in Cookies$/, function (paymentOption) {
        return browser.manage().getCookie('tmo2mytmo').then(function (cookies) {
            myTMOCookieData = cookies.value;
            myTMOCookieData = JSON.parse(myTMOCookieData);
            console.log(myTMOCookieData.paymentOption);
            expect(paymentOption).to.equal(myTMOCookieData.paymentOption);
        });
    });



    this.defineStep(/^I see "([^"]*)" as Product Family in Cookies$/, function (productFamily) {
        browser.manage().getCookie('tmo2mytmo').then(function (cookies) {
            myTMOCookieData = cookies.value;
            myTMOCookieData = JSON.parse(myTMOCookieData);
            console.log(myTMOCookieData.familyId);
            return expect(productFamily).to.equal(myTMOCookieData.familyId);
        });
    });

    this.defineStep(/^I should see 'Upgrade' and 'Add a Line' buttons in Product detail Page$/, function () {
        return upperFunnel.myTMOUpgradeCTA().getText().then(function (text) {
            console.log(text);
            expect(text).to.equal('Upgrade');
            upperFunnel.myTMOAddaLineCTA().getText().then(function (text) {
                console.log(text);
                expect(text).to.equal('Add A Line');
            });
        });
    });

    this.defineStep(/^I see 'Upgrade' button in disabled state$/, function () {
        return upperFunnel.myTMOUpgradeCTA().isEnabled().then(function (result) {
            expect(result).to.be.false;
        });
    });

    this.defineStep(/^I click on 'Upgrade' button on PDP$/, function () {
        browser.sleep(5000);
        return upperFunnel.myTMOUpgradeCTA().getText().then(function (text) {
            console.log(text);
            expect(text).to.equal('Upgrade');
            browser.wait(EC.visibilityOf(upperFunnel.myTMOUpgradeCTA(), 30000));
            upperFunnel.myTMOUpgradeCTA().getLocation().then(function (ctaLocation) {
                var x = ctaLocation.x;
                var y = ctaLocation.y - 200;
                browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
                browser.sleep(10000);
                upperFunnel.myTMOUpgradeCTA().click();
            });

        });
    });

    this.defineStep(/^I click on 'Add a Line' button on PDP$/, function () {
        browser.sleep(5000);
        return upperFunnel.myTMOAddaLineCTA().getText().then(function (text) {
            console.log(text);
            expect(text).to.equal('Add A Line');
            browser.wait(EC.visibilityOf(upperFunnel.myTMOAddaLineCTA(), 30000));
            upperFunnel.myTMOAddaLineCTA().getLocation().then(function (ctaLocation) {
                var x = ctaLocation.x;
                var y = ctaLocation.y - 200;
                browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
                browser.sleep(10000);
                upperFunnel.myTMOAddaLineCTA().click();
            });

        });
    });

    this.defineStep(/^I see MyTMO line selector page$/, function () {
        browser.sleep(5000);
        return browser.getCurrentUrl().then(function (currentUrl) {
            expect(currentUrl).to.equal('https://my.t-mobile.com/purchase/shop');
        });
    });





    //////////////////////////////*******************newly added add to cart button on the mini accessory pdp******************//////////////////////////////////

    this.defineStep(/^I click on add to cart button on mini pdp accessory$/, function () {
        browser.driver.sleep(5000);
        var AddtoCart = element(by.buttonText('Add to cart')); // add to cart in Mini PDP accessory locater is different from other
        AddtoCart.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        AddtoCart.click();
        browser.driver.sleep(5000);
    });

    ////////////////////////////////***********************************End*****************************************//////////////////////////////////////

    //**********************************************CART PROMOTIONS************************************************************************/

    this.defineStep(/^I see 'Add Promo Code' link in cart page below Accessories tile$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
    });

    this.defineStep(/^I click 'Add Promo Code' link$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
    });

    this.defineStep(/^I see a text field to enter the promo code$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
    });

    this.defineStep(/^I enter "([^"]*)" promo code in the promo code field$/, function (arg1, callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
    });

    this.defineStep(/^click on 'Apply' button$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
    });

    this.defineStep(/^promotion is applied$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
    });

    this.defineStep(/^I see a notification banner with applied promotion details$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
    });

    this.defineStep(/^I see 'Add Promo Code' link in cart page below Accessories tile$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
    });

    this.defineStep(/^I click 'Add Promo Code' link$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
    });

    this.defineStep(/^I see a text field to enter the promo code$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
    });

    this.defineStep(/^I enter "([^"]*)" promo code in the promo code field$/, function (arg1, callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
    });

    this.defineStep(/^click on 'Apply' button$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
    });

    this.defineStep(/^promotion is not applied$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
    });

    this.defineStep(/^I see a notification banner with invalid promotion details$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
    });

    ////////////////////////////*******************Added step file when according to e2e006 feature file********************************////////////////////////////////////////
    this.defineStep(/^I click on update button to add the service$/, function () {
        var updatebuttononotherservices = element(by.css('[ng-disabled="!devicePASelctor.protectionPlans.isEligibleForCart"]'));
        browser.sleep(2000);
        browser.executeScript("arguments[0].scrollIntoView(false);", updatebuttononotherservices.getWebElement());
        browser.sleep(1000);
        updatebuttononotherservices.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 450;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            return updatebuttononotherservices.click();
        });

        /*
        updatebuttononotherservices.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 450;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            browser.actions().doubleClick(element(by.css('[ng-bind-html="devicePASelctor.keyValues.services.addToCartCta | parseHtml"]'))).perform();
        });*/
    });

    ///////////////////////////////******************************end of e2e024 updated steps***************************************//////////////////////////////////////

    /////////////////////////////*******************************e2eoo9 updation******************************************************////////////////////////////////

    this.defineStep(/^on Cart page Add bundled services to the phone line$/, function () {
        browser.sleep(2000);
        var needmoreoptions = element(by.css('[ng-click="vm.myCartCtrl.zipCodeCheck(vm.lineItem,$index)"]'))
        browser.wait(EC.elementToBeClickable(needmoreoptions), 20000);

        needmoreoptions.getLocation().then(function (ElementLocation) {

            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            needmoreoptions.click();

            var zipcode = element(by.id('zipcode'));
            zipcode.clear();
            zipcode.sendKeys('98006');

            var nextonzipcode = element(by.css('[ng-click="setLocationFPOModalCtrl.validateZipCode(zipCodeForm)"]'));
            browser.wait(EC.visibilityOf(element(by.css('[ng-click="setLocationFPOModalCtrl.validateZipCode(zipCodeForm)"]'))), 10000);
            nextonzipcode.click();

            browser.sleep(2000);

            var getBundledServiceCategories = element.all(by.css('[ng-click="devicePASelctor.selectServiceCategory(category.type)"]')).get(1);
            browser.wait(EC.elementToBeClickable(getBundledServiceCategories, 10000));
            getBundledServiceCategories.click();

            var BundledServices = element.all(by.css('[ng-enter="devicePASelctor.selectService(plan)"]')).get(12);
            browser.wait(EC.visibilityOf(BundledServices), 5000);
            browser.wait(EC.elementToBeClickable(BundledServices), 5000);
            return BundledServices.getLocation().then(function (ElementLocation) {
                //can be used to select other bundled services category with get() instance

                var yAxisValue = ElementLocation.y - 250;
                browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
                BundledServices.click();
                browser.waitForAngular();
            });
        });
    });

    /////////////////////////////********************************end of e2e009********************************************************////////////////////////////

    //***************************************************Removing line in Cart****************************************************************************************
    this.defineStep(/^I Click on Remove line link in Cart Row$/, function () {
        return Smartcart.getRemoveText().click();
    });

    this.defineStep(/^I should see empty cart page$/, function () {
        return browser.wait(EC.visibilityOf(Smartcart.getEmptyCartHeader()), 50000).then(function () {
            Smartcart.getEmptyCartHeader().getText().then(function (emptyCartTitle) {
                expect(emptyCartTitle).to.be.not.null;
            });
        });
    });

    this.defineStep(/^I should see cart page$/, function () {
        browser.wait(EC.visibilityOf(Smartcart.getCartTitleElement()), 50000);
        return Smartcart.getCartTitleElement().getText().then(function (cartTitle) {
            expect(cartTitle).to.not.be.null;
        });
    });
    /////////////////////////////////////////*****************Removing accessory from the loaded cart e2e004 changes**************////////////////////////////////////////////////////**********************************************


    this.defineStep(/^I should see option for delete and edit$/, function () {
        var deleteaccesory = element(by.buttonText('Delete'));
        deleteaccesory.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            deleteaccesory.click(); //click on the delete option
        });
    });

    this.defineStep(/^I should see again a confirmation to delete the accessorry added to cart$/, function () {
        var deleteconfirmation = element(by.id('cnfdelBtnAcc'));
        deleteconfirmation.click(); //click on the delete confirmation option
    });

    //***********************************************Proceed to Checkout********************************************************************************************************

    this.defineStep(/^I click on Checkout Button in Cart Page$/, function () {
        browser.sleep(5000);
        return Smartcart.CheckoutButton().click();
    });

    this.defineStep(/^I should see Personal Info & shipping page$/, function () {
        return personalinfo.getPersonalInfoHeader().getText().then(function (personalinfoTitle) {
            expect(personalinfoTitle).to.equal('Personal Info');
        });
    });

    this.defineStep(/^I am on Info and Shipping page$/, function () {
        return personalinfo.getPersonalInfoHeader().getText().then(function (personalinfoTitle) {
            expect(personalinfoTitle).to.equal('Personal Info');
        });
    });

    this.defineStep(/^I am in cart page$/, function () {
        return Smartcart.deviceTileHeaderElement().getText().then(function (deviceTileHeader) {
            expect(deviceTileHeader).to.equal('DEVICE');
        });
    });

    this.defineStep(/^I am in Cart Page$/, function () {
        return browser.wait(EC.visibilityOf(Smartcart.getCartTitleElement()), 30000).then(function () {
            Smartcart.getCartTitleElement().getText().then(function (title) {
                Smartcart.getCartTitleElement().getLocation().then(function (cartTitleLocation) {
                    var x = cartTitleLocation.x;
                    var y = cartTitleLocation.y - 200;
                    browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
                });
                expect(title).to.not.be.null;
            });
        });
    });

    this.defineStep(/^I am in checkout cart page$/, function () {
        Smartcart.getCartTitleElement().getLocation().then(function (cartTitleLocation) {
            var x = cartTitleLocation.x;
            var y = cartTitleLocation.y - 200;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
        });
        return Smartcart.getCartTitleElement().getText().then(function (title) {
            expect(title).to.include('My Stuff');
        });
    });

    this.defineStep(/^I am in a loaded cart page$/, function () {
        browser.sleep(5000);
        return browser.wait(EC.visibilityOf(Smartcart.getCartTitleElement()), 50000).then(function () {
            Smartcart.getCartTitleElement().getLocation().then(function (cartTitleLocation) {
                var x = cartTitleLocation.x;
                var y = cartTitleLocation.y - 200;
                browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
            });
            Smartcart.getCartTitleElement().getText().then(function (title) {
                expect(title).to.not.be.null;
            });
        });

    });

    this.defineStep(/^I click on Add Device Icon in device tile$/, function () {
        Smartcart.getAddDeviceIconElement().get(0).getLocation().then(function (deviceIconLocation) {
            var x = deviceIconLocation.x;
            var y = deviceIconLocation.y - 200;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
        });
        return Smartcart.getAddDeviceIconElement().get(0).click();
    });

    this.defineStep(/^I click on 'Add a new phone' button in device tile$/, function () {
        return Smartcart.getAddNewPhoneLabelElement().click();
    });

    this.defineStep(/^I should see credit class modal$/, function () {
        return Smartcart.creditSelectorModalHeaderElement().getText().then(function (creditClassHeader) {
            expect(creditClassHeader).to.not.be.null;
        });
    });

    this.defineStep(/^I am in credit class modal$/, function () {
        return Smartcart.getAwesomeCreditClassLabelElement().getText().then(function (awesomeCreditLabel) {
            expect(awesomeCreditLabel).to.equal('AWESOME CREDIT');
        });
    });

    this.defineStep(/^I select Awesome credit radio button$/, function () {
        return Smartcart.getAwesomeCreditClassLabelElement().getLocation().then(function (ctaButtonLocation) {
            var x = ctaButtonLocation.x;
            var y = ctaButtonLocation.y - 200;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
            browser.sleep(5000);
            Smartcart.getAwesomeCreditClassLabelElement().click();
        });
    });

    this.defineStep(/^I click on credit class on sticky banner$/, function () {
        return Smartcart.stickyBannerCreditInitial().getText().then(function (creditValue) {
            var creditClassName = creditValue;
            Smartcart.stickyBannerCreditInitial().click();
        });
    });

    this.defineStep(/^I should see credit class changed in sticky banner$/, function () {
        return Smartcart.stickyBannerCreditInitial().getText().then(function (creditValue) {
            expect(creditValue).to.not.equal('creditClassName');
        });
    });

    this.defineStep(/^I click on 'Done' button$/, function () {
        return Smartcart.getBuildMyOrderButtonElement().click();
    });

    this.defineStep(/^I should see mini browse page$/, function () {
        return browser.wait(EC.visibilityOf(Smartcart.getMiniBrowseTitleElement()), 30000).then(function (isVisible) {
            if (isVisible) {
                Smartcart.getMiniBrowseTitleElement().getText().then(function (miniBrowseTitle) {
                    expect(miniBrowseTitle).to.not.be.null;
                });
            }
        });
    });

    this.defineStep(/^I am on mini browse page$/, function () {
        return Smartcart.getMiniBrowseTitleElement().getText().then(function (miniBrowseTitle) {
            expect(miniBrowseTitle).to.not.be.null;
        });
    });

    this.defineStep(/^I select "([^"]*)"$/, function (phoneName) {
        phoneNameLine1 = phoneName;
        browser.sleep(5000);
        var minibrowseDevice = element(by.cssContainingText('.selector-device-name', phoneName));
        return minibrowseDevice.click();
    });

    this.defineStep(/^I select "([^"]*)" from main browse as first Phone Line$/, function (phoneName) {
        phoneNameLine1 = phoneName;
        var selectPhone = element(by.cssContainingText('.product-name', phoneName));
        return selectPhone.click();
    });

    this.defineStep(/^I select "([^"]*)" from main browse as first Tablet Line$/, function (tablet) {
        tabletNameLine1 = tablet;
        var selectTablet = element(by.cssContainingText('.product-name', tablet));
        return selectTablet.click();
    });

    this.defineStep(/^I select "([^"]*)" for Line 2$/, function (phoneName) {
        phoneNameLine2 = phoneName;
        var minibrowseDevice = element(by.cssContainingText('.selector-device-name', phoneName));
        return minibrowseDevice.click();
    });

    this.defineStep(/^I should see "([^"]*)" in the mini pdp$/, function (task) {
        return Smartcart.getAddToCartButtonMiniPDP().getText().then(function (data) {
            expect(data).to.equal(task, "Add to cart");
        });
    });

    this.defineStep(/^I should see a button to add device to cart$/, function () {
        return Smartcart.getAddToCartButtonMiniPDP().getText().then(function (data) {
            expect(data).to.be.not.null;
        });
    });

    this.defineStep(/^I am on device add to cart page$/, function () {
        return Smartcart.getMiniBrowseTitle().getText().then(function (miniBrowseTitle) {
            expect(miniBrowseTitle).to.equal('Apple iPhone 7');
        });
    });

    this.defineStep(/^I click on 'Add to cart' button$/, function () {
        browser.sleep(5000);
        return browser.wait(EC.visibilityOf(Smartcart.getAddToCartButtonMiniPDP()), 50000).then(function () {
            Smartcart.getAddToCartButtonMiniPDP().getLocation().then(function (ctaButtonLocation) {
                var x = ctaButtonLocation.x;
                var y = ctaButtonLocation.y - 200;
                browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
                Smartcart.getAddToCartButtonMiniPDP().click();
            });
        });
    });

    this.defineStep(/^I click on the 'Checkout' button$/, function () {
        return Smartcart.CheckoutButton().isEnabled().then(function (result) {
            expect(result).to.be.true;
            Smartcart.CheckoutButton().click();
        });
    });

    /*this.defineStep(/^validate the device is added to cart$/, function () {

     browser.sleep(5000);
         var Deviceheader = element(by.id('cartDeviceSection_1')).element(by.css('h5[role="heading"]'));
     return Deviceheader.getText().then(function(CellPhoneDeviceHeader){
         expect(CellPhoneDeviceHeader).to.equal('Apple iPhone 7');
    });
    });*/

    this.defineStep(/^I checkout the cart$/, function () {
        var checkout = element.all(by.css('[aria-label="Checkout "]'));
        browser.sleep(8000);
        return checkout.click();
    });

    this.defineStep(/^validate the accessory is added to cart$/, function () {
        browser.sleep(5000);
        var Accheader = element.all(by.repeater('acc in vm.cartObj.accessories.accessoryDetails')).get(0).element(by.css('[class="small name ng-binding"]'));
        return Accheader.getText().then(function (AccTileHeader) {
            expect(AccTileHeader).to.equal('T-Mobile 4 ft. micro USB Cable - Black');
        });
    });

    this.defineStep(/^validate the iPad is added to cart$/, function () {
        browser.sleep(8000);
        var Deviceheader = element(by.id('cartDeviceSection_1')).element(by.css('h5[role="heading"]'));
        return Deviceheader.getText().then(function (IDDeviceHeader) {
            expect(IDDeviceHeader).to.equal('Apple iPad mini 4');
        });
    });

    this.defineStep(/^I select speaker as an accessory from mini browse$/, function () {
        browser.sleep(8000);
        var AccessoryAdd = element.all(by.css('[ng-if="vm.keyValuePair.accessories.addAccessoriesImage"]')).get(0);
        AccessoryAdd.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return AccessoryAdd.click()
    });

    this.defineStep(/^I go to the mini PDP$/, function () {
        var AccessoryAddSpeak = element.all(by.css('[alt="Product image"]')).get(0);
        browser.sleep(8000);
        return AccessoryAddSpeak.click()
    });

    this.defineStep(/^I add accessory to the cart$/, function () {
        var selectAccessoryMiniPDP = element.all(by.css('[ng-click="vm.addToCart()"]'));
        browser.sleep(8000);
        return selectAccessoryMiniPDP.click()
    });

    this.defineStep(/^I validate the BYOD cell phones SIM is added to cart$/, function () {
        browser.sleep(8000);
        var Deviceheadersim = element(by.id('cartDeviceSection_1')).element(by.css('h5[role="heading"]'));
        browser.wait(EC.invisibilityOf(element(by.css('.screen-lock'))), 20000);
        browser.wait(EC.elementToBeClickable(Deviceheadersim), 20000);
        return Deviceheadersim.getText().then(function (IDDeviceHeaderBYOD) {
            expect(IDDeviceHeaderBYOD).to.equal('T-Mobile 3-in-1 SIM Starter Kit');
            browser.sleep(8000);
        });
    });

    this.defineStep(/^I add empty tablet line to cart$/, function () {
        var selectTabletBYOD = element(by.css('[ng-click="vm.cartObj.addLineByLineType(\'TABLET\')"]'));
        browser.wait(EC.invisibilityOf(element(by.css('.screen-lock'))), 20000);
        browser.wait(EC.elementToBeClickable(selectTabletBYOD), 20000);
        selectTabletBYOD.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        selectTabletBYOD.click();
        return browser.sleep(8000);
    });

    this.defineStep(/^I click on plus button$/, function () {
        var selectBYODTablet = element(by.css('[aria-label="Tab alt txt"]'));
        selectBYODTablet.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        selectBYODTablet.click();
        return browser.sleep(8000);
    });

    this.defineStep(/^I click on plus button on tablets empty line$/, function () {
        var selectBYODTablet = element(by.css('[aria-label="Click to add a tablet"]'));
        selectBYODTablet.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        selectBYODTablet.click();
        return browser.sleep(8000);
    });

    this.defineStep(/^I click on 'Add Tablet' button to view the mini browse$/, function () {
        var selectBYODTabletButton = element(by.css('[class="btn btn-primary ng-binding"]'));
        selectBYODTabletButton.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        selectBYODTabletButton.click();
        return browser.sleep(8000);
    });

    this.defineStep(/^I click on 'Browse All' tablets$/, function () {
        var selectGoToMBrowse = element.all(by.css('[class="btn btn-primary ng-binding"]')).get(1);
        selectGoToMBrowse.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        selectGoToMBrowse.click();
        return browser.sleep(8000);
    });

    //updated for e2e008////
    this.defineStep(/^I click on plus button for Tablet$/, function () {
        var selectBYODTablet = element(by.css('[class="add-device-image-wrapper ng-scope tabletImgWrapper"]')).element(by.css('[class="cursor-pointer display-block ng-scope ng-isolate-scope"]')); //loacter for the add tablet from the cart page
        selectBYODTablet.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        selectBYODTablet.click();
        return browser.sleep(8000);
    });

    this.defineStep(/^I click on Add Tablet button to view the Tablet mini browse$/, function () {
        var selectBYODTabletButton = element.all(by.css('[class="btn btn-primary ng-binding"]')).get(1); //loacter for the add accessory from the cart page;
        selectBYODTabletButton.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        selectBYODTabletButton.click();
        return browser.sleep(8000);
    });

    this.defineStep(/^I click on Done Button in Credit Class Selector$/, function () {
        browser.sleep(3000);
        var donecta = element(by.buttonText('Done'));
        donecta.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        donecta.click();
        return browser.sleep(8000);
    });

    this.defineStep(/^I click on Browse All tablets modal$/, function () {
        var selectGoToMBrowse = element.all(by.css('[class="btn btn-primary ng-binding"]')).get(2);
        selectGoToMBrowse.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        selectGoToMBrowse.click();
        return browser.sleep(8000);
    });

    //end of updates

    this.defineStep(/^validate the BYOD internet\-devices SIM is added to cart$/, function () {
        var Deviceheader = element(by.id('cartDeviceSection_2')).element(by.css('h5[role="heading"]'));
        browser.wait(EC.invisibilityOf(element(by.css('.screen-lock'))), 20000);
        browser.wait(EC.elementToBeClickable(Deviceheader), 20000);
        return Deviceheader.getText().then(function (IDDeviceHeader) {
            expect(IDDeviceHeader).to.equal('T-Mobile 3-in-1 Mobile Internet SIM Kit');
            browser.sleep(8000);
        });
    });

    this.defineStep(/^validate the above accessory is added to cart$/, function () {
        var Accheader = element.all(by.repeater('acc in vm.cartObj.accessories.accessoryDetails')).get(0).element(by.css('[class="small name ng-binding"]'));
        return Accheader.getText().then(function (AccTileHeader) {
            expect(AccTileHeader).to.equal(accessoryNameLine1);
        });
    });

    this.defineStep(/^I click on creditclass link$/, function () {
        return Smartcart.getCartCreditClassLink().getText().then(function (link) {
            browser.sleep(2000);
            browser.executeScript("arguments[0].scrollIntoView(false);", Smartcart.getCartCreditClassLink().getWebElement());
            browser.sleep(1000);
            Smartcart.getCartCreditClassLink().getLocation().then(function (ElementLocation) {
                var yAxisValue = ElementLocation.y - 250;
                browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
                Smartcart.getCartCreditClassLink().click();
            });
        });
    });


    /*    var selectCredit = element.all(by.css('[ng-bind-html="myCartCtrl.cartCommonOperationsService.model.structCartData.selectedCreditClass.label || myCartCtrl.keyValues.class[0].label | parseHtml"]')).get(0);
        selectCredit.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectCredit.click();
    });*/

    this.defineStep(/^I should see cart credit class modal$/, function () {
        var checkModal = element.all(by.css('[ng-bind-html="creditSelectorModalCtrl.keyValues.creditSelector.header | parseHtml"]')).get(0);
        return checkModal.getText().then(function (credit) {
            expect(credit).to.include('Please select your credit');
        });
    });

    this.defineStep(/^I change the credit class to Awesome$/, function () {
        var selectAverage = element.all(by.css('[ng-bind-html="creditClass.label | parseHtml"]')).get(0);
        browser.sleep(5000);
        return selectAverage.click();
    });

    this.defineStep(/^I click on Done button to update the creditclass$/, function () {
        var submitSelection = element.all(by.css('[ng-bind-html="creditSelectorModalCtrl.keyValues.creditSelector.buildMyOrderCTA | parseHtml"]')).get(0);
        return submitSelection.click();
    });

    this.defineStep(/^I validate the cart credit type is changed to Awesome$/, function () {
        var checkCredit = element.all(by.css('[ng-bind-html="myCartCtrl.cartCommonOperationsService.model.structCartData.selectedCreditClass.label || myCartCtrl.keyValues.class[0].label | parseHtml"]')).get(0);
        browser.sleep(5000);
        return checkCredit.getText().then(function (credit) {
            console.log("Updated credit class is, " + credit)
            expect(credit).to.equal('AWESOME CREDIT');
        });
    });

    this.defineStep(/^I should see review order page$/, function () {
        return review.getReviewOrderTitle().getText().then(function (reviewOrderHeader) {
            expect(reviewOrderHeader).to.not.be.null;
        });
    });

    this.defineStep(/^I click on Edit in Cart link in review order page$/, function () {
        browser.sleep(3000);
        return review.getEditInCartLink().click();

        /*return browser.wait(EC.visibilityOf(shipping.getPaymentHeader()), 30000).then(function (isVisible) {
            if (isVisible) {
                review.getEditInCartLink().click();
            }
        });*/

    });

    this.defineStep(/^I click on edit cart on review page$/, function () {
        browser.sleep(10000);
        var selectReviewEdit = element.all(by.css('[class="fa fa-edit"]')).get(0);
        selectReviewEdit.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectReviewEdit.click();
    });

    this.defineStep(/^I should be taken back to cart page$/, function () {
        return browser.wait(EC.visibilityOf(Smartcart.getCartTitleElement()), 30000).then(function (isVisible) {
            if (isVisible) {
                Smartcart.getCartTitleElement().getText().then(function (title) {
                    expect(title).to.not.be.null;
                });
            }
        });
    });

    //---------------------------------Smart Cart- Add,Duplicate,Edit etc------------------------------------//

    //****************************************Updates for e2e010*************************************************************

    this.defineStep(/^I click on the Tablet mini Browse$/, function () {
        var productclick = element.all(by.css('[alt="DeviceImage"]')).get(0); // locater to select the product from the browse page
        productclick.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            productclick.getText().then(function (productname) {
                console.log("selected product name is", productname);
                productclick.click(); //click on the product selected from the browse page
            });
        });
    });

    this.defineStep(/^I add Tablet from mini PDP with monthly payment option$/, function () {
        var select = element(by.id('payment-option'));
        // var select = element(by.id('pricing'));
        select.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            select.$('[value="string:monthly"]').click(); //for monthly payment
            //select.$('[value="string:full"]').click();  //for full payment
        });
        var addtocartminiBrowse = element(by.buttonText('Add to cart'));
        addtocartminiBrowse.click();
    });

    this.defineStep(/^validate the iPad internet device is added to cart$/, function () {
        var IDeviceheader = element(by.id('cartDeviceSection_1')).element(by.css('h5[role="heading"]'));
        return IDeviceheader.getText().then(function (CellPhoneDeviceHeader) {
            expect(CellPhoneDeviceHeader).to.equal('Apple 9.7 inch iPad Pro');
        });
    });

    this.defineStep(/^I duplicate the added device line$/, function () {
        return element(by.id('cart_duplicate_line_0')).getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            element(by.id('cart_duplicate_line_0')).click();
            browser.driver.sleep(5000);
        });
    });

    this.defineStep(/^I duplicate the added cell phone line three$/, function () {
        browser.driver.sleep(5000);
        var duplicateCP = element.all(by.css('[class=" fa fa-copy"]')).get(2);
        duplicateCP.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        duplicateCP.click();
        browser.driver.sleep(8000);
    });

    this.defineStep(/^validate the duplicate internet device is added to cart$/, function () {
        browser.driver.sleep(8000);
        var IDeviceheader = element(by.id('cartDeviceSection_2')).element(by.css('h5[role="heading"]'));
        return IDeviceheader.getText().then(function (IDDeviceHeader) {
            expect(IDDeviceHeader).to.not.equal('');
        });
    });

    this.defineStep(/^I validate the selected device is added to cart tile two$/, function () {
        browser.driver.sleep(8000);
        var IDeviceheader = element(by.id('cartDeviceSection_2')).element(by.css('h5[role="heading"]'));
        return IDeviceheader.getText().then(function (IDDeviceHeader) {
            expect(IDDeviceHeader).to.not.equal('');
            browser.driver.sleep(8000);
        });
    });

    this.defineStep(/^I click on edit button for internet device$/, function () {
        browser.driver.sleep(3000);
        return element.all(by.css('[class=" fa fa-pencil"]')).get(1).getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            element.all(by.css('[class=" fa fa-pencil"]')).get(1).click();
            browser.driver.sleep(3000);
        });
    });

    this.defineStep(/^I select new color swatch from mini PDP$/, function () {
        return element.all(by.css('[ng-enter="miniPDPModalCtrl.setColor($index,product.Color)"]')).get(0).getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            element.all(by.css('[ng-enter="miniPDPModalCtrl.setColor($index,product.Color)"]')).get(0).click();
        });
    });

    this.defineStep(/^I select new color swatch in mini PDP$/, function () {
        return Smartcart.selectColor().getLocation().then(function (buttonLocation) {
            var x = buttonLocation.x;
            var y = buttonLocation.y - 200;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
            Smartcart.selectColor().get(1).click();
        });
    });

    this.defineStep(/^I should see Mini PDP of the selected device$/, function () {
        return browser.executeScript(EC.visibilityOf(Smartcart.getminiPDPHeader()), 30000).then(function () {
            Smartcart.getminiPDPHeader().getLocation().then(function (buttonLocation) {
                var x = buttonLocation.x;
                var y = buttonLocation.y - 200;
                browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
            });
            Smartcart.getminiPDPHeader().getText().then(function (deviceHeader) {
                expect(deviceHeader).to.not.be.null;
            });
        });
    });

    this.defineStep(/^I navigate to Mini PDP of "([^"]*)"$/, function (deviceName) {
        return Smartcart.getminiPDPHeader().getText().then(function (deviceHeader) {
            expect(deviceHeader).to.equal(deviceName);
        });
    });

    this.defineStep(/^I look for payment option dropdown$/, function () {
        browser.driver.sleep(5000);
        return element(by.id('payment-option')).getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            element(by.id('payment-option')).click();
            browser.driver.sleep(5000);
        });
    });

    this.defineStep(/^I select full price from payment option$/, function () {
        return element.all(by.css('[label="Pay in Full"]')).getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            element.all(by.css('[label="Pay in Full"]')).click();
            browser.sleep(5000);
        });
    });

    this.defineStep(/^validate full price payment option is selected$/, function () {
        browser.driver.sleep(5000);
        //var checkFRP = element.all(by.css('[value="string:monthly"]'));
        var checkFRP = element(by.css('[ng-change="miniPDPModalCtrl.setPaymentOption(miniPDPModalCtrl.paymentOption)"]'));
        return checkFRP.getText().then(function (SeeFRP) {
            expect(SeeFRP).to.include('Pay in Full');
            browser.driver.sleep(5000);
        });
    });

    this.defineStep(/^I click on add to cart button on mini pdp$/, function () {
        browser.driver.sleep(5000);
        return element.all(by.css('[ng-click="miniPDPModalCtrl.addToCart()"]')).get(0).getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            element.all(by.css('[ng-click="miniPDPModalCtrl.addToCart()"]')).get(0).click();
            browser.driver.sleep(5000);
        });
    });

    this.defineStep(/^I click on add a phone button$/, function () {
        browser.driver.sleep(5000);
        return element.all(by.css('[ng-click="vm.cartObj.addLine()"]')).get(0).getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            element.all(by.css('[ng-click="vm.cartObj.addLine()"]')).get(0).click();
            browser.driver.sleep(5000);
        });
    });

    this.defineStep(/^I click on plus button for phones$/, function () {
        browser.driver.sleep(2000);
        return element.all(by.css('[alt="Click to add a phone"]')).get(0).getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            element.all(by.css('[alt="Click to add a phone"]')).get(0).click();
            browser.driver.sleep(2000);
        });
    });

    this.defineStep(/^I click on plus button for phones for line two$/, function () {
        browser.driver.sleep(4000);
        return element.all(by.css('[alt="Click to add a phone"]')).get(0).getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            element.all(by.css('[alt="Click to add a phone"]')).get(0).click();
            browser.driver.sleep(4000);
        });
    });

    this.defineStep(/^I click on plus button for new phone line$/, function () {
        var selectPhones = element.all(by.css('[alt="Device alt text-test"]')).get(1);
        //var selectPhones = element.all(by.css('[alt="Click to add a phone"]')).get(1);
        selectPhones.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectPhones.click();
    });

    this.defineStep(/^I click on plus button for phone line two$/, function () {
        var selectPhones = element.all(by.css('[alt="Device alt text-test"]')).get(1);
        selectPhones.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectPhones.click();
    });

    this.defineStep(/^I click on 'Add a new phone' button to view the mini browse$/, function () {
        browser.driver.sleep(5000);
        return element.all(by.css('[class="btn btn-primary ng-binding"]')).get(2).getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            element.all(by.css('[class="btn btn-primary ng-binding"]')).get(2).click();
            browser.driver.sleep(5000);
        });
    });

    this.defineStep(/^I select a cell phone from mini browse$/, function () {
        browser.driver.sleep(5000);
        return element.all(by.css('[alt="DeviceImage"]')).get(1).getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            element.all(by.css('[alt="DeviceImage"]')).get(1).click();
            browser.driver.sleep(5000);
        });
    });

    this.defineStep(/^I add the selected cell phone to cart\.$/, function () {
        browser.sleep(10000);
        return element.all(by.css('[ng-enter="miniPDPModalCtrl.addToCart()"]')).get(0).getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 450;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            element.all(by.css('[ng-enter="miniPDPModalCtrl.addToCart()"]')).get(0).click();
            browser.sleep(10000);
        });
    });

    this.defineStep(/^I duplicate the added  cell phone line$/, function () {
        browser.driver.sleep(5000);
        return element(by.id('cart_duplicate_line_2')).getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            element(by.id('cart_duplicate_line_2')).click();
            browser.driver.sleep(8000);
        });
    });

    this.defineStep(/^validate the duplicate cell phone is added to cart$/, function () {
        browser.driver.sleep(8000);
        var IDeviceheader = element(by.id('cartDeviceSection_4')).element(by.css('h5[role="heading"]'));
        return IDeviceheader.getText().then(function (CellPhoneDeviceHeader) {
            expect(CellPhoneDeviceHeader).to.not.equal('');
        });
    });

    this.defineStep(/^I click on edit button for phone line$/, function () {
        browser.driver.sleep(5000);
        return element.all(by.css('[class=" fa fa-pencil"]')).get(3).getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            element.all(by.css('[class=" fa fa-pencil"]')).get(3).click();
            browser.driver.sleep(5000);
        });
    });

    this.defineStep(/^I look for memory option dropdown$/, function () {
        browser.driver.sleep(5000);
        return element(by.id('memory')).getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            element(by.id('memory')).click();
            browser.driver.sleep(5000);
        });
    });


    this.defineStep(/^I see mini pdp page of the device$/, function () {
        var memoryOpt = element(by.id('memory'));
        memoryOpt.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        memoryOpt.click();
    });

    this.defineStep(/^I select different memory from memory option$/, function () {
        browser.driver.sleep(5000);
        return element.all(by.css('[ng-selected="miniPDPModalCtrl.selectedMemory==product.Memory"]')).get(1).getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            element.all(by.css('[ng-selected="miniPDPModalCtrl.selectedMemory==product.Memory"]')).get(1).click();
            browser.driver.sleep(5000);
        });
    });

    this.defineStep(/^I select different memory from memory options dropdown$/, function () {
        return Smartcart.selectMemory().getLocation().then(function (buttonLocation) {
            var x = buttonLocation.x;
            var y = buttonLocation.y - 200;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
            Smartcart.selectMemory().get(1).click();
        });
    });

    this.defineStep(/^I select "([^"]*)" from memory drop down$/, function (memory) {
        var myMemory = new SelectWrapper(by.id('memory'));
        return myMemory.selectByText(memory);
    });

    this.defineStep(/^I click on edit button for line 1$/, function () {
        browser.driver.sleep(5000);
       // var editLineOne = element.all(by.css('[class=" fa fa-pencil"]')).get(0);
       return element.all(by.css('[class=" fa fa-pencil"]')).get(0).getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            element.all(by.css('[class=" fa fa-pencil"]')).get(0).click();
        });
        browser.driver.sleep(5000);
    });

    this.defineStep(/^I click on 'Add a new phone' button for line 2 to view the mini browse$/, function () {
        return element.all(by.css('[class="btn btn-primary ng-binding"]')).get(1).getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            element.all(by.css('[class="btn btn-primary ng-binding"]')).get(1).click();
        });
    });

    this.defineStep(/^I click on edit button for line two$/, function () {
        browser.driver.sleep(5000);
        return element.all(by.css('[class=" fa fa-pencil"]')).get(1).getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            element.all(by.css('[class=" fa fa-pencil"]')).get(1).click();
            browser.driver.sleep(5000);
        });
    });

    this.defineStep(/^I click on 'Add a new phone' button to view the mini phone browse$/, function () {
        var selectAddPhoneLine2 = element.all(by.css('[class="btn btn-primary ng-binding"]')).get(0);
        selectAddPhoneLine2.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectAddPhoneLine2.click();
    });

    this.defineStep(/^I remove the first line from cart$/, function () {
        var removeLine = element.all(by.css('[class=" fa fa-trash"]')).get(0);
        removeLine.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        removeLine.click();
    });

    this.defineStep(/^I click on plus button on tile for accessory$/, function () {
        var selectAccessoryTile = element(by.css('[ng-if="vm.keyValuePair.accessories.addAccessoriesImage"]'));
        selectAccessoryTile.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        selectAccessoryTile.click();
        return browser.sleep(2000);
    });

    this.defineStep(/^I select an accessory from mini browse to add as an second accessory$/, function () {
        var accTwo = element.all(by.css('[alt="Product image"]')).get(1);
        accTwo.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        accTwo.click();
    });

    this.defineStep(/^I select an accessory from mini browse to add as an third accessory$/, function () {
        var accThree = element.all(by.css('[alt="Product image"]')).get(7);
        accThree.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        accThree.click();
    });

    this.defineStep(/^I add the selected accessory to cart$/, function () {
        var accToCart = element(by.css('[ng-click="vm.addToCart()"]'));
        accToCart.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return accToCart.click();
    });

    this.defineStep(/^I select another accessory from mini browse$/, function () {
        var accThree = element.all(by.css('[alt="Product image"]')).get(8);
        accThree.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        accThree.click();
    });

    this.defineStep(/^I select a new cell phone from mini browse$/, function () {
        var selectAPhone = element.all(by.css('[alt="DeviceImage"]')).get(1);
        selectAPhone.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectAPhone.click();
    });

    this.defineStep(/^I delete the second line from cart$/, function () {
        var delLineTwo = element.all(by.css('[class=" fa fa-trash"]')).get(1);
        delLineTwo.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return delLineTwo.click();
    });

    //*****************************************E2E-028*******************************************

    this.defineStep(/^I add an service to the line one$/, function () {
        var addServiceL1 = element.all(by.css('[ng-bind-html="service.familyName"]')).get(0);
        browser.sleep(2000);
        browser.executeScript("arguments[0].scrollIntoView(false);", addServiceL1.getWebElement());
        browser.sleep(1000);
        addServiceL1.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return addServiceL1.click();
    });

    this.defineStep(/^I add an service to the line two$/, function () {
        browser.driver.sleep(3000);
        var addServiceL2 = element.all(by.css('[ng-bind-html="service.familyName"]')).get(2);
        addServiceL2.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        addServiceL2.click();
        return browser.sleep(5000);
    });

    this.defineStep(/^I click on 'Add a new phone' button for line 3 to view the mini browse$/, function () {
        var selectAddPhoneLine3 = element.all(by.css('[class="btn btn-primary ng-binding"]')).get(1);
        selectAddPhoneLine3.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectAddPhoneLine3.click();
    });

    this.defineStep(/^I select a cell phone from mini browse for line three$/, function () {
        var selectAPhone = element.all(by.css('[alt="DeviceImage"]')).get(2);
        selectAPhone.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectAPhone.click();
    });

    this.defineStep(/^I add an service to the line three$/, function () {
        var addServiceL3 = element.all(by.css('[ng-bind-html="service.familyName"]')).get(12);
        addServiceL3.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return addServiceL3.click();
    });

    this.defineStep(/^I click on plus button for new phone line three$/, function () {
        var selectPhones3 = element.all(by.css('[alt="Click to add a phone"]')).get(2);
        selectPhones3.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectPhones3.click();
    });

    this.defineStep(/^I click on plus button for new tablet line$/, function () {
        var selectTabBYOD = element.all(by.css('[alt="Tab alt txt"]')).get(0);
        selectTabBYOD.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectTabBYOD.click();
    });

    this.defineStep(/^I click on Add Tablet button on cart to view the mini browse for line five$/, function () {
        var selectAddTab = element.all(by.css('[class="btn btn-primary ng-binding"]')).get(2);
        selectAddTab.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectAddTab.click();
    });

    this.defineStep(/^I click on Add Tablet button on cart to view the mini browse for line six$/, function () {
        var selectAddTab = element.all(by.css('[class="btn btn-primary ng-binding"]')).get(3);
        selectAddTab.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectAddTab.click();
    });


    this.defineStep(/^I click on BYOD Tablet button to view the BYOD tablet mini PDP$/, function () {
        var addaDevice = element.all(by.css('[ng-enter="myCartCtrl.openOwnPhoneModal(selectedLine)"]')).get(1);
        addaDevice.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return addaDevice.click();
    });

    this.defineStep(/^I select a tab from mini browse for line five$/, function () {
        var selectATab = element.all(by.css('[alt="DeviceImage"]')).get(0);
        selectATab.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectATab.click();
    });

    this.defineStep(/^I add the selected BYOD tab to cart$/, function () {
        var toCart = element(by.css('[ng-click="ownPhone.addTocart()"]'));
        toCart.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        toCart.click();
    });

    this.defineStep(/^I add an service to the line five$/, function () {
        var addServiceL5 = element.all(by.css('[ng-bind-html="service.familyName"]')).get(24);
        addServiceL5.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return addServiceL5.click();
    });

    this.defineStep(/^I add an service to the line six$/, function () {
        var addServiceL5 = element.all(by.css('[ng-bind-html="service.familyName"]')).get(29);
        addServiceL5.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return addServiceL5.click();
    });

    this.defineStep(/^I add the selected tab to cart$/, function () {
        browser.driver.sleep(5000);
        var toCart = element(by.css('[ng-click="miniPDPModalCtrl.addToCart()"]'));
        toCart.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        toCart.click();
    });

    this.defineStep(/^I select a tab from mini browse for line six$/, function () {
        var selectATab = element.all(by.css('[alt="DeviceImage"]')).get(2);
        selectATab.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectATab.click();
    });

    this.defineStep(/^I click on plus button for adding a new tablet line number five$/, function () {
        var selectTabLine5 = element.all(by.css('[alt="Click to add a tablet"]')).get(1);
        selectTabLine5.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectTabLine5.click();
    });

    this.defineStep(/^I click on plus button for adding a new tablet line number six$/, function () {
        var selectTabLine6 = element.all(by.css('[alt="Click to add a tablet"]')).get(2);
        selectTabLine6.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectTabLine6.click();
    });

    this.defineStep(/^I click on Add a Tablet Button for new tab line$/, function () {
        var selectTabLine = element(by.id('cart_cta_tablet'));
        selectTabLine.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectTabLine.click();
    });

    this.defineStep(/^I click on plus button for adding a new tablet line number seven$/, function () {
        var selectTabLine7 = element.all(by.css('[alt="Click to add a tablet"]')).get(3);
        selectTabLine7.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectTabLine7.click();
    });

    this.defineStep(/^I click on Add Tablet or wearable button on cart to view the mini browse for line seven$/, function () {
        var selectAddTab = element.all(by.css('[class="btn btn-primary ng-binding"]')).get(4);
        selectAddTab.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectAddTab.click();
    });


    this.defineStep(/^I duplicate the wearable line seventh$/, function () {
        var duplicateWear = element.all(by.css('[class=" fa fa-copy"]')).get(6);
        duplicateWear.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return duplicateWear.click();
    });

    this.defineStep(/^I click on edit button for line eight$/, function () {
        var editLineEight = element.all(by.css('[class=" fa fa-pencil"]')).get(10);
        editLineEight.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        editLineEight.click();
    });

    this.defineStep(/^I click on 'Browse All' tablets to go to main browse$/, function () {
        var selectGoToMBrowse = element.all(by.css('[ng-click="deviceSelectorModalCtrl.browseAllCtaMethod($event)"]'));
        selectGoToMBrowse.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        selectGoToMBrowse.click();
        return browser.sleep(5000);
    });

    this.defineStep(/^I click on browse all Cta$/, function () {
        var selectGoToAccessoryBrowse = element(by.css('[ng-bind-html="vm.keyValues.accessoriesModal.browseAll | parseHtml"]'));
        selectGoToAccessoryBrowse.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectGoToAccessoryBrowse.click();
    });

    this.defineStep(/^I select a wearable from mini browse for line seven$/, function () {
        var selectATab = element(by.css('[ng-src="/content/dam/t-mobile/en-p/internet-devices/samsung/samsung-gear-s3-frontier/samsung-gear-s3-frontier-1-3x.jpg"]'));
        selectATab.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return selectATab.click();
    });

    this.defineStep(/^I select accessory one from mini browse$/, function () {
        var accOne = element.all(by.css('[alt="Product image"]')).get(1);
        accOne.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        accOne.click();
    });

    this.defineStep(/^I select accessory two from mini browse$/, function () {
        var accTwo = element.all(by.css('[alt="Product image"]')).get(2);
        accTwo.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        accTwo.click();
    });

    this.defineStep(/^I add the newly selected accessory to cart$/, function () {
        var accToCart = element.all(by.css('[class="btn btn-primary ng-binding"]')).get(6);
        accToCart.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return accToCart.click();
    });

    ///////////////////////********************Remove the already added accessory from the cart *****************************////////////////////////
    this.defineStep(/^Remove the accessory$/, function () {
        return Smartcart.getAccessoriesEditButtonInCart().getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            getAccessoriesEditButtonInCart.click(); //click on the remove option

            return Smartcart.getAccessoriesDeleteButtonInEditModal().getLocation().then(function (ElementLocation) {
                var yAxisValue = ElementLocation.y - 250;
                browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
                getAccessoriesDeleteButtonInEditModal.click(); //click on the delete option

                return Smartcart.getAccessoriesDeleteButtonConfimationInEditModal().click(); //click on the delete confirmation option
            });
        });
    });

    /************************************************************************************************/
    /**************************************SMARTCART - TRADE-IN**************************************/
    /************************************************************************************************/

    this.defineStep(/^I Click on Trade in Button$/, function () {
        Smartcart.getTradeinCTAElement().getLocation().then(function (tradeInLocation) {
            var x = tradeInLocation.x;
            var y = tradeInLocation.y - 200;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
        });
        return Smartcart.getTradeinCTAElement().click();
    });

    this.defineStep(/^I should see TQT Modal$/, function () {
        return Smartcart.getTQTModalHeader().getText().then(function (tqtTitle) {
            expect(tqtTitle).to.not.be.null;
        });
    });

    this.defineStep(/^I am in TQT Modal$/, function () {
        return Smartcart.getTQTModalHeader().getText().then(function (tqtTitle) {
            expect(tqtTitle).to.not.be.null;
        });
    });

    this.defineStep(/^I see Carrier Field$/, function () {
        Smartcart.getCarrierTitle().getText().then(function (carrierHeader) {
            expect(carrierHeader).to.not.be.null;
        });
    });

    this.defineStep(/^I see Make Field$/, function () {
        Smartcart.getMakeTitle().getText().then(function (makeHeader) {
            expect(makeHeader).to.not.be.null;
        });
    });

    this.defineStep(/^I see Model Field$/, function () {
        Smartcart.getModelTitle().getText().then(function (modelHeader) {
            expect(modelHeader).to.not.be.null;
        });
    });

    this.defineStep(/^I see IMEI Field and Apply Button$/, function () {
        Smartcart.getImeiTitle().getText().then(function (imeiHeader) {
            expect(imeiHeader).to.not.be.null;
        });
    });

    this.defineStep(/^I see Device Condition Header and Device Condition options$/, function () {
        Smartcart.getDeviceConditionTitle().getText().then(function (deviceConditionHeader) {
            expect(deviceConditionHeader).to.not.be.null;
        });

        Smartcart.getDeviceConditionGoodTitle().getText().then(function (deviceConditionGoodHeader) {
            expect(deviceConditionGoodHeader).to.not.be.null;
        });

        Smartcart.getDeviceConditionDamageTitle().getText().then(function (deviceConditionDamageHeader) {
            expect(deviceConditionDamageHeader).to.not.be.null;
        });
    });

    this.defineStep(/^I see Get Estimate Button in disabled state$/, function () {
        Smartcart.getTradeinCTA().getText().then(function (tradeinCTAHeader) {
            expect(tradeinCTAHeader).to.not.be.null;
        });

        Smartcart.getTradeinCTA().isEnabled().then(function (flag) {
            expect(flag).to.equal(flag);
        });
    });

    this.defineStep(/^I see Skip Tradein Button in TQT Modal$/, function () {
        return Smartcart.getSkipTradeinCTA().getText().then(function (skipTradeinCTAHeader) {
            expect(skipTradeinCTAHeader).to.not.be.null;
        });
    });

    this.defineStep(/^I Select Carrier from dropdown$/, function () {
        Smartcart.getTradeinCTA().isEnabled().then(function (result) {
            if (!result) {
                return Smartcart.getCarrier().get(3).click();
            }
        });
    });

    this.defineStep(/^I Select Carrier as "([^"]*)"$/, function (carrierName) {
        browser.sleep(10000);
        Smartcart.getTradeinCTA().isEnabled().then(function (result) {
            if (!result) {
                var myCarrier = new SelectWrapper(by.name('carrier'));
                return myCarrier.selectByText(carrierName);
            }
        });
    });

    this.defineStep(/^I Select Make from dropdown$/, function () {
        browser.sleep(1000);
        Smartcart.getTradeinCTA().isEnabled().then(function (result) {
            if (!result) {
                checkRepeaterCount(Smartcart.getMake());
                return Smartcart.getMake().get(1).click();
            }
        });
    });

    this.defineStep(/^I Select Make as "([^"]*)"$/, function (makeName) {
        browser.sleep(1000);
        Smartcart.getTradeinCTA().isEnabled().then(function (result) {
            if (!result) {
                //checkRepeaterCount(Smartcart.getMake());
                // return Smartcart.getMake().get(1).click();
                var myMake = new SelectWrapper(by.name('maker'));
                return myMake.selectByText(makeName);
            }
        });
    });

    this.defineStep(/^I Select Model as "([^"]*)"$/, function (modelName) {
        browser.sleep(1000);
        tradeinModelName = modelName;
        Smartcart.getTradeinCTA().isEnabled().then(function (result) {
            if (!result) {
                /* checkRepeaterCount(Smartcart.getModel());
                 Smartcart.getModel().get(1).getText().then(function (modelName) {
                     tradeinModelName = modelName;
                 });
                 return Smartcart.getModel().get(1).click(); */
                var myModel = new SelectWrapper(by.name('deviceModel'));
                return myModel.selectByText(modelName);

            }
        });
    });

    this.defineStep(/^I enter "([^"]*)" in IMEI field and click on Apply button$/, function (imeiNumber) {
        browser.sleep(1000);
        Smartcart.getTradeinCTA().isEnabled().then(function (result) {
            expect(result).to.equal(false);
            Smartcart.imei().sendKeys(imeiNumber);
            tradeinIMEI = imeiNumber;
            return Smartcart.imeiApplyButton().click();
        });
    });

    this.defineStep(/^I select Device Condition as Good$/, function () {
        Smartcart.getTradeinCTA().isEnabled().then(function (result) {
            if (!result) {
                browser.wait(EC.visibilityOf(Smartcart.getGoodDeviceCondition()), 50000);
                Smartcart.getGoodDeviceCondition().getLocation().then(function (buttonLocation) {
                    var x = buttonLocation.x;
                    var y = buttonLocation.y - 200;
                    browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
                });
                browser.sleep(7000);
                return Smartcart.getGoodDeviceCondition().click();
            }
        });
    });

    this.defineStep(/^I select Device Condition as Damage$/, function () {
        Smartcart.getTradeinCTA().isEnabled().then(function (result) {
            if (!result) {
                browser.wait(EC.visibilityOf(Smartcart.getDamageDeviceCondition()), 50000);
                Smartcart.getDamageDeviceCondition().getLocation().then(function (Location) {
                    var x = Location.x;
                    var y = Location.y - 250;
                    browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
                });
                browser.sleep(5000);
                return Smartcart.getDamageDeviceCondition().click();
            }
        });
    });

    this.defineStep(/^I click on Get Estimate Button$/, function () {
        Smartcart.getTradeinCTA().isEnabled().then(function (result) {
            if (result) {
                Smartcart.getTradeinCTA().getLocation().then(function (Location) {
                    var x = Location.x;
                    var y = Location.y - 200;
                    browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
                });
                return Smartcart.getTradeinCTA().click();
            }
        });
    });

    this.defineStep(/^I should see Confirmation Modal$/, function () {
        browser.wait(EC.visibilityOf(Smartcart.getConfirmationModalHeader()), 50000);
        return Smartcart.getConfirmationModalHeader().getText().then(function (Header) {
            expect(Header).to.not.be.null;
        });
    });

    this.defineStep(/^I am in TQT Confirmation Modal$/, function () {
        return Smartcart.getConfirmationModalHeader().getText().then(function (Header) {
            expect(Header).to.not.be.null;
        });
    });

    this.defineStep(/^I see TQT Confirmation Modal Header$/, function () {
        Smartcart.getConfirmationModalHeader().getText().then(function (Header) {
            expect(Header).to.not.be.null;
        });
    });

    this.defineStep(/^I see TQT Confirmation Modal Subheader$/, function () {
        Smartcart.getConfirmationModalSubheader().getText().then(function (Subheader) {
            expect(Subheader).to.not.be.null;
        });
    });

    this.defineStep(/^I see Standard Tradein option with Estimated Tradein value and legal text$/, function () {
        Smartcart.standardFlow().getText().then(function (standardFlowTitle) {
            expect(standardFlowTitle).to.not.be.null;
        });
        Smartcart.standardTradeinEstimate().getText().then(function (value) {
            expect(value).to.not.be.null;
        });
        /*Smartcart.standardTradeinLegalText().getText().then(function(legalText){
                    expect(legalText).to.not.be.null;
        });	*/

        /* Smartcart.promotionFlow().getText().then(function (promotionFlowTitle) {
            expect(promotionFlowTitle).to.not.be.null;
        })*/
    });

    this.defineStep(/^I see Agree and Continue Button$/, function () {
        Smartcart.tradeinAcceptButton().getText().then(function (acceptButtonText) {
            expect(acceptButtonText).to.not.be.null;
        });
        return Smartcart.tradeinAcceptButton().isEnabled().then(function (flag) {
            expect(flag).to.equal(flag);
        });
    });

    this.defineStep(/^I see Skip Tradein Button in Confirmation Modal$/, function () {
        return Smartcart.tradeinDeclineButton().getText().then(function (declineButtonText) {
            expect(declineButtonText).to.not.be.null;
        });
    });

    this.defineStep(/^I select standard trade in option$/, function () {
        Smartcart.standardTradeinEstimate().getText().then(function (value) {
            standardTradeinValue = value;
        })
        return Smartcart.standardFlow().click();
    });

    this.defineStep(/^I select promotion trade in option$/, function () {
        /*Smartcart.promotionTradeinEstimate().getText().then(function (value) {
            promotionTradeinValue = value;*/
        browser.sleep(5000);
        browser.wait(EC.visibilityOf(Smartcart.promotionFlow()), 50000);
        return Smartcart.promotionFlow().click();
    });

    this.defineStep(/^I click on Agree & Continue Button in Promo Flow$/, function () {
        return Smartcart.tradeinAcceptButton().click();
    });

    this.defineStep(/^I should see Promo Notification banner$/, function () {
        return Smartcart.tradeinNotification().getText().then(function (notificationText) {
            expect(notificationText).to.not.be.null;
        });
    });

    this.defineStep(/^I accept trade in and returned to the cart$/, function () {
        return Smartcart.getCartTitleElement().getText().then(function (title) {
            expect(title).to.not.be.null;
        });
    });

    this.defineStep(/^I see Tradein Header on device tile$/, function () {
        browser.wait(EC.visibilityOf(Smartcart.getTradeinHeader()), 50000);
        Smartcart.getTradeinHeader().getLocation().then(function (Location) {
            var x = Location.x;
            var y = Location.y - 200;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
        });
        return Smartcart.getTradeinHeader().getText().then(function (tradeinheader) {
            expect(tradeinheader).to.not.be.null;
        });
    });

    this.defineStep(/^I see the Model Name of the traded device on line "([^"]*)" in Standard Flow$/, function (linenum) {
        var lineItem = Smartcart.getCartLineItem(linenum);
        Smartcart.getaddedTradeinModelName(lineItem).getLocation().then(function (Location) {
            var x = Location.x;
            var y = Location.y - 200;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
        });
        return Smartcart.getaddedTradeinModelName(lineItem).getText().then(function (modelName) {
            expect(modelName).to.equal(tradeinModelName);
        });
    });

    this.defineStep(/^I see the Model Name of the traded device on line "([^"]*)" in Promo Flow$/, function (linenum) {
        var lineItem = Smartcart.getCartLineItem(linenum);
        Smartcart.getaddedPromoTradeinModelName(lineItem).getLocation().then(function (Location) {
            var x = Location.x;
            var y = Location.y - 200;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
        });
        return Smartcart.getaddedPromoTradeinModelName(lineItem).getText().then(function (modelName) {
            expect(modelName).to.equal(tradeinModelName);
        });
    });


    this.defineStep(/^I see the IMEI Number of the traded device on line "([^"]*)" in Standard Flow$/, function (linenum) {
        var lineItem = Smartcart.getCartLineItem(linenum);
        Smartcart.getaddedTradeinIMEINumber(lineItem).getLocation().then(function (Location) {
            var x = Location.x;
            var y = Location.y - 200;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
        });
        return Smartcart.getaddedTradeinIMEINumber(lineItem).getText().then(function (cartIMEIString) {
            expect(cartIMEIString).to.include(tradeinIMEI);
        });
    });

    this.defineStep(/^I see the IMEI Number of the traded device on line "([^"]*)" in Promo Flow$/, function (linenum) {
        var lineItem = Smartcart.getCartLineItem(linenum);
        Smartcart.getaddedPromoTradeinIMEINumber(lineItem).getLocation().then(function (Location) {
            var x = Location.x;
            var y = Location.y - 200;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
        });
        return Smartcart.getaddedPromoTradeinIMEINumber(lineItem).getText().then(function (cartIMEIString) {
            expect(cartIMEIString).to.include(tradeinIMEI);
        });
    });

    this.defineStep(/^I see the Trade in value of the traded device on line "([^"]*)" in Standard Flow$/, function (linenum) {
        var lineItem = Smartcart.getCartLineItem(linenum);
        Smartcart.getaddedTradeinValue(lineItem).getLocation().then(function (Location) {
            var x = Location.x;
            var y = Location.y - 200;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
        });
        return Smartcart.getaddedTradeinValue(lineItem).getText().then(function (cartTradeinValueString) {
            expect(cartTradeinValueString).to.include(standardTradeinValue);
        });
    });

    this.defineStep(/^I see the Trade in value of the traded device on line "([^"]*)" in Promo Flow$/, function (linenum) {
        var lineItem = Smartcart.getCartLineItem(linenum);
        Smartcart.getaddedPromoTradeinValue(lineItem).getLocation().then(function (Location) {
            var x = Location.x;
            var y = Location.y - 200;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
        });
        return Smartcart.getaddedPromoTradeinValue(lineItem).getText().then(function (cartTradeinValueString) {
            expect(cartTradeinValueString).to.not.be.null;
        });
    });

    this.defineStep(/^I see trade in value link on line "([^"]*)"$/, function (linenum) {
        var lineItem = Smartcart.getCartLineItem(linenum);
        return Smartcart.gettradeinValueLink(lineItem).getText().then(function (tradeinlink) {
            tradeinValueLink = tradeinlink;
        });
    });

    this.defineStep(/^I see trade in value link on line "([^"]*)" in Promo Flow$/, function (linenum) {
        var lineItem = Smartcart.getCartLineItem(linenum);
        return Smartcart.getPromotradeinValueLink(lineItem).getText().then(function (tradeinlink) {
            tradeinValueLink = tradeinlink;
        });
    });

    this.defineStep(/^I see Remove link to remove tradein on line "([^"]*)"$/, function (linenum) {
        var lineItem = Smartcart.getCartLineItem(linenum);
        return Smartcart.getTradeinRemoveLink(lineItem).getText().then(function (tradeinRemoveLink) {
            expect(tradeinRemoveLink).to.not.be.null;
        });
    });

    this.defineStep(/^I click on Duplicate link on line "([^"]*)" which has tradein information$/, function (linenum) {
        var lineItem = Smartcart.getCartLineItem(linenum);
        return Smartcart.getDuplicateIcon(lineItem).click();
    });

    this.defineStep(/^I see tradein button is displayed on device tile$/, function () {
        Smartcart.getTradeinCTAElement().getText().then(function (ctaText) {
            expect(ctaText).to.not.be.null;
        });
    });

    this.defineStep(/^I Duplicate the device on line "([^"]*)"$/, function (linenum) {
        browser.sleep(5000);
        var lineItem = Smartcart.getCartLineItem(linenum);
        return browser.wait(EC.visibilityOf(Smartcart.getDuplicateIcon(lineItem)), 50000).then(function () {
            Smartcart.getDuplicateIcon(lineItem).getLocation().then(function (buttonLocation) {
                var x = buttonLocation.x;
                var y = buttonLocation.y - 200;
                browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
            });
            Smartcart.getDuplicateIcon(lineItem).click();
        });
    });

    this.defineStep(/^I Add "([^"]*)" Tablet to the cart in second Line$/, function (deviceName) {
        Smartcart.addATabletOrWerableButton().getLocation().then(function (addTabletButtonLocation) {
            var x = addTabletButtonLocation.x;
            var y = addTabletButtonLocation.y - 200;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
        });
        Smartcart.addATabletOrWerableButton().click();
        Smartcart.getAddDeviceIconElement().get(1).getLocation().then(function (AddDeviceIconLocation) {
            var x = AddDeviceIconLocation.x;
            var y = AddDeviceIconLocation.y - 200;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
        });
        Smartcart.getAddDeviceIconElement().get(1).click();
        Smartcart.getAddNewPhoneLabelElement().click();
        Smartcart.getMiniBrowseTitleElement().getText().then(function (miniBrowseTitle) {
            expect(miniBrowseTitle).to.not.be.null;
        });
        var minibrowseDevice = element(by.cssContainingText('.selector-device-name', deviceName)).click();
        Smartcart.getAddToCartButtonMiniPDP().click();
    });

    this.defineStep(/^I click on Tradein Button in Device Tile$/, function () {
        Smartcart.getTradeinCTAElement().getLocation().then(function (tradeInLocation) {
            var x = tradeInLocation.x;
            var y = tradeInLocation.y - 200;
            browser.executeScript('window.scrollTo(' + x + ',' + y + ')');
        });
        return Smartcart.getTradeinCTAElement().click();
    });

    this.defineStep(/^I should see Tradein Quote Tool Modal$/, function () {
        return Smartcart.getTQTModalHeader().getText().then(function (tqtTitle) {
            expect(tqtTitle).to.not.be.null;
        });
    });

    this.defineStep(/^I enter required device details and select device device condition as good and Click Get Estimate Button$/, function () {
        Smartcart.getTradeinCTA().isEnabled().then(function (result) {
            if (!result) {
                Smartcart.getCarrier().get(16).click();
            }
        });

        browser.sleep(1000);

        Smartcart.getTradeinCTA().isEnabled().then(function (result) {
            if (!result) {
                checkRepeaterCount(Smartcart.getMake());
                Smartcart.getMake().get(1).click();
            }
        });

        browser.sleep(1000);

        Smartcart.getTradeinCTA().isEnabled().then(function (result) {
            if (!result) {
                checkRepeaterCount(Smartcart.getModel());
                Smartcart.getModel().get(1).click();
            }
        });

        Smartcart.getTradeinCTA().isEnabled().then(function (result) {
            expect(result).to.equal(false);
            Smartcart.imei().sendKeys('357759080728132');
            Smartcart.imeiApplyButton().click();
        });

        Smartcart.getTradeinCTA().isEnabled().then(function (result) {
            if (!result) {
                Smartcart.getDeviceCondition().click();
            }
        });

        Smartcart.getTradeinCTA().isEnabled().then(function (result) {
            if (result) {
                return Smartcart.getTradeinCTA().click();
            }
        });
    });

    this.defineStep(/^I should see Confirmation Flow modal$/, function () {
        return Smartcart.getConfirmationModalHeader().getText().then(function (Header) {
            expect(Header).to.not.be.null;
        });
    });

    this.defineStep(/^I Select Standard Trade in Option$/, function () {
        return Smartcart.standardFlow().click();
        /*Smartcart.getTradeinOptions().count().then(function (count) {
            if (expect(count).to.equal(1)) {
                return Smartcart.standardFlow().click();
            }
        });*/
    });

    this.defineStep(/^I click on Agree & Continue Button in Standard Flow$/, function () {
        return Smartcart.tradeinAcceptButton().click();
    });

    this.defineStep(/^I should see Standard Notification Banner$/, function () {
        browser.wait(EC.visibilityOf(Smartcart.tradeinNotification()), 50000);
        return Smartcart.tradeinNotification().getText().then(function (notificationText) {
            expect(notificationText).to.not.be.null;
        });
    });

    this.defineStep(/^I should see Promo Notification banner$/, function () {
        browser.wait(EC.visibilityOf(Smartcart.tradeinNotification()), 50000);
        return Smartcart.tradeinNotification().getText().then(function (notificationText) {
            expect(notificationText).to.not.be.null;
        });
    });

    this.defineStep(/^I should see Skip Tradein Notification Banner$/, function () {
        browser.wait(EC.visibilityOf(Smartcart.tradeinNotification()), 50000);
        return Smartcart.tradeinNotification().getText().then(function (notificationText) {
            expect(notificationText).to.not.be.null;
        });
    });

    this.defineStep(/^I should see Remove Tradein Notification Banner$/, function () {
        return Smartcart.tradeinNotification().getText().then(function (notificationText) {
            expect(notificationText).to.not.be.null;
        });
    });


    this.defineStep(/^I click on Skip Tradein Button in TQT Modal$/, function () {
        return Smartcart.getSkipTradeinCTA().click();
    });

    this.defineStep(/^I click on Skip Tradein Button in Standard Flow$/, function () {
        return Smartcart.tradeinDeclineButton().click();
    });

    this.defineStep(/^I click on Remove Tradein link for removing tradein on line "([^"]*)"$/, function (linenum) {
        var lineItem = Smartcart.getCartLineItem(linenum);
        return Smartcart.getTradeinRemoveLink(lineItem).click();
    });

    this.defineStep(/^I see tradein information is removed on line "([^"]*)"$/, function () {
    });

    /**********************************Remove Line***************************************************************************************/

    this.defineStep(/^I click on Remove link for removing line on line "([^"]*)"$/, function (linenum) {
        cartLineCount = Smartcart.getLineNumber().count();
        var lineItem = Smartcart.getCartLineItem(linenum);
        return Smartcart.getRemoveIcon(lineItem).click();
    });

    this.defineStep(/^I see the lines are removed from the cart$/, function () {
        cartLineCount = Smartcart.getLineNumber().count();
    });

    /********************************PRICE BREAKDOWN************************************************************************************/

    this.defineStep(/^I click on PriceBreakdown link on Sticky Banner$/, function () {
        return Smartcart.getPriceBreakDownLink().click();
    });

    this.defineStep(/^I should see PriceBreakdown Modal$/, function () {
        return Smartcart.getPriceBreakDownModalHeader().getText().then(function (PricebreakdownHeader) {
            expect(PricebreakdownHeader).to.not.be.null;
        });
    });

    this.defineStep(/^I am in PriceBreakdown Modal$/, function () {
        return Smartcart.getPriceBreakDownModalHeader().getText().then(function (PricebreakdownHeader) {
            expect(PricebreakdownHeader).to.not.be.null;
        });
    });

    this.defineStep(/^I see my added devices in the modal$/, function () {
        return Smartcart.getPriceBreakDownDeviceName().getText().then(function (priceBreakdownDeviceName) {
            expect(priceBreakdownDeviceName).to.equal(phoneNameLine1);
        });
    });

    this.defineStep(/^I see my trade in device value$/, function () {
        return Smartcart.getPriceBreakDownTradeinValue().getText().then(function (tradeinValue) {
            expect(tradeinValue).to.include(standardTradeinValue);
        });
    });

    this.defineStep(/^I see my promo trade in device value$/, function () {
        return Smartcart.getPriceBreakDownPromoTradeinValue().getText().then(function (tradeinValue) {
            expect(tradeinValue).to.not.be.null;
        });
    });

    this.defineStep(/^I see my standard trade in device model name$/, function () {
        return Smartcart.getPriceBreakDownTradeinDeviceModalName().getText().then(function (modelname) {
            expect(modelname).to.equal(tradeinModelName);
        });
    });

    this.defineStep(/^I see my promo trade in device model name$/, function () {
        return Smartcart.getPriceBreakDownPromoTradeinDeviceModalName().getText().then(function (modelname) {
            expect(modelname).to.equal(tradeinModelName);
        });
    });

    this.defineStep(/^I see my trade in device header and legal text$/, function () {
        Smartcart.getPriceBreakDownTradeinHeader().getText().then(function (header) {
            expect(header).to.not.be.null;
        });
        return Smartcart.getPriceBreakdownTradeinLegaltext().getText().then(function (legalText) {
            expect(legalText).to.not.be.null;
        });
    });

    this.defineStep(/^I see trade in value link in the modal$/, function () {
        return Smartcart.getPricebreakDownTradeinValueLink().getText().then(function (tradeinmodalvaluelink) {
            expect(tradeinmodalvaluelink).to.equal(tradeinValueLink);
        });
    });

    this.defineStep(/^I close the PriceBreakdown Modal$/, function () {
        return Smartcart.getPriceBreakDownModalCloseIcon().click();
    });

    /************************************************************************************************/
    /***********************************CHECKOUT - INFO & SHIPPING***********************************/
    /************************************************************************************************/

    this.defineStep(/^the browser will navigate to the checkout section$/, function () {
        browser.sleep(2000);
        return browser.wait(EC.visibilityOf(personalinfo.getPersonalInfoHeader()), 30000).then(function (isVisible) {
            if (isVisible) {
                personalinfo.getPersonalInfoHeader().getText().then(function (personalinfoTitle) {
                    expect(personalinfoTitle).to.equal('Personal Info');
                });
            }
        });
    });

    this.defineStep(/^I see the Personal Information section$/, function () {
        return personalinfo.getPersonalInfoHeader().getText().then(function (personalinfoTitle) {
            expect(personalinfoTitle).to.equal('Personal Info');
        });
    });

    this.defineStep(/^I click on viewOrderDetailsLink$/, function () {
        var EC = protractor.ExpectedConditions;
        browser.wait(EC.invisibilityOf(element(by.css('.acsModalBackdrop'))), 10000);
        Common.viewOrderCheckout().getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        Common.viewOrderCheckout().click();
        return browser.sleep(10000);
    });

    this.defineStep(/^I should see Order Details modal$/, function () {
        return Common.getViewOrderDetailsTitleElement().getText().then(function (viewOrderCheckoutTitle) {
            expect(viewOrderCheckoutTitle).to.equal('1');
        });
    });

    this.defineStep(/^I close the Order Details modal$/, function () {
        return element(by.css('button[ng-click="priceBreakdownCtrl.close()"]')).click();
    });

    this.defineStep(/^I see the Shipping section$/, function () {
        browser.waitForAngular();
        browser.driver.sleep(1000);
        return shipping.getShippingAddressLine1Label().getText().then(function (shippingAddress) {
            expect(shippingAddress).to.equal('Shipping Address Line 1');
        });
    });

    this.defineStep(/^I enter "([^"]*)" in the first name$/, function (name) {
        personalinfo.getFirstName().sendKeys(name);
        firstName = name;
        return personalinfo.getFirstName().getText().then(function (name) { });
    });

    this.defineStep(/^I enter "([^"]*)" in the middle initial$/, function (task) {
        return personalinfo.getMiddleName().sendKeys(task);
    });

    this.defineStep(/^I enter "([^"]*)" in the last name$/, function (task) {
        return personalinfo.getLastName().sendKeys(task);
    });

    this.defineStep(/^I enter "([^"]*)" in the email address$/, function (task) {
        return personalinfo.getEmailAddress().sendKeys(task);
    });

    this.defineStep(/^I enter "([^"]*)" in the phone number$/, function (phone) {
        return updateSendKeys(personalinfo.getPhoneNumber(), phone);
    });

    this.defineStep(/^I select Overnight shipping option$/, function () {
        shipping.getOvernightShippingOption().getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 400;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return shipping.getOvernightShippingOption().click();
    });

    this.defineStep(/^I select Saturday shipping option$/, function () {
        return shipping.getSaturdayShippingOption().click();
    });

    this.defineStep(/^I enter "([^"]*)" in the shipping address$/, function (task) {
        return shipping.getShippingAddressLine1().sendKeys(task);
    });

    this.defineStep(/^I enter "([^"]*)" in the city$/, function (task) {
        return shipping.getShippingCity().sendKeys(task);
    });

    this.defineStep(/^I select "([^"]*)" in the State$/, function (task) {
        return shipping.selectShippingStateInput().sendKeys(task);
    });

    this.defineStep(/^I enter "([^"]*)" in the zip$/, function (task) {
        return shipping.getShippingZip().sendKeys(task);
    });

    this.defineStep(/^I click on billing checkboxindicator$/, function () {
        shipping.getselectcheckboxe().getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return shipping.getselectcheckboxe().click();
    });

    this.defineStep(/^I click on user-consent checkbox$/, function () {
        browser.sleep(2000);
        return personalinfo.getConsentCheckbox().getText().then(function (consentText) {
            browser.executeScript("arguments[0].scrollIntoView(false);", personalinfo.getConsentCheckbox().getWebElement());
            personalinfo.getConsentCheckbox().click();
        })
    });

    this.defineStep(/^I click on 'Next' button$/, function () {
        browser.sleep(2000);
        return personalinfo.getPersonaInfoNextButton().getText().then(function (buttonText) {
            browser.executeScript("arguments[0].scrollIntoView(false);", personalinfo.getPersonaInfoNextButton().getWebElement());
            personalinfo.getPersonaInfoNextButton().click();
        })
    });

    this.defineStep(/^the browser will navigate to the Payment Information page$/, function () {
        browser.sleep(2000);
        return browser.wait(EC.visibilityOf(shipping.getPaymentHeader()), 30000).then(function (isVisible) {
            if (isVisible) {
                shipping.getPaymentHeader().getText().then(function (paymentTitle) {
                    expect(paymentTitle).to.equal('Payment');
                });
            }
        });
    });

    /************************************************************************************************/
    /***********************************CHECKOUT - PAYMENT & CREDIT**********************************/
    /************************************************************************************************/

    this.defineStep(/^I am on the Payment and Credit Page$/, function () {
        return shipping.getPaymentHeader().getText().then(function (paymentTitle) {
            expect(paymentTitle).to.equal('Payment');
        });
    });

    this.defineStep(/^I clear credit card number$/, function () {
        return shipping.getCreditCardNumberInput().clear();
    });

    this.defineStep(/^I enter "([^"]*)" in the credit card$/, function (task) {
        return shipping.getCreditCardNumberInput().sendKeys(task);
    });

    this.defineStep(/^I clear credit card expiry date$/, function () {
        return shipping.getCardExpiryDateInput().clear();
    });

    this.defineStep(/^I enter "([^"]*)" in the Expiry date$/, function (task) {
        return shipping.getCardExpiryDateInput().sendKeys(task);
    });

    this.defineStep(/^I clear CVV$/, function () {
        return shipping.getCVVInput().clear();
    });

    this.defineStep(/^I enter "([^"]*)" in the CVV$/, function (task) {
        return shipping.getCVVInput().sendKeys(task);
    });

    this.defineStep(/^I see the Credit check section$/, function () {
        return shipping.getCreditHeader().getText().then(function (creditTitle) {
            expect(creditTitle).to.be.not.null;
        });
    });

    this.defineStep(/^I select "([^"]*)" in the id$/, function (task) {
        return credit.getIdTypeDropdown().sendKeys(task);
    });

    this.defineStep(/^I enter "([^"]*)" in the id$/, function (task) {
        return credit.getIdNumberInput().sendKeys(task);
    });

    this.defineStep(/^I enter "([^"]*)" in the expiry date$/, function (task) {
        return credit.getExpirationDateField().sendKeys(task);
    });

    this.defineStep(/^I enter "([^"]*)" in the ssn$/, function (task) {
        return credit.getSocialSecurityField().sendKeys(task);
    });

    this.defineStep(/^I enter "([^"]*)" in the dob$/, function (dob) {
        return updateSendKeys(credit.getDateOfBirthField(), dob);
    });

    this.defineStep(/^I click on the 'Agree and Next' button$/, function () {
        browser.sleep(2000);
        return shipping.getPaymentCreditNextButton().getText().then(function (buttonText) {
            browser.executeScript("arguments[0].scrollIntoView(false);", shipping.getPaymentCreditNextButton().getWebElement());
            shipping.getPaymentCreditNextButton().click();
        })
    });

    this.defineStep(/^I click on the 'Continue' button$/, function () {
        browser.sleep(2000);
        return shipping.getPaymentCreditNextButton().getText().then(function (buttonText) {
            browser.executeScript("arguments[0].scrollIntoView(false);", shipping.getPaymentCreditNextButton().getWebElement());
            shipping.getPaymentCreditNextButton().click();
        })
    });

    this.defineStep(/^the browser will navigate to Review order page$/, function () {
        return browser.wait(EC.visibilityOf(review.getReviewOrderTitle()), 30000).then(function (isVisible) {
            if (isVisible) {
                review.getReviewOrderTitle().getText().then(function (reviewTitle) {
                    expect(reviewTitle).to.equal('Review order');
                });
            }
        });
    });

    /************************************************************************************************/
    /*************************************CHECKOUT - REVIEW ORDER************************************/
    /************************************************************************************************/

    this.defineStep(/^I am on review order page$/, function () {
        return review.getReviewOrderTitle().getText().then(function (reviewTitle) {
            expect(reviewTitle).to.equal('Review order');
            browser.sleep(2000);
        });
    });

    this.defineStep(/^I should see appropriate details of 2 Phones and 2 Tablets$/, function () {
        return review.getReviewOrderDetails().getText().then(function (reviewDetails) {
            expect(reviewDetails).to.include(phoneNameLine1 && tabletNameLine1 && phoneNameLine2 && tabletNameLine2);
        });
    });

    this.defineStep(/^I should see appropriate device details$/, function () {
        browser.wait(EC.visibilityOf(review.deviceFamilyName()), 30000);
        return review.deviceFamilyName().getText().then(function (lineName) {
            expect(phoneNameLine1).to.include(lineName);
        });
    });

    this.defineStep(/^I should see SIM kit details in review order page$/, function () {
        browser.wait(EC.visibilityOf(review.getsimKitName().get(0)), 30000);
        return review.getsimKitName().get(0).getText().then(function (lineName) {
            expect(lineName).to.include(phoneNameLine1);
        });
    });

    this.defineStep(/^I should see appropriate SIM kit details in review order page$/, function () {
        browser.wait(EC.visibilityOf(review.getsimKitName().get(0)), 30000);
        return review.getsimKitName().get(0).getText().then(function (lineName) {
            expect(lineName).to.include(SIMkitName);
        });
    });

    this.defineStep(/^I should see appropriate accessory details$/, function () {
        return review.accessoryName().getText().then(function (lineName) {
            expect(lineName).to.equal(ufAccessoryName);
        });
    });

    this.defineStep(/^I should see appropriate accessory details added to Cart$/, function () {
        return review.accessoryName().getText().then(function (lineName) {
            expect(lineName).to.equal(accessoryName);
        });
    });

    this.defineStep(/^I see my traded\-in device model$/, function () {
        return review.getTradeInDeviceModelName().getText().then(function (modelname) {
            expect(modelname).to.equal(tradeinModelName);
        });
    });

    this.defineStep(/^I see the traded\-in device value$/, function () {
        return review.getDeviceTradeInValue().getText().then(function (tradeinValue) {
            expect(tradeinValue).to.include(standardTradeinValue);
        });
    });

    this.defineStep(/^I see the trade in value link in order review page$/, function () {
        return review.getReviewTradeinValueLink().getText().then(function (reviewTradeinValueLink) {
            expect(reviewTradeinValueLink).to.not.be.null;
        });
    });

    this.defineStep(/^I see the trade\-in Terms & Conditions text$/, function () {
        return review.getTradeInTCText().getText().then(function (tradeInTCText) {
            expect(tradeInTCText).to.not.be.null;
        });
    });

    this.defineStep(/^I click on 'Terms and Conditions' link in trade\-in Terms & Conditions text$/, function () {
        browser.sleep(10000);
        review.getLinkIntradeInTCText().getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
        });
        return review.getLinkIntradeInTCText().click();
    });

    this.defineStep(/^it should open 'DEVICE RECOVERY PROGRAM Terms and Conditions' modal$/, function () {
        return review.getTradeInTCModalContent().getText().then(function (modalContent) {
            expect(modalContent).to.not.be.null;
        });
    });

    this.defineStep(/^I close the 'DEVICE RECOVERY PROGRAM Terms and Conditions' modal$/, function () {
        return review.getTradeInTCModalClose().click();
    });

    this.defineStep(/^trade\-in value should be removed for first device$/, function () {

    });

    this.defineStep(/^trade\-in value should be removed for second device$/, function () {

    });

    this.defineStep(/^trade\-in Terms & Conditions text should not be displayed$/, function () {

    });

    /************************************************************************************************/
    /************************************CHECKOUT - AGREE & SUBMIT***********************************/
    /************************************************************************************************/

    this.defineStep(/^I click on 'I Agree Disclaimer' Label for Terms and Conditions$/, function () {
        browser.sleep(2000);
        browser.executeScript("arguments[0].scrollIntoView(false);", review.getTCDisclaimer().getWebElement());
        return review.getTCDisclaimer().click();
    });

    this.defineStep(/^I click on 'Submit Order' Button$/, function () {
        return review.getReviewAgreeSubmitButton().getText().then(function (buttonData) {
            browser.sleep(2000);
            browser.executeScript("arguments[0].scrollIntoView(false);", review.getReviewAgreeSubmitButton().getWebElement());
            browser.sleep(1000);
            review.getReviewAgreeSubmitButton().getLocation().then(function (ElementLocation) {
                var yAxisValue = ElementLocation.y - 300;
                browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
                review.getReviewAgreeSubmitButton().click();
            });
        });
    });

    this.defineStep(/^I should see the Authorization fail modal$/, function () {
        return confirmationPage.getAuthFailButton1().getText().then(function (authFailGotIt) {
            expect(authFailGotIt).to.equal('Got it');
        });
    });

    this.defineStep(/^I am on Authorization fail modal$/, function () {
        return confirmationPage.getAuthFailButton1().getText().then(function (authFailGotIt) {
            expect(authFailGotIt).to.equal('Got it');
        });
    });

    this.defineStep(/^I click on 'Got it' button$/, function () {
        return confirmationPage.getAuthFailButton1().click();
    });

    this.defineStep(/^I click on 'Cancel and return to Cart' button$/, function () {
        return confirmationPage.getAuthFailButton2().click();
    });

    /**************************************************************************************************/
    /****************************************CONFIRMATION PAGE*****************************************/
    /**************************************************************************************************/

    this.defineStep(/^I should see the order confirmation page$/, function () {
        //browser.wait(EC.visibilityOf(confirmationPage.getConfirmationUserName()), 50000);
        return confirmationPage.getConfirmationUserName().getText().then(function (confirmationName) {
            expect(confirmationName).to.include(firstName);
        });
        browser.sleep(3000);
    });

    this.defineStep(/^I should see the hard stop screen$/, function () {
        return confirmationPage.getFraudCheckMessage().getText().then(function (fraudFailScreen) {
            expect(fraudFailScreen).to.not.be.null
        });
        browser.sleep(5000);
    });

    //****************************************************************************

    this.defineStep(/^I click on No Credit$/, function () {
        var checkNoCredit = element.all(by.repeater('creditClass in vm.model.creditClassData')).get(2); //locater to select the credit class
        checkNoCredit.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            checkNoCredit.click(); //click on the credit class
        });
    });

    this.defineStep(/^No Credit radio button should be selected $/, function () {
        var Nocredit = element.all(by.repeater('creditClass in vm.model.creditClassData')).get(2).element(by.css('.creditClassText')); //locater to get the text of the credit class
        Nocredit.getText().then(function (nocredittext) {
            console.log("you have selected", Nocredittext); // rpint the text of the credit class selected
            return browser.sleep(8000);
        });
    });

    this.defineStep(/^I am on phones browse page and select the device$/, function () {
        var productclick = element.all(by.binding('vm.product.productName')).get(2); // locater to select the product from the browse page
        productclick.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            productclick.getText().then(function (productname) {
                console.log("selected product name is", productname);
                productclick.click(); //click on the product selected from the browse page
                return browser.sleep(8000);
            });
        });
    });

    this.defineStep(/^Add a phone to the cart$/, function () {
        var addtocart = element(by.id('add-to-bag-btn')); //locater for add to cart CTA
        addtocart.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            addtocart.click(); // click on the add to cart CTA
        });
    });

    //Remove already added Tablet form cart
    this.defineStep(/^I remove the Tablet from cart$/, function () {
        var removephone = element(by.id('cart_remove_line_0')); //locater for removing 1st Tile
        removephone.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            removephone.click(); //click on the checkout page CTA
        });
    });

    //Remove already added Phone from cart
    this.defineStep(/^I remove the phone from cart$/, function () {
        var removetablet = element(by.id('cart_remove_line_1')); //locater for removing 2nd Tile
        removetablet.getLocation().then(function (ElementLocation) {
            var yAxisValue = ElementLocation.y - 250;
            browser.executeScript('window.scrollTo(' + ElementLocation.x + ',' + yAxisValue + ');');
            removetablet.click(); //click on the checkout page CTA
        });
    });

    /***********************************Marketing Consumer UNAV Header on Browse and PDP pages**********************************/

    this.defineStep(/^I should see tmobile logo link$/, function () {
        browser.manage().window().maximize();
        var checkTMOLogo = element(by.css('[data-analytics-id="123456798763663663663-logo"]'));
        return checkTMOLogo.getText().then(function (logo) {
            console.log(logo);
            expect(logo).to.not.be.null;
        });
    });

    this.defineStep(/^I should see deals link$/, function () {
        browser.manage().window().maximize();
        var checkdeals = element.all(by.repeater('link in vm.universalNavService.model.unavData.header.desktopLinks')).get(0).element(by.tagName('a'));
        return checkdeals.getText().then(function (deals) {
            console.log(deals);
            expect(deals).to.not.be.null;
        });
    });

    this.defineStep(/^I should see phones link$/, function () {
        browser.manage().window().maximize();
        var checkphones = element.all(by.repeater('link in vm.universalNavService.model.unavData.header.desktopLinks')).get(1).element(by.tagName('a'));
        return checkphones.getText().then(function (phones) {
            console.log(phones);
            expect(phones).to.not.be.null;
        });
    });

    this.defineStep(/^I should see plans link$/, function () {
        browser.manage().window().maximize();
        var checkplans = element.all(by.repeater('link in vm.universalNavService.model.unavData.header.desktopLinks')).get(2).element(by.tagName('a'));
        return checkplans.getText().then(function (plans) {
            console.log(plans);
            expect(plans).to.not.be.null;
        });
    });

    this.defineStep(/^I should see mytmobile link$/, function () {
        browser.manage().window().maximize();
        var checkmy = element.all(by.repeater('link in vm.universalNavService.model.unavData.header.desktopLinks')).get(3).element(by.tagName('a'));
        return checkmy.getText().then(function (mytmobile) {
            console.log(mytmobile);
            expect(mytmobile).to.not.be.null;
        });
    });

    this.defineStep(/^I should see cart icon$/, function () {
        var checkCart = element.all(by.css('[ng-if="!vm.universalNavService.model.isB2b"]')).get(0);
        return checkCart.getText().then(function (cart) {
            console.log(cart);
            expect(cart).to.not.be.null;
        });
    });

    this.defineStep(/^I should see search icon$/, function () {
        var checkSearch = element.all(by.css('[data-analytics-id="123456798763663663663-searchIcon"]')).get(0);
        return checkSearch.getText().then(function (search) {
            console.log(search);
            expect(search).to.not.be.null;
        });
    });

    this.defineStep(/^I should see business link$/, function () {
        var checkbusiness = element.all(by.css('[ng-bind-html="leftLink.text | parseHtml"]')).get(0);
        return checkbusiness.getText().then(function (business) {
            console.log(business);
            expect(business).to.not.be.null;
        });
    });

    this.defineStep(/^I should see espanol link$/, function () {
        var checkespanol = element.all(by.css('[ng-bind-html="leftLink.text | parseHtml"]')).get(1);
        return checkespanol.getText().then(function (espanol) {
            console.log(espanol);
            expect(espanol).to.not.be.null;
        });
    });

    this.defineStep(/^I should see store locator$/, function () {
        var checkStoreLocator = element(by.css('[data-analytics-id="123456798763663663663-storeLocator"]'));
        return checkStoreLocator.getText().then(function (storelocator) {
            console.log(storelocator);
            expect(storelocator).to.not.be.null;
        });
    });

    this.defineStep(/^I should see lets talk option$/, function () {
        var checkLetsTalk = element(by.css('[data-analytics-id="123456798763663663663-utility-phone-link1"]'));
        return checkLetsTalk.getText().then(function (letstalk) {
            console.log(letstalk);
            expect(letstalk).to.not.be.null;
        });
    });

    this.defineStep(/^click on hamburger menu$/, function () {
        var clickHamBurger = element.all(by.css('[class="hamburger"]'));
        clickHamBurger.click();
        return browser.driver.sleep(6000);
    });

    this.defineStep(/^I should see mytmobile link under hamburger$/, function () {
        var checkmyTMO = element(by.css('[data-analytics-id*="link1-side"]'));
        return checkmyTMO.getText().then(function (mytmobile) {
            console.log(mytmobile);
            expect(mytmobile).to.not.be.null;
        });
    });

    this.defineStep(/^I should see plans link under hamburger$/, function () {
        var checkplans = element(by.css('[data-analytics-id*="link2-side"]'));
        return checkplans.getText().then(function (plans) {
            console.log(plans);
            expect(plans).to.not.be.null;
        });
    });

    this.defineStep(/^I should see tmobile one link under hamburger$/, function () {
        var checkTMOOne = element(by.css('[data-analytics-id*="link2-side-sublink1"]'));
        return checkTMOOne.getText().then(function (TMOOne) {
            console.log(TMOOne);
            expect(TMOOne).to.not.be.null;
        });
    });

    this.defineStep(/^I should see prepaid plans link under hamburger$/, function () {
        var checkprepaidPlans = element(by.css('[data-analytics-id*="link2-side-sublink2"]'));
        return checkprepaidPlans.getText().then(function (prepaidPlans) {
            console.log(prepaidPlans);
            expect(prepaidPlans).to.not.be.null;
        });
    });

    this.defineStep(/^I should see phones link under hamburger$/, function () {
        var checkPhones = element(by.css('[data-analytics-id*="link3-side"]'));
        return checkPhones.getText().then(function (phones) {
            console.log(phones);
            expect(phones).to.not.be.null;
        });
    });

    this.defineStep(/^I should see tablets and devices link under hamburger$/, function () {
        var checkTabDev = element(by.css('[data-analytics-id*="link3-side-sublink1"]'));
        return checkTabDev.getText().then(function (TabDev) {
            console.log(TabDev);
            expect(TabDev).to.not.be.null;
        });
    });

    this.defineStep(/^I should see mobile hotspots link under hamburger$/, function () {
        var checkHotspot = element(by.css('[data-analytics-id*="link3-side-sublink2"]'));
        return checkHotspot.getText().then(function (hotspot) {
            console.log(hotspot);
            expect(hotspot).to.not.be.null;
        });
    });

    this.defineStep(/^I should see wearable tech link under hamburger$/, function () {
        var checkWearable = element(by.css('[data-analytics-id*="link3-side-sublink3"]'));
        return checkWearable.getText().then(function (wearable) {
            console.log(wearable);
            expect(wearable).to.not.be.null;
        });
    });

    this.defineStep(/^I should see accessories link under hamburger$/, function () {
        var checkAccessories = element(by.css('[data-analytics-id*="link3-side-sublink4"]'));
        return checkAccessories.getText().then(function (accessories) {
            console.log(accessories);
            expect(accessories).to.not.be.null;
        });
    });

    this.defineStep(/^I should see bring your own phone link under hamburger$/, function () {
        var checkBYOP = element(by.css('[data-analytics-id*="link3-side-sublink5"]'));
        return checkBYOP.getText().then(function (BYOP) {
            console.log(BYOP);
            expect(BYOP).to.not.be.null;
        });
    });

    this.defineStep(/^I should see prepaid phones link under hamburger$/, function () {
        var checkPrepaidPhones = element(by.css('[data-analytics-id*="link3-side-sublink6"]'));
        return checkPrepaidPhones.getText().then(function (PrepaidPhones) {
            console.log(PrepaidPhones);
            expect(PrepaidPhones).to.not.be.null;
        });
    });

    this.defineStep(/^I should see bring your own tablet link under hamburger$/, function () {
        var checkBYOT = element(by.css('[data-analytics-id*="link3-side-sublink7"]'));
        return checkBYOT.getText().then(function (BYOT) {
            console.log(BYOT);
            expect(BYOT).to.not.be.null;
        });
    });

    this.defineStep(/^I should see deals link under hamburger$/, function () {
        var checkLink = element(by.css('[data-analytics-id*="link4-side"]'));
        return checkLink.getText().then(function (Link) {
            console.log(Link);
            expect(Link).to.not.be.null;
        });
    });

    this.defineStep(/^I should see accessory finder link under hamburger$/, function () {
        var checkAccFinder = element(by.css('[data-analytics-id*="link4-side-sublink1"]'));
        return checkAccFinder.getText().then(function (accfind) {
            console.log(accfind);
            expect(accfind).to.not.be.null;
        });
    });

    this.defineStep(/^I should see tmobile magenta gear under hamburger$/, function () {
        var checkGear = element(by.css('[data-analytics-id*="link4-side-sublink2"]'));
        return checkGear.getText().then(function (gear) {
            console.log(gear);
            expect(gear).to.not.be.null;
        });
    });

    this.defineStep(/^I should see coverage under hamburger$/, function () {
        var checkCoverage = element(by.css('[data-analytics-id*="link5-side"]'));
        return checkCoverage.getText().then(function (coverage) {
            console.log(coverage);
            expect(coverage).to.not.be.null;
        });
    });

    this.defineStep(/^I should see LTE comparison map under hamburger$/, function () {
        var checkLTE = element(by.css('[data-analytics-id*="link5-side-sublink1"]'));
        return checkLTE.getText().then(function (LTE) {
            console.log(LTE);
            expect(LTE).to.not.be.null;
        });
    });

    this.defineStep(/^click on close button$/, function () {
        browser.driver.sleep(8000);
        var clickclose = element.all(by.css('[class="close-icon"]')).get(0);
        return clickclose.click();
    });

    /***********************************Marketing Consumer UNAV Header on Browse and PDP pages**********************************/

    this.defineStep(/^I should see tmobile work logo$/, function () {
        browser.manage().window().maximize();
        var checkTMOLogo = element(by.css('[class="logo at-work"]'));
        return checkTMOLogo.getText().then(function (logo) {
            console.log(logo);
            expect(logo).to.not.be.null;
        });
    });

    this.defineStep(/^I should see plans link for business site$/, function () {
        browser.manage().window().maximize();
        var checkplans = element.all(by.repeater('link in vm.universalNavService.model.unavData.header.desktopLinks')).get(0).element(by.tagName('a'));
        return checkplans.getText().then(function (plans) {
            console.log(plans);
            expect(plans).to.not.be.null;
        });
    });

    this.defineStep(/^I should see coverage link for business site$/, function () {
        browser.manage().window().maximize();
        var checkcoverage = element.all(by.repeater('link in vm.universalNavService.model.unavData.header.desktopLinks')).get(1).element(by.tagName('a'));
        return checkcoverage.getText().then(function (coverage) {
            console.log(coverage);
            expect(coverage).to.not.be.null;
        });
    });

    this.defineStep(/^I should see devices link for business site$/, function () {
        browser.manage().window().maximize();
        var checkdevice = element.all(by.repeater('link in vm.universalNavService.model.unavData.header.desktopLinks')).get(2).element(by.tagName('a'));
        return checkdevice.getText().then(function (devices) {
            console.log(devices);
            expect(devices).to.not.be.null;
        });
    });

    this.defineStep(/^I should see get started modal for business site$/, function () {
        browser.manage().window().maximize();
        var checkGetStarted = element(by.css('[aria-label="Get Started"]'));
        return checkGetStarted.getText().then(function (getstarted) {
            console.log(getstarted);
            expect(getstarted).to.not.be.null;
        });
    });

    this.defineStep(/^I should see search icon for business site$/, function () {
        var checkSearch = element.all(by.css('[class="icon slide"]')).get(0);
        return checkSearch.getText().then(function (search) {
            console.log(search);
            expect(search).to.not.be.null;
        });
    });

    this.defineStep(/^I should see consumer link for business site$/, function () {
        var checkconsumer = element.all(by.css('[ng-bind-html="leftLink.text | parseHtml"]')).get(0);
        return checkconsumer.getText().then(function (consumer) {
            console.log(consumer);
            expect(consumer).to.not.be.null;
        });
    });

    this.defineStep(/^I should see store locator for business site$/, function () {
        var checkStoreLocator = element(by.css('[aria-label="Store Locator"]'));
        return checkStoreLocator.getText().then(function (storelocator) {
            console.log(storelocator);
            expect(storelocator).to.not.be.null;
        });
    });

    this.defineStep(/^I should see lets talk option for business site$/, function () {
        var checkLetsTalk = element.all(by.css('[class="tmo_tfn_number ng-binding"]')).get(0);
        return checkLetsTalk.getText().then(function (letstalk) {
            console.log(letstalk);
            expect(letstalk).to.not.be.null;
        });
    });

    this.defineStep(/^I should see get help option for business site$/, function () {
        var checkLetsTalk = element.all(by.css('[class="tmo_tfn_number ng-binding"]')).get(1);
        return checkLetsTalk.getText().then(function (letstalk) {
            console.log(letstalk);
            expect(letstalk).to.not.be.null;
        });
    });

    this.defineStep(/^I should see devices link under hamburger for business site$/, function () {
        var checkDevices = element(by.css('[data-analytics-id*="link2-side"]'));
        return checkDevices.getText().then(function (devices) {
            console.log(devices);
            expect(devices).to.not.be.null;
        });
    });

    this.defineStep(/^I should see phones link under hamburger for business site$/, function () {
        var checkPhones = element(by.css('[data-analytics-id*="link2-side-sublink1"]'));
        return checkPhones.getText().then(function (phones) {
            console.log(phones);
            expect(phones).to.not.be.null;
        });
    });

    this.defineStep(/^I should see tablets and devices link under hamburger for business site$/, function () {
        var checkID = element(by.css('[data-analytics-id*="link2-side-sublink2"]'));
        return checkID.getText().then(function (ID) {
            console.log(ID);
            expect(ID).to.not.be.null;
        });
    });

    this.defineStep(/^I should see accessories link under hamburger for business site$/, function () {
        var checkAccessories = element(by.css('[data-analytics-id*="link2-side-sublink3"]'));
        return checkAccessories.getText().then(function (accessories) {
            console.log(accessories);
            expect(accessories).to.not.be.null;
        });
    });

    this.defineStep(/^I should see plans link under hamburger for business site$/, function () {
        var checkplans = element(by.css('[data-analytics-id*="link3-side"]'));
        return checkplans.getText().then(function (plans) {
            console.log(plans);
            expect(plans).to.not.be.null;
        });
    });

    this.defineStep(/^I should see TMO One unlimited plan link under hamburger for business site$/, function () {
        var checkTMOplan = element(by.css('[data-analytics-id*="link3-side-sublink1"]'));
        return checkTMOplan.getText().then(function (TMOplan) {
            console.log(TMOplan);
            expect(TMOplan).to.not.be.null;
        });
    });

    this.defineStep(/^I should see simple choice plan link under hamburger for business site$/, function () {
        var checkSCplan = element(by.css('[data-analytics-id*="link3-side-sublink2"]'));
        return checkSCplan.getText().then(function (SCplan) {
            console.log(SCplan);
            expect(SCplan).to.not.be.null;
        });
    });

    this.defineStep(/^I should see coverage under hamburger for business site$/, function () {
        var checkCoverage = element(by.css('[data-analytics-id*="link4-side"]'));
        return checkCoverage.getText().then(function (coverage) {
            console.log(coverage);
            expect(coverage).to.not.be.null;
        });
    });

    this.defineStep(/^I should see our coverage option under hamburger for business site$/, function () {
        var checkOurCoverage = element(by.css('[data-analytics-id*="link4-side-sublink1"]'));
        return checkOurCoverage.getText().then(function (ourcoverage) {
            console.log(ourcoverage);
            expect(ourcoverage).to.not.be.null;
        });
    });

    this.defineStep(/^I should see map option under hamburger for business site$/, function () {
        var checkMap = element(by.css('[data-analytics-id*="link4-side-sublink3"]'));
        return checkMap.getText().then(function (map) {
            console.log(map);
            expect(map).to.not.be.null;
        });
    });

    this.defineStep(/^I should see resources option under hamburger for business site$/, function () {
        var checkResource = element(by.css('[data-analytics-id*="link5-side"]'));
        return checkResource.getText().then(function (resource) {
            console.log(resource);
            expect(resource).to.not.be.null;
        });
    });

    this.defineStep(/^I should see success stories option under hamburger for business site$/, function () {
        var checkSS = element(by.css('[data-analytics-id*="link5-side-sublink1"]'));
        return checkSS.getText().then(function (ss) {
            console.log(ss);
            expect(ss).to.not.be.null;
        });
    });

    this.defineStep(/^I should see get expert advice option under hamburger for business site$/, function () {
        var checkAdvice = element(by.css('[data-analytics-id*="link5-side-sublink2"]'));
        return checkAdvice.getText().then(function (advice) {
            console.log(advice);
            expect(advice).to.not.be.null;
        });
    });

    /***********************************Marketing UNAV footer on consumer Browse and PDP pages**********************************/

    this.defineStep(/^I should see "([^"]*)" Link1$/, function (link1) {
        browser.manage().window().maximize();
        var checkContact = element(by.css('[ng-bind-html="vm.universalNavService.model.unavData.footer.contact_title | parseHtml"]'));
        return checkContact.getText().then(function (Contact) {
            console.log(Contact);
            expect(Contact).to.not.be.null;
        });
    });

    this.defineStep(/^I should see "([^"]*)" Link2$/, function (link2) {
        browser.manage().window().maximize();
        var checkSupport = element(by.css('[ng-bind-html="vm.universalNavService.model.unavData.footer.support_title | parseHtml"]'));
        return checkSupport.getText().then(function (Support) {
            console.log(Support);
            expect(Support).to.not.be.null;
        });
    });

    this.defineStep(/^I should see "([^"]*)" Link3$/, function (link3) {
        browser.manage().window().maximize();
        var checkBusiness = element(by.css('[ng-bind-html="vm.universalNavService.model.unavData.footer.business_title | parseHtml"]'));
        return checkBusiness.getText().then(function (Business) {
            console.log(Business);
            expect(Business).to.not.be.null;
        });
    });

    /***********************************Marketing UNAV footer on business Browse and PDP pages**********************************/
    this.defineStep(/^I should see "([^"]*)" business link1$/, function (link1) {
        browser.manage().window().maximize();
        var checkContact = element(by.css('[ng-bind-html="vm.universalNavService.model.unavData.footer.contact_title | parseHtml"]'));
        return checkContact.getText().then(function (connect) {
            console.log(connect);
            expect(connect).to.not.be.null;
        });
    });

    this.defineStep(/^I should see "([^"]*)" business link2$/, function (link2) {
        browser.manage().window().maximize();
        var checkSupport = element(by.css('[ng-bind-html="vm.universalNavService.model.unavData.footer.support_title | parseHtml"]'));
        return checkSupport.getText().then(function (care) {
            console.log(care);
            expect(care).to.not.be.null;
        });
    });

    this.defineStep(/^I should see "([^"]*)" business link3$/, function (link3) {
        browser.manage().window().maximize();
        var checkBusiness = element(by.css('[ng-bind-html="vm.universalNavService.model.unavData.footer.business_title | parseHtml"]'));
        return checkBusiness.getText().then(function (prog) {
            console.log(prog);
            expect(prog).to.not.be.null;
        });
    });

    /********************************* Robots.txt file*******************************************/

    this.defineStep(/^It should only contain keys$/i, function(data) {
        var robotContent = element(by.tagName('body'));
        return robotContent.getText().then(function(content) {
            if(content) {
                //var regex = /^(User-agent|Sitemap|Allow|Disallow):.+$/;
                var regex = new RegExp('^('+data.rows().join('|')+').+$');
                content.split('\n').forEach(function(line) {

                    if(line && 0 < line.length) {
                        expect(line).to.match(regex);
                    }
                });
            }
        });    
    });
    
    /***********************************Storing Cookie Details CMPID **************************************/

    this.defineStep(/^I see value for cookie "([^"]*)" is "([^"]*)"$/, function (KEY, CMPID) {
        browser.manage().getCookie(KEY).then(function (cookies) {
        console.log(cookies);
        //myTMOCookieData = cookies.value;
        return expect(CMPID).to.equal(cookies.value);
    });
});

/****************************IBA Opt-Out Happy Path redirection **********************************/

this.defineStep(/^I see correct URL parameters cid & tid$/, function () {
    //browser.sleep(5000);
    return browser.getCurrentUrl().then(function (currentUrl) {
        expect(currentUrl).to.contains('tid=');
        expect(currentUrl).to.contains('cid=');
    });
});


this.defineStep(/^I see Turn Off button$/, function () {
    var buttonTurnOff = element(by.css('[class="btn-primary btn-brand btn "]'));    
    return buttonTurnOff.getText().then(function (TurnOff) {
    expect(TurnOff).to.equal('Turn off');
    });    
});

/****************************IBA Opt-Out CTA + Modal window **********************************/

    this.defineStep(/^click on Turn off CTA$/, function () {
        var buttonTurnOff = element(by.css('[class="btn-primary btn-brand btn "]'));    
        return buttonTurnOff.click();
    });

     this.defineStep(/^I see modal window overtakes the iba screen$/, function () {
        browser.driver.sleep(5000);
        var modalAreYouSure = element(by.css('[class="marketing-section hero-content display-dialog  light-bg center top  bg-right bg-bottom  half-height  ng-scope"]'));
        expect(modalAreYouSure.isDisplayed()).to.not.be.null;
});

    this.defineStep(/^I see the modal title$/, function () {
        var modalTitle = element(by.css('[class="small no-text-transform "]'));
        return modalTitle.getText().then(function(AreYouSure) {
        expect(AreYouSure).to.not.be.null;
        });
});
    this.defineStep(/^I see the Turn off CTA button on modal$/, function () {
        var buttonTurnOff = element(by.css('[class="btn-primary btn-brand btn "]' && '[aria-label="Turn off"]'));
        return buttonTurnOff.getText().then(function (TurnOff) {
        expect(TurnOff).to.equal('Turn off');
    }); 
});

    this.defineStep(/^I see the Leave on CTA button on modal$/, function () {
        var buttonLeaveOn = element(by.css('[class="btn-secondary btn-brand btn "]' && '[aria-label="Lave on"]'));
        return buttonLeaveOn.getText().then(function (LeaveOn) {
        expect(LeaveOn).to.equal('Leave on');
    }); 
    
});

};
