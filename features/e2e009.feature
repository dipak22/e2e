Feature: Running Cucumber with Protractor #e2e009 Feature
  As a user I want add an accessory whose cost is greater than $69,
  and tablet and phone line with services,
  and from Checkout review section, edit /remove
  and add a new accessory such that the accessory cost greater than 69
  and again need the edit the cart from review section to edit/remove
  and add the existing accessory such that the accessory section cost is greater than $69


   Scenario: Launch t-mobile site and navigate to Accessories browse page
    Given I navigate to T-Mobile Accessories page
    Then I should navigate to accessories browse page
    Then I select Average Credit

  Scenario: Navigate to Accessories browse page and apply high to low sorting
    And I click on Sorting Drop Down in Accessory browse page
    And I select sort value as Price High To Low
    Then I validate Price High To Low is selected in sort options

  Scenario: Select "Beats Solo3 Wireless On-Ear Headphones - Black™" from main browse page
    When I select "Beats Solo3 Wireless On-Ear Headphones - Black™" accessory from main browse
    #Then I navigate to Product detail page of "Beats Solo3 Wireless On-Ear Headphones - Black™"

  Scenario: Add accessory to cart
    Given I am on Product detail page of "Beats Solo3 Wireless On-Ear Headphones - Black™"
    Then I add selected accessory to cart

 Scenario: Adding tablet to the Cart
    Then I add empty tablet line to cart
    #Then I click on plus button for new tablet line
	Then I click on plus button on tablets empty line
    And I click on Add Tablet button to view the Tablet mini browse
	#Then I click on Done Button in Credit Class Selector
    #Then I select "Apple iPad Pro 10.5-inch" tablet in mini browse
	Then I select "Apple iPad mini 4" tablet in mini browse
    Then I add the selected tab to cart
    #And I validate the selected device is added to cart tile two

  Scenario: Select Bundled services on cart
    #And I am in Cart page
    And I add an service to the line one

  Scenario: On cart page add phone Line
    #And I click on Add a Phone Button for adding new Line
	And I click on Add a Phone Button for adding New Line
    Then I click on plus button for phones for line two
    And I click on 'Add a new phone' button for line 2 to view the mini browse
	Then I select a cell phone from mini browse
    And I add the selected cell phone to cart

  Scenario: Select Alcarte services on cart
    #And I add an service to the line one
	And I add an service to the phone line two

  Scenario: Remove the already added accessory and add another accessory
    #Given I am in a loaded cart page
    When  I click on edit accessory option for accessory one
    Then  I should see option for delete and edit
    Then  I should see again a confirmation to delete the accessorry added to cart

  Scenario: On Cart Page Add an Accessory from mini PDP cost < $69
    Given I am in Cart page
    When I click on Accessory tile
    Then I should see Accessory mini Browse
    #When I select first tab in accessory mini browse
	When I select third tab in accessory mini browse
    #Then I select accessory which is under first tab in mini browse
	Then I select accessory which is under third tab in mini browse
    #Then I should see accessory product description page
    And I click on add to cart button on mini pdp accessory
    #Then accessory is added to the cart
    #And Clear the browser cache

  Scenario: Navigate to checkout
    Given I am in a loaded cart page
    When I click on the 'Checkout' button
    Then the browser will navigate to the checkout section

  Scenario: Checkout - Personal Information
    Given I see the Personal Information section
    And I enter "SDET" in the first name
    And I enter "T" in the middle initial
    And I enter "TMO" in the last name
    And I enter "qat.tmo@gmail.com" in the email address
    And I enter "4254254250" in the phone number

  Scenario: Checkout - Shipping Information
    Given I see the Shipping section
    And I enter "10 main st" in the shipping address
    And I enter "Beverly Hills" in the city
    And I select "CA" in the State
    And I enter "90210" in the zip
    And I click on user-consent checkbox
    And I click on 'Next' button
    Then the browser will navigate to the Payment Information page

  Scenario: Checkout - Payment Information
    Given I am on the Payment and Credit Page
    And I enter "4444444444444448" in the credit card
    And I enter "0122" in the Expiry date
    And I enter "111" in the CVV

  Scenario: Checkout - Credit Information
    Given I see the Credit check section
    And I select "Passport" in the id
    And I enter "A1234567890" in the id
    And I enter "0120" in the expiry date
    And I enter "001110000" in the ssn
    And I enter "12241984" in the dob
    And I click on the 'Agree and Next' button
    Then the browser will navigate to Review order page

  Scenario: Edit the cart
    Given I am on review order page
    #Then I click on edit cart on review page
    #Given I am in a loaded cart page
    #Then  I click on edit accessory
	When I click on Edit in Cart link in review order page
    Then I should be taken back to cart page
	When  I click on edit accessory option for accessory one
    Then  I should see option for delete and edit
    Then  I should see again a confirmation to delete the accessorry added to cart




  Scenario: On Cart Page Add an Accessory from mini PDP cost > $69
    Given I am in Cart page
    When I click on Accessory tile
    Then I should see Accessory mini Browse
    #When I select Accessory Category in mini browse
    #When I select accessory in mini browse
	When I select second tab in accessory mini browse
	Then I select accessory which is under second tab in mini browse
    #Then I should see accessory product description page
    #When I click on Add to Cart Button
	And I click on add to cart button on mini pdp accessory
    #Then accessory is added to the cart

  Scenario: Navigate to checkout
    Given I am in a loaded cart page
    When I click on the 'Checkout' button
    Then the browser will navigate to the checkout section

  Scenario: Check and re-enter required checkout form fields
    Given I am on Info and Shipping page
    And I click on user-consent checkbox
    And I click on 'Next' button
    Then the browser will navigate to the Payment Information page

  Scenario: Checkout - Payment Information
    Given I am on the Payment and Credit Page
    And I enter "4444444444444448" in the credit card
    And I enter "0122" in the Expiry date
    And I enter "111" in the CVV
    And I click on the 'Continue' button

  Scenario: Checkout - Credit Information
    Given I see the Credit check section
    And I select "Passport" in the id
    And I enter "A1234567890" in the id
    And I enter "0120" in the expiry date
    And I enter "001110000" in the ssn
    And I enter "12241984" in the dob
    And I click on the 'Agree and Next' button
    Then the browser will navigate to Review order page

  Scenario: Review Order details and submit
    Given I am on review order page
    When I click on 'I Agree Disclaimer' Label for Terms and Conditions
    And I click on 'Submit Order' Button
    Then I should see the order confirmation page

Scenario: Clean-up
    And Clear the browser cache
