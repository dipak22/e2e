#features/e2e005.feature
Feature:As a user I want to place an accessory only order with an accessory
And cost greater than $69

  Scenario: Navigate to Accessories browse page and select credit class
    Given I navigate to T-Mobile Accessories page
    Then I select Awesome Credit

  Scenario: Apply high to low sorting
    When I am on accessories browse page
    Then I click on Sorting Drop Down in Accessory browse page
    And I select sort value as Price High To Low
    And I validate Price High To Low is selected in sort options

  Scenario: Select "Beats Solo3 Wireless On-Ear Headphones - Rose Gold" from main browse page
    When I select "Beats Solo3 Wireless On-Ear Headphones - Rose Gold" accessory from main browse
    Then I navigate to Product detail page of "Beats Solo3 Wireless On-Ear Headphones - Rose Gold"
    Then I select Average credit from main PDP
    Then I add selected accessory to cart

  Scenario: Navigate to checkout
    Given I am in a loaded cart page
    When I click on the 'Checkout' button
    Then the browser will navigate to the checkout section

  Scenario: Checkout - Personal Information
    Given I see the Personal Information section
    And I enter "SDET" in the first name
    And I enter "T" in the middle initial
    And I enter "TMO" in the last name
    And I enter "qat.tmo@gmail.com" in the email address
    And I enter "4254254250" in the phone number

  Scenario: Checkout - Shipping Information
    Given I see the Shipping section
    And I enter "10 main st" in the shipping address
    And I enter "Beverly Hills" in the city
    And I select "CA" in the State
    And I enter "90210" in the zip
    And I click on user-consent checkbox
    And I click on 'Next' button
    Then the browser will navigate to the Payment Information page

  Scenario: Checkout - Payment Information
    Given I am on the Payment and Credit Page
    And I enter "4444444444444448" in the credit card
    And I enter "0122" in the Expiry date
    And I enter "111" in the CVV
    And I click on the 'Continue' button
    Then the browser will navigate to Review order page

  Scenario: Review Order details and submit
    Given I am on review order page
    When I click on 'I Agree Disclaimer' Label for Terms and Conditions
    And I click on 'Submit Order' Button
    Then I should see the order confirmation page
