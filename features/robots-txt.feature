Feature: robots-txt

   I want to test whether robots.txt loads from root context

    Scenario: visit /robots.txt
    When I go to "/robots.txt"
    Then It should only contain keys
        |keys       | 
        |User-agent:|
        |Sitemap:   |
        |Allow:     |
        |Disallow:  |
        
    
